##登录注册界面
登录要用滑块验证或者点击验证
注册不用验证
交友性质个人信息比较完全
人物标签
人物各种属性用于做相同兴趣爱好的人匹配
邮箱注册
把验证码放如redis中设置过期时间
注册完之后选择标签，确定人物画像
根据用户搜索的内容、爱好标签、收藏歌曲信息，猜他喜欢
##首页(模仿酷狗页面)
音乐为主
界面效果好
视频开头，往下划就进入各种歌曲
播放音乐
评论音乐
点赞音乐，获取歌曲类型，导入计算公式，增强人物描述
音乐分类
歌单
我的喜欢音乐
收藏的歌单
##全部榜单界面
##单独榜单界面
##音乐排行榜界面
##播放音乐界面
接收来自各个页面的音乐播放
##搜索结果界面
##个人中心界面
##别人个人中心界面
##收藏歌曲/歌单界面
##歌手中心界面
##歌手歌曲列表界面
##评论界面

##交际圈
社交为辅
实时匹配在线用户
相互关注后可通讯
交友实时通讯

##后台管理平台
用户管理
评论管理
交友信息管理


##酷狗音乐接口
搜索歌曲
>https://songsearch.kugou.com/song_search_v2?keyword=大鱼&page=1

>http://mobilecdn.kugou.com/api/v3/search/song?format=json&keyword=%E7%8E%8B%E5%8A%9B%E5%AE%8F&page=1&pagesize=20&showtype=1

播放歌曲
>https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=5F302EBD7FF308C0C7D9B62F87CB4142&dfid=dfid&mid=mid&platid=4&album_id=45305792

备注：拿到hash和albumd_id才能播放音乐

排行榜
volid=54894多少期
rankid=8888排行类型
pagesize=100歌曲数量
>http://mobilecdnbj.kugou.com/api/v3/rank/song?pagesize=100&page=1&volid=54894&rankid=8888&with_res_tag=1

获取所有榜单
>http://mobilecdnbj.kugou.com/api/v3/rank/list?version=9108&plat=0&showtype=2&parentid=0&apiver=6&area_code=1&withsong=1&with_res_tag=1

获取第几期
>http://mobilecdnbj.kugou.com/api/v3/rank/vol?ranktype=2&plat=0&rankid=6666&with_res_tag=1


获取歌单信息
>http://m.kugou.com/plist/index&json=true


获取新歌
type=1华语
type=2欧美
type=3日韩
>http://mobilecdnbj.kugou.com/api/v3/rank/newsong?version=9108&plat=0&with_cover=1&pagesize=100&type=1&area_code=1&page=1&with_res_tag=1

获取歌手
sextypes: 
[{key: 2,value: "女"},
{key: 3,value: "组合"},
{key: 1,value: "男"},
{key: 0,value: "全部"}],
types: 
[{key: 4,value: "其他"},
{key: 3,value: "日韩"},
{key: 5,value: "日本"},
{key: 2,value: "欧美"},
{key: 6,value: "韩国"},
{key: 1,value: "华语"},
{key: 0,value: "全部"}]
>http://mobilecdnbj.kugou.com/api/v5/singer/list?version=9108&showtype=1&plat=0&sextype=2&sort=0&pagesize=100&type=1&page=1&musician=0

获取歌单
>http://m.kugou.com/plist/index&json=true

获取歌单下的歌曲
>http://m.kugou.com/plist/list/125032?json=true

歌手详细信息
>http://mobilecdnbj.kugou.com/api/v3/singer/info?singerid=86747&with_res_tag=1

歌手的歌曲
>http://mobilecdnbj.kugou.com/api/v3/singer/song?sorttype=2&version=9108&identity=3&plat=0&pagesize=100&singerid=86747&area_code=1&page=1&with_res_tag=1


歌手专辑
有翻页
>http://mobilecdnbj.kugou.com/api/v3/singer/album?version=9108&plat=0&pagesize=20&singerid=86747&category=1&area_code=1&page=1&show_album_tag=0


专辑中的歌曲
>http://mobilecdn.kugou.com/api/v3/album/song?version=9108&albumid=38080072&plat=0&pagesize=100&area_code=1&page=1&with_res_tag=1

*************
***问题记录***
*************
注：采用倒叙记录，最新的写在最前面
任务列表：
登录界面的上的记住我
思路：登录前，是否选择记住我，记住我，就用localStorage保存token在本地，不记住就使用sessionStorage。（ok）
完成前端收到后端信息后的反馈，友好反馈 (ok)
将业务逻辑都迁移至service层，现在controller层的业务逻辑较多 (ok)
前端登录注册界面样式待更改
每个页面的Header和Footer待完成
登录时，将状态改变为在线状态，在线状态保存在redis中，数据库中不做设置,增加验证用户当前是否已经登录verifyUserLogin (ok)
之后向数据库取数据时，都导到redis中，这样速度更快
加入头像功能，需要图片服务器
计划：
1、个人中心界面（包括主页（我关注的用户，我喜欢的歌手，动态，和粉丝）、我的喜欢、播放历史、收藏歌单、设置）、用户主页、评论界面；
2、交友界面（匹配机制，在线匹配和根据歌单、喜欢的歌手、喜欢的歌曲，进行匹配），交友界面还可以做其他功能；


########################
严重问题：酷狗播放链接失效


https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback=jQuery19106121897016734785_1646715730175
                                                    &hash=F2E7E2EA86744B6511963A069C7CCB19
                                                    &dfid=4FIdx50ybr9u3A2S9v3w2yis
                                                    &appid=1014
                                                    &mid=26e5d2cedec54971335875cb02f62e89
                                                    &platid=4
                                                    &album_id=50223611
                                                    &album_audio_id=348254775
                                                    &_=1646715730190
                                                    
                                                    
                                                    
https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback=jQuery19106121897016734785_1646715730175&hash=5F302EBD7FF308C0C7D9B62F87CB4142&dfid=4FIdx50ybr9u3A2S9v3w2yis&appid=1014&mid=26e5d2cedec54971335875cb02f62e89&platid=4&album_id=45305792&_=1646721567000
########################

用户即时匹配（5天）、
聊天（交友社区固定页面聊天）（个人中心留言板）（10天）、
歌曲推荐（首页底部）（3天）、
歌单推荐（歌单页面推荐）（3天）、
点赞评论消息通知（5天）。
搜索
做一个审核平台，审核用户评论和动态评论还有歌单的审核，有时间做一个聊天文字审核。违规用户禁言

4.8:
在header中增加一个消息气泡，用户登录后全局存在，随时和好友沟通交流。

4.6:
在音乐播放界面新增分享功能，分享到私聊的窗口，分享内容类似一个卡片，需要有歌曲信息


3:28:
在关注列表中加入（取消关注，私聊），在粉丝关注列表中加入（私聊）
消息通知思路：
前端在头像中的消息通知加入红点，在socialIndex中的聊天气泡中加入红点，在header中加入websocket，整理出需要推送消息的接口
消息是否已读判断，在redis中新增List类型key存储消息，并加入是否已读status，当用户点击后，修改redis中消息已读标记

3.29:
关注也需要消息推送，后续考虑将消息推送利用表来配置

3.20:
用户匹配功能：
1、判断是否redis中是否存在匹配的键值对，不存在新建，存在往其中加入用户，5秒后无人响应，弹出list，匹配到人也立刻弹出双方list
流程示意：
点赞某条评论-》将信息传递给模板类-》保存在redisList<Map>类型中-》通过websocket通知前端，调用接口获取redis的消息内容。
是否已读：点击消息后，调用相应

增加一个模板类，存储消息类型（点赞你的评论、评论了你的评论，点赞了你的动态，评论了你的动态，给你发了一条消息）



2.28:
在TokenInterceptor在前端调用出错后返回json数据给予前端友好反馈

2.27:
对邮箱工具类做了整合
对redis工具类做了整合
增加修改密码的功能
新增邮箱模板配置表

2.25：
目前主要对用户模块进行修改；
用户登录设置redis中的key值为  业务类型_表名_字段名_主键值；

2.11:
每个用户都需要在redis里新建一个数据自己的位置。播放列表也是

1.30：
数据库公共音乐表中存在相同hash和album_id数据原因待排查

1.29:
新增根据歌单主键id查找歌单信息

1.25:
给歌单定义标签可以使用，hutool的中文分词工具类

1.24:
歌手页：后端需要做分页，前端点击页数，传给后端，后端需要一个分页对象，存储分页数据和总量
加入pageHelper分页插件，测试成功

1.21：
分页的操作为：前端传start和limit，后端返回分页数据
同步顺序
1、同步榜单 syncAllMusicList()   /musicControl/updateAllMusicList
2、同步歌单 syncMusicSheet()     /musicControl/updateMusicSheet
3、同步歌手 syncMusicSinger()    /musicControl/updateSinger
4、同步新歌 syncNewMusic()       /musicControl/updateNewMusic

1.19:
前端需要一个歌单歌曲展示页和歌手展示页，歌单歌曲展示页不需要在导航栏上加字段，
歌手展示页需要在导航栏上新加字段

1.14:
前端显示的音乐时间不是从音乐榜单列表获取，而是从公共音乐表中获取

1.10:
数据库表的字段名和实体类属性一定要规范实体类（驼峰），数据库（下划线）
调用排行榜全部音乐接口考虑是否需要将音乐导入本地，不导入本地，音乐时长前端展示为0：00
下一步前端排行榜页面美化，日了狗

1.9:
设计一个方法，凡是数据中有hash和album_id字段的都调用一个方法，导入歌曲信息到数据库。

1.8：
歌手导入不是全部，url里有分页pagesize=100&page=1，日后完善。
首页直接调用酷狗api的接口后续完善，热门歌曲不满10首，歌手图片不清晰，新歌加载不全。

12.23:
计划将酷狗接口调用后的数据存在数据库中，新增同步功能
歌单因酷狗限制只能查看前三首歌，
计划设计用户上传歌单到本地数据库中，新建歌单，加入歌曲：1、用户自己的歌单，新建保存给用户自己看；2、上传公共歌单，通过审核后可以展示给大家看。

12.19：
循环调用接口导致接口调用时间过长

12.16：

12.15：
处理好调用酷狗接口出现异常的问题

12.14:
新增一个全局异常的类
主页的新歌，歌曲时间显示有问题、换页直接点数字不能换页
主要逻辑又后端改，把前端逻辑都移到后端，前端需要什么都由后端取。

12.11：
完成首页热门歌曲功能

11.4：
1、前端增加榜单主页展示
2、后端加入榜单功能

10.29：
1、修改前端主页样式
2、验证图片服务器功能

10.28:
1、前端登录后显示头像，不登录显示按钮，加入退出登录功能


10.27：
1、修复不能注册的问题，获取redis的邮箱验证码key值取错了
2、注销登录，redis中的token，前端的token，登录状态从redis中取出
3、TokenInterceptor新增token异常的

10.26：
1、前端收到后端反馈优化
2、记住我功能，用户没登陆过，不勾选记住我，浏览器保存token，浏览器关闭就失效，勾选记住我后，本地token；
之后在每个页面中判断token存在什么位置，本地还是浏览器，将token放在请求头中发送请求
3、修复redis同一时间只能存一个用户的数据

10.25：
1、把枚举类的状态码修改到UserController中
2、将controller层的逻辑代码转移到service层
3、增加接口验证用户是否登陆，获取用户名


10.22：
1、增加状态枚举类
2、更新sql文件，加了注释

10.11：
1、公司端没有sql文件，保存sql文件在项目中
2、后端目前将除登录注册的功能请求全部拦截，前端之后发送的请求头中都需要带有token 
3、前端登录注册界面样式待更改
4、每个页面的Header和Footer待完成
5、登录时，将状态改变为在线状态，在线状态保存在redis中，数据库中不做设置
6、前端界面模仿酷狗界面

