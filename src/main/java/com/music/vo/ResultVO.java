package com.music.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@Accessors(chain = true)
public class ResultVO implements Serializable {
    private static final long serialVersionUID = 7301255874952131899L;
    private String msgCode;
    private Integer msgCode_other;//代码中的状态标识，与前端不同
    private String msg;
    private Object data;

    public final static String SUCCESS_CODE = "200";
    public final static String FAIL_CODE = "0";


    public static ResultVO success(Integer msgCode_other, String msg, Object data) {
        ResultVO resultVO = new ResultVO();
        return resultVO.setMsgCode(SUCCESS_CODE).setMsg(msg).setData(data).setMsgCode_other(msgCode_other);
    }

    public static ResultVO fail(Integer msgCode_other, String msg) {
        ResultVO resultVO = new ResultVO();
        return resultVO.setMsgCode(FAIL_CODE).setMsg(msg).setMsgCode_other(msgCode_other);
    }

    public static ResultVO result(Integer msgCode_other,String msg,Object data){
        if (msgCode_other%2==0){
            return success(msgCode_other, msg, data);
        }else {
            return fail(msgCode_other,msg);
        }
    }
}
