package com.music.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象
 */
@Data
public class PageVO<T> implements Serializable {
    private static final long serialVersionUID = -6674977271533807157L;

    //总数量
    private Integer pageTotal;

    //数据List
    private List<T> dataList;
}
