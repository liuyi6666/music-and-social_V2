package com.music.interceptor;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import com.alibaba.fastjson.JSON;
import com.music.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 判断请求中是否存在token，并给出响应
 */
@Slf4j
@Component
public class TokenInterceptor implements HandlerInterceptor {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${common.salt}")
    private String salt;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //浏览器会发送两次请求，第一次为预检，第二次为真实请求
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
            return true;
        }
        PrintWriter out;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        String token = request.getHeader("authentication");
        //判定令牌是否已经过期
        try {
            JWTValidator.of(token).validateDate(DateUtil.date());
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("登陆过期");
            out = response.getWriter();
            ResultVO resultVO = new ResultVO();
            resultVO.setMsg("未登录");
            resultVO.setMsgCode("-1");
            out.write(JSON.toJSONString(resultVO));
            return false;
        }
        //验证token
        try {
            boolean verify = JWTUtil.verify(token, salt.getBytes());
            if (!verify){
                out = response.getWriter();
                ResultVO resultVO = new ResultVO();
                resultVO.setMsg("token违法");
                resultVO.setMsgCode("-1");
                out.write(JSON.toJSONString(resultVO));
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("token验证失败");
            out = response.getWriter();
            ResultVO resultVO = new ResultVO();
            resultVO.setMsg("token违法");
            resultVO.setMsgCode("-1");
            out.write(JSON.toJSONString(resultVO));
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        System.out.println("==================postHandle=====================");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        System.out.println("==========================afterCompletion======================");
    }
}
