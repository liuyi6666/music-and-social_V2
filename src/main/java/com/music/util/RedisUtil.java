package com.music.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.music.constant.RedisBusTypeConstant;
import com.music.pojo.dto.ChatInfo;
import com.music.pojo.dto.MessageNotifyDTO;
import com.music.user.service.impl.MessageNotifyTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Redis中存放数据请遵循一下原则
 * <p>
 * 业务类型_表名_字段名_字段唯一值
 * <p>
 * 业务类型（注：如新增业务类型，在RedisBusTypeConstant中写明）
 */
@Component
@Slf4j
public class RedisUtil {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //邮箱验证码过期时间 分钟
    private static final Integer EMAIL_CODE_EXPIRES = 5;

    //聊天记录保存七天
    private static final Integer CHAT_RECORD_EXPIRES = 7;

    public void setEmailCode(String email, String emailCode, String busType) {
        try {
            redisTemplate.opsForValue().set(busType + "_User_email_" + email, emailCode);
            redisTemplate.expire(busType + "_User_email_" + email, EMAIL_CODE_EXPIRES, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("redis未启动！");
        }
    }

    public String getEmailCode(String busType, String email) {
        return redisTemplate.opsForValue().get(busType + "_User_email_" + email);
    }

    /**
     * 保存聊天记录到redis
     *
     * @param matchingSessionId redis的key
     * @param maps              聊天内容、用户信息
     */
    public void saveChatRecord(String matchingSessionId, List<ChatInfo> maps) {
        String infoJson = JSON.toJSONString(maps);
        log.info("saveChatRecord infoJson={}", JSON.toJSONString(infoJson));
        try {
            redisTemplate.opsForValue().set(RedisBusTypeConstant.CHAT + "_" + matchingSessionId, infoJson);
            redisTemplate.expire(RedisBusTypeConstant.CHAT + "_" + matchingSessionId, CHAT_RECORD_EXPIRES, TimeUnit.DAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取聊天记录
     *
     * @param matchingSessionId
     * @return
     */
    public List<ChatInfo> getChatRecord(String matchingSessionId) {
        String infoJson = redisTemplate.opsForValue().get(RedisBusTypeConstant.CHAT + "_" + matchingSessionId);
        return JSONArray.parseArray(infoJson, ChatInfo.class);
    }

    /**
     * 保存相互关注聊天记录到redis
     *
     * @param sessionId redis的key
     * @param maps              聊天内容、用户信息
     */
    public void saveChatRecordV2(String sessionId, List<ChatInfo> maps) {
        String infoJson = JSON.toJSONString(maps);
        log.info("saveChatRecord infoJson={}", JSON.toJSONString(infoJson));
        try {
            redisTemplate.opsForValue().set(RedisBusTypeConstant.MUTUAL_CHAT + "_" + sessionId, infoJson);
            redisTemplate.expire(RedisBusTypeConstant.MUTUAL_CHAT + "_" + sessionId, CHAT_RECORD_EXPIRES, TimeUnit.DAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取相互关注聊天记录
     *
     * @param sessionId
     * @return
     */
    public List<ChatInfo> getChatRecordV2(String sessionId) {
        String infoJson = redisTemplate.opsForValue().get(RedisBusTypeConstant.MUTUAL_CHAT + "_" + sessionId);
        return JSONArray.parseArray(infoJson, ChatInfo.class);
    }


    /**
     * 保存消息通知
     *
     * @param messageNotifyDTO
     */
    public void saveMessageNotify(MessageNotifyDTO messageNotifyDTO) {
        String redisMessage = JSON.toJSONString(messageNotifyDTO);
        log.info("saveMessageNotify redisMessage={}", redisMessage);
        redisTemplate.opsForList().leftPush(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + messageNotifyDTO.getToUserId(), redisMessage);
    }

    /**
     * 获取消息通知
     *
     * @param userId
     * @return
     */
    public List<MessageNotifyDTO> getMessageNotify(Integer userId) {
        List<String> allMessage = redisTemplate.opsForList().range(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, 0, -1);
        List<MessageNotifyDTO> messageNotifyDTOList = null;
        if (allMessage != null) {
            messageNotifyDTOList = allMessage.stream()
                    .map(message -> JSON.parseObject(message, MessageNotifyDTO.class))
                    .collect(Collectors.toList());
        }
        return messageNotifyDTOList;
    }
}
