package com.music.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.exceptions.ValidateException;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import com.music.enums.StatusEnum;

import java.util.HashMap;

public class MyJWTUtil {

    /**
     * 获取token中的数据
     *
     * @param token
     * @return
     */
    public static HashMap<String, Object> getJWTInfo(String token) {
        HashMap<String, Object> map = new HashMap<>();
        Integer userId = null;
        String userName = null;
        String email = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
            userName = (String) jwt.getPayload("userName");
            email = (String) jwt.getPayload("email");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", StatusEnum.TOKEN_EXCEPTION.getStatusCode());
            return map;
        }
        map.put("statusCode", StatusEnum.TOKEN_SUCCESS);
        map.put("userId", userId);
        map.put("userName", userName);
        map.put("eamil", email);
        return map;
    }

    /**
     * 验证token是否过期和正确性
     *
     * @param token
     * @return
     */
    public static Boolean verifyToken(String token){
        try {
            JWTValidator.of(token).validateDate(DateUtil.date());
        } catch (Exception e) {
            return false;
        }
        return JWTUtil.verify(token, "liuyi".getBytes());
    }
}
