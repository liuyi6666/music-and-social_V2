package com.music.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.imageaudit.model.v20191230.ScanTextRequest;
import com.aliyuncs.imageaudit.model.v20191230.ScanTextResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.music.pojo.entity.UserCommentCheck;
import com.music.user.service.RedisService;
import com.music.user.service.UserChatMatchingService;
import com.music.user.service.UserCommentCheckService;
import com.music.user.service.impl.OneToOneWebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AliyunContextVerifyUtil {

    private static final String accessKeyId = "LTAI5tJdXQn92o61TUbmR7vT";

    private static final String accessKeySecret = "glFIbRlhcm48nBq01DS0vinEEtP2Hm";

    @Autowired
    public void setUserCommentCheckService(UserCommentCheckService userCommentCheckService) {
        AliyunContextVerifyUtil.userCommentCheckService = userCommentCheckService;
    }

    private static UserCommentCheckService userCommentCheckService;


    private static IAcsClient client() {
        DefaultProfile profile = DefaultProfile.getProfile("cn-shanghai", accessKeyId, accessKeySecret);
        return new DefaultAcsClient(profile);
    }

    /*
    spam：文字垃圾内容识别
    politics：文字敏感内容识别
    abuse：文字辱骂内容识别
    terrorism：文字暴恐内容识别
    porn：文字鉴黄内容识别
    flood：文字灌水内容识别
    contraband：文字违禁内容识别
    ad：文字广告内容识别
    */

    //检测 种类
    private static List<ScanTextRequest.Labels> TextLabels() {
        List<ScanTextRequest.Labels> labels = new ArrayList<>();
        ScanTextRequest.Labels lab = new ScanTextRequest.Labels();
        ScanTextRequest.Labels lab3 = new ScanTextRequest.Labels();
        ScanTextRequest.Labels lab4 = new ScanTextRequest.Labels();
        lab.setLabel("abuse");
        lab3.setLabel("porn");
        lab4.setLabel("contraband");
        labels.add(lab);
        labels.add(lab3);
        labels.add(lab4);
        return labels;
    }

    public static String verify(UserCommentCheck userCommentCheck) {
        IAcsClient client = client();
        ScanTextRequest req = new ScanTextRequest();
        req.setActionName("ScanText");
        req.setLabelss(TextLabels());
        List<ScanTextRequest.Tasks> tasks = new ArrayList<>();
        ScanTextRequest.Tasks task = new ScanTextRequest.Tasks();
        task.setContent(userCommentCheck.getContext());
        tasks.add(task);
        req.setTaskss(tasks);
        String returnStr = "pass";
        //需要保存到数据库的内容
        String contextV2 = userCommentCheck.getContext();
        try {
            ScanTextResponse response = client.getAcsResponse(req);
            ScanTextResponse.Data.Element.Result result = response.getData().getElements().get(0).getResults().get(0);
            //调用后获取到他的返回对象，然后判断我们的文字 是什么内容
            if (!"pass".equals(result.getSuggestion())) {
                StringBuilder error = new StringBuilder();
                for (ScanTextResponse.Data.Element.Result.Detail detail : result.getDetails()) {
                    if ("abuse".equals(detail.getLabel())) {
                        //不同颜色标识不同违规
                        //红色
                        error.append("辱骂内容、");
                        for (ScanTextResponse.Data.Element.Result.Detail.Context detailContext : detail.getContexts()) {
                            contextV2 = contextV2.replaceAll(detailContext.getContext(), "<em style='color:red'>" + detailContext.getContext() + "</em>");
                        }
                    }
                    if ("porn".equals(detail.getLabel())) {
                        //黄色
                        error.append("色情内容、");
                        for (ScanTextResponse.Data.Element.Result.Detail.Context detailContext : detail.getContexts()) {
                            contextV2 = contextV2.replaceAll(detailContext.getContext(), "<em style='color:#afaf00'>" + detailContext.getContext() + "</em>");
                        }
                    }
                    if ("contraband".equals(detail.getLabel())) {
                        //蓝色
                        error.append("文字违规、");
                        for (ScanTextResponse.Data.Element.Result.Detail.Context detailContext : detail.getContexts()) {
                            contextV2 = contextV2.replaceAll(detailContext.getContext(), "<em style='color:blue'>" + detailContext.getContext() + "</em>");
                        }
                    }
                }
                returnStr = error.toString();
                String suggestion = response.getData().getElements().get(0).getResults().get(0).getSuggestion();
                String rate = response.getData().getElements().get(0).getResults().get(0).getRate().toString();
                //检测到异常
                userCommentCheck.setSensitiveContexts(contextV2)
                        .setLabel(returnStr)
                        .setSuggestion(suggestion)
                        .setRate(rate);
                userCommentCheckService.saveVerifyComment(userCommentCheck);
                return suggestion;
            } else {
                returnStr = "pass";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnStr;
    }

    public static void main(String[] args) {

    }

}
