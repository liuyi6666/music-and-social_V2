package com.music.util;

/**
 * 解决酷狗链接问题工具类
 */
public class KuGouUtil {

    /**
     * 解除酷狗接口调用限制
     *
     * @param url
     * @return
     */
    public static String relieveKuGouUrlLimit(String url) {
        url = url.replaceAll("<!--KG_TAG_RES_START-->", "");
        url = url.replaceAll("<!--KG_TAG_RES_END-->", "");
        return url;
    }

    /**
     * 传换时间格式
     *
     * @param timeLength
     * @return
     */
    public static String swapTime(int timeLength) {
        int musicLength = timeLength / 1000;//201秒
        int min = musicLength / 60;//3分钟
        int second = musicLength - (min * 60);
        String secondStr;
        if (second < 10) {
            secondStr = "0" + second;
        } else {
            secondStr = String.valueOf(second);
        }
        return min + ":" + secondStr;
    }
}
