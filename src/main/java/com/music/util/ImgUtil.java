package com.music.util;

import org.springframework.web.multipart.MultipartFile;

public class ImgUtil {

    /**
     * 字符串转化为byte，在转化为file
     * @param str
     * @return
     */
    public static MultipartFile swapToMultipartFile(String str){
        return BASE64DecodedMultipartFile.base64ToMultipart(str);
    }
}
