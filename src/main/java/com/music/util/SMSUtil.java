package com.music.util;

import cn.hutool.core.util.StrUtil;
import com.music.pojo.entity.SmsConfig;
import com.music.user.service.SmsConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.regex.Matcher;

@Slf4j
@Component
public class SMSUtil {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SmsConfigService smsConfigService;


    /**
     * 生成四位验证码
     *
     * @return
     */
    public static String randomNumBuilder() {
        String result = "";
        for (int i = 0; i < 4; i++) {
            result += Math.round(Math.random() * 9);
        }
        return result;
    }

    /**
     * 根据邮箱模板key，查询标题和内容并替换
     *
     * @param templateKey
     * @param param
     * @return
     */
    public SmsConfig getSmsInfo(String templateKey, String[] param) {

        SmsConfig smsConfig = smsConfigService.getSmsConfig(templateKey);

        String context = smsConfig.getContent();
        if (smsConfig.getParamCount() != param.length) {
            return null;
        }
        for (String s : param) {
            context = context.replaceFirst("\\$\\{}", s);
        }
        smsConfig.setContent(context);
        return smsConfig;
    }


    /**
     * 发送邮件
     *
     * @param fromEmail
     * @param toEmail
     * @param emailTitle
     * @param emailBody
     * @return
     */
    public Integer sentMessageV2(String fromEmail, String toEmail, String emailTitle, String emailBody) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setFrom(fromEmail);//设置发件邮箱
            message.setTo(toEmail);//设置收件邮箱
            message.setSubject(emailTitle);//设置标题
            message.setText(emailBody);//设置邮件体
            log.info("getEmail send email message:[{}]", message);
            mailSender.send(mimeMessage);//发送邮件
        } catch (Exception e) {
            log.error("[{}] send email message exception", toEmail, e);
            return 0;
        }
        return 200;
    }

    /**
     * 获取系统消息配置
     *
     * @param key
     * @param param
     * @return
     */
    public SmsConfig getSysConfig(String key, String[] param) {
        SmsConfig smsConfig = smsConfigService.getSmsConfig(key);
        String content = smsConfig.getContent();
        for (String s : param) {
            content=StrUtil.format(content, s);
        }
        smsConfig.setContent(content);
        return smsConfig;
    }
}
