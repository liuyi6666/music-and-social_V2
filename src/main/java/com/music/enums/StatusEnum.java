package com.music.enums;

import lombok.Getter;

@Getter
public enum StatusEnum {
    //成功代码为偶数，失败代码奇数
    /*
    ========================邮箱状态码===============================
     */
    EMAIL_NULL(1001, "邮箱为空"),
    EMAIL_SEND_FAIL(1003, "邮件发送失败"),
    EMAIL_SEND_SUCCESS(1002, "邮件发送成功"),
    EMAIL_BEREGISTERED(1005, "邮箱被注册"),
    EMAIL_TEMPLATE_WARNING(1007,"邮箱模板异常"),
    /*
    ========================验证码状态码=============================
     */
    CODE_OVERDUE(2001, "验证码过期"),
    CODE_ERROR(2003, "验证码错误"),
    CODE_NULL(2005, "请刷新验证码"),
    CODE_RIGHT(2002,"验证码正确"),
    /*
    ========================用户状态码===============================
     */
    USER_BEREGISTERED(3001, "用户被注册"),
    USER_PASSWORD_ERROR(3003, "用户名或密码错误"),
    USER_NOTREGISTER(3005,"用户不存在"),
    USER_EMAIL_NOT_IDENTICAL(3007,"用户邮箱不一致"),
    USER_IP_EXCEPTION(3009,"用户网络地址异常"),
    USER_UPDATE_SUCCESS(3002,"用户修改成功"),
    USER_UPDATE_FAIL(3011,"用户修改失败"),
    USER_LIKE_MUSIC(3004,"修改音乐喜欢状态成功"),
    USER_GET_LIKE_MUSIC_LIST(3006,"获取喜欢歌曲列表"),
    USER_LIKE_SHEET(3008,"修改歌单喜欢状态成功"),
    USER_GET_LIKE_SHEET_LIST(3010,"获取喜欢歌单列表"),
    USER_LIKE_SINGER(3012,"修改歌手喜欢状态成功"),
    USER_GET_LIKE_SINGER_LIST(3014,"获取喜欢歌手列表"),
    USER_GET_INFO(3016,"查询用户信息"),
    USER_STATUS_RIGHT(3018,"用户状态正常"),
    USER_COMMENT_SUCCESS(3020,"评论成功"),
    USER_COMMENT_DELETE_SUCCESS(3022,"删除评论成功"),
    USER_COMMENT_DELETE_ERROR(3022,"删除评论失败"),
    USER_COMMENT_LIKES_STATUS(3024,"点赞状态更新"),
    USER_MUSIC_COMMENT_SUCCESS(3026,"获取歌曲评论"),
    USER_ISSUE_COMMENT_SUCCESS(3028,"获取用户动态成功"),
    USER_ISSUE_RELEASE_SUCCESS(3030,"动态发布成功"),
    USER_RELATION(3032,"用户关系获取"),
    USER_CHAT_LIST(3034,"获取匹配用户列表"),
    USER_CHAT_RECORD(3034,"获取用户聊天记录"),
    USER_MESSAGE_NOTIFY(3036,"获取用户消息通知"),
    USER_MODIFY_STATUS_SUCCESS(3038,"修改状态成功"),
    USER_MODIFY_STATUS_ERROR(3037,"修改状态成功"),
    USER_NOT_ALLOW_COMMENT(3039,"您已被禁止评论或者发布动态"),
    /*
    ========================登录状态码===============================
     */
    LOGIN_SUCCESS(4000, "登录成功"),
    LOGIN_ERROR(4001,"登录失败"),
    REGISTER_SUCCESS(4002, "注册成功"),
    REGISTER_ERROR(4003,"注册失败"),
    LOGOUT_SUCCESS(4004,"注销成功"),
    LOGOUT_ERROR(4005,"注销失败"),
    /*
    ========================音乐状态码===============================
     */
    MUSIC_SEARCH(5000, "查找音乐"),
    MUSIC_PLAY(5002, "播放音乐"),
    MUSIC_LIST(5004,"获取音乐歌单"),
    MUSIC_HOT(5006,"获取热门歌曲"),
    MUSIC_NEW(5008,"获取新歌"),
    MUSIC_SINGER(5010,"获取歌手"),
    MUSIC_SINGER_MUSIC(5022,"获取歌手歌曲"),
    MUSIC_RANKING (5012,"获取排行榜"),
    MUSIC_VOLID(5014,"获取排行榜期数"),
    MUSIC_ALL_LIST(5016,"获取全部排行榜"),
    MUSIC_SHEET(5018,"获取音乐歌单"),
    MUSIC_SHEET_MUSIC(5020,"获取音乐歌单歌曲"),
    MUSIC_SINGER_ALBUM(5024,"获取歌手专辑"),
    MUSIC_SINGER_ALBUM_MUSIC(5026,"获取专辑歌曲"),
    MUSIC_PLAY_QUEUE_FROM_REDIS(5028,"从redis中获取播放列表"),
    MUSIC_COMMENT(5030,"获取音乐评论"),
    MUSIC_SYNC_SUCCESS(5032,"同步音乐成功"),
    MUSIC_SYNC_ERROR(5031,"同步音乐失败"),
    MUSIC_ONLINE_SUCCESS(5034,"在线查找成功"),
    MUSIC_ONLINE_ERROR(5035,"在线查找失败"),
    MUSIC_IMPORT_SUCCESS(5036,"导入音乐成功"),
    MUSIC_DELETE_SUCCESS(5038,"删除成功"),
    MUSIC_DELETE_ERROR(5037,"删除失败"),
    /*
    ========================Token状态码===============================
     */
    TOKEN_NULL(6001, "（服务器）未登录，请重新登录"),
    TOKEN_NOTNULL(6000, "登录成功"),
    TOKEN_OVERDUE(6003,"（服务器）登录状态过期，请重新登录"),
    TOKEN_OVERDUE_V2(6005,"令牌失效"),
    TOKEN_EXCEPTION(6007,"令牌异常"),
    TOKEN_SUCCESS(6002,"令牌状态正常"),
    /*
    =======================图片相关状态码===============================
     */
    IMAGE_SUCCESS(7000,"图片读取成功"),
    IMAGE_ERROR(7001,"图片读取失败"),
    IMAGE_DELETE_SUCCESS(7002,"删除成功"),
    IMAGE_DELETE_ERROR(7003,"删除失败"),
    /*
    =======================上传相关状态码===============================
     */
    UPLOAD_SUCCESS(8000,"上传成功"),
    UPLOAD_ERROR(8001,"上传失败"),
    UPLOAD_FORMAT_ERROR(8003,"格式错误"),
    /*
    =======================其他========================================
     */
    OTHER_SUCCESS(9000,"成功"),
    OTHER_ERROR(9001,"失败"),
    OTHER_DELETE_SUCCESS(9002,"删除成功"),
    OTHER_MATCHING_OVERTIME(9004,"匹配超时"),
    OTHER_MATCHING_SUCCESS(9006,"匹配成功"),
    OTHER_MATCHING_USER_LIKE_SUCCESS(9008,"匹配用户喜好成功"),
    OTHER_COMMENT_CHECK_BLOCK(9007,"您的评论违规，请规范发言"),
    OTHER_COMMENT_CHECK_REVIEW(9009,"您的评论内容有争议，以提交至后台审核");



    private Integer statusCode;
    private String statusMsg;

    StatusEnum(Integer statusCode, String statusMsg) {
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    public static String getMsg(Integer code) {
        for (StatusEnum rate : StatusEnum.values()) {
            if (code.equals(rate.getStatusCode())) {
                return rate.getStatusMsg();
            }
        }
        return "没有该状态码";
    }

}
