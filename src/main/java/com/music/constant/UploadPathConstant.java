package com.music.constant;

public class UploadPathConstant {

    /**
     * 用户路径
     */
    public static final String USER = "user";

    /**
     * 公共路径
     */
    public static final String COMMON = "common";

    /**
     * Banner
     */
    public static final String BANNER = "/walking";

    /**
     * 专辑路径
     */
    public static final String SHEET = "/sheet";

    /**
     * 头像图片路径
     */
    public static final String HEADER = "/header";

    /**
     * 用户背景路径
     */
    public static final String BACKGROUND = "/background";

    /**
     * 用户发动态图片
     */
    public static final String ISSUE = "/issue";
}
