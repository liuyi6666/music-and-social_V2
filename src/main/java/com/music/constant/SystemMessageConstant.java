package com.music.constant;

public class SystemMessageConstant {

    /**
     * 点赞了音乐评论
     */
    public static final String LIKE_MUSIC_COMMENT = "likeMusicComment";

    /**
     * 点赞了动态
     */
    public static final String LIKE_ISSUE = "likeIssue";

    /**
     * 点赞了动态评论
     */
    public static final String LIKE_ISSUE_COMMENT = "likeIssueComment";

    /**
     * 回复了音乐评论
     */
    public static final String REPLY_MUSIC_COMMENT = "replyMusicComment";

    /**
     * 回复了动态评论
     */
    public static final String REPLY_ISSUE_COMMENT = "replyIssueComment";

    /**
     * 关注了你
     */
    public static final String FOLLOW = "follow";
}
