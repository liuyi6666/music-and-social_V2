package com.music.constant;

public class CommentFromConstant {

    /**
     * 音乐评论V0
     */
    public static final Integer COMMENT_MUSIC_V0=0;

    /**
     * 音乐评论V1
     */
    public static final Integer COMMENT_MUSIC_V1=1;

    /**
     * 发布动态
     */
    public static final Integer COMMENT_ISSUE_V0=2;

    /**
     * 评论动态
     */
    public static final Integer COMMENT_ISSUE_V1=3;

}
