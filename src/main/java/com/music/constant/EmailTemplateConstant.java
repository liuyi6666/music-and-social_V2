package com.music.constant;

public class EmailTemplateConstant {
    /**
     * 找回密码邮箱模板key
     */
    public static final String EMAIL_FORGET_PASSWORD = "emailForgetPassword";

    /**
     * 注册用户邮箱模板key
     */
    public static final String EMAIL_REGISTER ="emailRegister";
}
