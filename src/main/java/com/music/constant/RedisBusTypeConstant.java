package com.music.constant;

public class RedisBusTypeConstant {
    /**
     * 注册
     */
    public static final String REGISTER = "register";

    /**
     * 登录
     */
    public static final String LOGIN = "login";

    /**
     * 找回密码
     */
    public static final String FIND_PASSWORD = "findPassword";

    /**
     * 聊天记录
     */
    public static final String CHAT = "chat";

    /**
     * 消息通知
     */
    public static final String MESSAGE_NOTIFY="messageNotify";

    /**
     * 相互关注聊天记录
     */
    public static final String MUTUAL_CHAT = "mutualChat";

}
