package com.music.mapper;

import com.music.pojo.entity.UserMusicHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserMusicHistory
 */
@Mapper
public interface UserMusicHistoryMapper extends BaseMapper<UserMusicHistory> {

    @Select("select * from user_music_history where user_id=#{userId} group by hash,album_id order by create_time desc limit 100")
    List<UserMusicHistory> findUserMusicPlayHistory(Integer userId);
}




