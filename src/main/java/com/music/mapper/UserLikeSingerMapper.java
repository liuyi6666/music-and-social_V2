package com.music.mapper;

import com.music.pojo.entity.UserLikeSinger;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserLikeSinger
 */
@Mapper
public interface UserLikeSingerMapper extends BaseMapper<UserLikeSinger> {

    @Select("select * from user_like_singer where user_id=#{userId} and singer_id=#{singerId}")
    UserLikeSinger getUserLikeSingerByUserIdAndSingerId(Integer userId, Integer singerId);

    @Select("select * from user_like_singer where user_id=#{userId}")
    List<UserLikeSinger> findUserLikeSingerByUserId(Integer userId);

    @Select("select * from user_like_singer where user_id=#{userId} and status=#{status}")
    List<UserLikeSinger> findUserLikeSingerByUserIdAndStatus(Integer userId, Integer status);

    @Select("select * from user_like_singer where user_id=#{userId} and status = 1")
    List<UserLikeSinger> getLikeSingerByUserId(Integer userId);
}




