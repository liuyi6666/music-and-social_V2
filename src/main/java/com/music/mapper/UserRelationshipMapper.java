package com.music.mapper;

import com.music.pojo.entity.UserRelationship;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Entity com.music.pojo.entity.UserRelationship
 */
@Mapper
public interface UserRelationshipMapper extends BaseMapper<UserRelationship> {

    @Select("select * from user_relationship where user_id=#{userId} and fan_id=#{toUserId} and status = 1")
    UserRelationship getUserRelationshipByToUserIdAndUserId(Integer toUserId, Integer userId);
}




