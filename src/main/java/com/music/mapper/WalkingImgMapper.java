package com.music.mapper;

import com.music.pojo.entity.WalkingImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.WalkingImg
 */
@Mapper
public interface WalkingImgMapper extends BaseMapper<WalkingImg> {

    @Select({"<script>",
            "select * from walking_img",
            "<where>",
            "<if test='status!=null'>",
            "and status =#{status} ",
            "</if>",
            "</where>",
            "</script>"})
    List<WalkingImg> findAllByStatus(Integer status);
}




