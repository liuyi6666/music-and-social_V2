package com.music.mapper;

import com.music.pojo.entity.UserCommentLikes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.music.pojo.entity.UserCommentLikes
 */
@Mapper
public interface UserCommentLikesMapper extends BaseMapper<UserCommentLikes> {

}




