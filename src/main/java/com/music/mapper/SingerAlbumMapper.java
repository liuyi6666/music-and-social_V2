package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.SingerAlbum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.SingerAlbum
 */
@Mapper
public interface SingerAlbumMapper extends BaseMapper<SingerAlbum> {

    @Select("select * from singer_album where album_id=#{albumId}")
    SingerAlbum getSingerAlbumByAlbumId(Integer albumId);

    @Select("select * from singer_album where singer_id=#{singerId}")
    List<SingerAlbum> findSingerAlbumBySingerId(Integer singerId);

    @Select("select * from singer_album")
    List<SingerAlbum> findAll();
}
