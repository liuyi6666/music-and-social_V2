package com.music.mapper;

import com.music.pojo.dto.UserCommentDTO;
import com.music.pojo.entity.UserComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserComment
 */
@Mapper
public interface UserCommentMapper extends BaseMapper<UserComment> {

    @Select("select * from user_comment where " +
            "hash=#{hash} " +
            "and album_id=#{albumId} " +
            "and comment_level= 1 " +
            "and comment_type = 0 " +
            "and status = 1 order by create_time desc")
    List<UserCommentDTO> findMusicCommentLevel1(String hash, String albumId);


    @Select("select * from user_comment where " +
            "hash=#{hash} " +
            "and album_id=#{albumId} " +
            "and parent_id=#{parentId} " +
            "and comment_type = 0 " +
            "and comment_level = 2 " +
            "and status = 1 order by create_time desc")
    List<UserComment> findMusicCommentLevel2(String hash, String albumId, Integer parentId);


    @Select("select * from user_comment where " +
            "user_id=#{userId} " +
            "and comment_type = 0 " +
            "and status = 1 order by create_time desc")
    List<UserCommentDTO> findMusicCommentByUserId(Integer userId);


    @Select("select * from user_comment where " +
            "user_id=#{userId} " +
            "and comment_type = 1 " +
            "and status = 1 ")
    List<UserCommentDTO> findUserIssueByUserId(Integer userId);

    @Select("select * from user_comment where " +
            "user_id=#{userId} " +
            "and comment_level = 1 " +
            "and comment_type = 1 " +
            "and status = 1 order by create_time desc")
    List<UserCommentDTO> findUserIssueCommentLevel1(Integer userId);


    @Select("select * from user_comment where " +
            "article_id=#{articleId} " +
            "and parent_id =#{parentId} " +
            "and comment_level = 2 " +
            "and comment_type = 1 " +
            "and status = 1 order by create_time desc")
    List<UserComment> findUserIssueCommentLevel2(Integer parentId, Long articleId);


    @Select("select * from user_comment where "+
            "comment_level = 1 " +
            "and comment_type = 1 " +
            "and status = 1 order by create_time desc")
    List<UserCommentDTO> findAllUserIssue();

    @Select("select * from user_comment where user_id=#{userId} and status = 1 order by create_time desc")
    List<UserComment> findUserCommentByUserId(Integer userId);
}




