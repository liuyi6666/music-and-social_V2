package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.NewMusic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.NewMusic
 */
@Mapper
public interface NewMusicMapper extends BaseMapper<NewMusic> {

    @Select("select * from new_music where hash=#{hash} and album_id=#{albumId}")
    NewMusic getNewMusicByHashAndAlbumId(String hash,String albumId);

    @Select("select * from new_music where type=#{type} limit 50")
    List<NewMusic> findNewMusicByType(Integer type);

    @Select("select * from new_music where file_name like concat(#{searchKey},'%')")
    List<NewMusic> findNewMusicByFileName(String searchKey);
}
