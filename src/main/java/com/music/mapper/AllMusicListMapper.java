package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.AllList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AllMusicListMapper extends BaseMapper<AllList> {

    @Select("select * from music_list where rank_id = #{rankId}")
    List<AllList> findAllListDTOByRankId(Integer rankId);

    @Select("select * from music_list")
    List<AllList> findAll();

    @Select("select * from music_list where rank_id=#{rankId}")
    AllList getMusicByRankId(Integer rankId);
}
