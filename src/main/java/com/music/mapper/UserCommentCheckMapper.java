package com.music.mapper;

import com.music.pojo.entity.UserCommentCheck;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.music.pojo.entity.UserCommentCheck
 */
@Mapper
public interface UserCommentCheckMapper extends BaseMapper<UserCommentCheck> {

}




