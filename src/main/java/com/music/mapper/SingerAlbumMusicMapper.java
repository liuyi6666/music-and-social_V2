package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.SingerAlbumMusic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.SingerAlbumMusic
 */
@Mapper
public interface SingerAlbumMusicMapper extends BaseMapper<SingerAlbumMusic> {


    @Select("select * from singer_album_music where hash=#{hash} and album_id=#{albumId}")
    SingerAlbumMusic getSingerAlbumMusicByHashAndAlbumId(String hash, String albumId);

    @Select("select * from singer_album_music where album_id=#{albumId}")
    List<SingerAlbumMusic> findSingerAlbumMusicByAlbumId(Integer albumId);

    @Select("select * from singer_album_music where file_name like concat(#{searchKey},'%')")
    List<SingerAlbumMusic> findSingerAlbumMusicByFileName(String searchKey);
}
