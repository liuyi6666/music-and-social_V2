package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.RankingListMusic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.RankingListMusic
 */
@Mapper
public interface RankingListMusicMapper extends BaseMapper<RankingListMusic> {

    @Select("select * from ranking_list_music where hash=#{hash} and album_id=#{albumId}")
    List<RankingListMusic> findMusicByHashAndAlbumId(String hash, String albumId);

    @Select("select * from ranking_list_music where rank_id=#{rankId} and rank_cid=#{rankCid}")
    List<RankingListMusic> findMusicByRankIdAndRankCid(Integer rankId, Integer rankCid);

    @Select("select * from ranking_list_music where rank_id=#{rankId} and rank_cid=#{rankCid} and hash=#{hash} and album_id=#{albumId}")
    List<RankingListMusic> findMusicByRankIdAndRankCidAndHashAndAlbumId(Integer rankId, Integer rankCid, String hash, String albumId);

    @Select("select * from ranking_list_music where rank_id=#{rankId}")
    List<RankingListMusic> findMusicByRankId(Integer rankId);

    @Select("select * from ranking_list_music where rank_id=8888 limit 10")
    List<RankingListMusic> findMusicTop500ByRankIdDesc10();
}
