package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.SmsConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Entity com.music.pojo.entity.SmsConfig
 */
@Mapper
public interface SmsConfigMapper extends BaseMapper<SmsConfig> {


    @Select("select * from sms_config where template_key=#{templateKey}")
    SmsConfig getSmsConfigByKey(String templateKey);
}
