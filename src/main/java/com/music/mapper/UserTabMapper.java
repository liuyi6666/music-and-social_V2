package com.music.mapper;

import com.music.pojo.entity.UserTab;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserTab
 */
@Mapper
public interface UserTabMapper extends BaseMapper<UserTab> {

    @Select("select tab_name from user_tab")
    List<String> findAll();
}




