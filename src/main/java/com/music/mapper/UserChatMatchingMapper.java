package com.music.mapper;

import com.music.pojo.entity.UserChatMatching;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.music.pojo.entity.UserChatMatching
 */
@Mapper
public interface UserChatMatchingMapper extends BaseMapper<UserChatMatching> {

}




