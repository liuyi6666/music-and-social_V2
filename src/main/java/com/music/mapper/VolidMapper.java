package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.Volid;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface VolidMapper extends BaseMapper<Volid> {

    @Select(value = "select * from music_volid where volid=#{volid}")
    List<Volid> findVolid(Integer volid);

    @Select("select * from music_volid")
    List<Volid> findAll();
}
