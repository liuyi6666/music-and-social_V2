package com.music.mapper;

import com.music.pojo.entity.UserAccessToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Entity com.music.pojo.entity.UserAccessToken
 */
@Mapper
public interface UserAccessTokenMapper extends BaseMapper<UserAccessToken> {

    @Select("select * from user_access_token where user_id=#{userId}")
    UserAccessToken getUserAccessTokenByUserId(Integer userId);
}




