package com.music.mapper;

import com.music.pojo.entity.User;
import com.music.pojo.entity.UserLikeMusic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserLikeMusic
 */
@Mapper
public interface UserLikeMusicMapper extends BaseMapper<UserLikeMusic> {

    @Select("select * from user_like_music where user_id=#{userId}")
    List<UserLikeMusic> findLikeMusicByUserId(Integer userId);

    @Select("select * from user_like_music where user_id=#{userId} and hash=#{hash} and album_id=#{albumId}")
    UserLikeMusic getLikeMusicByUserIdAndHashAndAlbumId(Integer userId, String hash, String albumId);

    @Select("select * from user_like_music where user_id=#{userId} and status =#{status}")
    List<UserLikeMusic> findUserLikeMusicByUserIdAndStatus(Integer userId, Integer status);

    @Select("select * from user_like_music where user_id=#{userId} and status =1")
    List<UserLikeMusic> getLikeMusicByUserId(Integer userId);
}




