package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.SheetMusic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.SheetMusic
 */
@Mapper
public interface SheetMusicMapper extends BaseMapper<SheetMusic> {

    @Select("select * from sheet_music where hash=#{hash} and album_id=#{albumId} and special_id=#{specialId}")
    SheetMusic getSheetMusicByHashAndAlbumIdAndSpecialId(String hash,String albumId,Integer specialId);

    @Select("select * from sheet_music where special_id=#{specialId}")
    List<SheetMusic> findSheetMusicBySpecialId(Integer specialId);

}
