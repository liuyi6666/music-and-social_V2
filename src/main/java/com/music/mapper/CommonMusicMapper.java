package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.entity.CommonMusic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * @Entity com.music.pojo.entity.CommonMusic
 */
@Mapper
public interface CommonMusicMapper extends BaseMapper<CommonMusic> {


    @Select("select * from common_music where hash=#{hash} and album_id=#{albumId}")
    CommonMusic getCommonMusicByHashAndAlbumId(String hash, String albumId);

    @Select("select * from common_music where author_id=#{singerId}")
    List<CommonMusic> findCommonMusicByAuthorId(Integer singerId);

    @Select("select * from common_music where album_id=#{albumId}")
    List<CommonMusic> findCommonMusicByAlbumId(Integer albumId);

    @Select("select * from common_music where audio_name like concat('%',#{searchKey},'%')")
    List<CommonMusic> findCommonMusicBySongName(String searchKey);

    @Select("select * from common_music where audio_name like concat('%',#{musicSearchKey},'%') limit #{start},#{size}")
    List<CommonMusic> findAll(Integer start, Integer size ,String musicSearchKey);

    @Select("select count(1) from common_music where audio_name like concat('%',#{searchKey},'%')")
    Integer countCommonMusic(String searchKey);
}
