package com.music.mapper;

import com.music.pojo.entity.UserProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserProblem
 */
@Mapper
public interface UserProblemMapper extends BaseMapper<UserProblem> {

    @Select("select * from user_problem limit #{start},#{size}")
    List<UserProblem> findAll(Integer start, Integer size);
}




