package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.dto.MusicSheetDTO;
import com.music.pojo.entity.MusicSheet;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MusicSheetMapper extends BaseMapper<MusicSheet> {

    @Select("select * from music_sheet where special_id = #{specialId}")
    List<MusicSheet> findMusicSheetBySpecialId(Integer specialId);

    @Select("select * from music_sheet")
    List<MusicSheet> findMusicSheetAll();

    @Select("SELECT * FROM music_sheet order by id desc limit 30")
    List<MusicSheet> findMusicSheetDefault();

    @Select("select * from music_sheet order by publish_time desc")
    List<MusicSheet> findMusicSheetByPublishTimeDesc();

    @Select("select * from music_sheet order by play_count desc")
    List<MusicSheet> findMusicSheetByPlayTimeDesc();

    @Select("select * from music_sheet order by publish_time desc limit 5")
    List<MusicSheet> findMusicSheetLimit5();

    @Select("select * from music_sheet where special_id = #{specialId}")
    MusicSheet getMusicSheetBySpecialId(Integer specialId);

    @Select("select * from music_sheet where special_name like concat('%',#{searchKey},'%')")
    List<MusicSheet> findMusicSheetBySpecialName(String searchKey);

    @Select({"<script>",
            "select * from music_sheet",
            "<where>",
            "<if test='user!=null'>",
            "and is_kugou_user =#{user} ",
            "</if>",
            "and special_name like concat('%',#{searchKey},'%')",
            "</where>",
            "limit #{start},#{size}",
            "</script>"})
    List<MusicSheet> findAll(Integer start, Integer size, String searchKey, Integer user);

    @Select({"<script>",
            "select count(1) from music_sheet",
            "<where>",
            "<if test='user!=null'>",
            "and is_kugou_user =#{user} ",
            "</if>",
            "and special_name like concat('%',#{searchKey},'%')",
            "</where>",
            "</script>"})
    Integer countMusicSheet(String searchKey, Integer user);
}
