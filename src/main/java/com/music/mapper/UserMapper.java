package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.User
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


    @Select("select * from User where email=#{email} and status = 1")
    User getUserByEmail(String email);

    @Update("update User set password=#{password} where email=#{email}")
    Integer updatePasswordByEmail(String email, String password);

    @Select("select * from user where username=#{userName} and status =1 ")
    User getUserByUserName(String userName);

    @Update("update User set #{col}=#{str} where id=#{userId};")
    void updateUserInfo(String col, Object str, Integer userId);

    @Select("select * from user where username like concat('%',#{searchKey},'%')")
    List<User> findUserByUserName(String searchKey);

    @Select("select * from user where id like concat('%',#{id},'%')")
    List<User> findUserByUserId(Integer id);

    @Select("select * from user where username like concat('%',#{userSearchKey},'%') limit ${start},#{size}")
    List<User> findAll(Integer start, Integer size, String userSearchKey);
}
