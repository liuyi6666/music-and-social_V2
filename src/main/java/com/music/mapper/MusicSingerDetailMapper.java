package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.entity.MusicSingerDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.MusicSingerDetail
 */
@Mapper
public interface MusicSingerDetailMapper extends BaseMapper<MusicSingerDetail> {

    @Select("select * from music_singer_detail where singer_id=#{singerId}")
    MusicSingerDetail getMusicSingerDetailBySingerId(Integer singerId);

    @Select("select * from music_singer_detail where singer_name like concat(#{searchKey},'%')")
    List<MusicSingerDetail> findSingerDetailBySingerName(String searchKey);
}
