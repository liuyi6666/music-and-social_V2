package com.music.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.music.pojo.dto.SingerDTO;
import com.music.pojo.entity.MusicSinger;
import com.music.pojo.entity.Singer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SingerMapper extends BaseMapper<Singer> {

    @Select("select * from music_singer where singer_id=#{singerId}")
    Singer findSingerBySingerId(Integer singerId);

    @Select("select * from music_singer order by heat desc limit 10")
    List<Singer> findSingerByHeatDesc();

    @Select("select * from music_singer where type=#{type} order by heat desc limit 20")
    List<Singer> findSingerByType(Integer type);

    @Select("select * from music_singer")
    List<Singer> findAll();

    List<Singer> findSingerByTypeAndSexType(@Param("type") Integer type,
                                            @Param("sex_type") Integer sexType);

    @Select("select * from music_singer where singer_id=#{singerId}")
    Singer getSingerBySingerId(Integer singerId);

    @Select("select * from music_singer where singer_name like concat(#{searchKey},'%')")
    List<Singer> findSingerBySingerName(String searchKey);

    @Select("select * from music_singer where singer_name like concat('%',#{searchKey},'%') limit ${start},#{size}")
    List<MusicSinger> findAllLimit(Integer start, Integer size, String searchKey);

    @Select("select count(1) from music_singer where singer_name like concat('%',#{searchKey},'%')")
    Integer countSinger(String searchKey);
}
