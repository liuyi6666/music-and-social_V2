package com.music.mapper;

import com.music.pojo.entity.UserLikeSheet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.music.pojo.entity.UserLikeSheet
 */
@Mapper
public interface UserLikeSheetMapper extends BaseMapper<UserLikeSheet> {

    @Select("select * from user_like_sheet where user_id=#{userId} and special_id=#{specialId}")
    UserLikeSheet getUserLikeSheetByUserIdAndSpecialId(Integer userId, Integer specialId);

    @Select("select * from user_like_sheet where user_id=#{userId} and status = #{status}")
    List<UserLikeSheet> findUserLikeSheetByUserIdAndStatus(Integer userId, Integer status);
}




