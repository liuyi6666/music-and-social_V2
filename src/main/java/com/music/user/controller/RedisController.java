package com.music.user.controller;

import cn.hutool.core.convert.Convert;
import com.music.enums.StatusEnum;
import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.entity.CommonMusic;
import com.music.user.service.RedisService;
import com.music.util.KuGouUtil;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/redis")
@CrossOrigin("*")
public class RedisController {

    @Autowired
    private RedisService redisService;


    /**
     * 将播放列表导入redis
     *
     * @param musicJson
     * @return
     */
    @ResponseBody
    @PostMapping("/setMusicPlayQueue")
    public ResultVO setMusicQueue(@RequestBody String musicJson){
        Boolean aBoolean = redisService.setMusicQueue(musicJson);
        if (aBoolean) {
            return ResultVO.result(200, "导入redis成功", null);
        } else {
            return ResultVO.result(0, "导入redis失败", null);
        }
    }

    /**
     * 从redis中取出播放列表
     *
     * @return
     */
    @PostMapping("/getMusicPlayQueue")
    public ResultVO getMusicQueue(){
        HashMap<String, Object> map = new HashMap<>();
        List<CommonMusic> musicPlayQueue=redisService.getMusicQueue();
        List<CommonMusicDTO> commonMusicDTOList = new ArrayList<>();
        for (CommonMusic commonMusic : musicPlayQueue) {
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            commonMusicDTOList.add(commonMusicDTO);
        }
        map.put("info", commonMusicDTOList);
        return ResultVO.result(StatusEnum.MUSIC_PLAY_QUEUE_FROM_REDIS.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_PLAY_QUEUE_FROM_REDIS.getStatusCode()), map);
    }

    /**
     * 匹配用户
     *
     * @param authentication
     * @return
     */
    @PostMapping("/matchingUser")
    public ResultVO matchingUser(@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.matchingUser(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取用户聊天记录
     *
     * @param userId
     * @param toUserId
     * @return
     */
    @PostMapping("/getUserChatRecord/{userId}/{toUserId}")
    public ResultVO getUserChatRecord(@PathVariable Integer userId,@PathVariable Integer toUserId){
        HashMap<String,Object> map = redisService.getUserChatRecord(userId,toUserId);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取通知
     *
     * @param authentication
     * @return
     */
    @PostMapping("/getMessageNotify")
    public ResultVO getMessageNotify(@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.getMessageNotify(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 删除全部通知
     *
     * @param authentication
     * @return
     */
    @PostMapping("/removeAllMessage")
    public ResultVO removeAllMessage(@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.removeAllMessage(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 删除指定通知
     *
     * @param authentication
     * @return
     */
    @PostMapping("/removeOneMessage/{index}")
    public ResultVO removeOneMessage(@PathVariable Integer index,@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.removeOneMessage(index,authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 已读指定通知
     *
     * @param authentication
     * @return
     */
    @PostMapping("/readOneMessage/{index}")
    public ResultVO readOneMessage(@PathVariable Integer index,@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.readOneMessage(index,authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 已读全部通知
     *
     * @param authentication
     * @return
     */
    @PostMapping("/readAllMessage")
    public ResultVO readAllMessage(@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.readAllMessage(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取相互关注的聊天记录
     *
     * @param toUserId
     * @param authentication
     * @return
     */
    @PostMapping("/mutualAttentionChatRecord/{toUserId}")
    public ResultVO getMutualAttentionChatRecord(@PathVariable Integer toUserId,@RequestHeader String authentication){
        HashMap<String,Object> map = redisService.getMutualAttentionChatRecord(toUserId,authentication);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

}
