package com.music.user.controller;

import com.music.enums.StatusEnum;
import com.music.user.service.WalkingService;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/walking")
@CrossOrigin("*")
public class WalkingController {

    @Autowired
    private WalkingService walkingService;

    @GetMapping("/getHeaderImg/{status}")
    private ResultVO getHeaderImg(@PathVariable String status){
        HashMap<String,Object> imageMap=walkingService.getHeaderImg(status);
        Integer statusCode=(Integer) imageMap.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), imageMap);
    }

}
