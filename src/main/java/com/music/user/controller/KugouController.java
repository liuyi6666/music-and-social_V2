package com.music.user.controller;

import cn.hutool.core.convert.Convert;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.music.enums.StatusEnum;
import com.music.pojo.dto.*;
import com.music.pojo.entity.*;
import com.music.user.service.*;
import com.music.util.KuGouUtil;
import com.music.vo.PageVO;
import com.music.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/kugou")
@CrossOrigin("*")
@Slf4j
public class KugouController {
    @Autowired
    private KugouService kugouService;

    @Autowired
    private AllListService allListService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private MusicSheetService musicSheetService;

    @Autowired
    private SheetMusicService sheetMusicService;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private SingerAlbumService singerAlbumService;

    @Autowired
    private SingerAlbumMusicService singerAlbumMusicService;

    @Autowired
    private UserLikeSingerService userLikeSingerService;

    @Autowired
    private UserCommentService userCommentService;

    /**
     * 搜索歌曲
     *
     * @param keyword
     * @return
     */
    @GetMapping("/search/{keyword}")
    public ResultVO search(@PathVariable String keyword) {
        HashMap<String, Object> map = kugouService.searchMusic(keyword);
        return ResultVO.success(StatusEnum.MUSIC_SEARCH.getStatusCode(), StatusEnum.MUSIC_SEARCH.getStatusMsg(), map);
    }

    /**
     * 播放音乐
     *
     * @param album_id
     * @param hash
     * @return
     */
    @GetMapping("/play/{album_id}/{hash}")
    public ResultVO play(@PathVariable String album_id, @PathVariable String hash) {
        HashMap<String, Object> map = kugouService.playMusic(album_id, hash);
        return ResultVO.success(StatusEnum.MUSIC_PLAY.getStatusCode(), StatusEnum.MUSIC_PLAY.getStatusMsg(), map);
    }

    /**
     * 播放音乐,直接调用酷狗api返回播放数据,并将结果更新到数据库中
     *
     * @param hash
     * @param albumId
     * @return
     */
    @GetMapping("/playMusic/{hash}/{albumId}")
    public ResultVO playMusic(@PathVariable String hash, @PathVariable String albumId, @RequestHeader String token) {
        HashMap<String, Object> map = new HashMap<>();
        CommonMusicDTO commonMusicDTO = commonMusicService.playMusicV2(hash, albumId, token);
        map.put("info", commonMusicDTO);
        return ResultVO.result(StatusEnum.MUSIC_PLAY.getStatusCode(), StatusEnum.MUSIC_PLAY.getStatusMsg(), map);

    }

    /**
     * 获取歌单
     *
     * @return
     */
    @GetMapping("/getMusicList")
    public ResultVO getMusicList() {
        HashMap<String, Object> map = kugouService.getMusicSheetV2();
        return ResultVO.result(StatusEnum.MUSIC_LIST.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_LIST.getStatusCode()), map);
    }

    /**
     * 热门歌曲
     *
     * @return
     */
    @GetMapping("/getHotMusic")
    public ResultVO getHotMusic() {
        HashMap<String, Object> map = kugouService.getHotMusicV2();
        return ResultVO.result(StatusEnum.MUSIC_HOT.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_HOT.getStatusCode()), map);
    }

    /**
     * 获取新歌
     *
     * @param musicType
     * @return
     */
    @GetMapping("/newMusic/{musicType}")
    public ResultVO getChinaNewMusic(@PathVariable Integer musicType) {
        HashMap<String, Object> map = kugouService.getNewMusicV2(musicType);
        return ResultVO.result(StatusEnum.MUSIC_NEW.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_NEW.getStatusCode()), map);
    }

    /**
     * 获取歌手
     *
     * @param singerType
     * @return
     */
    @GetMapping("/singer/{singerType}")
    public ResultVO getSinger(@PathVariable Integer singerType) {
        HashMap<String, Object> map = kugouService.getSingerV2(singerType);
        return ResultVO.result(StatusEnum.MUSIC_SINGER.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER.getStatusCode()), map);
    }


    /**
     * 获取指定类型的音乐排行榜
     *
     * @param rankId
     * @return
     */
    @GetMapping("/musicList/{rankId}")
    public ResultVO getMusicListForType(@PathVariable Integer rankId) {
        HashMap<String, Object> map = kugouService.getMusicListForType(rankId);
        return ResultVO.result(StatusEnum.MUSIC_RANKING.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_RANKING.getStatusCode()), map);
    }

    /**
     * 获取全部排行榜
     *
     * @return
     */
    @GetMapping("/musicList/getAllList")
    public ResultVO getAllList() {
        HashMap<String, Object> map = allListService.getAllListV2();
        return ResultVO.result(StatusEnum.MUSIC_ALL_LIST.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_ALL_LIST.getStatusCode()), map);
    }

    /**
     * 获取排行榜下的歌曲
     * 未登录时正常返回，登录后返回用户是否添加喜欢的音乐标识
     *
     * @param rankId
     * @return
     */
    @GetMapping("/musicListV2/{rankId}/{token}")
    public ResultVO getMusicListForTypeV2(@PathVariable Integer rankId, @PathVariable String token) {
        System.out.println("@@@@@@@@@@@@@@@@@@@@@" + token);
        HashMap<String, Object> map = kugouService.getMusicListForTypeV2(rankId, token);
        return ResultVO.result(StatusEnum.MUSIC_RANKING.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_RANKING.getStatusCode()), map);
    }

    /**
     * 获取歌单分类下的歌单
     *
     * @param type
     * @return
     */
    @GetMapping("/musicSheet/{type}")
    public ResultVO getMusicSheet(@PathVariable Integer type, @RequestHeader String token) {
        HashMap<String, Object> map = kugouService.findMusicSheetByType(type, token);
        return ResultVO.result(StatusEnum.MUSIC_SHEET.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SHEET.getStatusCode()), map);
    }

    /**
     * 测试
     *
     * @return
     */
    @GetMapping("/list")
    public ResultVO getSinger() {
        HashMap<String, Object> map = new HashMap<>();
        PageHelper.startPage(1, 4);
        List<Singer> singerTestList = singerService.findSingerTest();
        PageInfo<Singer> singerPageInfo = new PageInfo<>(singerTestList);
        List<Singer> list = singerPageInfo.getList();
        map.put("info", list);
        return ResultVO.result(StatusEnum.MUSIC_SHEET.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SHEET.getStatusCode()), map);
    }

    /**
     * 获取歌手信息
     *
     * @param singerType
     * @param singerNameSpell
     * @param singerCurrentPage
     * @param singerPageSize
     * @return
     */
    @GetMapping("/singerInfo")
    public ResultVO getSingerInfo(String singerType, String singerNameSpell, Integer singerCurrentPage, Integer singerPageSize) {
        log.info("getSingerInfo singerType={},singerNameSpell={},singerCurrentPage={},singerPageSize={}", singerType, singerNameSpell, singerCurrentPage, singerPageSize);
        HashMap<String, Object> map = new HashMap<>();
        PageVO<Singer> singerPageVO = singerService.findSingerByTypeAndLimitAndSingerName(singerType, singerNameSpell, singerCurrentPage, singerPageSize);
        map.put("info", singerPageVO);
        return ResultVO.result(StatusEnum.MUSIC_SINGER.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER.getStatusCode()), map);

    }

    /**
     * 根据歌单id获取歌单信息
     *
     * @param id
     * @return
     */
    @GetMapping("/musicSheetById/{id}")
    public ResultVO getMusicSheetById(@PathVariable Integer id) {
        HashMap<String, Object> map = new HashMap<>();
        MusicSheet musicSheet = musicSheetService.getMusicSheetById(id);
        MusicSheetDTO musicSheetDTO = Convert.convert(MusicSheetDTO.class, musicSheet);
        map.put("info", musicSheetDTO);
        return ResultVO.result(StatusEnum.MUSIC_SHEET.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SHEET.getStatusCode()), map);
    }

    /**
     * 根据歌单id获取歌单歌曲
     *
     * @param specialId
     * @return
     */
    @GetMapping("/sheetMusic/{specialId}")
    public ResultVO getSheetMusicBySpecialId(@PathVariable Integer specialId, @RequestHeader String token) {
        HashMap<String, Object> map = new HashMap<>();
        List<SheetMusicDTO> sheetMusicDTOS = sheetMusicService.findSheetMusicBySpecialId(specialId, token);
        map.put("info", sheetMusicDTOS);
        return ResultVO.result(StatusEnum.MUSIC_SHEET_MUSIC.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SHEET_MUSIC.getStatusCode()), map);
    }


    /**
     * 根据id获取歌手信息
     *
     * @param id
     * @return
     */
    @GetMapping("/singerDetail/{id}")
    public ResultVO getSingerInfoById(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        SingerDTO singerInfo = singerService.getSingerById(id);
        //给singerDTO赋值是否为用户喜欢歌手
        Boolean isUserLikeSinger = userLikeSingerService.isUserLikeSingerByUserIdAndSingerId(authentication,singerInfo.getSingerId());
        singerInfo.setLikeFlag(isUserLikeSinger ? 1 : 0);
        map.put("info", singerInfo);
        return ResultVO.result(StatusEnum.MUSIC_SINGER.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER.getStatusCode()), map);
    }

    /**
     * 根据id获取歌手单曲
     *
     * @param id
     * @return
     */
    @GetMapping("/singerMusic/{id}")
    public ResultVO findSingerMusicById(@PathVariable Integer id, @RequestHeader String token) {
        HashMap<String, Object> map = new HashMap<>();
        List<CommonMusicDTO> commonMusicDTOList = commonMusicService.findSingerMusicByAuthorId(id, token);
        map.put("info", commonMusicDTOList);
        return ResultVO.result(StatusEnum.MUSIC_SINGER_MUSIC.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER_MUSIC.getStatusCode()), map);
    }

    /**
     * 根据歌手id查找专辑
     *
     * @param id
     * @return
     */
    @GetMapping("/singerAlbum/{id}")
    public ResultVO findSingerAlbumBySingerId(@PathVariable Integer id) {
        HashMap<String, Object> map = new HashMap<>();
        SingerDTO singer = singerService.getSingerById(id);
        List<SingerAlbum> singerAlbumList = singerAlbumService.findSingerAlbumBySingerId(singer.getSingerId());
        List<SingerAlbumDTO> singerAlbumDTOList = new ArrayList<>();
        for (SingerAlbum singerAlbum : singerAlbumList) {
            SingerAlbumDTO singerAlbumDTO = Convert.convert(SingerAlbumDTO.class, singerAlbum);
            singerAlbumDTOList.add(singerAlbumDTO);
        }
        map.put("info", singerAlbumDTOList);
        return ResultVO.result(StatusEnum.MUSIC_SINGER_ALBUM.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER_ALBUM.getStatusCode()), map);

    }

    /**
     * 根据albumId获取专辑
     *
     * @param albumId
     * @return
     */
    @GetMapping("/singerAlbumDetail/{albumId}")
    public ResultVO getSingerAlbumByAlbumId(@PathVariable Integer albumId) {
        HashMap<String, Object> map = new HashMap<>();
        SingerAlbum singerAlbum = singerAlbumService.getSingerAlbumByAlbumId(albumId);
        SingerAlbumDTO singerAlbumDTO = Convert.convert(SingerAlbumDTO.class, singerAlbum);
        map.put("info", singerAlbumDTO);
        return ResultVO.result(StatusEnum.MUSIC_SINGER_ALBUM.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER_ALBUM.getStatusCode()), map);
    }

    /**
     * 根据albumId获取歌曲,并把获取的歌曲导入common音乐表中
     *
     * @param albumId
     * @return
     */
    @GetMapping("/singerAlbumMusic/{albumId}")
    public ResultVO findSingerAlbumMusicByAlbumId(@PathVariable Integer albumId, @RequestHeader String token) {
        HashMap<String, Object> map = new HashMap<>();
        List<CommonMusicDTO> commonMusicList = commonMusicService.findCommonMusicByAlbumId(albumId, token);
        map.put("info", commonMusicList);
        return ResultVO.result(StatusEnum.MUSIC_SINGER_ALBUM_MUSIC.getStatusCode(), StatusEnum.getMsg(StatusEnum.MUSIC_SINGER_ALBUM_MUSIC.getStatusCode()), map);
    }

    /**
     * 获取单个音乐信息
     *
     * @param hash
     * @param albumId
     * @return
     */
    @GetMapping("/musicInfo/{hash}/{albumId}")
    public ResultVO getMusicInfo(@PathVariable String hash,@PathVariable String albumId){
        CommonMusic commonMusic = commonMusicService.getCommonMusicByHashAndAlbumId(hash, albumId);
        CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
        commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
        Integer statusCode = StatusEnum.MUSIC_SEARCH.getStatusCode();
        return ResultVO.result(statusCode,StatusEnum.getMsg(statusCode),commonMusicDTO);
    }


    /**
     * 获取单个歌手信息
     *
     * @param singerId
     * @return
     */
    @GetMapping("/singerInfo/{singerId}")
    public ResultVO getMusicInfo(@PathVariable Integer singerId){
        SingerDTO singerDTO = singerService.getSingerBySingerId(singerId);
        Integer statusCode = StatusEnum.MUSIC_SINGER.getStatusCode();
        return ResultVO.result(statusCode,StatusEnum.getMsg(statusCode),singerDTO);
    }


    /**
     * 获取歌曲评论
     *
     * @param hash
     * @param albumId
     * @return
     */
    @GetMapping("/musicComment/{hash}/{albumId}")
    public ResultVO getMusicComment(@PathVariable String hash,@PathVariable String albumId,@RequestHeader String authentication){
        HashMap<String,Object> map = userCommentService.findMusicComment(hash,albumId,authentication);
        Integer statusCode=(Integer) map.get("statusCode");
        return ResultVO.result(statusCode,StatusEnum.getMsg(statusCode),map);
    }

    /**
     * 获取相似歌手
     *
     * @param singerId
     * @return
     */
    @GetMapping("/getSimilarSinger/{id}")
    public ResultVO getSimilarSinger(@PathVariable Integer id){
        HashMap<String,Object> map=singerService.getSimilarSinger(id);
        Integer statusCode=StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(statusCode,StatusEnum.getMsg(statusCode),map);
    }
}
