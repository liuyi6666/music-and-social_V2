package com.music.user.controller;

import com.music.enums.StatusEnum;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.entity.UserComment;
import com.music.user.service.*;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserTabService userTabService;

    @Autowired
    private UserCommentService userCommentService;

    @Autowired
    private UserCommentLikesService userCommentLikesService;

    @Autowired
    private UserMusicHistoryService userMusicHistoryService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserRelationshipService userRelationshipService;

    @Autowired
    private UserChatMatchingService userChatMatchingService;

    @Autowired
    private UserProblemService userProblemService;

    /**
     * 获取邮箱验证
     *
     * @param emailJson
     * @return
     */
    @PostMapping("/before/getEmailCode")
    public ResultVO getEmail(@RequestBody String emailJson) {
        System.out.println("00000000000000000" + emailJson);
        Integer isSendEmail = userService.getEmail(emailJson);
        return ResultVO.result(isSendEmail, StatusEnum.getMsg(isSendEmail), null);
    }

    /**
     * 忘记密码发送验证码
     *
     * @param emailJson
     * @return
     */
    @PostMapping("/before/getEmailCodeForForgetPassword")
    public ResultVO getEmailCodeForForgetPassword(@RequestBody String emailJson) {
        HashMap<String, Object> map;
        map = userService.getEmailCodeForForgetPassword(emailJson);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 验证用户，并签发密钥
     *
     * @param verifyBodyJson
     * @return
     */
    @PostMapping("/before/verifyChangePassword")
    public ResultVO verifyChangePassword(@RequestBody String verifyBodyJson) {
        HashMap<String, Object> map = userService.verifyChangePassword(verifyBodyJson);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 注册用户
     *
     * @param userJson
     * @return
     */
    @ResponseBody
    @PostMapping("/before/register")
    public ResultVO register(@RequestBody String userJson) {
        Integer statusCode = userService.insertUser(userJson);
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 获取验证码
     *
     * @param request
     * @param response
     */
    @PostMapping("/before/getCodeImg")
    public void getCodeImg(HttpServletRequest request, HttpServletResponse response) {
        userService.getCodeImg(request, response);
    }

    /**
     * 登陆
     *
     * @param loginJson
     * @return
     */
    @ResponseBody
    @PostMapping("/before/login")
    public ResultVO login(@RequestBody String loginJson) {
        HashMap<String, Object> map = userService.userLogin(loginJson);
        Integer statusCode = (Integer) map.get("statusCode");
        String token = (String) map.get("token");
        HashMap<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), tokenMap);
    }

    @GetMapping("/test")
    public String test() {
        return "这是测试";
    }

    /**
     * 验证用户登录信息
     *
     * @param tokenJson
     * @return
     */
    @PostMapping("/before/verifyUserLogin")
    public ResultVO verifyUserLogin(@RequestBody String tokenJson) {
        Map<String, Object> map = userService.verifyUserLogin(tokenJson);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 验证登录
     *
     * @param authentication
     * @return
     */
    @PostMapping("/verifyUser")
    public ResultVO verifyLogin(@RequestHeader String authentication) {
        //若能进到这里说明用户已登录
        HashMap<String, Object> map = userService.verifyLogin(authentication);
        Integer statusCode = StatusEnum.USER_STATUS_RIGHT.getStatusCode();
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }


    @PostMapping("/logout")
    public ResultVO logout(@RequestHeader String authentication) {
        Integer statusCode = userService.logout(authentication);
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 修改密码
     *
     * @param tokenJson
     * @return
     */
    @PostMapping("/before/updatePassword")
    public ResultVO updatePassword(@RequestBody String tokenJson) {
        HashMap<String, Object> map = userService.updatePassword(tokenJson);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 变更用户喜欢音乐
     *
     * @param musicJson
     * @param authentication
     * @return
     */
    @PostMapping("/likeMusic")
    public ResultVO likeMusic(@RequestBody String musicJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userService.likeMusic(musicJson, authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 获取用户喜欢歌曲列表
     *
     * @param authentication
     * @return
     */
    @PostMapping("/userLikeMusicList")
    public ResultVO userLikeMusicList(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.findUserLikeMusicList(authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 变更喜欢歌单状态
     *
     * @param musicJson
     * @param authentication
     * @return
     */
    @PostMapping("/likeSheet")
    public ResultVO likeSheet(@RequestBody String musicJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userService.likeSheet(musicJson, authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 获取用户喜欢歌单
     *
     * @param authentication
     * @return
     */
    @PostMapping("/userLikeSheetList")
    public ResultVO userLikeSheetList(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.findUserLikeSheetList(authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 变更用户喜欢歌手
     *
     * @param singerJson
     * @param authentication
     * @return
     */
    @PostMapping("/likeSinger")
    public ResultVO likeSinger(@RequestBody String singerJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userService.likeSinger(singerJson, authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), null);
    }

    /**
     * 查询用户喜欢歌手
     *
     * @param authentication
     * @return
     */
    @PostMapping("/userLikeSingerList")
    public ResultVO userLikeSingerList(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.findUserLikeSingerList(authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 获取用户详细信息
     *
     * @param authentication
     * @return
     */
    @PostMapping("/getUserInfo")
    public ResultVO getUserInfo(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.getUserInfo(authentication);
        Integer statusCode = (Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 获取全部tab
     *
     * @return
     */
    @GetMapping("/before/getAllTabs")
    public ResultVO getAllTabs() {
        HashMap<String, Object> map = userTabService.findAll();
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 更新用户信息
     *
     * @param info
     * @param authentication
     * @param param
     * @return
     */
    @PostMapping("/updateUserInfo/{info}")
    public ResultVO updateUserInfo(@PathVariable String info, @RequestHeader String authentication, @RequestBody String param) {
        HashMap<String, Object> map = userService.updateUserInfo(info, authentication, param);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 评论歌曲level0
     *
     * @param commentJson
     * @param authentication
     * @return
     */
    @PostMapping("/musicComment")
    public ResultVO musicComment(@RequestBody String commentJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.commitMusicCommentV0(commentJson, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 评论歌曲level1
     *
     * @param commentJson
     * @param authentication
     * @return
     */
    @PostMapping("/musicCommentLevel1")
    public ResultVO musicCommentLevel1(@RequestBody String commentJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.commitMusicCommentV1(commentJson, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 删除评论
     *
     * @param id
     * @param authentication
     * @return
     */
    @PostMapping("/deleteComment/{id}")
    public ResultVO deleteComment(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.deleteComment(id, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 修改点赞状态
     *
     * @param commentId
     * @param authentication
     * @return
     */
    @PostMapping("/commentLikes/{commentId}")
    public ResultVO commentLikes(@PathVariable Integer commentId, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentLikesService.commentLikes(commentId, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取用户歌曲评论
     *
     * @param authentication
     * @return
     */
    @PostMapping("/myMusicComment")
    public ResultVO myMusicComment(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.findMyMusicComment(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 根据用户id获取动态信息
     *
     * @param authentication
     * @return
     */
    @PostMapping("/userIssue")
    public ResultVO userIssue(@RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.findUserIssue(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 发布动态
     *
     * @param issueData
     * @param authentication
     * @return
     */
    @PostMapping("/releaseIssue")
    public ResultVO releaseIssue(@RequestBody String issueData, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.releaseIssue(issueData, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 删除评论
     *
     * @param id
     * @param authentication
     * @return
     */
    @PostMapping("/deleteIssue/{id}")
    public ResultVO deleteIssue(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.deleteIssue(id, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 评论动态
     *
     * @param commentInfo
     * @param authentication
     * @return
     */
    @PostMapping("/commentUserIssueLevel1")
    public ResultVO commentUserIssueLevel1(@RequestBody String commentInfo, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.commentUserIssueLevel1(commentInfo, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 获取其他用户的动态
     *
     * @param userId
     * @param authentication
     * @return
     */
    @PostMapping("/before/getOtherUserIssue/{userId}")
    public ResultVO getOtherUserIssue(@PathVariable Integer userId, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.getOtherUserIssue(userId, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 获取用户播放历史
     *
     * @param authentication
     * @return
     */
    @PostMapping("/userMusicPlayHistory")
    public ResultVO userMusicPlayHistory(@RequestHeader String authentication) {
        HashMap<String, Object> map = userMusicHistoryService.findUserMusicHistory(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 添加播放历史
     *
     * @param addJson
     * @param authentication
     * @return
     */
    @PostMapping("/addUserMusicPlayHistory")
    public ResultVO addUserMusicPlayHistory(@RequestBody String addJson, @RequestHeader String authentication) {
        HashMap<String, Object> map = userMusicHistoryService.addUserMusicHistory(addJson, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 清空播放历史
     *
     * @param authentication
     * @return
     */
    @PostMapping("/deleteMusicHistory")
    public ResultVO deleteMusicHistory(@RequestHeader String authentication) {
        HashMap<String, Object> map = userMusicHistoryService.deleteMusicHistory(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 用户匹配
     *
     * @param authentication
     * @return
     */
    @PostMapping("/matchingUser")
    public ResultVO matchingUser(@RequestHeader String authentication) {
        HashMap<String, Object> map = redisService.matchingUser(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 校验用户未填信息
     *
     * @param authentication
     * @return
     */
    @PostMapping("/verifyUserInfo")
    public ResultVO verifyUserInfo(@RequestHeader String authentication) {
        HashMap<String, Object> map = userService.verifyUserInfo(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取全部用户动态
     *
     * @param authentication
     * @return
     */
    @PostMapping("/before/getAllUserIssue")
    public ResultVO getAllUserIssue(@RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.getAllUserIssue(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 匹配用户
     *
     * @param selectInfo
     * @param authentication
     * @return
     */
    @PostMapping("/matchingUserLike")
    public ResultVO matchingUserLike(@RequestBody String selectInfo, @RequestHeader String authentication) {
        HashMap<String, Object> map = userService.matchingUserLike(selectInfo, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 根据id获取用户信息
     *
     * @param id
     * @return
     */
    @PostMapping("/getUserRelationshipById/{id}")
    public ResultVO getUserRelationshipById(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = userRelationshipService.getUserRelationshipById(id, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 加关注\取消关注
     *
     * @param id
     * @param authentication
     * @return
     */
    @PostMapping("/updateUserRelationship/{id}")
    public ResultVO addUserRelationship(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = userRelationshipService.updateUserRelationship(id, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 获取聊天用户列表
     *
     * @param authentication
     * @return
     */
    @PostMapping("/getMatchingChatUser")
    public ResultVO getMatchingChatUser(@RequestHeader String authentication) {
        HashMap<String, Object> map = userChatMatchingService.getMatchingChatUser(authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 根据id获取用户信息
     *
     * @param id
     * @return
     */
    @PostMapping("/getUserInfoById/{id}")
    public ResultVO getUserInfoById(@PathVariable Integer id) {
        UserDTO userInfoById = userService.getUserInfoById(id);
        Integer code = StatusEnum.USER_GET_INFO.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), userInfoById);
    }

    /**
     * 获取好友列表
     *
     * @param authentication
     * @return
     */
    @PostMapping("/getUserRelationship")
    public ResultVO getUserRelationship(@RequestHeader String authentication) {
        HashMap<String,Object> map=userRelationshipService.getUserRelationship(authentication);
        Integer code = StatusEnum.USER_GET_INFO.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 反馈问题
     *
     * @param problemJson
     * @return
     */
    @PostMapping("/before/addProblem")
    public ResultVO addProblem(@RequestBody String problemJson) {
        HashMap<String,Object> map=userProblemService.addProblem(problemJson);
        Integer code = StatusEnum.USER_GET_INFO.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

}
