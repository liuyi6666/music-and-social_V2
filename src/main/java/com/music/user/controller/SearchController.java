package com.music.user.controller;

import com.music.enums.StatusEnum;
import com.music.user.service.SearchService;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/search")
@CrossOrigin("*")
public class SearchController {

    @Autowired
    private SearchService searchService;

    /**
     * 查找音乐
     *
     * @param searchKey
     * @return
     */
    @PostMapping("/music/{searchKey}")
    public ResultVO searchMusic(@PathVariable String searchKey){
        HashMap<String,Object> map=searchService.searchMusic(searchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找歌手
     *
     * @param searchKey
     * @return
     */
    @PostMapping("/singer/{searchKey}")
    public ResultVO searchSinger(@PathVariable String searchKey){
        HashMap<String,Object> map=searchService.searchSinger(searchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找用户
     *
     * @param searchKey
     * @return
     */
    @PostMapping("/user/{searchKey}")
    public ResultVO searchUser(@PathVariable String searchKey){
        HashMap<String,Object> map=searchService.searchUser(searchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找歌单
     *
     * @param searchKey
     * @return
     */
    @PostMapping("/sheet/{searchKey}")
    public ResultVO searchSheet(@PathVariable String searchKey){
        HashMap<String,Object> map=searchService.searchSheet(searchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }
}
