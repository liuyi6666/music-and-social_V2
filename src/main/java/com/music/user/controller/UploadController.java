package com.music.user.controller;

import com.music.constant.UploadPathConstant;
import com.music.enums.StatusEnum;
import com.music.user.service.UploadService;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@RestController
@RequestMapping("/upload")
@CrossOrigin("*")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    /**
     * 上传专辑图片
     *
     * @param file
     * @return
     */
    @PostMapping("/uploadUserSheetImg")
    public ResultVO uploadUserSheetImg(@RequestParam("file") MultipartFile file){
        HashMap<String, Object> map = uploadService.uploadSheetImgFile(file, UploadPathConstant.USER);
        Integer statusCode =(Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 上传用户头像
     *
     * @param file
     * @return
     */
    @PostMapping("/uploadUserHeaderImg")
    public ResultVO uploadUserHeaderImg(@RequestParam("file") MultipartFile file){
        HashMap<String, Object> map = uploadService.uploadUserImgFile(file, UploadPathConstant.USER);
        Integer statusCode =(Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 上传用户背景图片
     *
     * @param file
     * @return
     */
    @PostMapping("/uploadUserCenterImg")
    public ResultVO uploadUserCenterImg(@RequestParam("file") MultipartFile file){
        HashMap<String, Object> map = uploadService.uploadUserCenterImg(file, UploadPathConstant.USER);
        Integer statusCode =(Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }

    /**
     * 上传Banner
     *
     * @param file
     * @return
     */
    @PostMapping("/uploadBanner")
    public ResultVO uploadBanner(@RequestParam("file") MultipartFile file){
        HashMap<String, Object> map = uploadService.uploadCommonBannerImg(file, UploadPathConstant.COMMON);
        Integer statusCode =(Integer) map.get("statusCode");
        return ResultVO.result(statusCode, StatusEnum.getMsg(statusCode), map);
    }
}
