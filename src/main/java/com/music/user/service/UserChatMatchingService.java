package com.music.user.service;

import com.music.pojo.entity.UserChatMatching;

import java.util.HashMap;

public interface UserChatMatchingService {

    void saveUserChat(Integer userId, Integer matchingUserId);

    HashMap<String, Object> getMatchingChatUser(String authentication);

    UserChatMatching getUserChatMatching(Integer userId, Integer toUserId);
}
