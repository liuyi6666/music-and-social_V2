package com.music.user.service;

import com.music.pojo.entity.UserLikeSheet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface UserLikeSheetService {

    UserLikeSheet getUserLikeSheetByUserIdAndSpecialId(Integer userId, Integer specialId);

    void saveUserLikeSheetByUserId(Integer userId, Integer specialId);

    List<UserLikeSheet> findUserLikeSheetByUserIdAndStatus(Integer userId, Integer status);
}
