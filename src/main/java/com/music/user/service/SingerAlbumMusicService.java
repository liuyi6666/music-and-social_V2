package com.music.user.service;

import com.music.pojo.entity.SingerAlbumMusic;

import java.util.List;

public interface SingerAlbumMusicService {
    List<SingerAlbumMusic> findSingerAlbumMusicByAlbumId(Integer albumId);

    List<SingerAlbumMusic> findSingerAlbumMusicByFileName(String searchKey);
}
