package com.music.user.service;

import com.music.pojo.entity.CommonMusic;

import java.util.HashMap;
import java.util.List;

public interface RedisService {

    Boolean setMusicQueue(String musicJson);

    List<CommonMusic> getMusicQueue();

    HashMap<String, Object> matchingUser(String authentication);

    void saveChatRecord(String matchingSessionId, String message, Integer userId ,Integer messageType , Integer chatType);

    HashMap<String, Object> getUserChatRecord(Integer userId,Integer toUserId);

    void saveMessageNotify(String message);

    HashMap<String, Object> getMessageNotify(String authentication);

    HashMap<String, Object> removeAllMessage(String authentication);

    HashMap<String, Object> removeOneMessage(Integer index, String authentication);

    HashMap<String, Object> readOneMessage(Integer index, String authentication);

    HashMap<String, Object> readAllMessage(String authentication);

    HashMap<String, Object> getMutualAttentionChatRecord(Integer toUserId, String authentication);
}
