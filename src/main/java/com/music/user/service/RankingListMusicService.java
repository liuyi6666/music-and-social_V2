package com.music.user.service;

import com.music.pojo.entity.RankingListMusic;

import java.util.List;

public interface RankingListMusicService {

    List<RankingListMusic> findHotMusic();

    RankingListMusic findRankingListMusicById(Integer ids);
}
