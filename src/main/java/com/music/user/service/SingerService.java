package com.music.user.service;

import com.music.pojo.dto.SingerDTO;
import com.music.pojo.entity.MusicSingerDetail;
import com.music.pojo.entity.Singer;
import com.music.vo.PageVO;

import java.util.HashMap;
import java.util.List;

public interface  SingerService {

    List<Singer> findSingerByHeatDesc();

    List<Singer> findSingerByType(Integer type);

    List<Singer> findSingerTest();

    PageVO<Singer> findSingerByTypeAndLimitAndSingerName(String singerType,
                                                         String singerNameSpell,
                                                         Integer ingerCurrentPage,
                                                         Integer singerPageSize);

    SingerDTO getSingerById(Integer id);

    SingerDTO getSingerBySingerId(Integer singerId);

    List<SingerDTO> findSingerDetailBySingerName(String searchKey);

    HashMap<String, Object> findAll(Integer start, Integer size, String singerSearchKey);

    HashMap<String, Object> getSimilarSinger(Integer id);
}
