package com.music.user.service;

import com.music.pojo.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;


public interface UserService {

    Integer insertUser(String userJson);

    HashMap<String, Object> userLogin(String loginJson);

    Integer getEmail(String emailJson);

    void getCodeImg(HttpServletRequest request, HttpServletResponse response);

    HashMap<String, Object> verifyUserLogin(String tokenJson);

    Integer logout(String tokenJson);

    HashMap<String, Object> getEmailCodeForForgetPassword(String emailJson);

    HashMap<String, Object> verifyChangePassword(String verifyBodyJson);

    HashMap<String, Object> updatePassword(String tokenJson);

    HashMap<String, Object> likeMusic(String musicJson, String authentication);

    HashMap<String, Object> findUserLikeMusicList(String authentication);

    HashMap<String, Object> likeSheet(String musicJson, String authentication);

    HashMap<String, Object> findUserLikeSheetList(String authentication);

    HashMap<String, Object> likeSinger(String singerJson, String authentication);

    HashMap<String, Object> findUserLikeSingerList(String authentication);

    HashMap<String, Object> getUserInfo(String authentication);

    HashMap<String, Object> updateUserInfo(String info, String authentication, String param);

    HashMap<String, Object> verifyLogin(String authentication);

    HashMap<String, Object> findMyMusicComment(String authentication);

    UserDTO getUserInfoById(Integer id);

    HashMap<String, Object> verifyUserInfo(String authentication);

    HashMap<String, Object> matchingUserLike(String selectInfo, String authentication);

    List<UserDTO> findUserByUserName(String searchKey);

    HashMap<String, Object> findAll(Integer start, Integer size, String userSearchKey);

    HashMap<String, Object> updateUserStatus(Integer userId, Integer status);

    HashMap<String, Object> findAllLimit(Integer start, Integer size, String userSearchKey);
}
