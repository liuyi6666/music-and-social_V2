package com.music.user.service;

import com.music.pojo.dto.SheetMusicDTO;

import java.util.HashMap;
import java.util.List;

public interface SheetMusicService {

    List<SheetMusicDTO> findSheetMusicBySpecialId(Integer specialId,String token);

    HashMap<String, Object> getSheetMusicBySpecialId(Integer specialId);
}
