package com.music.user.service;

import com.music.pojo.entity.UserTab;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
 *
 */
public interface UserTabService {

    HashMap<String, Object> findAll();

    HashMap<String, Object> addTab(String tabName);
}
