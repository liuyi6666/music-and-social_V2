package com.music.user.service;


import java.util.HashMap;

/**
 *
 */
public interface UserProblemService {

    HashMap<String, Object> addProblem(String problemJson);

    HashMap<String, Object> getUserProblem(Integer start, Integer size);
}
