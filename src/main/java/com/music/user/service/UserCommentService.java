package com.music.user.service;

import com.music.pojo.dto.UserCommentDTO;
import com.music.pojo.entity.UserComment;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
public interface UserCommentService {

    HashMap<String, Object> findMusicComment(String hash, String albumId, String authentication);

    HashMap<String, Object> commitMusicCommentV0(String commentJson, String authentication);

    HashMap<String, Object> commitMusicCommentV1(String commentJson, String authentication);

    HashMap<String, Object> deleteComment(Integer id, String authentication);

    List<UserCommentDTO> findMusicCommentByUserId(Integer userId);

    HashMap<String, Object> findUserIssue(String authentication);

    HashMap<String, Object> releaseIssue(String issueData, String authentication);

    HashMap<String, Object> deleteIssue(Integer id, String authentication);

    HashMap<String, Object> commentUserIssueLevel1(String commentInfo, String authentication);

    HashMap<String, Object> getOtherUserIssue(Integer userId, String authentication);

    HashMap<String, Object> getAllUserIssue(String authentication);

    HashMap<String, Object> findUserCommentByUserId(Integer userId);
}
