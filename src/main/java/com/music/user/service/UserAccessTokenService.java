package com.music.user.service;

/**
 *
 */
public interface UserAccessTokenService {

    void saveUserAccessToken(Integer userId,String token,Integer status);
}
