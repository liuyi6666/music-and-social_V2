package com.music.user.service;

import com.aliyuncs.imageaudit.model.v20191230.ScanTextResponse;
import com.music.pojo.entity.UserCommentCheck;

import java.util.HashMap;

/**
 *
 */
public interface UserCommentCheckService {

    void saveVerifyComment(UserCommentCheck userCommentCheck);

    HashMap<String, Object> showUserCommentCheck(Integer userId);
}
