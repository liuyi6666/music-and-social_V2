package com.music.user.service;

import com.music.pojo.dto.MusicSheetDTO;
import com.music.pojo.entity.MusicSheet;

import java.util.HashMap;
import java.util.List;

public interface MusicSheetService {
    List<MusicSheet> findMusicSheetByType(Integer type);

    List<MusicSheet> findMusicSheetForIndex();

    MusicSheet getMusicSheetById(Integer id);

    MusicSheetDTO getMusicSheetBySpecialId(Integer specialId);

    List<MusicSheet> findMusicSheetBySpecialName(String searchKey);

    HashMap<String, Object> findAll(Integer start, Integer size, String sheetSearchKey);

    HashMap<String, Object> deleteSheet(Integer id);

    HashMap<String, Object> importSheet(String sheetList);
}
