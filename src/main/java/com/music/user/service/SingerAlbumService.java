package com.music.user.service;

import com.music.pojo.entity.SingerAlbum;

import java.util.List;

public interface SingerAlbumService {

    List<SingerAlbum> findSingerAlbumBySingerId(Integer singerId);

    SingerAlbum getSingerAlbumByAlbumId(Integer albumId);
}
