package com.music.user.service.impl;

import cn.hutool.core.date.DateTime;
import com.music.pojo.dto.MessageNotifyDTO;
import com.music.pojo.entity.SmsConfig;
import com.music.user.service.RedisService;
import com.music.user.service.UserService;
import com.music.user.service.impl.MessageNotifyWebSocket;
import com.music.util.SMSUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@Component
public class MessageNotifyTemplate implements Serializable {
    private static final long serialVersionUID = 750896774252105579L;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private MessageNotifyWebSocket messageNotifyWebSocket;

    @Autowired
    private SMSUtil smsUtil;

    public void pushMessageNotify(Integer toUserId, Integer userId, String messageKey, String messageContext) {
        MessageNotifyDTO messageNotifyDTO = new MessageNotifyDTO();
        messageNotifyDTO.setToUserName(userService.getUserInfoById(toUserId).getUsername())
                .setUserName(userService.getUserInfoById(userId).getUsername())
                .setUserId(userId)
                .setToUserId(toUserId)
                .setStatus(1)
                .setMessageTime(new DateTime());
        String[] param = {messageNotifyDTO.getUserName(), messageContext};
        SmsConfig sysConfig = smsUtil.getSysConfig(messageKey, param);
        messageNotifyDTO.setMessageTitle(sysConfig.getTitle());
        messageNotifyDTO.setMessageContext(sysConfig.getContent());
        if (!toUserId.equals(userId)) {
            messageNotifyWebSocket.sendMessageNotify(messageNotifyDTO);
        }
    }
}
