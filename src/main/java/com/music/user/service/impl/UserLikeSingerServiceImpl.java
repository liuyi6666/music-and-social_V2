package com.music.user.service.impl;


import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.mapper.UserLikeSingerMapper;
import com.music.pojo.entity.UserLikeSinger;
import com.music.user.service.UserLikeSingerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Service
@Slf4j
public class UserLikeSingerServiceImpl implements UserLikeSingerService {

    @Autowired
    private UserLikeSingerMapper userLikeSingerMapper;


    /**
     * 变更用户喜欢歌手状态
     *
     * @param userId
     * @param singerId
     */
    @Override
    public void saveUserLikeSingerByUserId(Integer userId, Integer singerId) {
        UserLikeSinger userLikeSinger = userLikeSingerMapper
                .getUserLikeSingerByUserIdAndSingerId(userId, singerId);
        log.info("saveUserLikeSingerByUserId userLikeSinger={}",userLikeSinger);
        if (userLikeSinger == null) {
            userLikeSingerMapper.insert(new UserLikeSinger().setUserId(userId).setSingerId(singerId).setStatus(1));
        } else {
            userLikeSinger.setStatus(userLikeSinger.getStatus() == 0 ? 1 : 0);
            UpdateWrapper<UserLikeSinger> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", userId).eq("singer_id", singerId);
            userLikeSingerMapper.update(userLikeSinger, updateWrapper);
            updateWrapper.clear();
        }
    }

    /**
     * 根据用户id和歌手id判断用户是否喜欢该歌手
     *
     * @param authentication
     * @param singerId
     * @return
     */
    @Override
    public Boolean isUserLikeSingerByUserIdAndSingerId(String authentication, Integer singerId) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(authentication);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        UserLikeSinger userLikeSinger = userLikeSingerMapper.getUserLikeSingerByUserIdAndSingerId(userId, singerId);
        return !Objects.isNull(userLikeSinger) && userLikeSinger.getStatus() == 1;
    }

    public static void main(String[] args) {
        String authentication = "123";
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", "123");
        String token = JWTUtil.createToken(map, "liuyi".getBytes());
        try {
            boolean verify = JWTUtil.verify(token, "liuyi".getBytes());
            System.out.println(verify);
        } catch (Exception e) {
            System.out.println("有错");
            e.printStackTrace();
        }
    }

    /**
     * 根据用户获取喜欢歌手
     *
     * @param userId
     * @param status
     * @return
     */
    @Override
    public List<UserLikeSinger> findUserLikeSingerByUserIdAndStatus(Integer userId, Integer status) {
        return userLikeSingerMapper.findUserLikeSingerByUserIdAndStatus(userId,status);
    }
}




