package com.music.user.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.exceptions.ValidateException;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import com.alibaba.fastjson.JSON;
import com.aliyun.oss.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.constant.EmailTemplateConstant;
import com.music.constant.RedisBusTypeConstant;
import com.music.enums.StatusEnum;
import com.music.mapper.*;
import com.music.pojo.dto.*;
import com.music.pojo.entity.*;
import com.music.user.service.*;
import com.music.util.SMSUtil;
import com.music.util.KuGouUtil;
import com.music.util.MyJWTUtil;
import com.music.util.RedisUtil;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${spring.mail.username}")
    private String emailUserName;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private SMSUtil SMSUtil;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserLikeMusicService userLikeMusicService;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private UserLikeSheetService userLikeSheetService;

    @Autowired
    private MusicSheetService musicSheetService;

    @Autowired
    private UserLikeSingerService userLikeSingerService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private UserAccessTokenService userAccessTokenService;

    @Autowired
    private UserCommentService userCommentService;

    @Autowired
    private UserLikeMusicMapper userLikeMusicMapper;

    @Autowired
    private UserLikeSingerMapper userLikeSingerMapper;

    @Autowired
    private SingerMapper singerMapper;

    @Autowired
    private UserCommentCheckMapper userCommentCheckMapper;


    //登陆验证码
    private ShearCaptcha captcha;

    //盐值
    private static final String SECRET = "liuyi";

    //邮箱验证码过期时间 分钟
    private static final Integer EMAIL_CODE_EXPIRES = 5;

    /**
     * 注册用户
     *
     * @param userJson
     * @return
     */
    @Override
    public Integer insertUser(String userJson) {
        log.info("insertUser userJson={}", userJson);
        JSONObject jsonObject = JSONUtil.parseObj(userJson);
        System.out.println(userJson);
        String email = (String) jsonObject.get("email");
        String redisKey = RedisBusTypeConstant.REGISTER + "_User_email_" + email;
        //邮箱验证码验证
        if (redisTemplate.opsForValue().get(redisKey) == null) {
            return StatusEnum.CODE_OVERDUE.getStatusCode();
        }

        if (!jsonObject.get("emailCode").equals(redisTemplate.opsForValue().get(redisKey))) {
            return StatusEnum.CODE_ERROR.getStatusCode();
        }
        User user = new User();
        user.setUsername((String) jsonObject.get("username")).setEmail((String) jsonObject.get("email")).setPassword((String) jsonObject.get("pass"));
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", user.getUsername());
        if (userMapper.selectOne(queryWrapper) != null) {
            return StatusEnum.USER_BEREGISTERED.getStatusCode();//用户名存在
        }
        queryWrapper.clear();
        queryWrapper.eq("email", user.getEmail());
        if (userMapper.selectOne(queryWrapper) != null) {
            return StatusEnum.EMAIL_BEREGISTERED.getStatusCode();//邮箱为存在
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userMapper.insert(user);
        return StatusEnum.REGISTER_SUCCESS.getStatusCode();//注册成功
    }

    /**
     * 用户登录
     *
     * @param loginJson
     * @return
     */
    @Override
    public HashMap<String, Object> userLogin(String loginJson) {
        JSONObject jsonObject = JSONUtil.parseObj(loginJson);
        String code = (String) jsonObject.get("code");
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", "");
        map.put("token", "");
        if (captcha == null) {
            map.put("statusCode", StatusEnum.CODE_NULL.getStatusCode());
            return map;
        }
        if (!captcha.verify(code)) {
            map.put("statusCode", StatusEnum.CODE_ERROR.getStatusCode());
            return map;
        }
        captcha.createCode();
        String usernameOrEmail = (String) jsonObject.get("usernameOrEmail");
        String password = (String) jsonObject.get("password");
        String check = "^[A-Za-z\\d]+([-_.][A-Za-z\\d]+)*@([A-Za-z\\d]+[-.])+[A-Za-z\\d]{2,4}$";
        Pattern regex = Pattern.compile(check);
        Matcher matcher = regex.matcher(usernameOrEmail);
        String column = matcher.matches() ? "email" : "username";
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(column, usernameOrEmail);
        User user = userMapper.selectOne(queryWrapper);
        if (!ObjectUtil.isNotEmpty(user)) {
            map.put("statusCode", StatusEnum.USER_PASSWORD_ERROR.getStatusCode());
        } else {
            if (bCryptPasswordEncoder.matches(password, user.getPassword())) {
                map.put("statusCode", StatusEnum.LOGIN_SUCCESS.getStatusCode());
                HashMap<String, Object> jwtPayLoad = new HashMap<>();
                jwtPayLoad.put("userName", user.getUsername());
                jwtPayLoad.put("userId", user.getId());
                jwtPayLoad.put("email", user.getEmail());
                String token = JWTUtil.createToken(jwtPayLoad, SECRET.getBytes());
                DateTime iatDate = DateUtil.date();
                token = JWTUtil.parseToken(token)
                        .setExpiresAt(DateUtil.offsetDay(iatDate, 1))
                        .setSigner("HS256", SECRET.getBytes())
                        .sign();
                map.put("token", token);
                map.put("info", Convert.convert(UserDTO.class, user));
                userAccessTokenService.saveUserAccessToken(user.getId(), token, 1);
            } else {
                map.put("statusCode", StatusEnum.USER_PASSWORD_ERROR.getStatusCode());
            }
        }
        return map;
    }


    /**
     * 发送邮箱验证码
     *
     * @param emailJson
     * @return
     */
    @Override
    public Integer getEmail(String emailJson) {
        JSONObject jsonObject = JSONUtil.parseObj(emailJson);
        String email = (String) jsonObject.get("email");
        if (StringUtil.isNullOrEmpty(email)) {
            log.error("邮箱为空！");
            return StatusEnum.EMAIL_NULL.getStatusCode();
        }
        String emailCode = SMSUtil.randomNumBuilder();
        String[] param = new String[]{emailCode};
        SmsConfig smsInfo = SMSUtil.getSmsInfo(EmailTemplateConstant.EMAIL_REGISTER, param);
        redisUtil.setEmailCode(email, emailCode, RedisBusTypeConstant.REGISTER);
        Integer code = SMSUtil.sentMessageV2(emailUserName, email, smsInfo.getTitle(), smsInfo.getContent());
        if (code != 200) {
            return StatusEnum.EMAIL_SEND_FAIL.getStatusCode();
        }
        return StatusEnum.EMAIL_SEND_SUCCESS.getStatusCode();
    }

    /**
     * 忘记密码获取邮箱验证码
     *
     * @param emailJson
     * @return
     */
    @Override
    public HashMap<String, Object> getEmailCodeForForgetPassword(String emailJson) {
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(emailJson);
        String email = (String) jsonObject.get("email");
        if (StringUtil.isNullOrEmpty(email)) {
            log.error("邮箱为空！");
            map.put("statusCode", StatusEnum.EMAIL_NULL.getStatusCode());
            return map;
        }
        //先判断是否注册过
        User user = userMapper.getUserByEmail(email);
        if (Objects.isNull(user)) {
            map.put("statusCode", StatusEnum.USER_NOTREGISTER.getStatusCode());
            return map;
        }
        String emailCode = SMSUtil.randomNumBuilder();
        String[] param = new String[]{emailCode};
        SmsConfig smsInfo = SMSUtil.getSmsInfo(EmailTemplateConstant.EMAIL_FORGET_PASSWORD, param);
        if (Objects.isNull(smsInfo)) {
            map.put("statusCode", StatusEnum.EMAIL_TEMPLATE_WARNING.getStatusCode());
            return map;
        }
        redisUtil.setEmailCode(email, emailCode, RedisBusTypeConstant.FIND_PASSWORD);
        Integer code = SMSUtil.sentMessageV2(emailUserName, email, smsInfo.getTitle(), smsInfo.getContent());
        if (code != 200) {
            map.put("statusCode", StatusEnum.EMAIL_SEND_FAIL.getStatusCode());
            return map;
        }
        map.put("statusCode", StatusEnum.EMAIL_SEND_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 验证邮箱验证码，并签发密钥，提供给修改密码
     *
     * @param verifyBodyJson
     * @return
     */
    @Override
    public HashMap<String, Object> verifyChangePassword(String verifyBodyJson) {
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(verifyBodyJson);
        String email = (String) jsonObject.get("email");
        String emailCode = (String) jsonObject.get("emailCode");
        String redisEmailCode = redisUtil.getEmailCode(RedisBusTypeConstant.FIND_PASSWORD, email);
        if (StringUtil.isNullOrEmpty(redisEmailCode)) {
            map.put("statusCode", StatusEnum.CODE_OVERDUE.getStatusCode());
            return map;
        }
        if (!redisEmailCode.equals(emailCode)) {
            map.put("statusCode", StatusEnum.CODE_ERROR.getStatusCode());
        }
        map.put("statusCode", StatusEnum.CODE_RIGHT.getStatusCode());
        String ip = (String) jsonObject.get("ip");
        HashMap<String, Object> jwtPayLoad = new HashMap<>();
        jwtPayLoad.put("email", email);
        jwtPayLoad.put("ip", ip);
        DateTime IatDate = DateUtil.date();
        String token = JWTUtil.createToken(jwtPayLoad, SECRET.getBytes());
        token = JWTUtil.parseToken(token)
                .setExpiresAt(DateUtil.offsetMinute(IatDate, 3))
                .setSigner("HS256", SECRET.getBytes())
                .sign();
        map.put("info", token);
        return map;
    }

    /**
     * 图片验证码
     *
     * @param request
     * @param response
     */

    @Override
    public void getCodeImg(HttpServletRequest request, HttpServletResponse response) {
        captcha = CaptchaUtil.createShearCaptcha(100, 40, 4, 4);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(new MathGenerator());
        // 重新生成code
        captcha.createCode();
        try {
            request.getSession().setAttribute("CAPTCHA_KEY", captcha.getCode());
            response.setContentType("image/png");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);
            captcha.write(response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 验证是否用户登陆
     *
     * @param tokenJson
     * @return
     */

    @Override
    public HashMap<String, Object> verifyUserLogin(String tokenJson) {
        log.info("verifyUserLogin tokenJson={}", tokenJson);
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(tokenJson);
        Object token = jsonObject.get("token");
        //判断token是否为空
        if (token.equals(null)) {
            System.out.println("这是空的");
            map.put("statusCode", StatusEnum.TOKEN_NULL.getStatusCode());
        } else {
            //判断token是否过期
            try {
                JWTValidator.of((String) token).validateDate(DateUtil.date());
            } catch (Exception e) {
                e.printStackTrace();
                map.put("statusCode", StatusEnum.TOKEN_OVERDUE.getStatusCode());
                return map;
            }
            //判断token是否合法
            try {
                boolean verify = JWTUtil.verify((String) token, SECRET.getBytes());
                if (!verify) {
                    map.put("statusCode", StatusEnum.TOKEN_EXCEPTION.getStatusCode());
                    return map;
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("statusCode", StatusEnum.TOKEN_EXCEPTION.getStatusCode());
                return map;
            }
            //解析token信息
            JWT jwt = JWTUtil.parseToken((String) token);
            Integer userId = (Integer) jwt.getPayload("userId");
            User user = userMapper.selectById(userId);
            map.put("statusCode", StatusEnum.TOKEN_NOTNULL.getStatusCode());
            map.put("info", Convert.convert(UserDTO.class, user));
        }
        return map;
    }

    /**
     * 注销登录
     *
     * @param tokenJson
     * @return
     */
    @Override
    public Integer logout(String tokenJson) {
        JWT jwt = JWTUtil.parseToken(tokenJson);
        String redisUserKey = (String) jwt.getPayload("userName");
        Integer userId = (Integer) jwt.getPayload("userId");
        Boolean isDelete = redisTemplate.delete(redisUserKey);
        if (isDelete == null) {
            log.warn("boolean类型包装类为null");
        }
        if (!isDelete) {
            return StatusEnum.LOGOUT_ERROR.getStatusCode();
        }
        userAccessTokenService.saveUserAccessToken(userId, tokenJson, 0);
        return StatusEnum.LOGOUT_SUCCESS.getStatusCode();
    }


    /**
     * 设置邮箱标题、模板、验证码.(测试使用)
     *
     * @return 邮件内容
     */
    @Deprecated
    private String setEmailBody(String email) {
        //获取邮箱随机验证码
        String emailCode = SMSUtil.randomNumBuilder();
        //将邮箱验证码放入redis中
        try {
            redisTemplate.opsForValue().set(email, emailCode);
            redisTemplate.expire(email, EMAIL_CODE_EXPIRES, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("redis未启动！");
        }
        StringBuffer emailBody;
        emailBody = new StringBuffer();
        emailBody.append("这是测试的验证码\n\n")
                .append("您的验证码为：")
                .append(emailCode)
                .append("\n\n")
                .append("验证码有效期为5分钟");
        return emailBody.toString();
    }

    /**
     * 更新密码
     *
     * @param tokenJson
     * @return
     */
    @Override
    public HashMap<String, Object> updatePassword(String tokenJson) {
        log.info("updatePassword tokenJson={}", tokenJson);
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(tokenJson);
        String token = (String) jsonObject.get("token");
        JWT jwt = null;
        try {
            jwt = JWTUtil.parseToken(token);
        } catch (Exception e) {
            map.put("statusCode", StatusEnum.TOKEN_EXCEPTION.getStatusCode());
            return map;
        }
        //判断邮箱是否相同
        String email = (String) jsonObject.get("email");
        String jwtEmail = (String) jwt.getPayload("email");
        if (!email.equals(jwtEmail)) {
            map.put("statusCode", StatusEnum.USER_EMAIL_NOT_IDENTICAL.getStatusCode());
            return map;
        }
        //判断修改密码地址是否相同
        String ip = (String) jsonObject.get("ip");
        String jwtIp = (String) jwt.getPayload("ip");
        if (!ip.equals(jwtIp)) {
            map.put("statusCode", StatusEnum.USER_IP_EXCEPTION.getStatusCode());
            return map;
        }
        //判断令牌是否过期
        try {
            JWTValidator.of(jwt).validateDate(DateUtil.date());
        } catch (ValidateException e) {
            map.put("statusCode", StatusEnum.TOKEN_OVERDUE_V2.getStatusCode());
            return map;
        }
        String pass = (String) jsonObject.get("pass");
        Integer status = userMapper.updatePasswordByEmail(email, bCryptPasswordEncoder.encode(pass));
        if (status != 1) {
            map.put("statusCode", StatusEnum.USER_UPDATE_FAIL.getStatusCode());
            return map;
        }
        map.put("statusCode", StatusEnum.USER_UPDATE_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 验证token
     *
     * @param token
     * @return
     */
    private JWT analysisToken(String token) {
        JWT jwt = JWTUtil.parseToken(token);
        return null;
    }

    /**
     * 添加喜欢的音乐
     *
     * @param musicJson
     * @return
     */
    @Override
    public HashMap<String, Object> likeMusic(String musicJson, String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(musicJson);
        String hash = (String) jsonObject.get("hash");
        String albumId = (String) jsonObject.get("albumId");
        JWT jwt = JWTUtil.parseToken(authentication);
        Integer userId = (Integer) jwt.getPayload("userId");
        userLikeMusicService.saveLikeMusicByUserId(userId, hash, albumId);
        map.put("statusCode", StatusEnum.USER_LIKE_MUSIC.getStatusCode());
        return map;
    }

    /**
     * 获取用户喜欢歌曲，返回commonMusicList
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findUserLikeMusicList(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        List<UserLikeMusic> likeMusicList = userLikeMusicService.findLikeMusicByUserIdAndStatus(userId, 1);
        List<CommonMusicDTO> commonMusicDTOList = new ArrayList<>();
        for (UserLikeMusic userLikeMusic : likeMusicList) {
            CommonMusic commonMusic = commonMusicService.getCommonMusicByHashAndAlbumId(userLikeMusic.getHash(), userLikeMusic.getAlbumId());
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setLikeFlag(1);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            commonMusicDTOList.add(commonMusicDTO);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_GET_LIKE_MUSIC_LIST.getStatusCode());
        map.put("info", commonMusicDTOList);
        return map;
    }

    /**
     * 添加喜欢歌单
     *
     * @param musicJson
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> likeSheet(String musicJson, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        JSONObject jsonObject = JSONUtil.parseObj(musicJson);
        Integer specialId = (Integer) jsonObject.get("specialId");
        userLikeSheetService.saveUserLikeSheetByUserId(userId, specialId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_LIKE_SHEET.getStatusCode());
        return map;
    }

    /**
     * 查询用户喜欢
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findUserLikeSheetList(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        List<UserLikeSheet> userLikeSheetList = userLikeSheetService.findUserLikeSheetByUserIdAndStatus(userId, 1);
        List<MusicSheetDTO> sheetMusicDTOS = new ArrayList<>();
        for (UserLikeSheet userLikeSheet : userLikeSheetList) {
            MusicSheetDTO musicSheetDTO = musicSheetService.getMusicSheetBySpecialId(userLikeSheet.getSpecialId());
            musicSheetDTO.setLikeFlag(1);
            sheetMusicDTOS.add(musicSheetDTO);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_GET_LIKE_SHEET_LIST.getStatusCode());
        map.put("info", sheetMusicDTOS);
        return map;
    }

    /**
     * 变更用户喜欢歌手
     *
     * @param singerJson
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> likeSinger(String singerJson, String authentication) {
        log.info("likeSinger singerJson={},authentication={}", singerJson, authentication);
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        JSONObject jsonObject = JSONUtil.parseObj(singerJson);
        Integer singerId = (Integer) jsonObject.get("singerId");
        Integer userId = (Integer) jwtInfo.get("userId");
        userLikeSingerService.saveUserLikeSingerByUserId(userId, singerId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_LIKE_SINGER.getStatusCode());
        return map;
    }

    /**
     * 获取token中的信息
     *
     * @param token
     * @return
     */
    @Deprecated
    private HashMap<String, Object> getJWTInfo(String token) {
        HashMap<String, Object> map = new HashMap<>();
        Integer userId = null;
        String userName = null;
        String email = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
            userName = (String) jwt.getPayload("userName");
            email = (String) jwt.getPayload("email");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", StatusEnum.TOKEN_EXCEPTION.getStatusCode());
            return map;
        }
        map.put("statusCode", StatusEnum.TOKEN_SUCCESS);
        map.put("userId", userId);
        map.put("userName", userName);
        map.put("eamil", email);
        return map;
    }

    /**
     * 查找用户喜欢歌手
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findUserLikeSingerList(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        HashMap<String, Object> map = new HashMap<>();
        ArrayList<SingerDTO> singerDTOS = new ArrayList<>();
        List<UserLikeSinger> userLikeSingerList = userLikeSingerService.findUserLikeSingerByUserIdAndStatus(userId, 1);
        for (UserLikeSinger userLikeSinger : userLikeSingerList) {
            SingerDTO singerDTO = singerService.getSingerBySingerId(userLikeSinger.getSingerId());
            singerDTO.setLikeFlag(1);
            singerDTOS.add(singerDTO);
        }
        map.put("statusCode", StatusEnum.USER_GET_LIKE_SINGER_LIST.getStatusCode());
        map.put("info", singerDTOS);
        return map;
    }

    /**
     * 获取用户信息
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getUserInfo(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        HashMap<String, Object> map = new HashMap<>();
        Integer userId = (Integer) jwtInfo.get("userId");
        User user = userMapper.selectById(userId);
        UserDTO userDTO = Convert.convert(UserDTO.class, user);
        List<String> musicHobbyList;
        if (!StringUtils.isNullOrEmpty(user.getMusicHobby())) {
            musicHobbyList = JSON.parseArray(user.getMusicHobby(), String.class);
        } else {
            musicHobbyList = new ArrayList<>();
        }
        userDTO.setMusicHobby(musicHobbyList);
        map.put("statusCode", StatusEnum.USER_GET_INFO.getStatusCode());
        map.put("info", userDTO);
        return map;
    }

    /**
     * 更新用户信息
     *
     * @param info           字段名
     * @param authentication 获取用户id
     * @param param          字段值
     * @return
     */
    @Override
    @Transactional
    public HashMap<String, Object> updateUserInfo(String info, String authentication, String param) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(param);
        Object str;

        str = jsonObject.get("param");
        if ("personalSignature".equals(info)) {
            info = "personal_signature";
        }
        if ("musicHobby".equals(info)) {
            info = "music_hobby";
            str = str.toString();
        }
        if ("headerImg".equals(info)) {
            info = "header_img";
        }
        if ("personalCenterImg".equals(info)) {
            info = "personal_center_img";
        }

        System.out.println("+++++++++++++++++++++++" + str);
//        userMapper.updateUserInfo(info,str,userId);
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
        userUpdateWrapper.eq("id", userId);
        userUpdateWrapper.set(info, str);
        //修改生日时增加年龄
        Integer age = null;
        if ("birthday".equals(info)) {
            //当为生日字段时，自动存入用户年龄
            try {
                String dateStr = str + " 00:00:00";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");
                Date birthDay = sdf.parse(dateStr);
                age = getAge(birthDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userUpdateWrapper.set("age", age);
        }
        userMapper.update(null, userUpdateWrapper);
        map.put("statusCode", StatusEnum.USER_UPDATE_SUCCESS.getStatusCode());
        return map;
    }

    private Integer getAge(Date birthDay) {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) { //出生日期晚于当前时间，无法计算
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);  //当前年份
        int monthNow = cal.get(Calendar.MONTH);  //当前月份
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH); //当前日期
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;   //计算整岁数
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;//当前日期在生日之前，年龄减一
            } else {
                age--;//当前月份在生日之前，年龄减一
            }
        }
        return age;
    }

    /**
     * 验证登录
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> verifyLogin(String authentication) {
        return MyJWTUtil.getJWTInfo(authentication);
    }

    /**
     * 获取用户歌曲评论（歌曲信息、评论信息）
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findMyMusicComment(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if ((jwtInfo.get("statusCode")).equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        //获取评论
        List<UserCommentDTO> userCommentList = userCommentService.findMusicCommentByUserId(userId);
        for (UserCommentDTO userCommentDTO : userCommentList) {
            CommonMusic music = commonMusicService.getCommonMusicByHashAndAlbumId(userCommentDTO.getHash(), userCommentDTO.getAlbumId());
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, music);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(music.getTimeLength()));
            userCommentDTO.setCommonMusicDTO(commonMusicDTO);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_MUSIC_COMMENT_SUCCESS.getStatusCode());
        map.put("info", userCommentList);
        return map;
    }

    /**
     * 根据id获取用户信息
     *
     * @param id
     * @return
     */
    @Override
    public UserDTO getUserInfoById(Integer id) {
        User user = userMapper.selectById(id);
        UserDTO userDTO = Convert.convert(UserDTO.class, user);
        List<String> musicHobbyList;
        if (!StringUtils.isNullOrEmpty(user.getMusicHobby())) {
            musicHobbyList = JSON.parseArray(user.getMusicHobby(), String.class);
        } else {
            musicHobbyList = new ArrayList<>();
        }
        userDTO.setMusicHobby(musicHobbyList);
        return userDTO;
    }

    /**
     * 校验用户信息是否完整
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> verifyUserInfo(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        User user = userMapper.selectById(userId);
        List<String> list = new ArrayList<>();
        if (Objects.isNull(user.getMusicHobby()) || "".equals(user.getMusicHobby())) {
            list.add("兴趣");
        }
        if (Objects.isNull(user.getBirthday()) || "".equals(user.getBirthday())) {
            list.add("生日");
        }
        if (Objects.isNull(user.getSex()) || "".equals(user.getSex())) {
            list.add("性别");
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        map.put("info", list);
        return map;
    }

    /**
     * 匹配兴趣爱好相近的用户,随机展示用户情况
     * 第一个维度 用户喜欢歌曲（只比较hash）、歌手、标签相似情况
     * 当前用户的喜欢的歌曲同每个用户比较喜欢的歌曲比较，当前用户喜欢的歌手同每个用喜欢的歌手比较
     * 第二个维度 用户年龄 性别
     * 年龄在一定区间，性别不同
     *
     * @param selectInfo
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> matchingUserLike(String selectInfo, String authentication) {
        JSONObject jsonObject = JSONUtil.parseObj(selectInfo);
        List<Integer> age = (List<Integer>) jsonObject.get("age");
        Integer minAge = age.get(0);
        Integer maxAge = age.get(1);
        String sex = (String) jsonObject.get("sex");
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        //根据用户选择挑选用户范围
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq(!"".equals(sex), "sex", sex)
                .between("age", minAge, maxAge)
                .ne("id", userId);
        List<User> userList = userMapper.selectList(userQueryWrapper);
        //获取当前用户的喜欢歌曲
        List<UserLikeMusic> userLikeMusicList = userLikeMusicMapper.getLikeMusicByUserId(userId);
        //获取当前用户的喜欢歌手
        List<UserLikeSinger> userLikeSingerList = userLikeSingerMapper.getLikeSingerByUserId(userId);
        //获取当前用户的标签
        User nowUser = userMapper.selectById(userId);
        List<String> musicHobbyList = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(nowUser.getMusicHobby())) {
            musicHobbyList = JSON.parseArray(nowUser.getMusicHobby(), String.class);
        }
        //查找当前用户喜欢歌曲同其他用户比较
        SameUserLikeInfo sameUserLikeInfo = new SameUserLikeInfo();
        HashMap<Integer, Object> userLikeSameMusicMap = new HashMap<>();
        HashMap<Integer, Object> userLikeSameSingerMap = new HashMap<>();
        HashMap<Integer, Object> userSameHobbyTabsMap = new HashMap<>();
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : userList) {
            //用户喜欢相同歌曲
            List<UserLikeMusic> sameLikeMusic = new ArrayList<>();
            for (UserLikeMusic userLikeMusic : userLikeMusicMapper.getLikeMusicByUserId(user.getId())) {
                List<UserLikeMusic> collect = userLikeMusicList.stream()
                        .filter(music -> {
                            if (music.getHash().equals(userLikeMusic.getHash())) {
                                sameLikeMusic.add(music);
                                return true;
                            }
                            return false;
                        })
                        .collect(Collectors.toList());
                if (CollUtil.isNotEmpty(sameLikeMusic)) {
                    for (UserLikeMusic likeMusic : sameLikeMusic) {
                        likeMusic.setMusicName(commonMusicService.getCommonMusicByHashAndAlbumId(likeMusic.getHash(), likeMusic.getAlbumId()).getSongName());
                    }
                    userLikeSameMusicMap.put(user.getId(), sameLikeMusic);
                    userDTOS.add(Convert.convert(UserDTO.class, user));
                }
            }
            //用户喜欢相同歌手
            List<UserLikeSinger> sameLikeSinger = new ArrayList<>();
            for (UserLikeSinger userLikeSinger : userLikeSingerMapper.getLikeSingerByUserId(user.getId())) {
                List<UserLikeSinger> collect = userLikeSingerList.stream()
                        .filter(singer -> {
                            if (singer.getSingerId().equals(userLikeSinger.getSingerId())) {
                                sameLikeSinger.add(singer);
                                return true;
                            }
                            return false;
                        })
                        .collect(Collectors.toList());
                System.out.println("用户喜欢的歌手:" + sameLikeSinger);
                if (CollUtil.isNotEmpty(sameLikeSinger)) {
                    for (UserLikeSinger likeSinger : sameLikeSinger) {
                        likeSinger.setSingerName(singerMapper.getSingerBySingerId(likeSinger.getSingerId()).getSingerName());
                    }
                    userLikeSameSingerMap.put(user.getId(), sameLikeSinger);
                    userDTOS.add(Convert.convert(UserDTO.class, user));
                }
            }
            //用户具有相同的标签
            List<String> sameHobby = new ArrayList<>();
            for (String s : musicHobbyList) {
                if (user.getMusicHobby().contains(s)) {
                    sameHobby.add(s);
                    userDTOS.add(Convert.convert(UserDTO.class, user));
                    System.out.println(s);
                }
            }
            userSameHobbyTabsMap.put(user.getId(), sameHobby);
            System.out.println("++++++++" + sameHobby);
        }
        //去重userDTOS
        userDTOS = userDTOS.stream().distinct().collect(Collectors.toList());
        sameUserLikeInfo.setUserDTO(userDTOS);
        sameUserLikeInfo.setUserLikeSameMusicMap(userLikeSameMusicMap);
        sameUserLikeInfo.setUserLikeSameSingerMap(userLikeSameSingerMap);
        sameUserLikeInfo.setUserSameHobbyTabsMap(userSameHobbyTabsMap);
        System.out.println(JSONUtil.toJsonStr(sameUserLikeInfo));
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_MATCHING_USER_LIKE_SUCCESS.getStatusCode());
        map.put("info", sameUserLikeInfo);
        return map;
    }

    /**
     * 查找用户
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<UserDTO> findUserByUserName(String searchKey) {
        List<User> users = userMapper.findUserByUserName(searchKey);
        if (Objects.nonNull(searchKey) && searchKey.matches("\\d+")) {
            Integer id = Integer.valueOf(searchKey);
            List<User> users1 = userMapper.findUserByUserId(id);
            users.addAll(users1);
        }
        return Convert.convert(new TypeReference<List<UserDTO>>() {
        }, users).stream().distinct().collect(Collectors.toList());
    }

    /**
     * 查找用户分页
     *
     * @param start
     * @param size
     * @param userSearchKey
     * @return
     */
    @Override
    public HashMap<String, Object> findAll(Integer start, Integer size, String userSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(userSearchKey);
        String searchKey = (String) jsonObject.get("userSearchKey");
        List<UserDTO> userDTOS = findUserByUserName(searchKey);
        Integer userCount = userDTOS.size();
        List<UserDTO> userDTOList = userDTOS.stream().limit(size).collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userDTOList);
        map.put("total", userCount);
        return map;
    }

    /**
     * 禁言用户
     *
     * @param userId
     * @param status
     * @return
     */
    @Override
    public HashMap<String, Object> updateUserStatus(Integer userId, Integer status) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", userId);
        User user = userMapper.selectOne(queryWrapper);
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", userId).set("status", status);
        int update = userMapper.update(user, updateWrapper);
        HashMap<String, Object> map = new HashMap<>();
        if (update==1){
            map.put("statusCode", StatusEnum.USER_MODIFY_STATUS_SUCCESS.getStatusCode());
        }else {
            map.put("statusCode", StatusEnum.USER_MODIFY_STATUS_ERROR.getStatusCode());
        }
        return map;
    }

    /**
     * 分页查找全部用户
     *
     * @param start
     * @param size
     * @param userSearchKey
     * @return
     */
    @Override
    public HashMap<String, Object> findAllLimit(Integer start, Integer size, String userSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(userSearchKey);
        String searchKey = (String) jsonObject.get("userSearchKey");
        List<User> users=userMapper.findAll(start,size,searchKey);
        Integer userCount = userMapper.selectCount(null);
        List<UserDTO> userDTOS = users.stream().map(user -> {
            UserDTO userDTO = Convert.convert(UserDTO.class, user);
            QueryWrapper<UserCommentCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userDTO.getId());
            Integer volCount = userCommentCheckMapper.selectCount(queryWrapper);
            userDTO.setVioNum(volCount);
            return userDTO;
        }).collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userDTOS);
        map.put("total", userCount);
        return map;
    }
}
