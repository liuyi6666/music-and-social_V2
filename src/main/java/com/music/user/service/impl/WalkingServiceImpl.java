package com.music.user.service.impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.WalkingImgMapper;
import com.music.pojo.entity.WalkingImg;
import com.music.user.service.WalkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class WalkingServiceImpl implements WalkingService {

    @Autowired
    private WalkingImgMapper walkingImgMapper;

    @Override
    public HashMap<String, Object> getHeaderImg(String status) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.IMAGE_SUCCESS.getStatusCode());
        Integer status1=null;
        if (!"null".equals(status)){
            status1 = Integer.parseInt(status);
        }
        List<WalkingImg> walkingImgList = walkingImgMapper.findAllByStatus(status1);
        map.put("imgList", walkingImgList);
        return map;
    }

    /**
     * 删除Banner
     *
     * @param id
     * @return
     */
    @Override
    public HashMap<String, Object> deleteBanner(Integer id) {
        int i = walkingImgMapper.deleteById(id);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", i == 1 ? StatusEnum.IMAGE_DELETE_SUCCESS.getStatusCode() : StatusEnum.IMAGE_DELETE_ERROR.getStatusCode());
        return map;
    }

    /**
     * 增加Banner
     *
     * @param data
     * @return
     */
    @Override
    public HashMap<String, Object> addBanner(String data) {
        JSONObject jsonObject = JSONUtil.parseObj(data);
        String imgUrl =(String) jsonObject.get("imgUrl");
        String status =(String) jsonObject.get("status");
        WalkingImg walkingImg = new WalkingImg();
        walkingImg.setImgUrl(imgUrl).setStatus(Integer.parseInt(status));
        int insert = walkingImgMapper.insert(walkingImg);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", insert == 1 ? StatusEnum.UPLOAD_SUCCESS.getStatusCode() : StatusEnum.UPLOAD_ERROR.getStatusCode());
        return map;
    }
}
