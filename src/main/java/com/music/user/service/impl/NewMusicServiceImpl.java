package com.music.user.service.impl;

import com.music.mapper.NewMusicMapper;
import com.music.pojo.entity.NewMusic;
import com.music.user.service.NewMusicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class NewMusicServiceImpl implements NewMusicService {
    @Autowired
    private NewMusicMapper newMusicMapper;

    /**
     * 根据hash和albumId获取新歌
     *
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public NewMusic getNewMusicByHashAndAlbumId(String hash, String albumId) {
        return newMusicMapper.getNewMusicByHashAndAlbumId(hash,albumId);
    }

    /**
     * 查找歌曲
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<NewMusic> findNewMusicByFileName(String searchKey) {
        return newMusicMapper.findNewMusicByFileName(searchKey);
    }
}
