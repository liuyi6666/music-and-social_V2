package com.music.user.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.music.mapper.AllMusicListMapper;
import com.music.pojo.dto.AllListDTO;
import com.music.pojo.entity.AllList;
import com.music.user.service.AllListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class AllListServiceImpl implements AllListService {

    @Autowired
    private AllMusicListMapper allMusicListMapper;
    /**
     * 从本地查询全部排行榜
     *
     * @return
     */
    @Override
    @Transactional
    public HashMap<String, Object> getAllListV2() {
        HashMap<String, Object> map = new HashMap<>();
        List<AllList> allMusicListMapperAll = allMusicListMapper.findAll();
        ArrayList<AllListDTO> allListDTOArrayList = new ArrayList<>();
        for (AllList allList : allMusicListMapperAll) {
            AllListDTO allListDTO = Convert.convert(AllListDTO.class, allList);
            allListDTO.setAddTime(allList.getUpdateTime());
            allListDTOArrayList.add(allListDTO);
        }
        map.put("info", allListDTOArrayList);
        log.info("getAllListV2 map={}",map);
        return map;
    }

}
