package com.music.user.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.gson.JsonObject;
import com.music.enums.StatusEnum;
import com.music.mapper.CommonMusicMapper;
import com.music.mapper.SingerMapper;
import com.music.pojo.api.KugouMusic;
import com.music.pojo.api.KugouMusicSearch;
import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.entity.*;
import com.music.user.service.CommonMusicService;
import com.music.user.service.SingerAlbumMusicService;
import com.music.user.service.UserLikeMusicService;
import com.music.user.service.UserMusicHistoryService;
import com.music.util.KuGouUtil;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommonMusicServiceImpl implements CommonMusicService {

    @Autowired
    private CommonMusicMapper commonMusicMapper;

    @Autowired
    private SingerMapper singerMapper;

    @Autowired
    private SingerAlbumMusicService singerAlbumMusicService;

    @Autowired
    private UserLikeMusicService userLikeMusicService;

    @Autowired
    private UserMusicHistoryService userMusicHistoryService;

    /**
     * 保存公共音乐
     *
     * @param hash
     * @param albumId
     */
    @Transactional
    @Override
    @Async
    public void saveMusicByHashAndAlbumId(String hash, String albumId) {
        log.info("saveMusicByHashAndAlbumId hash={},albumId={}", hash, albumId);
        if (null != commonMusicMapper.getCommonMusicByHashAndAlbumId(hash, albumId)) {
            return;
        }
        String url = HttpUtil.get("https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=" + hash +
                "&dfid=4FIdx50ybr9u3A2S9v3w2yis" +
                "&mid=26e5d2cedec54971335875cb02f62e89" +
                "&platid=4" +
                "&appid=1014" +
                "&album_id=" + albumId +
                "_=" + System.currentTimeMillis());
        System.out.println("访问链接：" + url);
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        if ("Error request, response status: 609".equals(url)) {
            System.out.println("=======================================================");
            System.out.println("======================酷狗链接崩溃======================");
            System.out.println("=======================================================");
            return;
        }
        KugouMusic kugouMusic;
        try {
            kugouMusic = JSON.parseObject(url, KugouMusic.class);
        } catch (Exception e) {
            System.out.println("酷狗链接报错=====hash=" + hash + ",albumId=" + albumId);
            System.out.println("错误链接信息=====" + url);
            e.printStackTrace();
            return;
        }
        CommonMusic commonMusic = new CommonMusic();
        paramAssemble(commonMusic, kugouMusic);
        commonMusic.setUpdateTime(new Date());
        commonMusic.setCreateTime(new Date());
        commonMusicMapper.insert(commonMusic);
    }

    /**
     * 根据hash和albumId查找音乐
     *
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public CommonMusic getCommonMusicByHashAndAlbumId(String hash, String albumId) {
//        saveMusicByHashAndAlbumId(hash, albumId);
        return commonMusicMapper.getCommonMusicByHashAndAlbumId(hash, albumId);
    }

    /**
     * 根据歌手id获取歌曲
     *
     * @param id
     * @return
     */
    @Override
    public List<CommonMusicDTO> findSingerMusicByAuthorId(Integer id, String token) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Singer singer = singerMapper.selectById(id);
        List<CommonMusic> commonMusicList = commonMusicMapper.findCommonMusicByAuthorId(singer.getSingerId());
        List<CommonMusicDTO> commonMusicDTOList = new ArrayList<>();
        UserLikeMusic userLikeMusic;
        for (CommonMusic commonMusic : commonMusicList) {
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            userLikeMusic = userLikeMusicService
                    .getLikeMusicByUserIdAndHashAndAlbumId(userId, commonMusicDTO.getHash(), commonMusicDTO.getAlbumId());
            commonMusicDTO.setLikeFlag(userLikeMusic == null ? Integer.valueOf(0) : userLikeMusic.getStatus());
            commonMusicDTOList.add(commonMusicDTO);
        }
        log.info("findSingerMusicByAuthorId commonMusicList={}", commonMusicList);
        return commonMusicDTOList;
    }

    /**
     * 根据albumId获取歌曲,并把获取的歌曲导入common音乐表中
     *
     * @param albumId
     * @return
     */
    @Override
    public List<CommonMusicDTO> findCommonMusicByAlbumId(Integer albumId, String token) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<SingerAlbumMusic> singerAlbumMusicList = singerAlbumMusicService.findSingerAlbumMusicByAlbumId(albumId);
        for (SingerAlbumMusic singerAlbumMusic : singerAlbumMusicList) {
            saveMusicByHashAndAlbumId(singerAlbumMusic.getHash(), singerAlbumMusic.getAlbumId());
        }
        List<CommonMusic> commonMusicList = commonMusicMapper.findCommonMusicByAlbumId(albumId);
        List<CommonMusicDTO> commonMusicDTOList = new ArrayList<>();
        UserLikeMusic userLikeMusic;
        for (CommonMusic commonMusic : commonMusicList) {
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            userLikeMusic = userLikeMusicService
                    .getLikeMusicByUserIdAndHashAndAlbumId(userId, commonMusicDTO.getHash(), commonMusicDTO.getAlbumId());
            if (userLikeMusic == null) {
                commonMusicDTO.setLikeFlag(0);
            } else {
                commonMusicDTO.setLikeFlag(userLikeMusic.getStatus());
            }
            commonMusicDTOList.add(commonMusicDTO);
        }
        log.info("findCommonMusicByAlbumId commonMusicList={}", commonMusicList);
        return commonMusicDTOList;
    }

    /**
     * 直接调用酷狗api,并更新本地音乐信息
     *
     * @return
     */
    @Override
    public CommonMusic playMusic(String hash, String albumId) {
        String url = HttpUtil.get("https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=" + hash + "&dfid=dfid&mid=mid&platid=4&album_id=" + albumId);
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        KugouMusic kugouMusic = JSON.parseObject(url, KugouMusic.class);
        if (null == kugouMusic) {
            return null;
        }
        CommonMusic commonMusic = new CommonMusic();
        commonMusic.setAlbumId(kugouMusic.getData().getAlbum_id())
                .setAlbumName(kugouMusic.getData().getAlbum_name())
                .setAudioId(kugouMusic.getData().getAudio_id())
                .setAudioName(kugouMusic.getData().getAudio_name())
                .setAuthorId(kugouMusic.getData().getAuthor_id())
                .setAuthorName(kugouMusic.getData().getAuthor_name())
                .setFileSize(kugouMusic.getData().getFilesize())
                .setHash(kugouMusic.getData().getHash())
                .setImg(kugouMusic.getData().getImg())
                .setLyrics(kugouMusic.getData().getLyrics())
                .setPlayUrl(kugouMusic.getData().getPlay_url())
                .setPlayBackupUrl(kugouMusic.getData().getPlay_backup_url())
                .setSongName(kugouMusic.getData().getSong_name())
                .setTimeLength(kugouMusic.getData().getTimelength());
        if (null != commonMusicMapper.getCommonMusicByHashAndAlbumId(hash, albumId)) {
            UpdateWrapper<CommonMusic> updateWrapper = new UpdateWrapper<>();
            commonMusic.setUpdateTime(new Date());
            updateWrapper.eq("hash", commonMusic.getHash())
                    .eq("album_id", commonMusic.getAlbumId());
            commonMusicMapper.update(commonMusic, updateWrapper);
            updateWrapper.clear();
        } else {
            commonMusic.setCreateTime(new Date()).setUpdateTime(new Date());
            commonMusicMapper.insert(commonMusic);
        }
        return commonMusic;
    }

    /**
     * 查询本地库中的音乐
     *
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public CommonMusicDTO playMusicV2(String hash, String albumId, String token) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonMusic commonMusic = updateCommonMusic(hash, albumId);
        //播放一次播放次数+1
        CommonMusic music = commonMusicMapper.getCommonMusicByHashAndAlbumId(hash, albumId);
        music.setPlayCount(music.getPlayCount() == null ? 1 : music.getPlayCount() + 1);
        UpdateWrapper<CommonMusic> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("hash", hash).eq("album_id", albumId);
        commonMusicMapper.update(music, updateWrapper);
        updateWrapper.clear();
        CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
        UserLikeMusic userLikeMusic = userLikeMusicService
                .getLikeMusicByUserIdAndHashAndAlbumId(userId, commonMusicDTO.getHash(), commonMusicDTO.getAlbumId());
        commonMusicDTO.setLikeFlag(userLikeMusic == null ? Integer.valueOf(0) : userLikeMusic.getStatus());
        if (userId != null) {
            userMusicHistoryService.addUserMusicHistoryV2(userId, hash, albumId);
        }
        return commonMusicDTO;
    }

    /**
     * 参数组装
     *
     * @param commonMusic
     * @param kugouMusic
     */
    private void paramAssemble(CommonMusic commonMusic, KugouMusic kugouMusic) {
        commonMusic.setAlbumId(kugouMusic.getData().getAlbum_id());
        commonMusic.setAlbumName(kugouMusic.getData().getAlbum_name());
        commonMusic.setAudioId(kugouMusic.getData().getAudio_id());
        commonMusic.setAudioName(kugouMusic.getData().getAudio_name());
        commonMusic.setAuthorId(kugouMusic.getData().getAuthor_id());
        commonMusic.setAuthorName(kugouMusic.getData().getAuthor_name());
        commonMusic.setFileSize(kugouMusic.getData().getFilesize());
        commonMusic.setHash(kugouMusic.getData().getHash());
        commonMusic.setLyrics(kugouMusic.getData().getLyrics());
        commonMusic.setImg(kugouMusic.getData().getImg());
        commonMusic.setPlayBackupUrl(kugouMusic.getData().getPlay_backup_url());
        commonMusic.setPlayUrl(kugouMusic.getData().getPlay_url());
        commonMusic.setSongName(kugouMusic.getData().getSong_name());
        commonMusic.setTimeLength(kugouMusic.getData().getTimelength());
    }

    /**
     * 酷狗链接失效，在原有链接后面增加时间戳
     *
     * @param hash
     * @param albumId
     * @return
     */
    public CommonMusic updateCommonMusic(String hash, String albumId) {
        String url = HttpUtil.get("https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=" + hash +
                "&dfid=4FIdx50ybr9u3A2S9v3w2yis" +
                "&mid=26e5d2cedec54971335875cb02f62e89" +
                "&platid=4" +
                "&appid=1014" +
                "&album_id=" + albumId +
                "&_=" + System.currentTimeMillis() + "");
        KugouMusic kugouMusic = JSON.parseObject(url, KugouMusic.class);
        System.out.println(JSONUtil.toJsonStr(kugouMusic));
        QueryWrapper<CommonMusic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("hash", hash).eq("album_id", albumId);
        CommonMusic commonMusic1 = commonMusicMapper.selectOne(queryWrapper);
        if (commonMusic1 == null) {
            commonMusic1 = new CommonMusic();
            paramAssemble(commonMusic1, kugouMusic);
            commonMusicMapper.insert(commonMusic1);
            return commonMusic1;
        } else {
            CommonMusic commonMusic = new CommonMusic();
            paramAssemble(commonMusic, kugouMusic);
            UpdateWrapper<CommonMusic> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("hash", hash).eq("album_id", albumId);
            commonMusicMapper.update(commonMusic, updateWrapper);
            updateWrapper.clear();
            return commonMusic;
        }
    }


    /**
     * 根据id查找music
     *
     * @param ids
     * @return
     */
    @Override
    public CommonMusic findCommonMusicById(Integer ids) {
        return commonMusicMapper.selectById(ids);
    }

    /**
     * 根据歌曲名称搜索歌曲
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<CommonMusic> findCommonMusicBySongName(String searchKey) {
        return commonMusicMapper.findCommonMusicBySongName(searchKey);
    }

    /**
     * 获取全部音乐,加入搜索
     *
     * @return
     */
    @Override
    public HashMap<String, Object> findAll(Integer start, Integer size, String musicSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(musicSearchKey);
        String searchKey = (String) jsonObject.get("musicSearchKey");
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", commonMusicMapper.findAll(start, size, searchKey));
        map.put("total", commonMusicMapper.countCommonMusic(searchKey));
        return map;
    }

    /**
     * 将搜索到的歌曲导入到数据库
     *
     * @param musicList
     * @return
     */
    @Override
    public HashMap<String, Object> importMusic(String musicList) {
        JSONObject jsonObject = JSONUtil.parseObj(musicList);
        String musicJson = jsonObject.get("musicList").toString();
        List<KugouMusicSearch.DataBean.InfoBean> infoBeans = JSON.parseArray(musicJson, KugouMusicSearch.DataBean.InfoBean.class);
        List<CommonMusic> commonMusics = infoBeans.stream().map(onlineMusic -> {
            CommonMusic commonMusic = new CommonMusic();
            commonMusic.setHash(onlineMusic.getHash())
                    .setAlbumId(onlineMusic.getAlbum_id());
            return commonMusic;
        }).collect(Collectors.toList());
        commonMusics.forEach(commonMusic -> {
            updateCommonMusic(commonMusic.getHash(), commonMusic.getAlbumId());
        });
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.MUSIC_IMPORT_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 删除音乐（物理删除）
     *
     * @param id
     * @return
     */
    @Override
    public HashMap<String, Object> deleteMusic(Integer id) {
        int i = commonMusicMapper.deleteById(id);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", i == 1 ? StatusEnum.MUSIC_DELETE_SUCCESS.getStatusCode() : StatusEnum.MUSIC_DELETE_ERROR.getStatusCode());
        return map;
    }


}
