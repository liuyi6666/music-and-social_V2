package com.music.user.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.dto.SimpleMusicDTO;
import com.music.pojo.dto.SingerDTO;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.entity.*;
import com.music.user.service.*;
import com.music.util.KuGouUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private UserService userService;

    @Autowired
    private MusicSheetService musicSheetService;

    /**
     * 搜索歌曲
     *
     * @param searchKey
     * @return
     */
    @Override
    public HashMap<String, Object> searchMusic(String searchKey) {
        HashMap<String, Object> map = new HashMap<>();
        List<CommonMusic> commonMusics = commonMusicService.findCommonMusicBySongName(searchKey);
        List<CommonMusicDTO> commonMusicDTOS = commonMusics.stream().map(commonMusic -> {
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            return commonMusicDTO;
        }).collect(Collectors.toList());
        map.put("info", commonMusicDTOS);
        return map;
    }

    /**
     * 查找歌手
     *
     * @param searchKey
     * @return
     */
    @Override
    public HashMap<String, Object> searchSinger(String searchKey) {
        List<SingerDTO> singerDetails=singerService.findSingerDetailBySingerName(searchKey);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", singerDetails);
        return map;
    }

    /**
     * 查找用户
     *
     * @param searchKey
     * @return
     */
    @Override
    public HashMap<String, Object> searchUser(String searchKey) {
        List<UserDTO> userDTOS=userService.findUserByUserName(searchKey);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userDTOS);
        return map;
    }

    /**
     * 查找歌单
     *
     * @param searchKey
     * @return
     */
    @Override
    public HashMap<String, Object> searchSheet(String searchKey) {
        List<MusicSheet> musicSheets=musicSheetService.findMusicSheetBySpecialName(searchKey);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", musicSheets);
        return map;
    }
}
