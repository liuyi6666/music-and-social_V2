package com.music.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.music.mapper.MusicSingerDetailMapper;
import com.music.mapper.SingerMapper;
import com.music.pojo.dto.RankingListMusicDTO;
import com.music.pojo.dto.SingerDTO;
import com.music.pojo.entity.MusicSinger;
import com.music.pojo.entity.MusicSingerDetail;
import com.music.pojo.entity.Singer;
import com.music.user.service.SingerService;
import com.music.util.GeneralUtil;
import com.music.vo.PageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SingerServiceImpl implements SingerService {

    @Autowired
    private SingerMapper singerMapper;

    @Autowired
    private MusicSingerDetailMapper musicSingerDetailMapper;


    /**
     * 按热度查找前十
     *
     * @return
     */
    @Override
    public List<Singer> findSingerByHeatDesc() {
        return singerMapper.findSingerByHeatDesc();
    }

    /**
     * 根据歌手类别查找
     *
     * @param type
     * @return
     */
    @Override
    public List<Singer> findSingerByType(Integer type) {
        return singerMapper.findSingerByType(type);
    }

    /**
     * 测试分页
     *
     * @return
     */
    @Override
    public List<Singer> findSingerTest() {
        return singerMapper.findAll();
    }


    /**
     * 根据歌手类型和首字母查歌手
     *
     * @param singerType
     * @param singerNameSpell
     * @return
     */
    @Override
    public PageVO<Singer> findSingerByTypeAndLimitAndSingerName(String singerType,
                                                                String singerNameSpell,
                                                                Integer singerCurrentPage,
                                                                Integer singerPageSize) {
        log.info("findSingerByTypeAndLimitAndSingerName singerType={},singerNameSpell={},singerCurrentPage={},singerPageSize={}", singerType, singerNameSpell, singerCurrentPage, singerPageSize);
        String[] split = singerType.split("-");
        Integer sexType = Integer.parseInt(split[0]);
        Integer type = Integer.parseInt(split[1]);
        sexType = 0 == sexType ? null : sexType;
        type = 0 == type ? null : type;


        //需要在查询条件之前执行
        List<Singer> singerList = null;
        singerList = singerMapper.findSingerByTypeAndSexType(type, sexType);

        if (!"all".equals(singerNameSpell)) {
            //根据singerList查询出来的结果进行筛选利用首字母筛选
            assert singerList != null;
            singerList = singerList.stream().filter(singer -> {
                String singerName = singer.getSingerName();
                if (GeneralUtil.isChinese(singerName)) {
                    String spells = GeneralUtil.getSpells(singerName);
                    Character firstLetter = GeneralUtil.getFirstLetter(spells.charAt(0));
                    assert firstLetter != null;
                    return firstLetter.toString().equals(singerNameSpell);
                } else if (GeneralUtil.isEnglish(singerName)) {
                    char firstLetter = singerName.charAt(0);
                    return Character.toString(firstLetter).equals(singerNameSpell);
                } else {
                    return false;
                }
            }).collect(Collectors.toList());
            System.out.println("筛选后的：" + singerList);
        }

        //singerList是全部的数据需要对全部数据分页返回给前端
        PageVO<Singer> singerPageVO = new PageVO<>();
        singerPageVO.setDataList(singerList);
        singerPageVO.setPageTotal(singerList.size());
        log.info("findSingerByTypeAndLimitAndSingerName singerPageVO={}", JSON.toJSONString(singerPageVO));
        return singerPageVO;
    }

    /**
     * 根据主键Id查出歌手全部信息和详细信息
     *
     * @param id
     * @return
     */
    @Override
    public SingerDTO getSingerById(Integer id) {
        Singer singer = singerMapper.selectById(id);
        MusicSingerDetail singerDetail = musicSingerDetailMapper.getMusicSingerDetailBySingerId(singer.getSingerId());
        SingerDTO singerDTO = Convert.convert(SingerDTO.class, singer);
        singerDTO.setIntro(singerDetail.getIntro());
        singerDTO.setMvCount(singerDetail.getMvCount());
        singerDTO.setAlbumCount(singerDetail.getAlbumCount());
        singerDTO.setSongCount(singerDetail.getSongCount());
        return singerDTO;
    }

    /**
     * 根据歌手id获取歌手
     *
     * @param singerId
     * @return
     */
    @Override
    public SingerDTO getSingerBySingerId(Integer singerId) {
        Singer singer = singerMapper.getSingerBySingerId(singerId);
        SingerDTO singerDTO = Convert.convert(SingerDTO.class, singer);
        if (!Objects.isNull(singer)) {
            MusicSingerDetail singerDetail = musicSingerDetailMapper.getMusicSingerDetailBySingerId(singerId);
            singerDTO.setIntro(singerDetail.getIntro())
                    .setSongCount(singerDetail.getSongCount())
                    .setAlbumCount(singerDetail.getAlbumCount())
                    .setMvCount(singerDetail.getMvCount());
        }
        return singerDTO;
    }

    /**
     * 查找歌手信息
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<SingerDTO> findSingerDetailBySingerName(String searchKey) {
        List<Singer> singers = singerMapper.findSingerBySingerName(searchKey);
        List<SingerDTO> singerDTOS = Convert.convert(new TypeReference<List<SingerDTO>>() {
        }, singers);
        if (Objects.nonNull(singers)) {
            singerDTOS = singerDTOS.stream().peek(singerDTO -> {
                MusicSingerDetail singerDetail = musicSingerDetailMapper.getMusicSingerDetailBySingerId(singerDTO.getSingerId());
                singerDTO.setIntro(singerDetail.getIntro())
                        .setSongCount(singerDetail.getSongCount())
                        .setAlbumCount(singerDetail.getAlbumCount())
                        .setMvCount(singerDetail.getMvCount());
            }).collect(Collectors.toList());
        }
        return singerDTOS;
    }

    /**
     * 查找歌手
     *
     * @param start
     * @param size
     * @param singerSearchKey
     * @return
     */
    @Override
    public HashMap<String, Object> findAll(Integer start, Integer size, String singerSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(singerSearchKey);
        String searchKey = (String) jsonObject.get("singerSearchKey");
        HashMap<String, Object> map = new HashMap<>();
        List<MusicSinger> singers = singerMapper.findAllLimit(start, size, searchKey);
        List<SingerDTO> singerDTOS = singers.stream().map(singer -> {
            MusicSingerDetail detail = musicSingerDetailMapper.getMusicSingerDetailBySingerId(singer.getSingerId());
            SingerDTO singerDTO = Convert.convert(SingerDTO.class, singer);
            singerDTO.setIntro(detail.getIntro())
                    .setMvCount(detail.getMvCount())
                    .setAlbumCount(detail.getAlbumCount())
                    .setSongCount(detail.getSongCount());
            return singerDTO;
        }).collect(Collectors.toList());
        map.put("info",singerDTOS);
        map.put("total", singerMapper.countSinger(searchKey));
        return map;
    }

    /**
     * 获取相似歌手
     *
     * @param singerId
     * @return
     */
    @Override
    public HashMap<String, Object> getSimilarSinger(Integer id) {
        HashMap<String, Object> map = new HashMap<>();
        QueryWrapper<Singer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", id);
        Singer singer = singerMapper.selectOne(queryWrapper);
        queryWrapper.clear();
        queryWrapper.eq("sex_type", singer.getSexType()).eq("type", singer.getType());
        List<Singer> singers = singerMapper.selectList(queryWrapper);
        List<Singer> singerList = singers.stream().filter(singer1 -> !singer1.getSingerId().equals(singer.getSingerId())).collect(Collectors.toList());
        List<Singer> randomSingers = RandomUtil.randomEleList(singerList, 2);
        map.put("info", randomSingers);
        return map;
    }
}
