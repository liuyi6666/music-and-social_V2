package com.music.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.mapper.CommonMusicMapper;
import com.music.mapper.NewMusicMapper;
import com.music.mapper.RankingListMusicMapper;
import com.music.mapper.VolidMapper;
import com.music.pojo.api.*;
import com.music.pojo.dto.*;
import com.music.pojo.entity.*;
import com.music.user.service.*;

import com.music.util.KuGouUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 前端展示相应接口
 */
@Service
@Slf4j
public class KugouServiceImpl implements KugouService {

    @Autowired
    private VolidMapper volidMapper;

    @Autowired
    private RankingListMusicMapper rankingListMusicMapper;

    @Autowired
    private CommonMusicMapper commonMusicMapper;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private NewMusicMapper newMusicMapper;

    @Autowired
    private NewMusicService newMusicService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private RankingListMusicService rankingListMusicService;

    @Autowired
    private MusicSheetService musicSheetService;

    @Autowired
    private UserLikeMusicService userLikeMusicService;

    @Autowired
    private UserLikeSheetService userLikeSheetService;

    /**
     * 播放音乐，返回音乐信息
     *
     * @param album_id
     * @param hash
     * @return
     */
    @Override
    public HashMap<String, Object> playMusic(String album_id, String hash) {
        log.info("plagMusic album_id={} hash={}", album_id, hash);
        HashMap<String, Object> map = new HashMap<>();
        String kugouApi = HttpUtil.get("https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=" + hash + "&dfid=dfid&mid=mid&platid=4&album_id=" + album_id + "");
        if (kugouApi.equals("Error request, response status: 609")) {
            return null;
        }
        KugouMusic kugouMusic = JSON.parseObject(kugouApi, KugouMusic.class);
        map.put("info", kugouMusic);
        log.info("playMusic返回参数 info={}", JSON.toJSONString(kugouMusic));
        return map;
    }

    /**
     * 搜索音乐，返回搜索信息
     *
     * @param keyword
     * @return
     */
    @Override
    public HashMap<String, Object> searchMusic(String keyword) {
        HashMap<String, Object> map = new HashMap<>();
        String s = HttpUtil.get("https://songsearch.kugou.com/song_search_v2?keyword=" + keyword + "&page=1");
        KugouSearch kugouSearch = JSON.parseObject(s, KugouSearch.class);
        map.put("info", kugouSearch);
        return map;
    }

    /**
     * 获取音乐榜单
     * 获取榜单的前五个
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getMusicList() {
        HashMap<String, Object> map = new HashMap<>();
        String s = HttpUtil.get("http://m.kugou.com/plist/index&json=true");
        KugouMusicList kugouMusicList = JSON.parseObject(s, KugouMusicList.class);
        map.put("info", kugouMusicList.getPlist().getList().getInfo());
        log.info("getMusicList kugouMusicList={}", kugouMusicList.getPlist().getList().getInfo());
        return map;
    }

    /**
     * 首页获取歌单前五个
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getMusicSheetV2() {
        HashMap<String, Object> map = new HashMap<>();
        List<MusicSheet> musicSheetForIndexList = musicSheetService.findMusicSheetForIndex();
        List<MusicSheetDTO> musicSheetDTOList = new ArrayList<>();
        for (MusicSheet musicSheet : musicSheetForIndexList) {
            MusicSheetDTO musicSheetDTO = Convert.convert(MusicSheetDTO.class, musicSheet);
            musicSheetDTOList.add(musicSheetDTO);
        }
        map.put("info", musicSheetDTOList);
        return map;
    }

    /**
     * 获取Top500歌曲
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getHotMusic() {
        HashMap<String, Object> map = new HashMap<>();
        String s1 = HttpUtil.get("https://m.kugou.com/rank/info/?rankid=8888&page=1&json=true");
        String s2 = HttpUtil.get("https://m.kugou.com/rank/info/?rankid=6666&page=1&json=true");

        KugouMusicTop500 top500 = JSON.parseObject(s1, KugouMusicTop500.class);
        KugouMusicTop500 soar = JSON.parseObject(s2, KugouMusicTop500.class);
        int soarSize = soar.getSongs().getList().size();
        //将飙升榜和top500拼在一起，因为数量不够，酷狗api原因
        for (int i = 0; i < soarSize; i++) {
            top500.getSongs().getList().add(soar.getSongs().getList().get(i));
        }
        List<KugouMusicTop500.SongsBean.ListBean> listBeans = top500.getSongs().getList();
        List<KugouMusicTop500.SongsBean.ListBean> listBeans1 = listBeans.stream()
                .limit(10)
                .collect(Collectors.toList());
        top500.getSongs().setList(listBeans1);
        map.put("info", top500);
        log.info("getHotMusic返回参数 top500={}", JSON.toJSONString(top500));
        return map;
    }


    /**
     * 获取新歌 （慢接口）
     *
     * @param musicType
     * @return
     */
    @Override
    public HashMap<String, Object> getNewMusic(Integer musicType) {
        log.info("getNewMusic musicType={}", musicType);
        HashMap<String, Object> map = new HashMap<>();
        String url = "";
        if (musicType.equals(1)) {
            //国产
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/newsong?version=9108&plat=0&with_cover=1&pagesize=100&type=1&area_code=1&page=1&with_res_tag=1");

        } else if (musicType.equals(2)) {
            //欧美
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/newsong?version=9108&plat=0&with_cover=1&pagesize=100&type=2&area_code=1&page=1&with_res_tag=1");

        } else if (musicType.equals(3)) {
            //日韩
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/newsong?version=9108&plat=0&with_cover=1&pagesize=100&type=3&area_code=1&page=1&with_res_tag=1");

        } else {
            return map;
        }
        //解决酷狗对json的限制
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        KugouNewMusic kugouNewMusic = JSON.parseObject(url, KugouNewMusic.class);
        List<NewMusicDTO> musicDTOSList = new ArrayList<>();
        List<KugouNewMusic.DataBean.InfoBean> infoBeanList = kugouNewMusic.getData().getInfo();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < infoBeanList.size(); i++) {
            musicDTOSList.add(new NewMusicDTO());
            musicDTOSList.get(i)
                    .setSortNum(i + 1)
                    .setAlbumId(infoBeanList.get(i).getAlbum_id())
                    .setFileName(infoBeanList.get(i).getFilename())
                    .setHash(infoBeanList.get(i).getHash())
                    .setRemark(infoBeanList.get(i).getRemark())
                    .setAddTime(infoBeanList.get(i).getAddtime())
                    .setAudioId(infoBeanList.get(i).getAlbum_audio_id())
                    .setAlbumCover(infoBeanList.get(i).getAlbum_cover())
                    .setFileSize(infoBeanList.get(i).getFilesize());
            KugouMusic kugouMusic = playMusic1(musicDTOSList.get(i).getAlbumId(), musicDTOSList.get(i).getHash());
            if (ObjectUtil.isNull(kugouMusic)) {
                musicDTOSList.get(i).setTimeLength("0:00");
            } else {
                musicDTOSList.get(i).setTimeLength(KuGouUtil.swapTime(kugouMusic.getData().getTimelength()));
            }
        }
        map.put("info", musicDTOSList);
        long endTime = System.currentTimeMillis();
        System.out.println("运行时间：" + (endTime - startTime));
        log.info("getNewMusic返回参数 musicDTOSList={}", JSON.toJSONString(musicDTOSList));
        return map;
    }

    private KugouMusic playMusic1(String album_id, String hash) {
        String kugouApi = HttpUtil.get("https://wwwapi.kugou.com/yy/index.php?r=play/getdata&hash=" + hash + "&dfid=dfid&mid=mid&platid=4&album_id=" + album_id + "");
        if (kugouApi.equals("Error request, response status: 609")) {
            return null;
        }
        return JSON.parseObject(kugouApi, KugouMusic.class);

    }


    /**
     * 获取歌手，只取15条，将男女歌手整合
     *
     * @param singerType
     * @return
     */
    @Override
    public HashMap<String, Object> getSinger(Integer singerType) {
        log.info("getSinger singerType={}", singerType);
        HashMap<String, Object> map = new HashMap<>();
        String url = "";
        String url_other = "";
        if (singerType.equals(1)) {
            //热门
            url = HttpUtil.get("http://m.kugou.com/singer/list/88?json=true");
        } else if (singerType.equals(2)) {
            //华语
            url = HttpUtil.get("http://m.kugou.com/singer/list/1?json=true");
            url_other = HttpUtil.get("http://m.kugou.com/singer/list/2?json=true");
        } else if (singerType.equals(3)) {
            //欧美
            url = HttpUtil.get("http://m.kugou.com/singer/list/7?json=true");
            url_other = HttpUtil.get("http://m.kugou.com/singer/list/8?json=true");
        } else if (singerType.equals(4)) {
            //日韩
            url = HttpUtil.get("http://m.kugou.com/singer/list/4?json=true");
            url_other = HttpUtil.get("http://m.kugou.com/singer/list/5?json=true");
        } else {
            return map;
        }
        KugouSinger kugouSingerMan = JSON.parseObject(url, KugouSinger.class);
        List<Singer> singerDTOSList = new ArrayList<>();
        //男
        for (int i = 0; i < 10; i++) {
            singerDTOSList.add(new Singer());
            singerDTOSList.get(i)
                    .setSingerName(kugouSingerMan.getSingers().getList().getInfo().get(i).getSingername())
                    .setImgUrl((kugouSingerMan.getSingers().getList().getInfo().get(i).getImgurl()).replaceAll("\\{size}/", ""))
                    .setFansCount(kugouSingerMan.getSingers().getList().getInfo().get(i).getFanscount());
        }
        //女
        if (singerType != 1) {
            KugouSinger kugouSingerWoman = JSON.parseObject(url_other, KugouSinger.class);
            for (int i = 10; i < 20; i++) {
                singerDTOSList.add(new Singer());
                singerDTOSList.get(i)
                        .setSingerName(kugouSingerWoman.getSingers().getList().getInfo().get(i - 10).getSingername())
                        .setImgUrl((kugouSingerWoman.getSingers().getList().getInfo().get(i - 10).getImgurl()).replaceAll("\\{size}/", ""))
                        .setFansCount(kugouSingerWoman.getSingers().getList().getInfo().get(i - 10).getFanscount());
            }
        }
        map.put("info", singerDTOSList);
        log.info("getSinger返回参数 singerDTOSList={}", JSON.toJSONString(singerDTOSList));
        return map;

    }

    /**
     * 获取指定类型的音乐排行榜（慢接口）获取前50条
     *
     * @param rankId
     * @return
     */
    @Override
    public HashMap<String, Object> getMusicListForType(Integer rankId) {
        log.info("getMusicListForType rankId={}", rankId);
        Volid volidDTO = getVolid(rankId);
        Integer volid = volidDTO.getVolid();
        HashMap<String, Object> map = new HashMap<>();
        String s = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/song?pagesize=50&page=1&volid=" + volid + "&rankid=" + rankId + "&with_res_tag=1");
        s = KuGouUtil.relieveKuGouUrlLimit(s);
        KugouMusicListForRandId kugouMusicListForRandId = JSON.parseObject(s, KugouMusicListForRandId.class);
        List<MusicListForRandIdDTO> musicListForRandIdDTOS = new ArrayList<>();
        for (int i = 0; i < kugouMusicListForRandId.getData().getInfo().size(); i++) {
            musicListForRandIdDTOS.add(new MusicListForRandIdDTO());
            musicListForRandIdDTOS.get(i)
                    .setAddtime(kugouMusicListForRandId.getData().getInfo().get(i).getAddtime())
                    .setAlbum_id(kugouMusicListForRandId.getData().getInfo().get(i).getAlbum_id())
                    .setFilename(kugouMusicListForRandId.getData().getInfo().get(i).getFilename())
                    .setHash(kugouMusicListForRandId.getData().getInfo().get(i).getHash())
                    .setRemark(kugouMusicListForRandId.getData().getInfo().get(i).getRemark())
                    .setRank_cid(kugouMusicListForRandId.getData().getInfo().get(i).getRank_cid());
            KugouMusic kugouMusic = playMusic1(kugouMusicListForRandId.getData().getInfo().get(i).getAlbum_id(),
                    kugouMusicListForRandId.getData().getInfo().get(i).getHash());
            if (Objects.isNull(kugouMusic)) {
                musicListForRandIdDTOS.get(i).setTimeLength("0:00");
            } else {
                musicListForRandIdDTOS.get(i).setTimeLength(KuGouUtil.swapTime(kugouMusic.getData().getTimelength()));
            }
        }
        map.put("info", musicListForRandIdDTOS);
        log.info("getMusicListForType出参 musicListForRandIdDTOS={}", musicListForRandIdDTOS);
        return map;
    }

    /**
     * 从本地数据库中查出每个排行榜前50条音乐数据
     *
     * @param rankId
     * @return
     */
    @Override
    public HashMap<String, Object> getMusicListForTypeV2(Integer rankId, String token) {
        HashMap<String, Object> map = new HashMap<>();
        List<RankingListMusic> musicList = rankingListMusicMapper.findMusicByRankId(rankId);
        for (RankingListMusic music : musicList) {
            CommonMusic commonMusic = commonMusicMapper.getCommonMusicByHashAndAlbumId(music.getHash(), music.getAlbumId());
            if (null != commonMusic) {
                music.setTimeLength(commonMusic.getTimeLength());
            }
        }
        List<RankingListMusicDTO> rankingListMusicDTOS = Convert.convert(new TypeReference<List<RankingListMusicDTO>>() {
        }, musicList);
        try {
            JWT jwt = JWTUtil.parseToken(token);
            Integer userId = (Integer) jwt.getPayload("userId");
            for (RankingListMusicDTO rankingListMusicDTO : rankingListMusicDTOS) {
                UserLikeMusic userLikeMusic = userLikeMusicService
                        .getLikeMusicByUserIdAndHashAndAlbumId(userId, rankingListMusicDTO.getHash(), rankingListMusicDTO.getAlbumId());
                if (userLikeMusic == null) {
                    rankingListMusicDTO.setLikeFlag(0);
                } else {
                    rankingListMusicDTO.setLikeFlag(userLikeMusic.getStatus());
                }
            }
        } catch (Exception e) {
            log.warn("用户未登录，不返回用户喜欢");
            e.printStackTrace();
        }

        map.put("info", rankingListMusicDTOS);
        return map;
    }

    /**
     * 获取最新一期排行榜期数
     * 并存入数据库
     *
     * @return
     */
    public Volid getVolid(Integer rankId) {
        log.info("getVolid rankId={}", rankId);
        String s = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/vol?ranktype=2&plat=0&rankid=" + rankId + "&with_res_tag=1");
        s = KuGouUtil.relieveKuGouUrlLimit(s);
        KugouVolid kugouVolid = JSON.parseObject(s, KugouVolid.class);
        Volid volid = new Volid();
        volid.setVolid(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVolid());
        volid.setVolName(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVolname());
        volid.setRank_cid(kugouVolid.getData().getRank_cid());
        volid.setRankId(rankId);
        volid.setVolTitle(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVoltitle());
        volid.setYear(kugouVolid.getData().getInfo().get(0).getYear());
        UpdateWrapper<Volid> updateWrapper = new UpdateWrapper<>();
        if (CollUtil.isNotEmpty(volidMapper.findVolid(volid.getVolid()))) {
            volid.setUpdateTime(new Date());
            updateWrapper.eq("volid", volid.getVolid());
            volidMapper.update(volid, updateWrapper);
            updateWrapper.clear();
        } else {
            volid.setUpdateTime(new Date());
            volid.setCreateTime(new Date());
            volidMapper.insert(volid);
        }
        log.info("getVolid出参 volidDTO={}", volid);
        return volid;
    }

    /**
     * 获取全部排行榜
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getAllList() {
        HashMap<String, Object> map = new HashMap<>();
        String s = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/list?version=9108&plat=0&showtype=2&parentid=0&apiver=6&area_code=1&withsong=1&with_res_tag=1");
        s = KuGouUtil.relieveKuGouUrlLimit(s);
        KugouAllList kugouAllList = JSON.parseObject(s, KugouAllList.class);
        List<AllList> allLists = new ArrayList<>();
        for (int i = 0; i < kugouAllList.getData().getInfo().size(); i++) {
            allLists.add(new AllList());
            allLists.get(i)
                    .setPlayTimes(kugouAllList.getData().getInfo().get(i).getPlay_times())
                    .setRankCid(kugouAllList.getData().getInfo().get(i).getRank_cid())
                    .setRankId(kugouAllList.getData().getInfo().get(i).getRankid())
                    .setRankName(kugouAllList.getData().getInfo().get(i).getRankname());
        }
        map.put("info", allLists);
        log.info("getAllList出参 allListDTOS={}", allLists);
        return map;
    }

    /**
     * 从本地读取新歌
     *
     * @param musicType
     * @return
     */
    @Override
    public HashMap<String, Object> getNewMusicV2(Integer musicType) {
        HashMap<String, Object> map = new HashMap<>();
        int sortNum = 1;
        List<NewMusic> newMusicByTypeList = newMusicMapper.findNewMusicByType(musicType);
        List<NewMusicDTO> newMusicDTOList = new ArrayList<>();
        for (NewMusic newMusic : newMusicByTypeList) {
            NewMusicDTO newMusicDTO = Convert.convert(NewMusicDTO.class, newMusic);
            CommonMusic commonMusic = commonMusicService.getCommonMusicByHashAndAlbumId(newMusicDTO.getHash(), newMusicDTO.getAlbumId());
            newMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            newMusicDTO.setSortNum(sortNum++);
            newMusicDTOList.add(newMusicDTO);
        }
        map.put("info", newMusicDTOList);
        return map;
    }

    /**
     * 本地获取歌手
     *
     * @param singerType
     * @return
     */
    @Override
    public HashMap<String, Object> getSingerV2(Integer singerType) {
        HashMap<String, Object> map = new HashMap<>();
        List<SingerDTO> singerDTOList = new ArrayList<>();
        if (1 == singerType) {
            List<Singer> singerByHeatDescList = singerService.findSingerByHeatDesc();
            for (Singer singer : singerByHeatDescList) {
                SingerDTO singerDTO = Convert.convert(SingerDTO.class, singer);
                singerDTOList.add(singerDTO);
            }
            map.put("info", singerDTOList);
            return map;
        }
        List<Singer> singerByTypeList = singerService.findSingerByType(singerType - 1);
        for (Singer singer : singerByTypeList) {
            SingerDTO singerDTO = Convert.convert(SingerDTO.class, singer);
            singerDTOList.add(singerDTO);
        }
        map.put("info", singerDTOList);
        return map;
    }

    /**
     * 获取播放量最多前十
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getHotMusicV2() {
        HashMap<String, Object> map = new HashMap<>();
        QueryWrapper<CommonMusic> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("play_count").last("limit 10");
        List<CommonMusic> commonMusics = commonMusicMapper.selectList(queryWrapper);
        List<CommonMusicDTO> commonMusicDTOS = commonMusics.stream().map(commonMusic -> {
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            return commonMusicDTO;
        }).collect(Collectors.toList());
        map.put("info", commonMusicDTOS);
        return map;
    }

    /**
     * 本地获取歌单信息
     * 根据type类型获取推荐、最近更新、播放最多的歌单
     *
     * @param type
     * @return
     */
    @Override
    public HashMap<String, Object> findMusicSheetByType(Integer type, String token) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, Object> map = new HashMap<>();
        List<MusicSheet> musicSheetByTypeList = musicSheetService.findMusicSheetByType(type);
        List<MusicSheetDTO> musicSheetDTOSList = new ArrayList<>();
        for (MusicSheet musicSheet : musicSheetByTypeList) {
            MusicSheetDTO musicSheetDTO = Convert.convert(MusicSheetDTO.class, musicSheet);
            UserLikeSheet userLikeSheet=userLikeSheetService
                    .getUserLikeSheetByUserIdAndSpecialId(userId,musicSheetDTO.getSpecialId());
            if (userLikeSheet == null){
                musicSheetDTO.setLikeFlag(0);
            }else {
                musicSheetDTO.setLikeFlag(userLikeSheet.getStatus());
            }
            musicSheetDTOSList.add(musicSheetDTO);
        }
        map.put("info", musicSheetDTOSList);
        return map;
    }
}
