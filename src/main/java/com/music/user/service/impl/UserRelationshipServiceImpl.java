package com.music.user.service.impl;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.UserMapper;
import com.music.mapper.UserRelationshipMapper;
import com.music.pojo.dto.ChatInfo;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.dto.UserRelationshipDTO;
import com.music.pojo.entity.User;
import com.music.pojo.entity.UserRelationship;
import com.music.user.service.RedisService;
import com.music.user.service.UserRelationshipService;
import com.music.user.service.UserService;
import com.music.util.GeneralUtil;
import com.music.util.MyJWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class UserRelationshipServiceImpl implements UserRelationshipService {

    @Autowired
    private UserRelationshipMapper userRelationshipMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    /**
     * 获取用户关系列表
     *
     * @param id 用户id
     * @return
     */
    @Override
    public HashMap<String, Object> getUserRelationshipById(Integer id, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_RELATION.getStatusCode());
        HashMap<String, Object> userRelationshipMap = getUserRelationship(id, userId);
        map.put("relationStatus", userRelationshipMap.get("relationStatus"));
        //获取关注用户和粉丝用户
        QueryWrapper<UserRelationship> relationshipQueryWrapper = new QueryWrapper<>();
        //我的关注
        relationshipQueryWrapper.eq("fan_id", id).eq("status", 1);
        List<UserRelationship> followUserList = userRelationshipMapper.selectList(relationshipQueryWrapper);
        List<UserDTO> followUserDTOList = followUserList.stream()
                .map(userRelation -> Convert.convert(UserDTO.class, userMapper.selectById(userRelation.getUserId())))
                .collect(Collectors.toList());
        relationshipQueryWrapper.clear();
        //我的粉丝
        relationshipQueryWrapper.eq("user_id", id).eq("status", 1);
        List<UserRelationship> fanUserList = userRelationshipMapper.selectList(relationshipQueryWrapper);
        List<UserDTO> fanUserDTOList = fanUserList.stream()
                .map(userRelation -> Convert.convert(UserDTO.class, userMapper.selectById(userRelation.getFanId())))
                .collect(Collectors.toList());
        map.put("followUser", followUserDTOList);
        map.put("fanUser", fanUserDTOList);
        return map;
    }


    /**
     * 获取用户关系
     *
     * @param toUserId
     * @param userId
     * @return
     */
    private HashMap<String, Object> getUserRelationship(Integer toUserId, Integer userId) {
        HashMap<String, Object> map = new HashMap<>();
        //是否关注
        QueryWrapper<UserRelationship> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", toUserId)
                .eq("fan_id", userId)
                .eq("status", 1);
        UserRelationship userRelationship = userRelationshipMapper.selectOne(queryWrapper);
        queryWrapper.clear();
        queryWrapper.eq("user_id", userId)
                .eq("fan_id", toUserId)
                .eq("status", 1);
        UserRelationship userRelationshipOther = userRelationshipMapper.selectOne(queryWrapper);
        if (!Objects.isNull(userRelationship) && !Objects.isNull(userRelationshipOther)) {
            map.put("relationStatus", 2);
        } else if (!Objects.isNull(userRelationship)) {
            map.put("relationStatus", 1);
        } else {
            map.put("relationStatus", 0);
        }
        return map;
    }

    /**
     * 加关注\或者取消关注
     *
     * @param id
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> updateUserRelationship(Integer id, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        QueryWrapper<UserRelationship> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", id)
                .eq("fan_id", userId);
        UserRelationship userRelationship = userRelationshipMapper.selectOne(queryWrapper);
        queryWrapper.clear();
        if (Objects.isNull(userRelationship)) {
            UserRelationship userRelationship1 = new UserRelationship();
            userRelationship1.setFanId(userId)
                    .setUserId(id)
                    .setStatus(1);
            userRelationshipMapper.insert(userRelationship1);
        } else {
            UpdateWrapper<UserRelationship> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", id)
                    .eq("fan_id", userId)
                    .set("status", userRelationship.getStatus() == 1 ? 0 : 1);
            userRelationshipMapper.update(userRelationship, updateWrapper);
            updateWrapper.clear();
        }
        HashMap<String, Object> userRelationshipById = getUserRelationship(id, userId);
        if (userRelationshipById.get("relationStatus").equals(2)) {
            String sessionMD5 = GeneralUtil.sessionMD5(userId, id);
            queryWrapper.clear();
            queryWrapper.eq("user_id", userId)
                    .eq("fan_id", id)
                    .eq("status", 1);
            UserRelationship relationship = userRelationshipMapper.selectOne(queryWrapper);
            UpdateWrapper<UserRelationship> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", userId)
                    .eq("fan_id", id)
                    .eq("status", 1)
                    .set("session_id", sessionMD5);
            userRelationshipMapper.update(relationship, updateWrapper);
            queryWrapper.clear();
            queryWrapper.eq("user_id", id)
                    .eq("fan_id", userId)
                    .eq("status", 1);
            relationship = userRelationshipMapper.selectOne(queryWrapper);
            updateWrapper.clear();
            updateWrapper.eq("user_id", id)
                    .eq("fan_id", userId)
                    .eq("status", 1)
                    .set("session_id", sessionMD5);
            userRelationshipMapper.update(relationship, updateWrapper);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 通过用户id获取用户关注用户
     *
     * @param nowUserId
     * @return
     */
    @Override
    public List<UserRelationship> getFollowUserByUserId(Integer nowUserId) {
        QueryWrapper<UserRelationship> relationshipQueryWrapper = new QueryWrapper<>();
        relationshipQueryWrapper.eq("fan_id", nowUserId).eq("status", 1);
        return userRelationshipMapper.selectList(relationshipQueryWrapper);
    }

    /**
     * 获取好友列表
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getUserRelationship(String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        QueryWrapper<UserRelationship> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("status", 1);
        List<UserRelationship> userRelationshipList = userRelationshipMapper.selectList(queryWrapper);
        queryWrapper.clear();
        queryWrapper.eq("fan_id", userId).eq("status", 1);
        List<UserRelationship> fanRelationshipList = userRelationshipMapper.selectList(queryWrapper);
        List<UserDTO> userDTOS = userRelationshipList.stream().map(userRelationship -> {
            for (UserRelationship relationship : fanRelationshipList) {
                if (userRelationship.getUserId().equals(relationship.getFanId()) && userRelationship.getFanId().equals(relationship.getUserId())) {
                    UserDTO userDTO = userService.getUserInfoById(userRelationship.getFanId());
                    userDTO.setCreateTime(userRelationship.getUpdateTime());
                    HashMap<String, Object> mutualAttentionChatRecord = redisService.getMutualAttentionChatRecord(userRelationship.getFanId(), authentication);
                    List<ChatInfo> info = (List<ChatInfo>) mutualAttentionChatRecord.get("info");
                    if (CollUtil.isNotEmpty(info)) {
                        //获取最后一条消息
                        String message = info.get(info.size() - 1).getMessage();
                        userDTO.setLastMessage(message);
                    }
                    return userDTO;
                }
            }
            return null;
        }).collect(Collectors.toList());
        userDTOS.removeAll(Collections.singleton(null));
        map.put("info", userDTOS);
        return map;
    }

    /**
     * 获取关系
     *
     * @param toUserId
     * @param userId
     * @return
     */
    @Override
    public UserRelationship getUserRelationshipByToUserIdAndUserId(Integer toUserId, Integer userId) {
        return userRelationshipMapper.getUserRelationshipByToUserIdAndUserId(toUserId, userId);
    }
}




