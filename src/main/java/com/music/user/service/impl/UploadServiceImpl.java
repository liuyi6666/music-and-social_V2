package com.music.user.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.music.config.OSSConfiguration;
import com.music.constant.UploadPathConstant;
import com.music.enums.StatusEnum;
import com.music.user.service.UploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Service
@Slf4j
public class UploadServiceImpl implements UploadService {

    @Autowired
    private OSS ossClient;

    @Autowired
    private OSSConfiguration ossConfiguration;


    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg", ".jpeg", ".gif", ".png"};


    /**
     * 校验图片格式
     *
     * @param file
     * @return
     */
    private Boolean verifyImg(MultipartFile file) {
        boolean isLegal = false;
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), type)) {
                isLegal = true;
                break;
            }
        }
        return isLegal;
    }

    /**
     * 获取图片文件后缀
     *
     * @param file
     * @return
     */
    private String imgSuffix(MultipartFile file) {
        String suffix = "";
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(), type)) {
                suffix = type;
                break;
            }
        }
        return suffix;
    }


    /**
     * 上传行为
     *
     * @param file
     * @param basePath
     * @param busTypePath
     * @return
     */
    private HashMap<String, Object> uploadFile(MultipartFile file, String basePath, String busTypePath) {
        HashMap<String, Object> map = new HashMap<>();
        String fileName = "";
        try {
            fileName = UUID.randomUUID().toString();
            InputStream inputStream = file.getInputStream();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(inputStream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(file.getContentType());
            objectMetadata.setContentDisposition("inline;filename=" + fileName);
            fileName = basePath + busTypePath + "/" + fileName + imgSuffix(file);
            // 上传文件
            ossClient.putObject(ossConfiguration.getBucketName(), fileName, inputStream, objectMetadata);
        } catch (IOException e) {
            log.error("Error occurred: {}", e.getMessage(), e);
            map.put("statusCode", StatusEnum.UPLOAD_ERROR.getStatusCode());
            return map;
        }
        map.put("statusCode", StatusEnum.UPLOAD_SUCCESS.getStatusCode());
        String remoteAccessUrl = getRemoteAccessUrl(fileName, 1);
        map.put("fileName", remoteAccessUrl);
        return map;
    }

    /**
     * 获取访问地址
     *
     * @param filename
     * @param expDays
     * @return
     */
    public String getRemoteAccessUrl(String filename, int expDays) {
        Date expiration = new Date(System.currentTimeMillis() + expDays * 1000 * 60 * 60 * 24 * 365 * 10);
        URL url = ossClient.generatePresignedUrl(ossConfiguration.getBucketName(), filename, expiration);
        if (url != null) {
            return url.toString();
        }
        return null;
    }

    /**
     * 上传用户歌单封面
     *
     * @param file
     * @param basePath
     * @return
     */
    @Override
    public HashMap<String, Object> uploadSheetImgFile(MultipartFile file, String basePath) {
        HashMap<String, Object> map = new HashMap<>();
        if (!verifyImg(file)) {
            map.put("statusCode", StatusEnum.UPLOAD_FORMAT_ERROR.getStatusCode());
            return map;
        }
        return uploadFile(file, basePath, UploadPathConstant.SHEET);
    }

    /**
     * 上传用户头像
     *
     * @param file
     * @param basePath
     * @return
     */
    @Override
    public HashMap<String, Object> uploadUserImgFile(MultipartFile file, String basePath) {
        HashMap<String, Object> map = new HashMap<>();
        if (!verifyImg(file)) {
            map.put("statusCode", StatusEnum.UPLOAD_FORMAT_ERROR.getStatusCode());
            return map;
        }
        return uploadFile(file, basePath, UploadPathConstant.HEADER);
    }

    /**
     * 上传用户背景图片
     *
     * @param file
     * @param basePath
     * @return
     */
    @Override
    public HashMap<String, Object> uploadUserCenterImg(MultipartFile file, String basePath) {
        HashMap<String, Object> map = new HashMap<>();
        if (!verifyImg(file)) {
            map.put("statusCode", StatusEnum.UPLOAD_FORMAT_ERROR.getStatusCode());
            return map;
        }
        return uploadFile(file, basePath, UploadPathConstant.BACKGROUND);
    }

    /**
     * 上传用户动态图片
     *
     * @param file
     * @param basePath
     * @return
     */
    @Override
    public String uploadUserIssueImg(MultipartFile file, String basePath) {
        HashMap<String, Object> map = uploadFile(file, basePath, UploadPathConstant.ISSUE);
        return (String) map.get("fileName");
    }

    /**
     * 上传首页Banner
     *
     * @param file
     * @param basePath
     * @return
     */
    @Override
    public HashMap<String,Object> uploadCommonBannerImg(MultipartFile file, String basePath) {
        HashMap<String, Object> map = new HashMap<>();
        if (!verifyImg(file)) {
            map.put("statusCode", StatusEnum.UPLOAD_FORMAT_ERROR.getStatusCode());
            return map;
        }
        return uploadFile(file, basePath, UploadPathConstant.BANNER);
    }
}
