package com.music.user.service.impl;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.UserProblemMapper;
import com.music.pojo.entity.UserProblem;
import com.music.user.service.UserProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
@Service
public class UserProblemServiceImpl implements UserProblemService {

    @Autowired
    private UserProblemMapper userProblemMapper;

    /**
     * 提交问题
     *
     * @param problemJson
     * @return
     */
    @Override
    public HashMap<String, Object> addProblem(String problemJson) {
        JSONObject jsonObject = JSONUtil.parseObj(problemJson);
        String information = (String) jsonObject.get("information");
        String problemDescription = (String) jsonObject.get("problemDescription");
        UserProblem userProblem = new UserProblem();
        userProblem.setInformation(information).setProblemDescription(problemDescription);
        int insert = userProblemMapper.insert(userProblem);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", insert > 0 ? StatusEnum.OTHER_SUCCESS.getStatusCode() : StatusEnum.OTHER_ERROR.getStatusCode());
        return map;
    }

    /**
     * 获取全部用户反馈
     *
     * @return
     */
    @Override
    public HashMap<String, Object> getUserProblem(Integer start, Integer size) {
        List<UserProblem> userProblems= userProblemMapper.findAll(start,size);
        Integer num = userProblemMapper.selectCount(null);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userProblems);
        map.put("total", num);
        return map;
    }
}




