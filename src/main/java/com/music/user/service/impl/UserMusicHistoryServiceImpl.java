package com.music.user.service.impl;


import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.CommonMusicMapper;
import com.music.mapper.UserMusicHistoryMapper;
import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.entity.CommonMusic;
import com.music.pojo.entity.UserLikeMusic;
import com.music.pojo.entity.UserMusicHistory;
import com.music.user.service.CommonMusicService;
import com.music.user.service.UserLikeMusicService;
import com.music.user.service.UserMusicHistoryService;
import com.music.util.KuGouUtil;
import com.music.util.MyJWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class UserMusicHistoryServiceImpl implements UserMusicHistoryService {

    @Autowired
    private UserMusicHistoryMapper userMusicHistoryMapper;

    @Autowired
    private CommonMusicMapper commonMusicMapper;

    @Autowired
    private UserLikeMusicService userLikeMusicService;

    /**
     * 获取用户播放历史
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findUserMusicHistory(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer)jwtInfo.get("userId");
        List<UserMusicHistory> userMusicHistoryList=userMusicHistoryMapper.findUserMusicPlayHistory(userId);
        List<CommonMusicDTO> commonMusicDTOList = new ArrayList<>();
        for (UserMusicHistory userMusicHistory : userMusicHistoryList) {
            CommonMusic commonMusic = commonMusicMapper.getCommonMusicByHashAndAlbumId(userMusicHistory.getHash(), userMusicHistory.getAlbumId());
            CommonMusicDTO commonMusicDTO = Convert.convert(CommonMusicDTO.class, commonMusic);
            commonMusicDTO.setTimeLength(KuGouUtil.swapTime(commonMusic.getTimeLength()));
            UserLikeMusic userLikeMusic = userLikeMusicService
                    .getLikeMusicByUserIdAndHashAndAlbumId(userId, commonMusicDTO.getHash(), commonMusicDTO.getAlbumId());
            commonMusicDTO.setLikeFlag(userLikeMusic == null ? Integer.valueOf(0) : userLikeMusic.getStatus());
            commonMusicDTOList.add(commonMusicDTO);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        map.put("info", commonMusicDTOList);
        return map;
    }

    /**
     * 添加用户播放历史
     *
     * @param addJson
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> addUserMusicHistory(String addJson, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId=(Integer) jwtInfo.get("userId");
        JSONObject jsonObject = JSONUtil.parseObj(addJson);
        String hash = (String) jsonObject.get("hash");
        String albumId=(String) jsonObject.get("albumId");
        UserMusicHistory userMusicHistory = new UserMusicHistory();
        userMusicHistory.setUserId(userId).setAlbumId(albumId).setHash(hash);
        userMusicHistoryMapper.insert(userMusicHistory);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 增加播放历史V2
     *
     * @param userId
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public Boolean addUserMusicHistoryV2(Integer userId, String hash, String albumId) {
        UserMusicHistory userMusicHistory = new UserMusicHistory();
        userMusicHistory.setUserId(userId).setAlbumId(albumId).setHash(hash);
        userMusicHistoryMapper.insert(userMusicHistory);
        return true;
    }

    /**
     * 清空播放历史
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> deleteMusicHistory(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId=(Integer) jwtInfo.get("userId");
        QueryWrapper<UserMusicHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        userMusicHistoryMapper.delete(queryWrapper);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_DELETE_SUCCESS.getStatusCode());
        return map;
    }
}




