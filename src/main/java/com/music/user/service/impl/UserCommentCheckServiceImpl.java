package com.music.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.mapper.UserCommentCheckMapper;
import com.music.pojo.entity.UserCommentCheck;
import com.music.user.service.UserCommentCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
@Service
public class UserCommentCheckServiceImpl implements UserCommentCheckService {

    @Autowired
    private UserCommentCheckMapper userCommentCheckMapper;

    /**
     * 保存检测评论
     *
     * @param userCommentCheck
     */
    @Override
    public void saveVerifyComment(UserCommentCheck userCommentCheck) {
        userCommentCheckMapper.insert(userCommentCheck);
    }

    /**
     * 展示用户违规记录
     *
     * @param userId
     * @return
     */
    @Override
    public HashMap<String, Object> showUserCommentCheck(Integer userId) {
        QueryWrapper<UserCommentCheck> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<UserCommentCheck> userCommentChecks = userCommentCheckMapper.selectList(queryWrapper);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userCommentChecks);
        return map;
    }
}




