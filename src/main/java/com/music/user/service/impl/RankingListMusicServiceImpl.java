package com.music.user.service.impl;

import com.music.mapper.RankingListMusicMapper;
import com.music.pojo.entity.RankingListMusic;
import com.music.user.service.RankingListMusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankingListMusicServiceImpl implements RankingListMusicService {

    @Autowired
    private RankingListMusicMapper rankingListMusicMapper;

    /**
     * 热门歌曲10首
     *
     * @return
     */
    @Override
    public List<RankingListMusic> findHotMusic() {
        return rankingListMusicMapper.findMusicTop500ByRankIdDesc10();
    }

    /**
     * 根据id查找歌曲
     *
     * @param ids
     * @return
     */
    @Override
    public RankingListMusic findRankingListMusicById(Integer ids) {
        return rankingListMusicMapper.selectById(ids);
    }
}
