package com.music.user.service.impl;

import com.music.mapper.SingerAlbumMusicMapper;
import com.music.pojo.entity.SingerAlbumMusic;
import com.music.user.service.CommonMusicService;
import com.music.user.service.SingerAlbumMusicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SingerAlbumMusicServiceImpl implements SingerAlbumMusicService{

    @Autowired
    private SingerAlbumMusicMapper singerAlbumMusicMapper;


    /**
     * 根据专辑id查找歌曲
     *
     * @param albumId
     * @return
     */
    @Override
    public List<SingerAlbumMusic> findSingerAlbumMusicByAlbumId(Integer albumId) {
        List<SingerAlbumMusic> singerAlbumMusicList=singerAlbumMusicMapper.findSingerAlbumMusicByAlbumId(albumId);
        log.info("findSingerAlbumMusicByAlbumId singerAlbumMusicList={}",singerAlbumMusicList);
        return singerAlbumMusicList;
    }

    /**
     * 查找音乐
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<SingerAlbumMusic> findSingerAlbumMusicByFileName(String searchKey) {
        return singerAlbumMusicMapper.findSingerAlbumMusicByFileName(searchKey);
    }
}
