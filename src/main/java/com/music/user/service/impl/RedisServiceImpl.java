package com.music.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.music.constant.RedisBusTypeConstant;
import com.music.enums.StatusEnum;
import com.music.mapper.CommonMusicMapper;
import com.music.pojo.dto.*;
import com.music.pojo.entity.CommonMusic;
import com.music.pojo.entity.UserChatMatching;
import com.music.pojo.entity.UserRelationship;
import com.music.user.service.*;
import com.music.util.MyJWTUtil;
import com.music.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private CommonMusicMapper commonMusicMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private UserChatMatchingService userChatMatchingService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserRelationshipService userRelationshipService;

    //存放在redis中的播放队列过期时间  单位分钟
    private static final Integer MUSIC_PLAY_QUEUE_KEY_TIME = 10;

    /**
     * 导入播放列表到redis中
     *
     * @param musicJson
     * @return
     */
    @Override
    public Boolean setMusicQueue(String musicJson) {
        log.info("setMusicQueue musicJson={}", musicJson);
        JSONObject jsonObject = JSONUtil.parseObj(musicJson);
        List musicPlayQueue = jsonObject.get("musicPlayQueue", ArrayList.class);
        //清除之前保存的播放列表
        redisTemplate.opsForList().trim("musicPlayQueue", 1, 0);
        for (Object o : musicPlayQueue) {
            MusicPlayQueueDTO musicPlayQueueDTO = Convert.convert(MusicPlayQueueDTO.class, o);
            //获取前端传来的hash和albumId
            redisTemplate.opsForList().rightPush("musicPlayQueue", musicPlayQueueDTO.getHash() + "#" + musicPlayQueueDTO.getAlbumId());
//            redisTemplate.expire("musicPlayQueue", MUSIC_PLAY_QUEUE_KEY_TIME, TimeUnit.MINUTES);
        }
        return true;
    }

    /**
     * 从redis中取出播放列表
     *
     * @return
     */
    @Override
    public List<CommonMusic> getMusicQueue() {
        List<String> musicPlayQueue = redisTemplate.opsForList().range("musicPlayQueue", 0, -1);
        if (null == musicPlayQueue) {
            return null;
        }
        List<String> resultMusicPlayQueue = musicPlayQueue
                .stream()
                .distinct()
                .collect(Collectors.toList());
        List<CommonMusic> commonMusicList = new ArrayList<>();
        for (String s : resultMusicPlayQueue) {
            String[] split = s.split("#");
            CommonMusic commonMusic = commonMusicMapper.getCommonMusicByHashAndAlbumId(split[0], split[1]);
            commonMusicList.add(commonMusic);
        }
        return commonMusicList;
    }

    /**
     * 匹配用户
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> matchingUser(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        HashMap<String, Object> map = new HashMap<>();
        Integer userId = (Integer) jwtInfo.get("userId");
        long userAddTime = System.currentTimeMillis();
        redisTemplate.opsForList().leftPush("myList", String.valueOf(userId));
        while (true) {
            //超时5秒返回
            if (System.currentTimeMillis() - userAddTime >= 5000) {
                String overTimeUser = redisTemplate.opsForList().rightPop("myList");
                System.out.println("超时弹出：" + overTimeUser);
                map.put("statusCode", StatusEnum.OTHER_MATCHING_OVERTIME.getStatusCode());
                break;
            }
            //当list中用户大于1一个时，弹出两个比较，相同再入，不同弹出并返回用户信息
            if (redisTemplate.opsForList().size("myList") > 1) {
                String user1 = redisTemplate.opsForList().leftPop("myList");
                System.out.println(user1 + "-------" + userId);
                assert user1 != null;
                if (userId.equals(Integer.valueOf(user1))) {
                    redisTemplate.opsForList().leftPush("myList", String.valueOf(userId));
                    continue;
                }
                map.put("statusCode", StatusEnum.OTHER_MATCHING_SUCCESS.getStatusCode());
                UserDTO userDTO = userService.getUserInfoById(Integer.valueOf(user1));
                //保存用户匹配数据
                userChatMatchingService.saveUserChat(userId, Integer.valueOf(user1));
                map.put("info", userDTO);
                break;
            }
        }
        return map;
    }

    /**
     * 保存聊天记录
     *
     * @param matchingSessionId
     * @param message
     */
    @Override
    public void saveChatRecord(String matchingSessionId, String message, Integer userId, Integer messageType, Integer chatType) {
        List<ChatInfo> record = null;
        record = chatType == 0 ? redisUtil.getChatRecord(matchingSessionId) : redisUtil.getChatRecordV2(matchingSessionId);
        if (record == null) {
            record = new ArrayList<ChatInfo>();
        }
        ChatInfo chatInfo = new ChatInfo();
        chatInfo.setUserId(userId).setMessage(message).setMsgType(messageType);
        record.add(chatInfo);
        if (chatType==0){
            redisUtil.saveChatRecord(matchingSessionId, record);
        }else {
            redisUtil.saveChatRecordV2(matchingSessionId, record);
        }
    }

    /**
     * 获取用户聊天记录
     *
     * @param userId
     * @param toUserId
     * @return
     */
    @Override
    public HashMap<String, Object> getUserChatRecord(Integer userId, Integer toUserId) {
        UserChatMatching userChatMatching = userChatMatchingService.getUserChatMatching(userId, toUserId);
        System.out.println("userChatMatching=" + JSON.toJSONString(userChatMatching));
        List<ChatInfo> chatRecord = redisUtil.getChatRecord(userChatMatching.getMatchingSessionId());
        System.out.println("chatRecord=" + JSON.toJSONString(chatRecord));
        if (CollUtil.isEmpty(chatRecord)) {
            chatRecord = new ArrayList<>();
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_CHAT_RECORD.getStatusCode());
        map.put("info", chatRecord);
        return map;
    }

    /**
     * 保存消息通知
     *
     * @param message
     */
    @Override
    public void saveMessageNotify(String message) {
        MessageNotifyDTO messageNotifyDTO = JSON.parseObject(message, MessageNotifyDTO.class);
        redisUtil.saveMessageNotify(messageNotifyDTO);
    }

    /**
     * 获取消息通知
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getMessageNotify(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        List<MessageNotifyDTO> messageNotifyDTOList = redisUtil.getMessageNotify(userId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_MESSAGE_NOTIFY.getStatusCode());
        map.put("info", messageNotifyDTOList);
        return map;
    }

    /**
     * 删除全部通知
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> removeAllMessage(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        redisTemplate.opsForList().trim(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, 1, 0);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 删除指定通知
     *
     * @param index
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> removeOneMessage(Integer index, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        String value = redisTemplate.opsForList().index(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, index);
        assert value != null;
        redisTemplate.opsForList().remove(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, 1, value);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 已读指定通知
     *
     * @param index
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> readOneMessage(Integer index, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        String value = redisTemplate.opsForList().index(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, index);
        MessageNotifyDTO messageNotifyDTO = JSON.parseObject(value, MessageNotifyDTO.class);
        assert messageNotifyDTO != null;
        messageNotifyDTO.setStatus(0);
        String value2 = JSON.toJSONString(messageNotifyDTO);
        redisTemplate.opsForList().set(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, index, value2);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 已读全部通知
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> readAllMessage(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        List<String> list = redisTemplate.opsForList().range(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, 0, -1);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                String s = redisTemplate.opsForList().rightPop(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId);
                MessageNotifyDTO messageNotifyDTO = JSON.parseObject(s, MessageNotifyDTO.class);
                assert messageNotifyDTO != null;
                messageNotifyDTO.setStatus(0);
                String s1 = JSON.toJSONString(messageNotifyDTO);
                redisTemplate.opsForList().leftPush(RedisBusTypeConstant.MESSAGE_NOTIFY + "_" + userId, s1);
            }
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 获取相互关注用户聊天信息，和用户信息
     *
     * @param toUserId
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getMutualAttentionChatRecord(Integer toUserId, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        UserDTO toUserInfo = userService.getUserInfoById(toUserId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("toUserInfo", toUserInfo);
        UserRelationship userRelationship = userRelationshipService.getUserRelationshipByToUserIdAndUserId(toUserId, userId);
        List<ChatInfo> mutualChatRecord = redisUtil.getChatRecordV2(userRelationship.getSessionId());
        if (CollUtil.isEmpty(mutualChatRecord)) {
            mutualChatRecord = new ArrayList<>();
        }
        map.put("info", mutualChatRecord);
        return map;
    }
}
