package com.music.user.service.impl;


import cn.hutool.core.convert.Convert;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.music.mapper.SheetMusicMapper;
import com.music.pojo.dto.SheetMusicDTO;
import com.music.pojo.entity.CommonMusic;
import com.music.pojo.entity.SheetMusic;
import com.music.pojo.entity.UserLikeMusic;
import com.music.user.service.CommonMusicService;
import com.music.user.service.SheetMusicService;
import com.music.user.service.UserLikeMusicService;
import com.music.util.KuGouUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class SheetMusicServiceImpl implements SheetMusicService {

    @Autowired
    private SheetMusicMapper sheetMusicMapper;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private UserLikeMusicService userLikeMusicService;

    /**
     * 根据歌单id获取歌单歌曲
     *
     * @param specialId
     * @return
     */
    @Override
    public List<SheetMusicDTO> findSheetMusicBySpecialId(Integer specialId, String token) {
        Integer userId = null;
        try {
            JWT jwt = JWTUtil.parseToken(token);
            userId = (Integer) jwt.getPayload("userId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<SheetMusic> sheetMusicList = sheetMusicMapper.findSheetMusicBySpecialId(specialId);
        ArrayList<SheetMusicDTO> sheetMusicDTOS = new ArrayList<>();
        UserLikeMusic userLikeMusic;
        for (SheetMusic sheetMusic : sheetMusicList) {
            SheetMusicDTO sheetMusicDTO = Convert.convert(SheetMusicDTO.class, sheetMusic);
            CommonMusic music = commonMusicService.getCommonMusicByHashAndAlbumId(sheetMusicDTO.getHash(), sheetMusicDTO.getAlbumId());
            if (!Objects.isNull(music)) {
                sheetMusicDTO.setTimeLength(KuGouUtil.swapTime(music.getTimeLength()));
            }
            userLikeMusic = userLikeMusicService
                    .getLikeMusicByUserIdAndHashAndAlbumId(userId, sheetMusicDTO.getHash(), sheetMusicDTO.getAlbumId());
            if (userLikeMusic == null) {
                sheetMusicDTO.setLikeFlag(0);
            } else {
                sheetMusicDTO.setLikeFlag(userLikeMusic.getStatus());
            }
            sheetMusicDTOS.add(sheetMusicDTO);
        }
        return sheetMusicDTOS;
    }

    /**
     * 获取指定歌单歌曲
     *
     * @param specialId
     * @return
     */
    @Override
    public HashMap<String, Object> getSheetMusicBySpecialId(Integer specialId) {
        List<SheetMusic> sheetMusics = sheetMusicMapper.findSheetMusicBySpecialId(specialId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", sheetMusics);
        return map;
    }
}
