package com.music.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.constant.SystemMessageConstant;
import com.music.enums.StatusEnum;
import com.music.mapper.UserCommentLikesMapper;
import com.music.mapper.UserCommentMapper;
import com.music.pojo.entity.UserComment;
import com.music.pojo.entity.UserCommentLikes;
import com.music.user.service.UserCommentLikesService;
import com.music.util.MyJWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Objects;

/**
 *
 */
@Service
public class UserCommentLikesServiceImpl implements UserCommentLikesService {

    @Autowired
    private UserCommentLikesMapper userCommentLikesMapper;

    @Autowired
    private UserCommentMapper userCommentMapper;

    @Autowired
    private MessageNotifyTemplate messageNotifyTemplate;

    /**
     * 修改点赞状态
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> commentLikes(Integer commentId, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        QueryWrapper<UserCommentLikes> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("comment_id", commentId);
        UserCommentLikes userCommentLikes = userCommentLikesMapper.selectOne(queryWrapper);
        if (Objects.isNull(userCommentLikes)) {
            UserCommentLikes userCommentLikes1 = new UserCommentLikes();
            userCommentLikes1.setUserId(userId).setCommentId(commentId).setStatus(1);
            userCommentLikesMapper.insert(userCommentLikes1);
            pushMessage(userId, commentId);
        } else {
            userCommentLikes.setStatus(userCommentLikes.getStatus() == 1 ? 0 : 1);
            UpdateWrapper<UserCommentLikes> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", userId).eq("comment_id", commentId);
            userCommentLikesMapper.update(userCommentLikes, updateWrapper);
            if (userCommentLikes.getStatus() == 1){
                pushMessage(userId, commentId);
            }
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_COMMENT_LIKES_STATUS.getStatusCode());
        return map;
    }

    //推送消息
    private void pushMessage(Integer userId, Integer commentId) {
        QueryWrapper<UserComment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", commentId);
        UserComment userComment = userCommentMapper.selectOne(queryWrapper);
        Integer toUserId = userComment.getUserId();
        if (userComment.getCommentType() == 0) {
            //歌曲评论 点赞推送
            messageNotifyTemplate.pushMessageNotify(toUserId, userId, SystemMessageConstant.LIKE_MUSIC_COMMENT, null);
        } else {
            //动态 点赞推送
            messageNotifyTemplate.pushMessageNotify(toUserId, userId, SystemMessageConstant.LIKE_ISSUE, null);
        }
    }

    /**
     * 更新评论点赞数
     *
     * @param commentId
     * @return
     */
    @Override
    public Integer updateCommentLikesNum(Integer commentId) {
        QueryWrapper<UserCommentLikes> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("comment_id", commentId).eq("status", 1);
        Integer likeNum = userCommentLikesMapper.selectCount(queryWrapper);
        UpdateWrapper<UserComment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", commentId);
        UserComment userComment = new UserComment();
        userComment.setLikeNum(likeNum);
        userCommentMapper.update(userComment, updateWrapper);
        return likeNum;
    }

    /**
     * 判断用户是否点赞了该评论
     *
     * @param userId
     * @param commentId
     * @return
     */
    @Override
    public Integer isUserLikes(Integer userId, Integer commentId) {
        QueryWrapper<UserCommentLikes> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("comment_id", commentId);
        UserCommentLikes userCommentLikes = userCommentLikesMapper.selectOne(queryWrapper);
        if (Objects.isNull(userCommentLikes)) {
            return 0;
        }
        return userCommentLikes.getStatus();
    }
}




