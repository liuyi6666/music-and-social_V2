package com.music.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.music.pojo.dto.MyMessage;
import com.music.pojo.entity.UserChatMatching;
import com.music.pojo.entity.UserRelationship;
import com.music.user.service.RedisService;
import com.music.user.service.UserChatMatchingService;
import com.music.user.service.UserRelationshipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@ServerEndpoint(value = "/test/oneToOne/{userId}")
@Component
public class OneToOneWebSocket {

    private static RedisService redisService;

    @Autowired
    public void setRedisService(RedisService redisService) {
        OneToOneWebSocket.redisService = redisService;
    }

    private static UserChatMatchingService userChatMatchingService;

    @Autowired
    public void setUserChatMatchingService(UserChatMatchingService userChatMatchingService) {
        OneToOneWebSocket.userChatMatchingService = userChatMatchingService;
    }

    private static UserRelationshipService userRelationshipService;

    @Autowired
    public void setUserChatMatchingService(UserRelationshipService userRelationshipService) {
        OneToOneWebSocket.userRelationshipService = userRelationshipService;
    }


    /**
     * 记录当前在线连接数
     */
    private static AtomicInteger onlineCount = new AtomicInteger(0);

    /**
     * 存放所有在线的客户端
     */
    private static Map<String, Session> clients = new ConcurrentHashMap<>();

    /**
     * 存放用户id和session的映射关系
     */
    private static Map<Integer, Session> userMap = new HashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") Integer userId) {
        System.out.println("userId=" + userId);
        onlineCount.incrementAndGet(); // 在线数加1
        clients.put(session.getId(), session);
        userMap.put(userId, session);
        log.info("有新连接加入：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        onlineCount.decrementAndGet(); // 在线数减1
        clients.remove(session.getId());
        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("服务端收到客户端[{}]的消息[{}]", session.getId(), message);
        try {
            MyMessage myMessage = JSON.parseObject(message, MyMessage.class);
            if (myMessage != null) {
                userMap.put(myMessage.getUserId(), session);
                Session toSession = userMap.get(myMessage.getToUserId());
                //两个用户未连接时，需要保存发送记录，供再次进入查询
                try {
                    if (myMessage.getChatType() == 0){
                        UserChatMatching userChatMatching = userChatMatchingService.getUserChatMatching(myMessage.getUserId(), myMessage.getToUserId());
                        redisService.saveChatRecord(userChatMatching.getMatchingSessionId(), myMessage.getMessage(), myMessage.getUserId(), myMessage.getMessageType(),myMessage.getChatType());
                    }else {
                        UserRelationship relationship = userRelationshipService.getUserRelationshipByToUserIdAndUserId(myMessage.getToUserId(), myMessage.getUserId());
                        redisService.saveChatRecord(relationship.getSessionId(), myMessage.getMessage(), myMessage.getUserId(),myMessage.getMessageType(), myMessage.getChatType());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (toSession != null) {
                    this.sendMessage(myMessage.getMessage(), toSession);
                    System.out.println("userId=" + myMessage.getUserId() + " toUserId=" + myMessage.getToUserId());
                }
            }
        } catch (Exception e) {
            log.error("解析失败：{}", e);
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息[{}]", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败：{}", e);
        }
    }
}
