package com.music.user.service.impl;

import com.music.mapper.SingerAlbumMapper;
import com.music.pojo.entity.SingerAlbum;
import com.music.user.service.SingerAlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SingerAlbumServiceImpl implements SingerAlbumService {

    @Autowired
    private SingerAlbumMapper singerAlbumMapper;

    /**
     * 根据歌手id查找专辑
     *
     * @param singerId
     * @return
     */
    @Override
    public List<SingerAlbum> findSingerAlbumBySingerId(Integer singerId) {

        List<SingerAlbum> singerAlbumList=singerAlbumMapper.findSingerAlbumBySingerId(singerId);
        log.info("findSingerAlbumBySingerId singerAlbumList={}",singerAlbumList);
        return singerAlbumList;
    }

    /**
     * 根据albumId获取专辑
     *
     * @param albumId
     * @return
     */
    @Override
    public SingerAlbum getSingerAlbumByAlbumId(Integer albumId) {
        SingerAlbum singerAlbum = singerAlbumMapper.getSingerAlbumByAlbumId(albumId);
        log.info("getSingerAlbumByAlbumId singerAlbum={}",singerAlbum);
        return singerAlbum;
    }
}
