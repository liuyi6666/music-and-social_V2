package com.music.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.mapper.UserAccessTokenMapper;
import com.music.pojo.entity.UserAccessToken;
import com.music.user.service.UserAccessTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 *
 */
@Service
public class UserAccessTokenServiceImpl implements UserAccessTokenService {

    @Autowired
    private UserAccessTokenMapper userAccessTokenMapper;

    /**
     * 数据库保存一份token
     *
     * @param userId
     * @param token
     */
    @Override
    public void saveUserAccessToken(Integer userId, String token, Integer status) {
        UserAccessToken userAccessToken=userAccessTokenMapper.getUserAccessTokenByUserId(userId);
        if (Objects.isNull(userAccessToken)){
            userAccessTokenMapper.insert(new UserAccessToken().setToken(token).setUserId(userId).setStatus(status));
        }else {
            UpdateWrapper<UserAccessToken> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", userId);
            userAccessTokenMapper.update(userAccessToken.setStatus(status).setToken(token),updateWrapper);
            updateWrapper.clear();
        }

    }
}




