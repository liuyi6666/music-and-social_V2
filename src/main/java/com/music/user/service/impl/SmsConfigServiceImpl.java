package com.music.user.service.impl;

import com.music.mapper.SmsConfigMapper;
import com.music.pojo.entity.SmsConfig;
import com.music.user.service.SmsConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmsConfigServiceImpl implements SmsConfigService {

    @Autowired
    private SmsConfigMapper smsConfigMapper;
    /**
     * 获取配置项
     *
     * @param key
     * @return
     */
    @Override
    public SmsConfig getSmsConfig(String templateKey) {
        return smsConfigMapper.getSmsConfigByKey(templateKey);
    }
}
