package com.music.user.service.impl;


import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.UserTabMapper;
import com.music.pojo.entity.UserTab;
import com.music.user.service.UserTabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *
 */
@Service
public class UserTabServiceImpl implements UserTabService {

    @Autowired
    private UserTabMapper userTabMapper;

    /**
     * 获取全部标签
     *
     * @return
     */
    @Override
    public HashMap<String, Object> findAll() {
        HashMap<String, Object> map = new HashMap<>();
        List<String> userTabs = userTabMapper.findAll();
        map.put("info", userTabs);
        return map;
    }

    /**
     * 增加tab
     *
     * @param tabName
     * @return
     */
    @Override
    public HashMap<String, Object> addTab(String tabName) {
        QueryWrapper<UserTab> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tab_name", tabName);
        HashMap<String, Object> map = new HashMap<>();
        UserTab userTab = userTabMapper.selectOne(queryWrapper);
        if (Objects.isNull(userTab)) {
            userTab = new UserTab().setTabName(tabName);
            userTabMapper.insert(userTab);
        }
        map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        return map;
    }
}




