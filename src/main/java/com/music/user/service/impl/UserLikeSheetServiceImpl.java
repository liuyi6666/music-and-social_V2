package com.music.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.mapper.UserLikeSheetMapper;
import com.music.pojo.entity.UserLikeSheet;
import com.music.user.service.UserLikeSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class UserLikeSheetServiceImpl implements UserLikeSheetService {

    @Autowired
    private UserLikeSheetMapper userLikeSheetMapper;

    /**
     * 根据userId和歌单id获取喜欢的值
     *
     * @param userId
     * @param specialId
     * @return
     */
    @Override
    public UserLikeSheet getUserLikeSheetByUserIdAndSpecialId(Integer userId, Integer specialId) {
        return userLikeSheetMapper.getUserLikeSheetByUserIdAndSpecialId(userId,specialId);
    }

    /**
     * 保存喜欢歌单
     *
     * @param userId
     * @param specialId
     */
    @Override
    public void saveUserLikeSheetByUserId(Integer userId, Integer specialId) {
        UserLikeSheet userLikeSheet = userLikeSheetMapper
                .getUserLikeSheetByUserIdAndSpecialId(userId, specialId);
        if (userLikeSheet == null){
            UserLikeSheet likeSheet = new UserLikeSheet();
            likeSheet.setUserId(userId).setSpecialId(specialId).setStatus(1);
            userLikeSheetMapper.insert(likeSheet);
        }else {
            UpdateWrapper<UserLikeSheet> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("user_id", userId).eq("special_id", specialId);
            userLikeSheet.setStatus(userLikeSheet.getStatus() == 1 ? 0 : 1);
            userLikeSheetMapper.update(userLikeSheet, updateWrapper);
            updateWrapper.clear();
        }
    }

    /**
     * 查找用户喜欢歌单列表
     *
     * @param userId
     * @param status
     * @return
     */
    @Override
    public List<UserLikeSheet> findUserLikeSheetByUserIdAndStatus(Integer userId, Integer status) {
        return userLikeSheetMapper.findUserLikeSheetByUserIdAndStatus(userId,status);
    }
}




