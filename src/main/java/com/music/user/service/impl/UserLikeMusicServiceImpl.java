package com.music.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.mapper.UserLikeMusicMapper;
import com.music.pojo.entity.UserLikeMusic;
import com.music.user.service.UserLikeMusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class UserLikeMusicServiceImpl implements UserLikeMusicService {

    @Autowired
    private UserLikeMusicMapper userLikeMusicMapper;

    /**
     * 根据userId查找用户喜欢歌曲
     *
     * @param userId
     * @return
     */
    @Override
    public List<UserLikeMusic> findLikeMusicByUserId(Integer userId) {
        return userLikeMusicMapper.findLikeMusicByUserId(userId);
    }

    /**
     * 保存喜欢的歌曲
     *
     * @param userId
     * @param hash
     * @param albumId
     */
    @Override
    public void saveLikeMusicByUserId(Integer userId, String hash, String albumId) {
        UserLikeMusic likeMusic = userLikeMusicMapper.getLikeMusicByUserIdAndHashAndAlbumId(userId, hash, albumId);
        if (likeMusic == null ){
            UserLikeMusic userLikeMusic = new UserLikeMusic();
            userLikeMusic.setUserId(userId).setHash(hash).setAlbumId(albumId).setStatus(1);
            userLikeMusicMapper.insert(userLikeMusic);
        }else {
            UpdateWrapper<UserLikeMusic> updateWrapper = new UpdateWrapper<>();
            likeMusic.setStatus(likeMusic.getStatus() == 1 ? 0 : 1);
            updateWrapper.eq("user_id", userId).eq("hash", hash).eq("album_id", albumId);
            userLikeMusicMapper.update(likeMusic, updateWrapper);
            updateWrapper.clear();
        }

    }

    /**
     * 根据userId hash albumId查询用户喜欢
     *
     * @param userId
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public UserLikeMusic getLikeMusicByUserIdAndHashAndAlbumId(Integer userId, String hash, String albumId) {
        return userLikeMusicMapper.getLikeMusicByUserIdAndHashAndAlbumId(userId,hash,albumId);
    }

    /**
     * 根据用户id和状态查喜欢歌曲
     *
     * @param userId
     * @param status
     * @return
     */
    @Override
    public List<UserLikeMusic> findLikeMusicByUserIdAndStatus(Integer userId, Integer status) {
        return userLikeMusicMapper.findUserLikeMusicByUserIdAndStatus(userId, status);
    }
}




