package com.music.user.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.admin.service.KugouInfoSynchronizationService;
import com.music.enums.StatusEnum;
import com.music.mapper.MusicSheetMapper;
import com.music.pojo.api.KugouMusicSearch;
import com.music.pojo.api.KugouSheetSearch;
import com.music.pojo.dto.MusicSheetDTO;
import com.music.pojo.entity.MusicSheet;
import com.music.user.service.MusicSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MusicSheetServiceImpl implements MusicSheetService {

    @Autowired
    private MusicSheetMapper musicSheetMapper;

    @Autowired
    private KugouInfoSynchronizationService kugouInfoSynchronizationService;

    /**
     * 根据type类型获取各种歌单
     * 1 推荐 默认排行
     * 2 最近更新
     * 3 根据播放次数
     *
     * @param type
     * @return
     */
    @Override
    public List<MusicSheet> findMusicSheetByType(Integer type) {
        if (1 == type) {
            return musicSheetMapper.findMusicSheetDefault();
        } else if (2 == type) {
            return musicSheetMapper.findMusicSheetByPublishTimeDesc();
        } else if (3 == type) {
            return musicSheetMapper.findMusicSheetByPlayTimeDesc();
        }
        return null;
    }

    /**
     * 获取首页的歌单前五首
     *
     * @return
     */
    @Override
    public List<MusicSheet> findMusicSheetForIndex() {
        return musicSheetMapper.findMusicSheetLimit5();
    }

    /**
     * 根据id获取歌单
     *
     * @return
     */
    @Override
    public MusicSheet getMusicSheetById(Integer id) {
        return musicSheetMapper.selectById(id);
    }

    /**
     * 根据歌单id查歌单
     *
     * @param specialId
     * @return
     */
    @Override
    public MusicSheetDTO getMusicSheetBySpecialId(Integer specialId) {
        MusicSheet musicSheet = musicSheetMapper.getMusicSheetBySpecialId(specialId);
        return Convert.convert(MusicSheetDTO.class, musicSheet);
    }

    /**
     * 查找歌单
     *
     * @param searchKey
     * @return
     */
    @Override
    public List<MusicSheet> findMusicSheetBySpecialName(String searchKey) {
        return musicSheetMapper.findMusicSheetBySpecialName(searchKey);
    }

    /**
     * 查询歌单
     *
     * @param start
     * @param size
     * @param sheetJson
     * @return
     */
    @Override
    public HashMap<String, Object> findAll(Integer start, Integer size, String sheetJson) {
        System.out.println("sheetJson====" + sheetJson);
        JSONObject jsonObject = JSONUtil.parseObj(sheetJson);
        String searchKey = (String) jsonObject.get("sheetSearchKey");
        String userStr = (String) jsonObject.get("user");
        Integer user = null;
        if (StrUtil.isNotEmpty(userStr)) {
            user = Integer.valueOf(userStr);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", musicSheetMapper.findAll(start, size, searchKey, user));
        map.put("total", musicSheetMapper.countMusicSheet(searchKey, user));
        return map;
    }

    /**
     * 删除歌单
     *
     * @param id
     * @return
     */
    @Override
    public HashMap<String, Object> deleteSheet(Integer id) {
        int i = musicSheetMapper.deleteById(id);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", i == 1 ? StatusEnum.MUSIC_DELETE_SUCCESS.getStatusCode() : StatusEnum.MUSIC_DELETE_ERROR.getStatusCode());
        return map;
    }

    /**
     * 导入歌单，并自动导入歌单音乐
     *
     * @param sheetList
     * @return
     */
    @Override
    public HashMap<String, Object> importSheet(String sheetList) {
        JSONObject jsonObject = JSONUtil.parseObj(sheetList);
        String sheetJson = jsonObject.get("sheetList").toString();
        List<KugouSheetSearch.DataBean.InfoBean> infoBeans = JSON.parseArray(sheetJson, KugouSheetSearch.DataBean.InfoBean.class);
        List<MusicSheet> musicSheets = infoBeans.stream().map(infoBean -> {
            MusicSheet musicSheet = new MusicSheet();
            musicSheet.setSpecialId(infoBean.getSpecialid())
                    .setSpecialName(infoBean.getSpecialname())
                    .setIntro(infoBean.getIntro())
                    .setImgUrl(infoBean.getImgurl())
                    .setPublishTime(infoBean.getPublishtime())
                    .setUserName(infoBean.getNickname())
                    .setPlayCount(infoBean.getPlaycount())
                    .setIsKugouUser(1);
            return musicSheet;
        }).collect(Collectors.toList());
        musicSheets.forEach(musicSheet -> {
            QueryWrapper<MusicSheet> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("special_id", musicSheet.getSpecialId());
            MusicSheet sheet = musicSheetMapper.selectOne(queryWrapper);
            if (Objects.isNull(sheet)) {
                musicSheetMapper.insert(musicSheet);
                //自动同步歌单音乐
                kugouInfoSynchronizationService.syncOneSheetMusic(musicSheet.getSpecialId());
            }
        });
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.MUSIC_IMPORT_SUCCESS.getStatusCode());
        return map;
    }
}
