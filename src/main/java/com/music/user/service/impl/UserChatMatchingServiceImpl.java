package com.music.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.UserChatMatchingMapper;
import com.music.pojo.dto.ChatInfo;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.entity.UserChatMatching;
import com.music.user.service.RedisService;
import com.music.user.service.UserChatMatchingService;
import com.music.user.service.UserService;
import com.music.util.GeneralUtil;
import com.music.util.MyJWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


@Service
@Slf4j
public class UserChatMatchingServiceImpl implements UserChatMatchingService {

    @Autowired
    private UserChatMatchingMapper userChatMatchingMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    /**
     * 保存用户匹配数据
     *
     * @param userId
     * @param matchingUserId
     */
    @Override
    public void saveUserChat(Integer userId, Integer matchingUserId) {
        String sessionMD5 = GeneralUtil.sessionMD5(userId, matchingUserId);
        QueryWrapper<UserChatMatching> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("matching_user_id", matchingUserId);
        UserChatMatching userChatMatching = userChatMatchingMapper.selectOne(queryWrapper);
        if (Objects.isNull(userChatMatching)) {
            UserChatMatching chatMatching = new UserChatMatching();
            chatMatching.setUserId(userId).setMatchingUserId(matchingUserId).setMatchingSessionId(sessionMD5);
            userChatMatchingMapper.insert(chatMatching);
        }
    }


    /**
     * 查询匹配成功后的用户聊天列表
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getMatchingChatUser(String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        QueryWrapper<UserChatMatching> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<UserChatMatching> chatMatchingList = userChatMatchingMapper.selectList(queryWrapper);
        List<UserDTO> userDTOList = chatMatchingList.stream()
                .map(userChatMatching -> {
                    UserDTO userInfoById = userService.getUserInfoById(userChatMatching.getMatchingUserId());
                    userInfoById.setCreateTime(userChatMatching.getUpdateTime());
                    HashMap<String, Object> userChatRecord = redisService.getUserChatRecord(userChatMatching.getUserId(), userChatMatching.getMatchingUserId());
                    log.info("getMatchingChatUser userChatRecord={}", JSON.toJSONString(userChatRecord));
                    List<ChatInfo> info = (List<ChatInfo>) userChatRecord.get("info");
                    if (CollUtil.isNotEmpty(info)){
                        //获取最后一条消息
                        String message = info.get(info.size() - 1).getMessage();
                        userInfoById.setLastMessage(message);
                    }
                    return userInfoById;
                })
                .collect(Collectors.toList());
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_CHAT_LIST.getStatusCode());
        map.put("info", userDTOList);
        map.put("myUserId", userId);
        return map;
    }

    /**
     * 获取聊天会话id
     *
     * @param userId
     * @param toUserId
     * @return
     */
    @Override
    public UserChatMatching getUserChatMatching(Integer userId, Integer toUserId) {
        System.out.println("&&&&&&&&&&&&&&" + userId + "----" + toUserId);
        QueryWrapper<UserChatMatching> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("matching_user_id", toUserId);
        return userChatMatchingMapper.selectOne(queryWrapper);
    }
}




