package com.music.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.music.pojo.dto.MessageNotifyDTO;
import com.music.user.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@ServerEndpoint(value = "/message/{userId}")
@Component
public class MessageNotifyWebSocket {

    private static RedisService redisService;

    @Autowired
    public void setRedisService(RedisService redisService) {
        MessageNotifyWebSocket.redisService = redisService;
    }

    /**
     * 记录当前在线连接数
     */
    private static AtomicInteger onlineCount = new AtomicInteger(0);

    /**
     * 存放所有在线的客户端
     */
    private static Map<String, Session> clients = new ConcurrentHashMap<>();

    /**
     * 存放用户id和session的映射关系
     */
    private static Map<Integer, Session> userMap = new HashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") Integer userId) {
        System.out.println("userId=" + userId);
        onlineCount.incrementAndGet(); // 在线数加1
        clients.put(session.getId(), session);
        userMap.put(userId, session);
        log.info("有新连接加入：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        onlineCount.decrementAndGet(); // 在线数减1
        clients.remove(session.getId());
        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId(), onlineCount.get());
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 系统推送消息
     *
     * @param messageNotifyDTO
     */
    public void sendMessageNotify(MessageNotifyDTO messageNotifyDTO){
        try {
            log.info("服务端给客户端推送消息[{}]", messageNotifyDTO);
            Session selfSession=userMap.get(messageNotifyDTO.getToUserId());
            String message = JSON.toJSONString(messageNotifyDTO);
            selfSession.getBasicRemote().sendText(message);
            try {
                redisService.saveMessageNotify(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            log.error("服务端给客户端推送消息失败：{}", e);
        }
    }
}
