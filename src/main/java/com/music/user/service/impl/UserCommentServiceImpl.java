package com.music.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.gson.JsonObject;
import com.music.constant.UploadPathConstant;
import com.music.enums.StatusEnum;
import com.music.mapper.UserCommentLikesMapper;
import com.music.mapper.UserCommentMapper;
import com.music.mapper.UserMapper;
import com.music.pojo.dto.UserCommentDTO;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.entity.User;
import com.music.pojo.entity.UserComment;
import com.music.pojo.entity.UserCommentLikes;
import com.music.pojo.entity.UserRelationship;
import com.music.user.service.*;
import com.music.util.ImgUtil;
import com.music.util.MyJWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
@Service
public class UserCommentServiceImpl implements UserCommentService {

    @Autowired
    private UserCommentMapper userCommentMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserCommentLikesService userCommentLikesService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private UserRelationshipService userRelationshipService;


    /**
     * 获取歌曲评论
     *
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public HashMap<String, Object> findMusicComment(String hash, String albumId, String authentication) {
        //当前登录用户id
        Integer nowUserId = null;
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (!Objects.isNull(authentication) && !(jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode()))) {
            nowUserId = (Integer) jwtInfo.get("userId");
        }
        HashMap<String, Object> map = new HashMap<>();
        //获取一级评论
        List<UserCommentDTO> userCommentDTOList = userCommentMapper.findMusicCommentLevel1(hash, albumId);
        if (CollUtil.isNotEmpty(userCommentDTOList)) {
            for (UserCommentDTO userCommentDTO : userCommentDTOList) {
                if (!Objects.isNull(userCommentDTO)) {
                    //获取一级用户头像
                    User user = userMapper.selectById(userCommentDTO.getUserId());
                    userCommentDTO.setUserHeaderImg(user.getHeaderImg());
                    //更新当前评论的点赞数量
                    userCommentDTO.setLikeNum(userCommentLikesService.updateCommentLikesNum(userCommentDTO.getId()));
                    //判断当前用户是否点赞了该评论
                    userCommentDTO.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userCommentDTO.getId()));
                    userCommentDTO.setUserCommentList(new ArrayList<UserComment>());
                    //查找是否有二级评论
                    List<UserComment> userCommentList = userCommentMapper.findMusicCommentLevel2(hash, albumId, userCommentDTO.getId());
                    if (CollUtil.isNotEmpty(userCommentList)) {
                        for (UserComment userComment : userCommentList) {
                            //获取二级用户头像
                            User user1 = userMapper.selectById(userComment.getUserId());
                            userComment.setUserHeaderImg(user1.getHeaderImg());
                            //更新当前评论的点赞数量
                            userComment.setLikeNum(userCommentLikesService.updateCommentLikesNum(userComment.getId()));
                            //判断当前用户是否点赞了该评论
                            userComment.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userComment.getId()));
                            userCommentDTO.getUserCommentList().add(userComment);
                        }
                    }
                }
            }
        }
        QueryWrapper<UserComment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("hash", hash).eq("album_id", albumId).eq("status", 1);
        Integer num = userCommentMapper.selectCount(queryWrapper);
        map.put("statusCode", StatusEnum.MUSIC_COMMENT.getStatusCode());
        map.put("commentNum", num);
        map.put("info", userCommentDTOList);
        return map;
    }

    /**
     * 歌曲页面发起评论
     *
     * @param commentJson
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> commitMusicCommentV0(String commentJson, String authentication) {
        JSONObject jsonObject = JSONUtil.parseObj(commentJson);
        String userComment = (String) jsonObject.get("content");
        String hash = (String) jsonObject.get("hash");
        String albumId = (String) jsonObject.get("albumId");
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        String userName = (String) jwtInfo.get("userName");
        UserComment comment = new UserComment();
        comment.setUserId(userId)
                .setUserName(userName)
                .setHash(hash)
                .setAlbumId(albumId)
                .setContent(userComment)
                .setCommentLevel(1)
                .setCommentType(0)
                .setStatus(1);
        userCommentMapper.insert(comment);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_COMMENT_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 评论level1
     *
     * @param commentJson
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> commitMusicCommentV1(String commentJson, String authentication) {
        JSONObject jsonObject = JSONUtil.parseObj(commentJson);
        String content = (String) jsonObject.get("content");
        String hash = (String) jsonObject.get("hash");
        String albumId = (String) jsonObject.get("albumId");
        Integer parentId = (Integer) jsonObject.get("parentId");
        Integer parentUserId = (Integer) jsonObject.get("parentUserId");
        String parentUserName = (String) jsonObject.get("parentUserName");
        Integer replyCommentId = (Integer) jsonObject.get("replyCommentId");
        Integer replyCommentUserId = (Integer) jsonObject.get("replyCommentUserId");
        String replyCommentUserName = (String) jsonObject.get("replyCommentUserName");
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        String userName = (String) jwtInfo.get("userName");
        UserComment userComment = new UserComment();
        userComment.setUserId(userId)
                .setUserName(userName)
                .setContent(content)
                .setHash(hash)
                .setAlbumId(albumId)
                .setParentId(parentId)
                .setParentUserId(parentUserId)
                .setParentUserName(parentUserName)
                .setReplyCommentId(replyCommentId)
                .setReplyCommentUserId(replyCommentUserId)
                .setReplyCommentUserName(replyCommentUserName)
                .setStatus(1)
                .setCommentLevel(2)
                .setCommentType(0);
        userCommentMapper.insert(userComment);
        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_COMMENT_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 删除评论
     *
     * @param id
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> deleteComment(Integer id, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        UpdateWrapper<UserComment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        UserComment userComment = new UserComment();
        userComment.setStatus(0);
        int update = userCommentMapper.update(userComment, updateWrapper);
        HashMap<String, Object> map = new HashMap<>();
        if (update == 1) {
            map.put("statusCode", StatusEnum.USER_COMMENT_DELETE_SUCCESS.getStatusCode());
        } else {
            map.put("statusCode", StatusEnum.USER_COMMENT_DELETE_ERROR.getStatusCode());
        }
        return map;
    }

    /**
     * 根据用户id查找用户音乐评论
     *
     * @param userId
     * @return
     */
    @Override
    public List<UserCommentDTO> findMusicCommentByUserId(Integer userId) {
        return userCommentMapper.findMusicCommentByUserId(userId);
    }

    /**
     * 获取用户动态
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> findUserIssue(String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        Integer nowUserId = null;
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        nowUserId = (Integer) jwtInfo.get("userId");
        //一级评论
        List<UserCommentDTO> userCommentDTOList = userCommentMapper.findUserIssueCommentLevel1(nowUserId);
        //获取关注用户的动态
        List<UserRelationship> userRelationshipList = userRelationshipService.getFollowUserByUserId(nowUserId);
        for (UserRelationship userRelationship : userRelationshipList) {
            userCommentDTOList.addAll(userCommentMapper.findUserIssueCommentLevel1(userRelationship.getUserId()));
        }
        //按照发布时间对动态排序
        userCommentDTOList.sort((o1, o2) -> -(o1.getCreateTime().compareTo(o2.getCreateTime())));
        if (CollUtil.isNotEmpty(userCommentDTOList)) {
            for (UserCommentDTO userCommentDTO : userCommentDTOList) {
                //获取一级动态用户头像
                User user = userMapper.selectById(userCommentDTO.getUserId());
                userCommentDTO.setUserHeaderImg(user.getHeaderImg());
                //更新当前评论的点赞数量
                userCommentDTO.setLikeNum(userCommentLikesService.updateCommentLikesNum(userCommentDTO.getId()));
                //判断当前用户是否点赞了该评论
                userCommentDTO.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userCommentDTO.getId()));
                userCommentDTO.setUserCommentList(new ArrayList<UserComment>());
                //判断是否存在二级评论
                List<UserComment> userCommentList = userCommentMapper.findUserIssueCommentLevel2(userCommentDTO.getId(), userCommentDTO.getArticleId());
                if (CollUtil.isNotEmpty(userCommentList)) {
                    for (UserComment userComment : userCommentList) {
                        //获取二级用户的头像信息
                        User user1 = userMapper.selectById(userComment.getUserId());
                        //更新当前评论的点赞数量
                        userComment.setLikeNum(userCommentLikesService.updateCommentLikesNum(userComment.getId()));
                        //判断当前用户是否点赞了该评论
                        userComment.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userComment.getId()));
                        userComment.setUserHeaderImg(user1.getHeaderImg());
                        //加入到list中
                        userCommentDTO.getUserCommentList().add(userComment);
                    }
                }
            }
        }
        map.put("info", userCommentDTOList);
        map.put("statusCode", StatusEnum.USER_ISSUE_COMMENT_SUCCESS.getStatusCode());
        return map;

    }

    /**
     * 发布动态
     *
     * @param issueData
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> releaseIssue(String issueData, String authentication) {
        JSONObject jsonObject = JSONUtil.parseObj(issueData);
        String strDate = (String) jsonObject.get("content");
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        System.out.println(strDate + "strDate:");
        Integer userId = (Integer) jwtInfo.get("userId");
        String userName = (String) jwtInfo.get("userName");
        HashMap<String, Object> map = new HashMap<>();
        //将二进制中的issueData抽取出来
        HashMap<String, Object> base64ImgAndTextMap = getBase64ImgAndText(strDate);
        String content = (String) base64ImgAndTextMap.get("content");
        if (null != base64ImgAndTextMap.get("isImg")) {
            if (!(Boolean) base64ImgAndTextMap.get("isImg")) {
                map.put("statusCode", StatusEnum.UPLOAD_FORMAT_ERROR.getStatusCode());
                return map;
            }
            //纯文字直接入库，图片进判断
            if ((Boolean) base64ImgAndTextMap.get("isImg")) {
                List<String> base64Img = (List<String>) base64ImgAndTextMap.get("Base64Img");
                System.out.println("base64Img:" + JSONUtil.toJsonStr(base64Img));
                System.out.println("content:" + content);
                //循环转化成MultipartFile上传到oss
                for (String s : base64Img) {
                    MultipartFile file = ImgUtil.swapToMultipartFile(s);
                    String url = uploadService.uploadUserIssueImg(file, UploadPathConstant.USER);
                    content = packageImgUrlAndContent(url, content);
                }
            }
        }
        System.out.println("转化后content:" + content);
        //给动态设置id
        Long articleId = System.currentTimeMillis();
        UserComment userComment = new UserComment();
        userComment.setUserId(userId)
                .setUserName(userName)
                .setArticleId(articleId)
                .setContent(content)
                .setCommentType(1)
                .setCommentLevel(1)
                .setStatus(1);
        userCommentMapper.insert(userComment);
        map.put("statusCode", StatusEnum.USER_ISSUE_RELEASE_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 将前端传来的文字和图片拆分开
     *
     * @param strData
     * @return
     */
    private HashMap<String, Object> getBase64ImgAndText(String strData) {
        HashMap<String, Object> map = new HashMap<>();
        //无图片、单张图片格式不对，多张图片格式不对
        //无图片直接返回
        List<String> stringList = new ArrayList<>();
        if (strData.contains("<img src=\"data:")) {
            //判断多张图片格式是否正确
            if (!duplicateStrNum(strData, "<img src=\"data:").equals(duplicateStrNum(strData, "<img src=\"data:image"))) {
                map.put("isImg", false);
            } else {
                map.put("isImg", true);
                String substring = null;
                String img = "";
                Pattern p_image;
                Matcher m_image;
                // String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
                String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
                p_image = Pattern.compile(regEx_img, Pattern.CASE_INSENSITIVE);
                m_image = p_image.matcher(strData);
                while (m_image.find()) {
                    // 得到<img />数据
                    img = m_image.group();
                    // 匹配<img>中的src数据
                    Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
                    while (m.find()) {
                        stringList.add(m.group(1));
                        strData = strData.replace("<img src=\"" + m.group(1) + "\">", "<#>");
                    }
                }
            }
        }
        map.put("Base64Img", stringList);
        map.put("content", strData);
        return map;
    }

    /**
     * 封装图片路径到内容
     *
     * @param imgUrl
     * @param content
     * @return
     */
    private String packageImgUrlAndContent(String imgUrl, String content) {
        String template = "<img style=\"min-width: 200px;min-height: 200px;max-height: 400px;max-width: 400px\" src=\"" + imgUrl + "\"></img>";
        return content.replaceFirst("<#>", template);
    }

    /**
     * 相同字符串出现次数
     *
     * @param parentStr   父字符串
     * @param childrenStr 相同字符串
     * @return
     */
    private Integer duplicateStrNum(String parentStr, String childrenStr) {
        // 获取原始字符串的长度
        int oldCount = parentStr.length();
        // 将 ab 替换为空之后字符串的长度
        int newCount = parentStr.replace(childrenStr, "").length();
        // 由于统计的字符串长度是2，所以出现的次数要除以要统计字符串的长度
        return (oldCount - newCount) / childrenStr.length();

    }

    /**
     * 删除动态和评论
     *
     * @param id
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> deleteIssue(Integer id, String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        UpdateWrapper<UserComment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        UserComment userComment = new UserComment();
        userComment.setStatus(0);
        int update = userCommentMapper.update(userComment, updateWrapper);
        if (update == 1) {
            map.put("statusCode", StatusEnum.USER_COMMENT_DELETE_SUCCESS.getStatusCode());
        } else {
            map.put("statusCode", StatusEnum.USER_COMMENT_DELETE_ERROR.getStatusCode());
        }
        return map;
    }

    /**
     * 评论一级动态
     *
     * @param commentInfo
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> commentUserIssueLevel1(String commentInfo, String authentication) {
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        if (jwtInfo.get("statusCode").equals(StatusEnum.TOKEN_EXCEPTION.getStatusCode())) {
            return jwtInfo;
        }
        Integer userId = (Integer) jwtInfo.get("userId");
        String userName = (String) jwtInfo.get("userName");
        JSONObject jsonObject = JSONUtil.parseObj(commentInfo);
        String content = (String) jsonObject.get("content");
        Long articleId = (Long) jsonObject.get("articleId");
        Integer parentId = (Integer) jsonObject.get("parentId");
        Integer parentUserId = (Integer) jsonObject.get("parentUserId");
        String parentUserName = (String) jsonObject.get("parentUserName");
        Integer replyCommentId = (Integer) jsonObject.get("replyCommentId");
        Integer replyCommentUserId = (Integer) jsonObject.get("replyCommentUserId");
        String replyCommentUserName = (String) jsonObject.get("replyCommentUserName");
        UserComment userComment = new UserComment();
        userComment.setUserId(userId)
                .setUserName(userName)
                .setArticleId(articleId)
                .setParentId(parentId)
                .setParentUserId(parentUserId)
                .setReplyCommentUserName(replyCommentUserName)
                .setReplyCommentId(replyCommentId)
                .setReplyCommentUserId(replyCommentUserId)
                .setParentUserName(parentUserName)
                .setContent(content)
                .setCommentLevel(2)
                .setCommentType(1)
                .setStatus(1);
        userCommentMapper.insert(userComment);

        HashMap<String, Object> map = new HashMap<>();
        map.put("statusCode", StatusEnum.USER_COMMENT_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 查找其他的动态的
     *
     * @param userId
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getOtherUserIssue(Integer userId, String authentication) {
        HashMap<String, Object> map = new HashMap<>();
        Boolean isLogin = MyJWTUtil.verifyToken(authentication);
        Integer loginUserId = null;
        if (isLogin) {
            HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
            loginUserId = (Integer) jwtInfo.get("userId");
        }
        //一级评论
        List<UserCommentDTO> userCommentDTOList = userCommentMapper.findUserIssueCommentLevel1(userId);
        if (CollUtil.isNotEmpty(userCommentDTOList)) {
            for (UserCommentDTO userCommentDTO : userCommentDTOList) {
                //获取一级动态用户头像
                User user = userMapper.selectById(userId);
                userCommentDTO.setUserHeaderImg(user.getHeaderImg());
                //更新当前评论的点赞数量
                userCommentDTO.setLikeNum(userCommentLikesService.updateCommentLikesNum(userCommentDTO.getId()));
                //判断当前用户是否点赞了该评论
                userCommentDTO.setIsUserLike(loginUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(loginUserId, userCommentDTO.getId()));
                userCommentDTO.setUserCommentList(new ArrayList<UserComment>());
                //判断是否存在二级评论
                List<UserComment> userCommentList = userCommentMapper.findUserIssueCommentLevel2(userCommentDTO.getId(), userCommentDTO.getArticleId());
                if (CollUtil.isNotEmpty(userCommentList)) {
                    for (UserComment userComment : userCommentList) {
                        //获取二级用户的头像信息
                        User user1 = userMapper.selectById(userComment.getUserId());
                        //更新当前评论的点赞数量
                        userComment.setLikeNum(userCommentLikesService.updateCommentLikesNum(userComment.getId()));
                        //判断当前用户是否点赞了该评论
                        userComment.setIsUserLike(loginUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(loginUserId, userComment.getId()));
                        userComment.setUserHeaderImg(user1.getHeaderImg());
                        //加入到list中
                        userCommentDTO.getUserCommentList().add(userComment);
                    }
                }
            }
        }
        User user = userMapper.selectById(userId);
        map.put("statusCode", StatusEnum.USER_ISSUE_COMMENT_SUCCESS.getStatusCode());
        map.put("loginUserId", loginUserId);
        map.put("user", Convert.convert(UserDTO.class, user));
        map.put("info", userCommentDTOList);
        return map;
    }

    /**
     * 查询评论
     *
     * @param userCommentDTOList 评论数据
     * @param nowUserId          当前登录用户id
     * @return
     */
    private List<UserCommentDTO> getUserCommentInfo(List<UserCommentDTO> userCommentDTOList, Integer nowUserId) {
        if (CollUtil.isNotEmpty(userCommentDTOList)) {
            for (UserCommentDTO userCommentDTO : userCommentDTOList) {
                //获取一级动态用户头像
                User user = userMapper.selectById(userCommentDTO.getUserId());
                userCommentDTO.setUserHeaderImg(user.getHeaderImg());
                //更新当前评论的点赞数量
                userCommentDTO.setLikeNum(userCommentLikesService.updateCommentLikesNum(userCommentDTO.getId()));
                //判断当前用户是否点赞了该评论
                userCommentDTO.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userCommentDTO.getId()));
                userCommentDTO.setUserCommentList(new ArrayList<UserComment>());
                //判断是否存在二级评论
                List<UserComment> userCommentList = userCommentMapper.findUserIssueCommentLevel2(userCommentDTO.getId(), userCommentDTO.getArticleId());
                if (CollUtil.isNotEmpty(userCommentList)) {
                    for (UserComment userComment : userCommentList) {
                        //获取二级用户的头像信息
                        User user1 = userMapper.selectById(userComment.getUserId());
                        //更新当前评论的点赞数量
                        userComment.setLikeNum(userCommentLikesService.updateCommentLikesNum(userComment.getId()));
                        //判断当前用户是否点赞了该评论
                        userComment.setIsUserLike(nowUserId == null ? Integer.valueOf(0) : userCommentLikesService.isUserLikes(nowUserId, userComment.getId()));
                        userComment.setUserHeaderImg(user1.getHeaderImg());
                        //加入到list中
                        userCommentDTO.getUserCommentList().add(userComment);
                    }
                }
            }
        }
        return userCommentDTOList;
    }

    /**
     * 获取全部用户动态
     *
     * @param authentication
     * @return
     */
    @Override
    public HashMap<String, Object> getAllUserIssue(String authentication) {
        Boolean isLogin = MyJWTUtil.verifyToken(authentication);
        Integer loginUserId = null;
        if (isLogin) {
            HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
            loginUserId = (Integer) jwtInfo.get("userId");
        }
        HashMap<String, Object> map = new HashMap<>();
        //一级评论
        List<UserCommentDTO> userCommentDTOList = userCommentMapper.findAllUserIssue();
        List<UserCommentDTO> userCommentInfo = getUserCommentInfo(userCommentDTOList, loginUserId);
        map.put("info", userCommentInfo);
        map.put("loginUserId", loginUserId == null ? 0 : loginUserId);
        map.put("statusCode", StatusEnum.USER_ISSUE_COMMENT_SUCCESS.getStatusCode());
        return map;
    }

    /**
     * 获取用户评论和动态
     *
     * @param userId
     * @return
     */
    @Override
    public HashMap<String, Object> findUserCommentByUserId(Integer userId) {
        List<UserComment> userComments=userCommentMapper.findUserCommentByUserId(userId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("info", userComments);
        return map;
    }
}




