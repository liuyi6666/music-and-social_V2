package com.music.user.service;

import com.music.pojo.entity.UserCommentLikes;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
 *
 */
public interface UserCommentLikesService {

    HashMap<String, Object> commentLikes(Integer commentId,String authentication);

    Integer updateCommentLikesNum(Integer commentId);

    Integer isUserLikes(Integer userId,Integer commentId);
}
