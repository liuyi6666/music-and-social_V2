package com.music.user.service;

import com.music.pojo.entity.NewMusic;

import java.util.List;

public interface NewMusicService {

    NewMusic getNewMusicByHashAndAlbumId(String hash,String albumId);

    List<NewMusic> findNewMusicByFileName(String searchKey);
}
