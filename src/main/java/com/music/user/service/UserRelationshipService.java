package com.music.user.service;

import com.music.pojo.entity.UserRelationship;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
public interface UserRelationshipService {

    HashMap<String, Object> getUserRelationshipById(Integer id, String authentication);

    HashMap<String, Object> updateUserRelationship(Integer id, String authentication);

    List<UserRelationship> getFollowUserByUserId(Integer nowUserId);

    HashMap<String, Object> getUserRelationship(String authentication);

    UserRelationship getUserRelationshipByToUserIdAndUserId(Integer toUserId, Integer userId);
}
