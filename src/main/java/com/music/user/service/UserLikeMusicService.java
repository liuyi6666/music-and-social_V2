package com.music.user.service;

import com.music.pojo.entity.UserLikeMusic;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface UserLikeMusicService {

    List<UserLikeMusic> findLikeMusicByUserId(Integer userId);

    void saveLikeMusicByUserId(Integer userId, String hash, String albumId);

    UserLikeMusic getLikeMusicByUserIdAndHashAndAlbumId(Integer userId, String hash, String albumId);

    List<UserLikeMusic> findLikeMusicByUserIdAndStatus(Integer userId,Integer status);
}
