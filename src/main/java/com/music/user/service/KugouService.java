package com.music.user.service;

import java.util.HashMap;


public interface KugouService {
    HashMap<String,Object> playMusic(String album_id, String play_url);

    HashMap<String,Object> searchMusic(String keyword);

    HashMap<String,Object> getMusicList();

    HashMap<String,Object> getMusicSheetV2();

    HashMap<String,Object> getHotMusic();

    HashMap<String, Object> getNewMusic(Integer musicType);

    HashMap<String, Object> getSinger(Integer singerType);

    HashMap<String, Object> getMusicListForType(Integer rankId);

    HashMap<String, Object> getAllList();

    HashMap<String,Object> getMusicListForTypeV2(Integer rankId,String token);

    HashMap<String,Object> getNewMusicV2(Integer musicType);

    HashMap<String,Object> getSingerV2(Integer singerType);

    HashMap<String,Object> getHotMusicV2();

    HashMap<String, Object> findMusicSheetByType(Integer type,String token);
}
