package com.music.user.service;


import java.util.HashMap;

/**
 *
 */
public interface UserMusicHistoryService{

    HashMap<String, Object> findUserMusicHistory(String authentication);

    HashMap<String, Object> addUserMusicHistory(String addJson, String authentication);

    Boolean addUserMusicHistoryV2(Integer userId,String hash,String albumId);

    HashMap<String, Object> deleteMusicHistory(String authentication);
}
