package com.music.user.service;

import com.music.pojo.entity.SmsConfig;

public interface SmsConfigService {
    SmsConfig getSmsConfig(String key);
}
