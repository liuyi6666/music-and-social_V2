package com.music.user.service;

import java.util.HashMap;

public interface WalkingService {
    HashMap<String, Object> getHeaderImg(String status);

    HashMap<String, Object> deleteBanner(Integer id);

    HashMap<String, Object> addBanner(String data);
}
