package com.music.user.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

public interface UploadService {

    HashMap<String, Object> uploadSheetImgFile(MultipartFile file, String basePath);

    HashMap<String, Object> uploadUserImgFile(MultipartFile file, String basePath);

    HashMap<String, Object> uploadUserCenterImg(MultipartFile file, String basePath);

    String uploadUserIssueImg(MultipartFile file, String basePath);

    HashMap<String, Object> uploadCommonBannerImg(MultipartFile file,String basePath);
}
