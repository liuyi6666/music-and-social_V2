package com.music.user.service;

import com.music.pojo.entity.UserLikeSinger;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface UserLikeSingerService  {

    void saveUserLikeSingerByUserId(Integer userId, Integer singerId);

    Boolean isUserLikeSingerByUserIdAndSingerId(String authentication, Integer singerId);

    List<UserLikeSinger> findUserLikeSingerByUserIdAndStatus(Integer userId,Integer status);
}
