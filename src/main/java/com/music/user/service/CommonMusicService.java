package com.music.user.service;

import com.music.pojo.dto.CommonMusicDTO;
import com.music.pojo.entity.CommonMusic;

import java.util.HashMap;
import java.util.List;

public interface CommonMusicService {

    void saveMusicByHashAndAlbumId(String hash, String albumId);

    CommonMusic getCommonMusicByHashAndAlbumId(String hash, String albumId);

    List<CommonMusicDTO> findSingerMusicByAuthorId(Integer id, String token);

    List<CommonMusicDTO> findCommonMusicByAlbumId(Integer albumId, String token);

    CommonMusic playMusic(String hash, String albumId);

    CommonMusicDTO playMusicV2(String hash, String albumId, String token);

    CommonMusic findCommonMusicById(Integer ids);

    List<CommonMusic> findCommonMusicBySongName(String searchKey);

    HashMap<String, Object> findAll(Integer start, Integer size, String musicSearchKey);

    CommonMusic updateCommonMusic(String hash, String albumId);

    HashMap<String, Object> importMusic(String musicList);

    HashMap<String, Object> deleteMusic(Integer id);
}
