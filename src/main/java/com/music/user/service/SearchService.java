package com.music.user.service;

import java.util.HashMap;

public interface SearchService {
    HashMap<String, Object> searchMusic(String searchKey);

    HashMap<String, Object> searchSinger(String searchKey);

    HashMap<String, Object> searchUser(String searchKey);

    HashMap<String, Object> searchSheet(String searchKey);
}
