package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName user_access_token
 */
@TableName(value ="user_access_token")
@Data
@Accessors(chain = true)
public class UserAccessToken implements Serializable {
    private static final long serialVersionUID = 1819208837487768554L;
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 令牌
     */
    private String token;

    /**
     * 
     */
    private Date createTime;

    /**
     * 用户在线状态 0离线 1在线
     */
    private Integer status;

    /**
     *
     */
    private Date updateTime;

}