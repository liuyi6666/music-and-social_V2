package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName music_singer_detail
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="music_singer_detail")
@Data
@Accessors(chain = true)
public class MusicSingerDetail extends Base {
    private static final long serialVersionUID = 2338938255100216477L;
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 歌手id
     */
    private Integer singerId;

    /**
     * 歌手名字
     */
    private String singerName;

    /**
     * 简介
     */
    private String intro;

    /**
     * 简介
     */
    private String profile;

    /**
     * mv数量
     */
    private Integer mvCount;

    /**
     * 歌曲数量
     */
    private Integer songCount;

    /**
     * 专辑数量
     */
    private Integer albumCount;

    /**
     * 歌手头像
     */
    private String imgUrl;

}