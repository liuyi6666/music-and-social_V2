package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName user_relationship
 */
@TableName(value ="user_relationship")
@Data
@Accessors(chain = true)
public class UserRelationship implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 粉丝id
     */
    private Integer fanId;

    /**
     * 状态 0取消关注 1关注
     */
    private Integer status;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 相互关注后生成会话Id
     */
    private String sessionId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}