package com.music.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;

/**
 *
 * @TableName user_comment
 */
@Data
@Accessors(chain = true)
public class UserComment implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 上级级评论id
     */
    private Integer parentId;

    /**
     * 上一级评论用户id
     */
    private Integer parentUserId;

    /**
     * 上一级评论用户名
     */
    private String parentUserName;

    /**
     * 动态或者歌曲下id
     */
    private Long articleId;

    /**
     * 动态或者歌曲名
     */
    private String articleTitle;

    /**
     * 被回复的评论id
     */
    private Integer replyCommentId;

    /**
     * 被回复的评论的用户id
     */
    private Integer replyCommentUserId;

    /**
     * 被回复的评论的用户名
     */
    private String replyCommentUserName;

    /**
     * 评论级别，回复的歌曲或者动态的是一级评论，其他的都是2级
     */
    private Integer commentLevel;

    /**
     * 0是歌曲评论，1是动态评论
     */
    private Integer commentType;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论状态
     */
    private Integer status;

    /**
     * 评论点赞数
     */
    private Integer likeNum;

    /**
     * 评论时间
     */
    private Date createTime;

    /**
     * 评论更新时间比如点赞
     */
    private Date updateTime;

    /**
     * 歌曲hash
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    //用户头像
    @Transient
    @TableField(exist = false)
    private String userHeaderImg;

    @Transient
    @TableField(exist = false)
    private Integer isUserLike;

     /*
    ========================前端需要参数=========================
     */
    /**
     * 评论功能是否显示
     */
    @Transient
    @TableField(exist = false)
    private String isShowComment = "none";

    /**
     * 输入框
     */
    @Transient
    @TableField(exist = false)
    private String inputText= "";

    private static final long serialVersionUID = 1L;
}
