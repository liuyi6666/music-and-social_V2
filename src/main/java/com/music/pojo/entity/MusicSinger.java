package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName music_singer
 */
@TableName(value ="music_singer")
@Data
public class MusicSinger implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 歌手Id
     */
    @TableId
    private Integer singerId;

    /**
     * 歌手名字
     */
    private String singerName;

    /**
     * 歌手热度
     */
    private Integer heat;

    /**
     * 歌手头像
     */
    private String imgUrl;

    /**
     * 性别类型  1男 2女 3组合
     */
    private Integer sexType;

    /**
     * 歌手类型 1华语 2欧美 3日韩 4其他 5日本 6韩国
     */
    private Integer type;

    /**
     * 粉丝数 数据来源酷狗
     */
    private Integer fansCount;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}