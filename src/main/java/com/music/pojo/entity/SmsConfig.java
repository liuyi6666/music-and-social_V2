package com.music.pojo.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 *
 * @TableName sms_config
 */
@Data
@Accessors(chain = true)
public class SmsConfig implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 模板key
     */
    private String templateKey;

    /**
     * 模板类型
     */
    private String smsType;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 描述
     */
    private String description;

    /**
     * 参数个数
     */
    private Integer paramCount;

    private static final long serialVersionUID = 1L;
}
