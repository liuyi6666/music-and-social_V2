package com.music.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;

/**
 *
 * @TableName user_like_music
 */
@Data
@Accessors(chain = true)
public class UserLikeMusic implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户主键id
     */
    private Integer userId;

    /**
     * hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    private Integer status;

    @Transient
    @TableField(exist = false)
    private String musicName;

    private static final long serialVersionUID = 1L;
}
