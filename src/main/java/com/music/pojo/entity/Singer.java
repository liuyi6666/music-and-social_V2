package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("music_singer")
public class Singer extends Base {

    private static final long serialVersionUID = 7648182057691266675L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //粉丝数
    private Integer fansCount;

    //歌手ID
    private Integer singerId;

    //歌手名字
    private String singerName;

    //歌手热度
    private Integer heat;

    //歌手头像
    private String imgUrl;

    //性别类型
    private Integer sexType;

    //歌手类型
    private Integer type;
}
