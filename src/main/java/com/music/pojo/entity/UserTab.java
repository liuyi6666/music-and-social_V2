package com.music.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 *
 * @TableName user_tab
 */
@Data
@Accessors(chain = true)
public class UserTab implements Serializable {
    /**
     * 标签id
     */
    @TableId(value = "tab_id", type = IdType.AUTO)
    private Integer tabId;

    /**
     * 标签名
     */
    private String tabName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}
