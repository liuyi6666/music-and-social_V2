package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;


/**
 *
 * @TableName ranking_list_music
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("ranking_list_music")
@Accessors(chain = true)
public class RankingListMusic extends Base {
    private static final long serialVersionUID = -2192077469951084351L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件大小
     */
    private Integer fileSize;

    /**
     * 专辑封面
     */
    private String albumSizableCover;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 歌曲名
     */
    private String fileName;

    /**
     * 歌曲hash值
     */
    private String hash;

    /**
     * 添加到排行榜的事件
     */
    private String addTime;

    /**
     * 专辑ID
     */
    private String albumId;


    /**
     * 最新期数
     */
    private Integer rankCid;

    /**
     * 排行榜Id
     */
    private Integer rankId;

    /**
     * 音乐时长
     */
    @Transient
    private Integer timeLength;
}
