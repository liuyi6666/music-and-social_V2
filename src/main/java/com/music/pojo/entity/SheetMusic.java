package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName sheet_music
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="sheet_music")
@Data
@Accessors(chain = true)
public class SheetMusic extends Base {
    private static final long serialVersionUID = 6057596067961490062L;
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 歌曲hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 歌曲名
     */
    private String fileName;

    /**
     * 歌曲id
     */
    private Integer audioId;

    /**
     * 歌曲文件大小
     */
    private Integer fileSize;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 歌单id
     */
    private Integer specialId;

}