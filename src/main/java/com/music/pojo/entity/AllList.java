package com.music.pojo.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("music_list")
public class AllList extends Base {
    private static final long serialVersionUID = 4477684163881557092L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //榜单id
    private Integer rankId;

    //榜单热度
    private Integer playTimes;

    //榜单信息
    private String intro;

    //榜单名
    private String rankName;

    //最新期数的volid值
    private Integer rankCid;

    //榜单图片
    private String img;

}
