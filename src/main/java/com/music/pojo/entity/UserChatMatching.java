package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 *
 * @TableName user_chat_matching
 */
@TableName(value ="user_chat_matching")
@Data
@Accessors(chain = true)
public class UserChatMatching implements Serializable {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 匹配到的用户id
     */
    private Integer matchingUserId;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Date updateTime;

    /**
     * 匹配会话id
     */
    private String matchingSessionId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
