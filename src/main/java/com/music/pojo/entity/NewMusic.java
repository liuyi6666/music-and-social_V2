package com.music.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 * @TableName new_music
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("new_music")
@Accessors(chain = true)
public class NewMusic extends Base {
    private static final long serialVersionUID = -4824082707568274689L;
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件大小
     */
    private Integer fileSize;

    /**
     * 歌曲名字
     */
    private String fileName;

    /**
     * 歌曲id
     */
    private Integer audioId;

    /**
     * 专辑图片
     */
    private String albumCover;

    /**
     * hash值
     */
    private String hash;

    /**
     * 添加时间
     */
    private String addTime;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 新歌类型 1华语 2欧美 3日韩
     */
    private Integer type;

}
