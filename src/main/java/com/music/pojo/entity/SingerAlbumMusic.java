package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName singer_album_music
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="singer_album_music")
@Data
@Accessors(chain = true)
public class SingerAlbumMusic extends Base {
    private static final long serialVersionUID = -7666661139666829686L;
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 歌曲id
     */
    private Integer audioId;

    /**
     * 歌曲大小
     */
    private Integer fileSize;

    /**
     * 简介
     */
    private String remark;

    /**
     * 歌曲名
     */
    private String fileName;

}