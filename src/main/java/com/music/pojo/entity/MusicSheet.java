package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("music_sheet")
@Accessors(chain = true)
public class MusicSheet extends Base {

    private static final long serialVersionUID = -1118645813689387475L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //歌单id
    private Integer specialId;

    //播放数量
    private Integer playCount;

    //歌单提交时间
    private String publishTime;

    //创建歌单用户Id
    private Integer userId;

    //创建歌单用户
    private String userName;

    //歌单名
    private String specialName;

    //用户头像地址
    private String userAvatar;

    //歌单简介
    private String intro;

    //歌单图片地址
    private String imgUrl;

    //是否为酷狗用户提交的歌单
    private Integer isKugouUser;
}
