package com.music.pojo.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class Base implements Serializable {
    private static final long serialVersionUID = -6356749491972875297L;
    private Date createTime;
    private Date updateTime;
}
