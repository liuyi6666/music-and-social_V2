package com.music.pojo.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @TableName user
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class User extends Base {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    private String username;

    /**
     *
     */
    private String email;

    /**
     *
     */
    private String password;

    /**
     *
     */
    private String sex;

    /**
     *
     */
    private Integer age;


    /**
     * 头像
     */
    private String headerImg;

    /**
     *
     */
    private String musicHobby;

    /**
     *
     */
    private Integer status;

    /**
     *
     */
    private String birthday;

    /**
     * 主页背景
     */
    private String personalCenterImg;

    /**
     * 个性签名
     */
    private String personalSignature;


    private static final long serialVersionUID = 1L;

}
