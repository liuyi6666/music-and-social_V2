package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @TableName user_comment_check
 */
@TableName(value = "user_comment_check")
@Data
@Accessors(chain = true)
public class UserCommentCheck implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     *
     */
    private Integer userId;

    /**
     *
     */
    private String username;

    /**
     * 评论内容
     */
    private String context;

    /**
     * 检测标签
     */
    private String label;

    /**
     * 百分比
     */
    private String rate;

    /**
     * 检测建议
     */
    private String suggestion;

    /**
     * 违规字段
     */
    private String sensitiveContexts;

    /**
     * 用户token
     */
    private String authentication;

    /**
     * 用户评论详细信息
     */
    private String contextInfo;

    /**
     * 评论来源标识
     */
    private Integer commentFromFlag;

    private Date createTime;

    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
