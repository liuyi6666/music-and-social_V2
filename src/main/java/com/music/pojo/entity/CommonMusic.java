package com.music.pojo.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *
 * @TableName common_music
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("common_music")
@Accessors(chain = true)
public class CommonMusic extends Base {
    private static final long serialVersionUID = -5406383146311955442L;
    /**
     *
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 歌曲时长
     */
    private Integer timeLength;

    /**
     * 歌曲大小
     */
    private Integer fileSize;

    /**
     * 歌曲名
     */
    private String audioName;

    /**
     * 专辑名
     */
    private String albumName;

    /**
     * 歌曲封面
     */
    private String img;

    /**
     * 歌手名
     */
    private String authorName;

    /**
     * 歌曲名
     */
    private String songName;

    /**
     * 歌词
     */
    private String lyrics;

    /**
     * 歌手id
     */
    private String authorId;

    /**
     * 播放地址
     */
    private String playUrl;

    /**
     * 歌曲id
     */
    private String audioId;

    /**
     * 播放备用地址
     */
    private String playBackupUrl;

    /**
     * 专辑歌曲id
     */
    private Integer albumAudioId;

    /**
     * 已经播放次数
     */
    private Integer playCount;
}
