package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @TableName singer_album
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="singer_album")
@Accessors(chain = true)
@Data
public class SingerAlbum extends Base {
    private static final long serialVersionUID = -3490006575342558192L;
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 专辑名
     */
    private String albumName;

    /**
     * 专辑信息
     */
    private String intro;

    /**
     * 添加时间
     */
    private String publishTime;

    /**
     * 歌手名称
     */
    private String singerName;

    /**
     * 专辑id
     */
    private Integer albumId;

    /**
     * 歌手id
     */
    private Integer singerId;

    /**
     * 专辑图片
     */
    private String imgUrl;


}