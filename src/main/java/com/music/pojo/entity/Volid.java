package com.music.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("music_volid")
public class Volid extends Base {
    private static final long serialVersionUID = -5825466073253402576L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //期数id
    private Integer volid;

    //最新一期的volid值
    private Integer rank_cid;

    //期数名
    private String volName;

    //期数标题
    private String volTitle;

    //榜单id
    private Integer rankId;

    //年份
    private Integer year;
}
