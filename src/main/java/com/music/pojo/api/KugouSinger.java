package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

public class KugouSinger implements Serializable {
    private static final long serialVersionUID = 5347164873352982209L;

    /**
     * JS_CSS_DATE : 20130320
     * kg_domain : https://m.kugou.com
     * src : http://downmobile.kugou.com/promote/package/download/channel=6
     * fr : null
     * ver : v3
     * classid : 1
     * classname : 华语男歌手
     * singers : {"page":1,"pagesize":30,"total":null,"list":{"timestamp":1639615971,"total":189185,"enu_list":{"types":[{"key":4,"value":"其他"},{"key":3,"value":"日韩"},{"key":5,"value":"日本"},{"key":2,"value":"欧美"},{"key":6,"value":"韩国"},{"key":1,"value":"华语"},{"key":0,"value":"全部"}],"sextypes":[{"key":2,"value":"女"},{"key":3,"value":"组合"},{"key":1,"value":"男"},{"key":0,"value":"全部"}]},"info":[{"mvcount":0,"fanscount":13058992,"descibe":"6123条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3520%2F13058930","sortoffset":0,"singerid":3520,"songcount":0,"banner_url":"","singername":"周杰伦","is_settled":0,"heatoffset":0,"heat":203624,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3520/13058930","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg"},{"mvcount":0,"fanscount":8477973,"descibe":"1.7万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1574%2F8477948","sortoffset":1,"singerid":1574,"songcount":0,"banner_url":"","singername":"林俊杰","is_settled":1,"heatoffset":0,"heat":102308,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1574/8477948","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210328/20210328211239904.jpg"},{"mvcount":0,"fanscount":15329717,"descibe":"6万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3060%2F15329701","sortoffset":-6,"singerid":3060,"songcount":0,"banner_url":"","singername":"薛之谦","is_settled":1,"heatoffset":0,"heat":79247,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3060/15329701","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200603/20200603112123228.jpg"},{"mvcount":0,"fanscount":4046895,"descibe":"8094条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F722869%2F4046880","sortoffset":-39,"singerid":722869,"songcount":0,"banner_url":"","singername":"毛不易","is_settled":1,"heatoffset":2,"heat":57037,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/722869/4046880","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210621/20210621142437964.jpg"},{"mvcount":0,"fanscount":3145024,"descibe":"3.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F548441%2F3144966","sortoffset":2,"singerid":548441,"songcount":0,"banner_url":"","singername":"海来阿木","is_settled":1,"heatoffset":-1,"heat":53429,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/548441/3144966","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210525/20210525110634207124.jpg"},{"mvcount":0,"fanscount":4288752,"descibe":"8055条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F420%2F4288727","sortoffset":66,"singerid":420,"songcount":0,"banner_url":"","singername":"陈奕迅","is_settled":1,"heatoffset":-1,"heat":51879,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/420/4288727","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210623/20210623182605222.jpg"},{"mvcount":0,"fanscount":4077730,"descibe":"843条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3521%2F4077698","sortoffset":280,"singerid":3521,"songcount":0,"banner_url":"","singername":"张学友","is_settled":0,"heatoffset":0,"heat":50492,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3521/4077698","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200706/20200706184106791.jpg"},{"mvcount":0,"fanscount":2556252,"descibe":"13.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F169967%2F2556216","sortoffset":12,"singerid":169967,"songcount":0,"banner_url":"","singername":"周深","is_settled":1,"heatoffset":-1,"heat":48465,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/169967/2556216","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg"},{"mvcount":0,"fanscount":8950295,"descibe":"1.2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3539%2F8950281","sortoffset":-12,"singerid":3539,"songcount":0,"banner_url":"","singername":"张杰","is_settled":1,"heatoffset":-1,"heat":48101,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3539/8950281","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210510/20210510095204419.jpg"},{"mvcount":0,"fanscount":3358624,"descibe":"1.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3047%2F3358625","sortoffset":39,"singerid":3047,"songcount":0,"banner_url":"","singername":"许嵩","is_settled":1,"heatoffset":0,"heat":45763,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3047/3358625","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180906/20180906181115284.jpg"},{"mvcount":0,"fanscount":4283899,"descibe":"1.9万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1573%2F4283893","sortoffset":0,"singerid":1573,"songcount":0,"banner_url":"","singername":"刘德华","is_settled":1,"heatoffset":-2,"heat":44352,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1573/4283893","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210816/20210816164409933.jpg"},{"mvcount":0,"fanscount":1126384,"descibe":"764条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2725%2F1126375","sortoffset":-6,"singerid":2725,"songcount":0,"banner_url":"","singername":"王杰","is_settled":0,"heatoffset":-1,"heat":37221,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2725/1126375","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160910/20160910145826790.jpg"},{"mvcount":0,"fanscount":829956,"descibe":"791条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1383%2F829955","sortoffset":-20,"singerid":1383,"songcount":0,"banner_url":"","singername":"黄家驹","is_settled":0,"heatoffset":0,"heat":31201,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1383/829955","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20140219/20140219104100727871.jpg"},{"mvcount":0,"fanscount":1969240,"descibe":"4.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F753317%2F1969213","sortoffset":-9,"singerid":753317,"songcount":0,"banner_url":"","singername":"小阿枫","is_settled":1,"heatoffset":-1,"heat":29993,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/753317/1969213","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211105/20211105161913836.jpg"},{"mvcount":0,"fanscount":3227689,"descibe":"1539条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F730%2F3227679","sortoffset":-336,"singerid":730,"songcount":0,"banner_url":"","singername":"刀郎","is_settled":0,"heatoffset":1,"heat":29607,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/730/3227679","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20190115/20190115150401884.jpg"},{"mvcount":0,"fanscount":1786781,"descibe":"2.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3538%2F1786761","sortoffset":27,"singerid":3538,"songcount":0,"banner_url":"","singername":"郑源","is_settled":1,"heatoffset":3,"heat":28242,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3538/1786761","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210325/20210325155813732.jpg"},{"mvcount":0,"fanscount":1458773,"descibe":"763条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3525%2F1458772","sortoffset":-52,"singerid":3525,"songcount":0,"banner_url":"","singername":"张信哲","is_settled":0,"heatoffset":-1,"heat":26420,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3525/1458772","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211202/20211202152848469345.jpg"},{"mvcount":0,"fanscount":3699709,"descibe":"2950条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F93475%2F3699702","sortoffset":0,"singerid":93475,"songcount":0,"banner_url":"","singername":"李荣浩","is_settled":1,"heatoffset":-1,"heat":26170,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/93475/3699702","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20191209/20191209164452855.jpg"},{"mvcount":0,"fanscount":1220007,"descibe":"1万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3534%2F1219992","sortoffset":-23,"singerid":3534,"songcount":0,"banner_url":"","singername":"周传雄","is_settled":1,"heatoffset":0,"heat":25380,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3534/1219992","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210520/20210520142503390.jpg"},{"mvcount":0,"fanscount":4229496,"descibe":"2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1485%2F4229490","sortoffset":0,"singerid":1485,"songcount":0,"banner_url":"","singername":"汪苏泷","is_settled":1,"heatoffset":2,"heat":24930,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1485/4229490","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201125/20201125182034721.jpg"},{"mvcount":0,"fanscount":514937,"descibe":"159条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1595%2F514933","sortoffset":0,"singerid":1595,"songcount":0,"banner_url":"","singername":"林子祥","is_settled":0,"heatoffset":-2,"heat":24487,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1595/514933","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210208/20210208134415204.jpg"},{"mvcount":0,"fanscount":443323,"descibe":"58条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2728%2F443322","sortoffset":53,"singerid":2728,"songcount":0,"banner_url":"","singername":"伍佰","is_settled":1,"heatoffset":0,"heat":23033,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2728/443322","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200910/20200910112958425.jpg"},{"mvcount":0,"fanscount":1143300,"descibe":"478条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3043%2F1143295","sortoffset":120,"singerid":3043,"songcount":0,"banner_url":"","singername":"许巍","is_settled":0,"heatoffset":2,"heat":22899,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3043/1143295","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201020/20201020101833509.jpg"},{"mvcount":0,"fanscount":757747,"descibe":"3290条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F556720%2F757723","sortoffset":434,"singerid":556720,"songcount":0,"banner_url":"","singername":"严浩翔","is_settled":1,"heatoffset":292,"heat":22754,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/556720/757723","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115111609320.jpg"},{"mvcount":0,"fanscount":1117543,"descibe":"393条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1576%2F1117536","sortoffset":-39,"singerid":1576,"songcount":0,"banner_url":"","singername":"李宗盛","is_settled":0,"heatoffset":-2,"heat":21815,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1576/1117536","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210125/20210125133627693.jpg"},{"mvcount":0,"fanscount":921995,"descibe":"145条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3537%2F921987","sortoffset":-12,"singerid":3537,"songcount":0,"banner_url":"","singername":"张宇","is_settled":0,"heatoffset":0,"heat":20298,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3537/921987","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210202/20210202131408586.jpg"},{"mvcount":0,"fanscount":1067514,"descibe":"389条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2627%2F1067508","sortoffset":20,"singerid":2627,"songcount":0,"banner_url":"","singername":"谭咏麟","is_settled":0,"heatoffset":1,"heat":20150,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2627/1067508","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115101136412.jpg"},{"mvcount":0,"fanscount":1118386,"descibe":"4033条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2404%2F1118372","sortoffset":0,"singerid":2404,"songcount":0,"banner_url":"","singername":"任贤齐","is_settled":1,"heatoffset":-1,"heat":20138,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2404/1118372","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200502/20200502105138263.jpg"},{"mvcount":0,"fanscount":1218316,"descibe":"1350条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3523%2F1218307","sortoffset":270,"singerid":3523,"songcount":0,"banner_url":"","singername":"张国荣","is_settled":0,"heatoffset":4,"heat":20080,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3523/1218307","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160909/20160909203241500.jpg"},{"mvcount":0,"fanscount":5391241,"descibe":"5.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F90875%2F5391242","sortoffset":2,"singerid":90875,"songcount":0,"banner_url":"","singername":"华晨宇","is_settled":1,"heatoffset":2,"heat":20059,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/90875/5391242","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211009/20211009174756159.jpg"}]}}
     * pagesize : 30
     * __Tpl : singer/list.html
     */

    private int JS_CSS_DATE;
    private String kg_domain;
    private String src;
    private Object fr;
    private String ver;
    private int classid;
    private String classname;
    private SingersBean singers;
    private int pagesize;
    private String __Tpl;

    public int getJS_CSS_DATE() {
        return JS_CSS_DATE;
    }

    public void setJS_CSS_DATE(int JS_CSS_DATE) {
        this.JS_CSS_DATE = JS_CSS_DATE;
    }

    public String getKg_domain() {
        return kg_domain;
    }

    public void setKg_domain(String kg_domain) {
        this.kg_domain = kg_domain;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Object getFr() {
        return fr;
    }

    public void setFr(Object fr) {
        this.fr = fr;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public SingersBean getSingers() {
        return singers;
    }

    public void setSingers(SingersBean singers) {
        this.singers = singers;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public String get__Tpl() {
        return __Tpl;
    }

    public void set__Tpl(String __Tpl) {
        this.__Tpl = __Tpl;
    }

    public static class SingersBean {
        /**
         * page : 1
         * pagesize : 30
         * total : null
         * list : {"timestamp":1639615971,"total":189185,"enu_list":{"types":[{"key":4,"value":"其他"},{"key":3,"value":"日韩"},{"key":5,"value":"日本"},{"key":2,"value":"欧美"},{"key":6,"value":"韩国"},{"key":1,"value":"华语"},{"key":0,"value":"全部"}],"sextypes":[{"key":2,"value":"女"},{"key":3,"value":"组合"},{"key":1,"value":"男"},{"key":0,"value":"全部"}]},"info":[{"mvcount":0,"fanscount":13058992,"descibe":"6123条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3520%2F13058930","sortoffset":0,"singerid":3520,"songcount":0,"banner_url":"","singername":"周杰伦","is_settled":0,"heatoffset":0,"heat":203624,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3520/13058930","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg"},{"mvcount":0,"fanscount":8477973,"descibe":"1.7万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1574%2F8477948","sortoffset":1,"singerid":1574,"songcount":0,"banner_url":"","singername":"林俊杰","is_settled":1,"heatoffset":0,"heat":102308,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1574/8477948","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210328/20210328211239904.jpg"},{"mvcount":0,"fanscount":15329717,"descibe":"6万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3060%2F15329701","sortoffset":-6,"singerid":3060,"songcount":0,"banner_url":"","singername":"薛之谦","is_settled":1,"heatoffset":0,"heat":79247,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3060/15329701","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200603/20200603112123228.jpg"},{"mvcount":0,"fanscount":4046895,"descibe":"8094条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F722869%2F4046880","sortoffset":-39,"singerid":722869,"songcount":0,"banner_url":"","singername":"毛不易","is_settled":1,"heatoffset":2,"heat":57037,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/722869/4046880","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210621/20210621142437964.jpg"},{"mvcount":0,"fanscount":3145024,"descibe":"3.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F548441%2F3144966","sortoffset":2,"singerid":548441,"songcount":0,"banner_url":"","singername":"海来阿木","is_settled":1,"heatoffset":-1,"heat":53429,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/548441/3144966","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210525/20210525110634207124.jpg"},{"mvcount":0,"fanscount":4288752,"descibe":"8055条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F420%2F4288727","sortoffset":66,"singerid":420,"songcount":0,"banner_url":"","singername":"陈奕迅","is_settled":1,"heatoffset":-1,"heat":51879,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/420/4288727","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210623/20210623182605222.jpg"},{"mvcount":0,"fanscount":4077730,"descibe":"843条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3521%2F4077698","sortoffset":280,"singerid":3521,"songcount":0,"banner_url":"","singername":"张学友","is_settled":0,"heatoffset":0,"heat":50492,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3521/4077698","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200706/20200706184106791.jpg"},{"mvcount":0,"fanscount":2556252,"descibe":"13.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F169967%2F2556216","sortoffset":12,"singerid":169967,"songcount":0,"banner_url":"","singername":"周深","is_settled":1,"heatoffset":-1,"heat":48465,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/169967/2556216","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg"},{"mvcount":0,"fanscount":8950295,"descibe":"1.2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3539%2F8950281","sortoffset":-12,"singerid":3539,"songcount":0,"banner_url":"","singername":"张杰","is_settled":1,"heatoffset":-1,"heat":48101,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3539/8950281","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210510/20210510095204419.jpg"},{"mvcount":0,"fanscount":3358624,"descibe":"1.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3047%2F3358625","sortoffset":39,"singerid":3047,"songcount":0,"banner_url":"","singername":"许嵩","is_settled":1,"heatoffset":0,"heat":45763,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3047/3358625","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180906/20180906181115284.jpg"},{"mvcount":0,"fanscount":4283899,"descibe":"1.9万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1573%2F4283893","sortoffset":0,"singerid":1573,"songcount":0,"banner_url":"","singername":"刘德华","is_settled":1,"heatoffset":-2,"heat":44352,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1573/4283893","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210816/20210816164409933.jpg"},{"mvcount":0,"fanscount":1126384,"descibe":"764条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2725%2F1126375","sortoffset":-6,"singerid":2725,"songcount":0,"banner_url":"","singername":"王杰","is_settled":0,"heatoffset":-1,"heat":37221,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2725/1126375","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160910/20160910145826790.jpg"},{"mvcount":0,"fanscount":829956,"descibe":"791条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1383%2F829955","sortoffset":-20,"singerid":1383,"songcount":0,"banner_url":"","singername":"黄家驹","is_settled":0,"heatoffset":0,"heat":31201,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1383/829955","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20140219/20140219104100727871.jpg"},{"mvcount":0,"fanscount":1969240,"descibe":"4.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F753317%2F1969213","sortoffset":-9,"singerid":753317,"songcount":0,"banner_url":"","singername":"小阿枫","is_settled":1,"heatoffset":-1,"heat":29993,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/753317/1969213","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211105/20211105161913836.jpg"},{"mvcount":0,"fanscount":3227689,"descibe":"1539条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F730%2F3227679","sortoffset":-336,"singerid":730,"songcount":0,"banner_url":"","singername":"刀郎","is_settled":0,"heatoffset":1,"heat":29607,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/730/3227679","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20190115/20190115150401884.jpg"},{"mvcount":0,"fanscount":1786781,"descibe":"2.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3538%2F1786761","sortoffset":27,"singerid":3538,"songcount":0,"banner_url":"","singername":"郑源","is_settled":1,"heatoffset":3,"heat":28242,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3538/1786761","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210325/20210325155813732.jpg"},{"mvcount":0,"fanscount":1458773,"descibe":"763条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3525%2F1458772","sortoffset":-52,"singerid":3525,"songcount":0,"banner_url":"","singername":"张信哲","is_settled":0,"heatoffset":-1,"heat":26420,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3525/1458772","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211202/20211202152848469345.jpg"},{"mvcount":0,"fanscount":3699709,"descibe":"2950条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F93475%2F3699702","sortoffset":0,"singerid":93475,"songcount":0,"banner_url":"","singername":"李荣浩","is_settled":1,"heatoffset":-1,"heat":26170,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/93475/3699702","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20191209/20191209164452855.jpg"},{"mvcount":0,"fanscount":1220007,"descibe":"1万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3534%2F1219992","sortoffset":-23,"singerid":3534,"songcount":0,"banner_url":"","singername":"周传雄","is_settled":1,"heatoffset":0,"heat":25380,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3534/1219992","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210520/20210520142503390.jpg"},{"mvcount":0,"fanscount":4229496,"descibe":"2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1485%2F4229490","sortoffset":0,"singerid":1485,"songcount":0,"banner_url":"","singername":"汪苏泷","is_settled":1,"heatoffset":2,"heat":24930,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1485/4229490","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201125/20201125182034721.jpg"},{"mvcount":0,"fanscount":514937,"descibe":"159条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1595%2F514933","sortoffset":0,"singerid":1595,"songcount":0,"banner_url":"","singername":"林子祥","is_settled":0,"heatoffset":-2,"heat":24487,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1595/514933","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210208/20210208134415204.jpg"},{"mvcount":0,"fanscount":443323,"descibe":"58条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2728%2F443322","sortoffset":53,"singerid":2728,"songcount":0,"banner_url":"","singername":"伍佰","is_settled":1,"heatoffset":0,"heat":23033,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2728/443322","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200910/20200910112958425.jpg"},{"mvcount":0,"fanscount":1143300,"descibe":"478条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3043%2F1143295","sortoffset":120,"singerid":3043,"songcount":0,"banner_url":"","singername":"许巍","is_settled":0,"heatoffset":2,"heat":22899,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3043/1143295","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201020/20201020101833509.jpg"},{"mvcount":0,"fanscount":757747,"descibe":"3290条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F556720%2F757723","sortoffset":434,"singerid":556720,"songcount":0,"banner_url":"","singername":"严浩翔","is_settled":1,"heatoffset":292,"heat":22754,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/556720/757723","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115111609320.jpg"},{"mvcount":0,"fanscount":1117543,"descibe":"393条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1576%2F1117536","sortoffset":-39,"singerid":1576,"songcount":0,"banner_url":"","singername":"李宗盛","is_settled":0,"heatoffset":-2,"heat":21815,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1576/1117536","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210125/20210125133627693.jpg"},{"mvcount":0,"fanscount":921995,"descibe":"145条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3537%2F921987","sortoffset":-12,"singerid":3537,"songcount":0,"banner_url":"","singername":"张宇","is_settled":0,"heatoffset":0,"heat":20298,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3537/921987","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210202/20210202131408586.jpg"},{"mvcount":0,"fanscount":1067514,"descibe":"389条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2627%2F1067508","sortoffset":20,"singerid":2627,"songcount":0,"banner_url":"","singername":"谭咏麟","is_settled":0,"heatoffset":1,"heat":20150,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2627/1067508","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115101136412.jpg"},{"mvcount":0,"fanscount":1118386,"descibe":"4033条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2404%2F1118372","sortoffset":0,"singerid":2404,"songcount":0,"banner_url":"","singername":"任贤齐","is_settled":1,"heatoffset":-1,"heat":20138,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2404/1118372","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200502/20200502105138263.jpg"},{"mvcount":0,"fanscount":1218316,"descibe":"1350条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3523%2F1218307","sortoffset":270,"singerid":3523,"songcount":0,"banner_url":"","singername":"张国荣","is_settled":0,"heatoffset":4,"heat":20080,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3523/1218307","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160909/20160909203241500.jpg"},{"mvcount":0,"fanscount":5391241,"descibe":"5.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F90875%2F5391242","sortoffset":2,"singerid":90875,"songcount":0,"banner_url":"","singername":"华晨宇","is_settled":1,"heatoffset":2,"heat":20059,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/90875/5391242","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211009/20211009174756159.jpg"}]}
         */

        private int page;
        private int pagesize;
        private Object total;
        private ListBean list;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPagesize() {
            return pagesize;
        }

        public void setPagesize(int pagesize) {
            this.pagesize = pagesize;
        }

        public Object getTotal() {
            return total;
        }

        public void setTotal(Object total) {
            this.total = total;
        }

        public ListBean getList() {
            return list;
        }

        public void setList(ListBean list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * timestamp : 1639615971
             * total : 189185
             * enu_list : {"types":[{"key":4,"value":"其他"},{"key":3,"value":"日韩"},{"key":5,"value":"日本"},{"key":2,"value":"欧美"},{"key":6,"value":"韩国"},{"key":1,"value":"华语"},{"key":0,"value":"全部"}],"sextypes":[{"key":2,"value":"女"},{"key":3,"value":"组合"},{"key":1,"value":"男"},{"key":0,"value":"全部"}]}
             * info : [{"mvcount":0,"fanscount":13058992,"descibe":"6123条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3520%2F13058930","sortoffset":0,"singerid":3520,"songcount":0,"banner_url":"","singername":"周杰伦","is_settled":0,"heatoffset":0,"heat":203624,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3520/13058930","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg"},{"mvcount":0,"fanscount":8477973,"descibe":"1.7万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1574%2F8477948","sortoffset":1,"singerid":1574,"songcount":0,"banner_url":"","singername":"林俊杰","is_settled":1,"heatoffset":0,"heat":102308,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1574/8477948","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210328/20210328211239904.jpg"},{"mvcount":0,"fanscount":15329717,"descibe":"6万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3060%2F15329701","sortoffset":-6,"singerid":3060,"songcount":0,"banner_url":"","singername":"薛之谦","is_settled":1,"heatoffset":0,"heat":79247,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3060/15329701","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200603/20200603112123228.jpg"},{"mvcount":0,"fanscount":4046895,"descibe":"8094条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F722869%2F4046880","sortoffset":-39,"singerid":722869,"songcount":0,"banner_url":"","singername":"毛不易","is_settled":1,"heatoffset":2,"heat":57037,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/722869/4046880","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210621/20210621142437964.jpg"},{"mvcount":0,"fanscount":3145024,"descibe":"3.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F548441%2F3144966","sortoffset":2,"singerid":548441,"songcount":0,"banner_url":"","singername":"海来阿木","is_settled":1,"heatoffset":-1,"heat":53429,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/548441/3144966","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210525/20210525110634207124.jpg"},{"mvcount":0,"fanscount":4288752,"descibe":"8055条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F420%2F4288727","sortoffset":66,"singerid":420,"songcount":0,"banner_url":"","singername":"陈奕迅","is_settled":1,"heatoffset":-1,"heat":51879,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/420/4288727","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210623/20210623182605222.jpg"},{"mvcount":0,"fanscount":4077730,"descibe":"843条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3521%2F4077698","sortoffset":280,"singerid":3521,"songcount":0,"banner_url":"","singername":"张学友","is_settled":0,"heatoffset":0,"heat":50492,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3521/4077698","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200706/20200706184106791.jpg"},{"mvcount":0,"fanscount":2556252,"descibe":"13.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F169967%2F2556216","sortoffset":12,"singerid":169967,"songcount":0,"banner_url":"","singername":"周深","is_settled":1,"heatoffset":-1,"heat":48465,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/169967/2556216","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg"},{"mvcount":0,"fanscount":8950295,"descibe":"1.2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3539%2F8950281","sortoffset":-12,"singerid":3539,"songcount":0,"banner_url":"","singername":"张杰","is_settled":1,"heatoffset":-1,"heat":48101,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3539/8950281","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210510/20210510095204419.jpg"},{"mvcount":0,"fanscount":3358624,"descibe":"1.3万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3047%2F3358625","sortoffset":39,"singerid":3047,"songcount":0,"banner_url":"","singername":"许嵩","is_settled":1,"heatoffset":0,"heat":45763,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3047/3358625","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180906/20180906181115284.jpg"},{"mvcount":0,"fanscount":4283899,"descibe":"1.9万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1573%2F4283893","sortoffset":0,"singerid":1573,"songcount":0,"banner_url":"","singername":"刘德华","is_settled":1,"heatoffset":-2,"heat":44352,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1573/4283893","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210816/20210816164409933.jpg"},{"mvcount":0,"fanscount":1126384,"descibe":"764条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2725%2F1126375","sortoffset":-6,"singerid":2725,"songcount":0,"banner_url":"","singername":"王杰","is_settled":0,"heatoffset":-1,"heat":37221,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2725/1126375","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160910/20160910145826790.jpg"},{"mvcount":0,"fanscount":829956,"descibe":"791条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1383%2F829955","sortoffset":-20,"singerid":1383,"songcount":0,"banner_url":"","singername":"黄家驹","is_settled":0,"heatoffset":0,"heat":31201,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1383/829955","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20140219/20140219104100727871.jpg"},{"mvcount":0,"fanscount":1969240,"descibe":"4.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F753317%2F1969213","sortoffset":-9,"singerid":753317,"songcount":0,"banner_url":"","singername":"小阿枫","is_settled":1,"heatoffset":-1,"heat":29993,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/753317/1969213","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211105/20211105161913836.jpg"},{"mvcount":0,"fanscount":3227689,"descibe":"1539条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F730%2F3227679","sortoffset":-336,"singerid":730,"songcount":0,"banner_url":"","singername":"刀郎","is_settled":0,"heatoffset":1,"heat":29607,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/730/3227679","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20190115/20190115150401884.jpg"},{"mvcount":0,"fanscount":1786781,"descibe":"2.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3538%2F1786761","sortoffset":27,"singerid":3538,"songcount":0,"banner_url":"","singername":"郑源","is_settled":1,"heatoffset":3,"heat":28242,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3538/1786761","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210325/20210325155813732.jpg"},{"mvcount":0,"fanscount":1458773,"descibe":"763条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3525%2F1458772","sortoffset":-52,"singerid":3525,"songcount":0,"banner_url":"","singername":"张信哲","is_settled":0,"heatoffset":-1,"heat":26420,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3525/1458772","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211202/20211202152848469345.jpg"},{"mvcount":0,"fanscount":3699709,"descibe":"2950条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F93475%2F3699702","sortoffset":0,"singerid":93475,"songcount":0,"banner_url":"","singername":"李荣浩","is_settled":1,"heatoffset":-1,"heat":26170,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/93475/3699702","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20191209/20191209164452855.jpg"},{"mvcount":0,"fanscount":1220007,"descibe":"1万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3534%2F1219992","sortoffset":-23,"singerid":3534,"songcount":0,"banner_url":"","singername":"周传雄","is_settled":1,"heatoffset":0,"heat":25380,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3534/1219992","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210520/20210520142503390.jpg"},{"mvcount":0,"fanscount":4229496,"descibe":"2万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1485%2F4229490","sortoffset":0,"singerid":1485,"songcount":0,"banner_url":"","singername":"汪苏泷","is_settled":1,"heatoffset":2,"heat":24930,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1485/4229490","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201125/20201125182034721.jpg"},{"mvcount":0,"fanscount":514937,"descibe":"159条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1595%2F514933","sortoffset":0,"singerid":1595,"songcount":0,"banner_url":"","singername":"林子祥","is_settled":0,"heatoffset":-2,"heat":24487,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1595/514933","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210208/20210208134415204.jpg"},{"mvcount":0,"fanscount":443323,"descibe":"58条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2728%2F443322","sortoffset":53,"singerid":2728,"songcount":0,"banner_url":"","singername":"伍佰","is_settled":1,"heatoffset":0,"heat":23033,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2728/443322","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200910/20200910112958425.jpg"},{"mvcount":0,"fanscount":1143300,"descibe":"478条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3043%2F1143295","sortoffset":120,"singerid":3043,"songcount":0,"banner_url":"","singername":"许巍","is_settled":0,"heatoffset":2,"heat":22899,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3043/1143295","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20201020/20201020101833509.jpg"},{"mvcount":0,"fanscount":757747,"descibe":"3290条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F556720%2F757723","sortoffset":434,"singerid":556720,"songcount":0,"banner_url":"","singername":"严浩翔","is_settled":1,"heatoffset":292,"heat":22754,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/556720/757723","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115111609320.jpg"},{"mvcount":0,"fanscount":1117543,"descibe":"393条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F1576%2F1117536","sortoffset":-39,"singerid":1576,"songcount":0,"banner_url":"","singername":"李宗盛","is_settled":0,"heatoffset":-2,"heat":21815,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/1576/1117536","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210125/20210125133627693.jpg"},{"mvcount":0,"fanscount":921995,"descibe":"145条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3537%2F921987","sortoffset":-12,"singerid":3537,"songcount":0,"banner_url":"","singername":"张宇","is_settled":0,"heatoffset":0,"heat":20298,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3537/921987","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210202/20210202131408586.jpg"},{"mvcount":0,"fanscount":1067514,"descibe":"389条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2627%2F1067508","sortoffset":20,"singerid":2627,"songcount":0,"banner_url":"","singername":"谭咏麟","is_settled":0,"heatoffset":1,"heat":20150,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2627/1067508","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211115/20211115101136412.jpg"},{"mvcount":0,"fanscount":1118386,"descibe":"4033条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F2404%2F1118372","sortoffset":0,"singerid":2404,"songcount":0,"banner_url":"","singername":"任贤齐","is_settled":1,"heatoffset":-1,"heat":20138,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/2404/1118372","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20200502/20200502105138263.jpg"},{"mvcount":0,"fanscount":1218316,"descibe":"1350条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3523%2F1218307","sortoffset":270,"singerid":3523,"songcount":0,"banner_url":"","singername":"张国荣","is_settled":0,"heatoffset":4,"heat":20080,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3523/1218307","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20160909/20160909203241500.jpg"},{"mvcount":0,"fanscount":5391241,"descibe":"5.8万条问答","offline_url":"https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F90875%2F5391242","sortoffset":2,"singerid":90875,"songcount":0,"banner_url":"","singername":"华晨宇","is_settled":1,"heatoffset":2,"heat":20059,"url":"https://h5.kugou.com/apps/singer-qa-v2/#/singer2/90875/5391242","albumcount":0,"intro":"","imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20211009/20211009174756159.jpg"}]
             */

            private int timestamp;
            private int total;
            private EnuListBean enu_list;
            private List<InfoBean> info;

            public int getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(int timestamp) {
                this.timestamp = timestamp;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public EnuListBean getEnu_list() {
                return enu_list;
            }

            public void setEnu_list(EnuListBean enu_list) {
                this.enu_list = enu_list;
            }

            public List<InfoBean> getInfo() {
                return info;
            }

            public void setInfo(List<InfoBean> info) {
                this.info = info;
            }

            public static class EnuListBean {
                private List<TypesBean> types;
                private List<SextypesBean> sextypes;

                public List<TypesBean> getTypes() {
                    return types;
                }

                public void setTypes(List<TypesBean> types) {
                    this.types = types;
                }

                public List<SextypesBean> getSextypes() {
                    return sextypes;
                }

                public void setSextypes(List<SextypesBean> sextypes) {
                    this.sextypes = sextypes;
                }

                public static class TypesBean {
                    /**
                     * key : 4
                     * value : 其他
                     */

                    private int key;
                    private String value;

                    public int getKey() {
                        return key;
                    }

                    public void setKey(int key) {
                        this.key = key;
                    }

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }
                }

                public static class SextypesBean {
                    /**
                     * key : 2
                     * value : 女
                     */

                    private int key;
                    private String value;

                    public int getKey() {
                        return key;
                    }

                    public void setKey(int key) {
                        this.key = key;
                    }

                    public String getValue() {
                        return value;
                    }

                    public void setValue(String value) {
                        this.value = value;
                    }
                }
            }

            public static class InfoBean {
                /**
                 * mvcount : 0
                 * fanscount : 13058992
                 * descibe : 6123条问答
                 * offline_url : https://miniapp.kugou.com/node/v2?type=1&id=120&path=%2Findex.html%23%2Fsinger2%2F3520%2F13058930
                 * sortoffset : 0
                 * singerid : 3520
                 * songcount : 0
                 * banner_url :
                 * singername : 周杰伦
                 * is_settled : 0
                 * heatoffset : 0
                 * heat : 203624
                 * url : https://h5.kugou.com/apps/singer-qa-v2/#/singer2/3520/13058930
                 * albumcount : 0
                 * intro :
                 * imgurl : http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg
                 */

                private int mvcount;
                private int fanscount;
                private String descibe;
                private String offline_url;
                private int sortoffset;
                private int singerid;
                private int songcount;
                private String banner_url;
                private String singername;
                private int is_settled;
                private int heatoffset;
                private int heat;
                private String url;
                private int albumcount;
                private String intro;
                private String imgurl;

                public int getMvcount() {
                    return mvcount;
                }

                public void setMvcount(int mvcount) {
                    this.mvcount = mvcount;
                }

                public int getFanscount() {
                    return fanscount;
                }

                public void setFanscount(int fanscount) {
                    this.fanscount = fanscount;
                }

                public String getDescibe() {
                    return descibe;
                }

                public void setDescibe(String descibe) {
                    this.descibe = descibe;
                }

                public String getOffline_url() {
                    return offline_url;
                }

                public void setOffline_url(String offline_url) {
                    this.offline_url = offline_url;
                }

                public int getSortoffset() {
                    return sortoffset;
                }

                public void setSortoffset(int sortoffset) {
                    this.sortoffset = sortoffset;
                }

                public int getSingerid() {
                    return singerid;
                }

                public void setSingerid(int singerid) {
                    this.singerid = singerid;
                }

                public int getSongcount() {
                    return songcount;
                }

                public void setSongcount(int songcount) {
                    this.songcount = songcount;
                }

                public String getBanner_url() {
                    return banner_url;
                }

                public void setBanner_url(String banner_url) {
                    this.banner_url = banner_url;
                }

                public String getSingername() {
                    return singername;
                }

                public void setSingername(String singername) {
                    this.singername = singername;
                }

                public int getIs_settled() {
                    return is_settled;
                }

                public void setIs_settled(int is_settled) {
                    this.is_settled = is_settled;
                }

                public int getHeatoffset() {
                    return heatoffset;
                }

                public void setHeatoffset(int heatoffset) {
                    this.heatoffset = heatoffset;
                }

                public int getHeat() {
                    return heat;
                }

                public void setHeat(int heat) {
                    this.heat = heat;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getAlbumcount() {
                    return albumcount;
                }

                public void setAlbumcount(int albumcount) {
                    this.albumcount = albumcount;
                }

                public String getIntro() {
                    return intro;
                }

                public void setIntro(String intro) {
                    this.intro = intro;
                }

                public String getImgurl() {
                    return imgurl;
                }

                public void setImgurl(String imgurl) {
                    this.imgurl = imgurl;
                }
            }
        }
    }
}
