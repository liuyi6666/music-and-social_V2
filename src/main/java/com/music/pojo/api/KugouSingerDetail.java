package com.music.pojo.api;

import java.io.Serializable;

public class KugouSingerDetail implements Serializable {
    private static final long serialVersionUID = -1442955908377249051L;

    /**
     * status : 1
     * error :
     * data : {"identity":15,"mvcount":939,"has_long_intro":1,"intro":"周杰伦（Jay Chou），1979年1月18日出生于台湾省新北市，中国台湾流行乐男歌手、音乐人、演员、导演、编剧、监制、商人。\n 2000年发行首张个人专辑《Jay》。2001年发行的专辑《范特西》奠定其融合中西方音乐的风格。2002年举行\u201cThe One\u201d世界巡回演唱会。2003年成为美国《时代周刊》封面人物。2004年获得世界音乐大奖中国区最畅销艺人奖。2005年凭借动作片《头文字D》获得台湾电影金马奖、香港电影金像奖最佳新人奖。2006年起连续三年获得世界音乐大奖中国区最畅销艺人奖。\n 2007年自编自导的文艺片《不能说的秘密》获得台湾电影金马奖年度台湾杰出电影奖。\n 2008年凭借歌曲《青花瓷》获得第19届金曲奖最佳作曲人奖。同年，捐款援建希望小学。\n 2009年入选美国CNN评出的\u201c25位亚洲最具影响力的人物\u201d；同年凭借专辑《魔杰座》获得第20届金曲奖最佳国语男歌手奖。\n 2010年入选美国《Fast Company》评出的\u201c全球百大创意人物\u201d。\n 2011年凭借专辑《跨时代》再度获得金曲奖最佳国语男歌手奖，并且第4次获得金曲奖最佳国语专辑奖；同年主演好莱坞电影《青蜂侠》。同年，担任华硕笔电设计师，并入股香港文化传信集团。\n 2012年登福布斯中国名人榜榜首。\n 2014年发行华语乐坛首张数字音乐专辑《哎呦，不错哦》。同年，担任中国禁毒宣传形象大使。\n 2016年发行专辑《周杰伦的床边故事》。\n 2018年举行\u201c地表最强2世界巡回演唱会\u201d。\n 2021年，监制并出演的电影《叱咤风云》在中国内地上映；同年，参加央视春节联欢晚会，演唱歌曲《莫吉托》。同年5月12日，凭借《莫吉托》入围第32届金曲奖最佳单曲制作人奖。","songcount":969,"imgurl":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg","profile":"周杰伦（Jay Chou），1979年1月18日出生于台湾省新北市，中国台湾流行乐男歌手、音乐人、演员、导演、编剧、监制、商人。\n 2000年发行首张个人专辑《Jay》。2001年发行的专辑《范特西》奠定其融合中西方音乐的风格。2002年举行\u201cThe One\u201d世界巡回演唱会。2003年成为美国《时代周刊》封面人物。2004年获得世界音乐大奖中国区最畅销艺人奖。2005年凭借动作片《头文字D》获得台湾电影金马奖、香港电影金像奖最佳新人奖。2006年起连续三年获得世界音乐大奖中国区最畅销艺人奖。\n 2007年自编自导的文艺片《不能说的秘密》获得台湾电影金马奖年度台湾杰出电影奖。\n 2008年凭借歌曲《青花瓷》获得第19届金曲奖最佳作曲人奖。同年，捐款援建希望小学。\n 2009年入选美国CNN评出的\u201c25位亚洲最具影响力的人物\u201d；同年凭借专辑《魔杰座》获得第20届金曲奖最佳国语男歌手奖。\n 2010年入选美国《Fast Company》评出的\u201c全球百大创意人物\u201d。\n 2011年凭借专辑《跨时代》再度获得金曲奖最佳国语男歌手奖，并且第4次获得金曲奖最佳国语专辑奖；同年主演好莱坞电影《青蜂侠》。同年，担任华硕笔电设计师，并入股香港文化传信集团。\n 2012年登福布斯中国名人榜榜首。\n 2014年发行华语乐坛首张数字音乐专辑《哎呦，不错哦》。同年，担任中国禁毒宣传形象大使。\n 2018年举行\u201c地表最强2世界巡回演唱会\u201d。\n 2021年，监制并出演的电影《叱咤风云》在中国内地上映；同年，参加央视春节联欢晚会，演唱歌曲《莫吉托》。同年5月12日，凭借《莫吉托》入围第32届金曲奖最佳单曲制作人奖。","singerid":3520,"grade":1,"singername":"周杰伦","albumcount":38}
     * errcode : 0
     */

    private int status;
    private String error;
    private DataBean data;
    private int errcode;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public static class DataBean {
        /**
         * identity : 15
         * mvcount : 939
         * has_long_intro : 1
         * intro : 周杰伦（Jay Chou），1979年1月18日出生于台湾省新北市，中国台湾流行乐男歌手、音乐人、演员、导演、编剧、监制、商人。
         2000年发行首张个人专辑《Jay》。2001年发行的专辑《范特西》奠定其融合中西方音乐的风格。2002年举行“The One”世界巡回演唱会。2003年成为美国《时代周刊》封面人物。2004年获得世界音乐大奖中国区最畅销艺人奖。2005年凭借动作片《头文字D》获得台湾电影金马奖、香港电影金像奖最佳新人奖。2006年起连续三年获得世界音乐大奖中国区最畅销艺人奖。
         2007年自编自导的文艺片《不能说的秘密》获得台湾电影金马奖年度台湾杰出电影奖。
         2008年凭借歌曲《青花瓷》获得第19届金曲奖最佳作曲人奖。同年，捐款援建希望小学。
         2009年入选美国CNN评出的“25位亚洲最具影响力的人物”；同年凭借专辑《魔杰座》获得第20届金曲奖最佳国语男歌手奖。
         2010年入选美国《Fast Company》评出的“全球百大创意人物”。
         2011年凭借专辑《跨时代》再度获得金曲奖最佳国语男歌手奖，并且第4次获得金曲奖最佳国语专辑奖；同年主演好莱坞电影《青蜂侠》。同年，担任华硕笔电设计师，并入股香港文化传信集团。
         2012年登福布斯中国名人榜榜首。
         2014年发行华语乐坛首张数字音乐专辑《哎呦，不错哦》。同年，担任中国禁毒宣传形象大使。
         2016年发行专辑《周杰伦的床边故事》。
         2018年举行“地表最强2世界巡回演唱会”。
         2021年，监制并出演的电影《叱咤风云》在中国内地上映；同年，参加央视春节联欢晚会，演唱歌曲《莫吉托》。同年5月12日，凭借《莫吉托》入围第32届金曲奖最佳单曲制作人奖。
         * songcount : 969
         * imgurl : http://singerimg.kugou.com/uploadpic/softhead/{size}/20180515/20180515002522714.jpg
         * profile : 周杰伦（Jay Chou），1979年1月18日出生于台湾省新北市，中国台湾流行乐男歌手、音乐人、演员、导演、编剧、监制、商人。
         2000年发行首张个人专辑《Jay》。2001年发行的专辑《范特西》奠定其融合中西方音乐的风格。2002年举行“The One”世界巡回演唱会。2003年成为美国《时代周刊》封面人物。2004年获得世界音乐大奖中国区最畅销艺人奖。2005年凭借动作片《头文字D》获得台湾电影金马奖、香港电影金像奖最佳新人奖。2006年起连续三年获得世界音乐大奖中国区最畅销艺人奖。
         2007年自编自导的文艺片《不能说的秘密》获得台湾电影金马奖年度台湾杰出电影奖。
         2008年凭借歌曲《青花瓷》获得第19届金曲奖最佳作曲人奖。同年，捐款援建希望小学。
         2009年入选美国CNN评出的“25位亚洲最具影响力的人物”；同年凭借专辑《魔杰座》获得第20届金曲奖最佳国语男歌手奖。
         2010年入选美国《Fast Company》评出的“全球百大创意人物”。
         2011年凭借专辑《跨时代》再度获得金曲奖最佳国语男歌手奖，并且第4次获得金曲奖最佳国语专辑奖；同年主演好莱坞电影《青蜂侠》。同年，担任华硕笔电设计师，并入股香港文化传信集团。
         2012年登福布斯中国名人榜榜首。
         2014年发行华语乐坛首张数字音乐专辑《哎呦，不错哦》。同年，担任中国禁毒宣传形象大使。
         2018年举行“地表最强2世界巡回演唱会”。
         2021年，监制并出演的电影《叱咤风云》在中国内地上映；同年，参加央视春节联欢晚会，演唱歌曲《莫吉托》。同年5月12日，凭借《莫吉托》入围第32届金曲奖最佳单曲制作人奖。
         * singerid : 3520
         * grade : 1
         * singername : 周杰伦
         * albumcount : 38
         */

        private int identity;
        private int mvcount;
        private int has_long_intro;
        private String intro;
        private int songcount;
        private String imgurl;
        private String profile;
        private int singerid;
        private int grade;
        private String singername;
        private int albumcount;

        public int getIdentity() {
            return identity;
        }

        public void setIdentity(int identity) {
            this.identity = identity;
        }

        public int getMvcount() {
            return mvcount;
        }

        public void setMvcount(int mvcount) {
            this.mvcount = mvcount;
        }

        public int getHas_long_intro() {
            return has_long_intro;
        }

        public void setHas_long_intro(int has_long_intro) {
            this.has_long_intro = has_long_intro;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public int getSongcount() {
            return songcount;
        }

        public void setSongcount(int songcount) {
            this.songcount = songcount;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public int getSingerid() {
            return singerid;
        }

        public void setSingerid(int singerid) {
            this.singerid = singerid;
        }

        public int getGrade() {
            return grade;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }

        public String getSingername() {
            return singername;
        }

        public void setSingername(String singername) {
            this.singername = singername;
        }

        public int getAlbumcount() {
            return albumcount;
        }

        public void setAlbumcount(int albumcount) {
            this.albumcount = albumcount;
        }
    }
}
