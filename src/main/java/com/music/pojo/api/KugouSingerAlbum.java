package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

public class KugouSingerAlbum implements Serializable {
    private static final long serialVersionUID = -8103508643031204775L;

    /**
     * status : 1
     * error :
     * data : {"timestamp":1644211991,"info":[{"albumname":"想哭","intro":"\u201d我寄居回忆等一个相遇，把没说的话都附送给你\u201c不再联系，想哭却哭不出声音，对着影子 安慰着自己，风小筝新歌《想哭》发行上线，用心聆听！","publishtime":"2021-09-09 00:00:00","singername":"风小筝","albumid":48639853,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210908/20210908190810685165.jpg"},{"albumname":"《我们的黄金年代》电影原声","intro":"《我们的黄金年代》是由夏天、郑喜宁执导，安杰、阳知、张琛、琪格主演的青春校园爱情电影。插曲《好好陪着你》由玉璇玑演唱，片尾曲《如果看见流星》由风小筝演唱。","publishtime":"2021-09-06 00:00:00","singername":"风小筝、玉璇玑","albumid":48468581,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210831/20210831175019995616.jpg"},{"albumname":"风筝","intro":"\u201c风儿在打盹，云在窗外等，一晃几个春\u201d这一生就像是做了一场梦，路上开满了星辰。我就像是风筝，你就像是那条长绳，爱你是我的本能，因为有你，梦才完整。风小筝新歌《风筝》发行上线，用\u201c风筝\u201d讲述历程，握着彼此的线，不需要担心风的猛烈。用心聆听这首叫做\u201c我们\u201d的故事吧.......","publishtime":"2021-07-02 00:00:00","singername":"风小筝","albumid":46344662,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210701/20210701155511684345.jpg"},{"albumname":"南不懂北的夜","intro":"逃不过的情劫，终究成了一场恩怨，我这里白雪纷飞，你那边花开遍野，我们又怎样才能回到最初呢？从梦中惊醒后眼泪顺着脸颊滑落，想起了你说的离别，一个人在黑夜孤独的唱着悲伤，千言万语写不出我对你的思念，许过的誓约随着梦飘向远方，时过境迁，你终是我南不懂北的夜.......风小筝新歌《南不懂北的夜》，用歌声讲述缘起缘灭，用心聆听........","publishtime":"2021-06-17 00:00:00","singername":"风小筝","albumid":45678458,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210614/20210614165158411575.jpg"},{"albumname":"点绛唇","intro":"新歌发布哈，喜欢的朋友记得关注我风小筝哟！","publishtime":"2021-05-15 00:00:00","singername":"Cici、风小筝","albumid":44658802,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210515/20210515192447571796.jpg"},{"albumname":"书生醉","intro":"\u201c小小书生惭愧，没拭去你眼角泪，饮下爱恨一杯，就化作我的憔悴\u201d一杯小酒，十分相思，梦里与你再次相见，遥望月寒，深陷在你我的回忆，当琴声在岁月里流转，当相爱的人都离散，当恋人都忘了浪漫，当我将你的名字再次轻唤，饮下爱恨一杯，我用深情换长醉。风小筝新歌《书生醉》，一起描绘夏风青山，爱恨别离...","publishtime":"2021-04-23 00:00:00","singername":"风小筝","albumid":43748984,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210419/20210419165929866580.jpg"},{"albumname":"由你决定","intro":"最后是你做的决定 我不想听 我不相信 才回过神 你已经 在我的生命里出局 我们曾经那么好，现在连声问候都怕是打扰！ 不能再深究这段感情里的对与错，你决定要走，再多遗憾我也不再挽留...","publishtime":"2021-03-10 00:00:00","singername":"风小筝","albumid":42359833,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210309/20210309183302448677.jpg"},{"albumname":"故城","intro":"风小筝新歌《故城》\u201c曾梦回故城小巷，花树下的陈酿\u201d青石板路走一走，忽梦醒，余兴绵长......","publishtime":"2021-01-20 00:00:00","singername":"风小筝","albumid":41163345,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210114/20210114180102839666.jpg"},{"albumname":"千秋禄\u2022唐宴","intro":"《千秋录-唐宴》是以唐代为故事背景，由知名作家创作故事，由等什么君，回音哥，彭十六，草帽酱，风小筝，阿yueyue，曲肖冰，锦零等众多古风知名KOL以及抖音大V组成的阵容，以故事为主线，以音乐为主要表现载体，弘扬中国传统历史文化，打造新类型的\u201c听读一体\u201d国风企划 她，距长安城八百里外，留侠镇七星镖局的二小姐，一个不爱红装爱武装的女子。父亲平日对她管教严厉，不让她练武，心里希望她像姐姐一样，成为精通琴棋书画，温文尔雅的大家闺秀。愁着她这辈子嫁不出去，可她心中早已有了以心相许的如意郎君。   他，南野镇，屡次科举不中的落第秀才，镇里人眼中的\u201c大文豪\u201d，商贾眼中的穷酸秀才。十年如一日，寒窗苦读。他曾发过誓言，待考取功名，定十里红妆，八抬大轿，来娶他的心上人，七星镖局的二小姐。   他们的故事这就开始讲起了。 三十里外，南野镇打擂日。她又如往常一样\u201c梳洗打扮\u201d，这可不是寻常女子的施粉黛，别发簪，为掩人耳目，女扮男装，活脱脱一个帅气的书生，看起来比平常书生，还多了几分英气。 酒仙楼，他正和旁乡的好友斗诗，她在东南角看着两月一次的打擂台，看的她是内心澎湃，跃跃欲试。他斗诗惜败，被猛灌了一壶白酒，一个踉跄，身子翻过栏杆掉入了南运河里。好友也急的手忙脚乱，但不谙水性，也无从下手，眼看着河水湍急，便大声呼叫着人来帮忙。她，听到呼救声，不假思索几个箭步便冲到了河边，一下扎到了河里。他被救上来了，一口一个感谢，他也是没想到一个书生，竟然有这么好的水性。 他和她相谈甚欢，从诗词歌赋，武术拳脚，到人生理想。他认定她这个兄弟交定了，况且还是他的救命恩人。而她，听完他的\u201c长篇大论\u201d，没有对这个穷酸书生有一点反感，反倒多了几分倾慕，和惺惺相惜。二人喝的酣畅，一醉到天明。   翌日，他为表感激和尽地主之谊，带着她载小船听雨落，也教她吟诗作对。她在杨柳堤旁教他拳脚用来防身。在武艺切磋时，他不小心打掉了她的发簪，才知她是女儿身。 二人含情脉脉。她，红袖添香，为他洗笔研磨，活脱脱变了个人。他说待考取功名，定光明正大娶她回家。他知道她父亲喜欢习武的人，他便时常找她请教武学上的技巧要的。   时间如白马过隙，流水无形。到了进京赶考的时日。 他， 直奔梦想的天国长安城，夺金榜，得功名。 她， 回了镖局，做回了她的二小姐，整日茶饭不思，等着他凯旋而归。 到了发榜的时日，她听说他喜中状元，她内心何以用喜悦二字够形容。过了数月，也不见他归来，甚至连一封书信都没有。自从上次偷溜出去，父亲对她更是严加看管，不让她出镖局半步。她对他的思念犹如泉涌，不争气的从眼眶里流了出来。 又过半月，听姐姐说起，南野镇出了个新科状元，皇上将自己的妹妹许给了新科状元，成了当朝的驸马爷。她还不死心，追问姐姐这驸马爷姓甚名谁。她心如死灰，本就蜡黄消瘦的脸，没了一点精神。 她，为了忘记他，没日没夜的练武，显然成了一个武痴。父亲担心她是得了癔症，遍请名医来访，终不见成效。她说着自己没患病，让父亲不必担忧。   一月后，深夜，大雪纷飞。 她骑着白鬃马，遮着面纱，一袭红装，美艳动人。 她身背着长剑，直奔长安城\u2026\u2026","publishtime":"2021-01-12 00:00:00","singername":"草帽酱、回音哥、风小筝、曲肖冰、等什么君(邓寓君)","albumid":41050810,"sum_ownercount":0,"songcount":20,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210313/20210313172902543988.jpg"},{"albumname":"雪花飘","intro":"风小筝最新单曲《雪花飘》，诉说一段离别的爱情，凄美了这个飘雪的冬季，\u201c漫天的雪花飘啊飘，飘到哪里寻不到，舍不得也忘不了，让时光带走你微笑...\u201d","publishtime":"2020-12-19 00:00:00","singername":"风小筝","albumid":40617031,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201216/20201216125104854117.jpg"},{"albumname":"是我太傻","intro":"风小筝全新伤感单曲《是我太傻》催泪来袭！这首歌曲展现了一个人在感情中越爱越伤的困顿和挣扎，伤感的旋律，句句戳心的歌词，歌手用温柔的声线对这首作品做了最好的诠释！作为风小筝年度的重磅歌曲，本次音乐制作同样是精心打造，力保音乐品质，绝对惊艳你的耳朵！","publishtime":"2020-11-13 00:00:00","singername":"风小筝","albumid":40111820,"sum_ownercount":0,"songcount":8,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201111/20201111150003755673.jpg"},{"albumname":"他爱你","intro":"世上有很多种遇见， 再见， 最后再见。 世上最遥远的距离， 是仅存的回忆， 「风小筝（覃沐曦）」最新单曲《他爱你》， 以第三视角倾心演绎一段铭心的回忆。","publishtime":"2020-10-25 00:00:00","singername":"风小筝","albumid":39661294,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201022/20201022151015463470.jpg"},{"albumname":"再后来","intro":"再后来你没有我会不会真的快乐 再后来你是否还那样温柔和冷漠 再后来你是否小心取舍计算 值得不值得 后来的你是否还能再想起我","publishtime":"2020-09-30 00:00:00","singername":"风小筝","albumid":39406738,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200929/20200929162401232616.jpg"},{"albumname":"斩风刀 电影原声带","intro":"影片主要讲述了魔刀门徒鸦青卷入江湖\u201c蛇人\u201d阴谋，被迫逃亡过程中结识两位好友，三人共同挫败幕后黑手，揭秘一段父辈间宿怨恩仇的热血故事。 知名女歌手风小筝（覃沐曦）携手音乐人吕宏斌，联袂献唱电影宣传曲《三人行》，将现代rap与戏腔结合，碰撞出不一样的音乐火花，朗朗上口的旋律，铿锵有力的歌声，唱出一段江湖佳话。 电影插曲《无非陌路》由知名女歌手风小筝（覃沐曦）倾情献唱，深情又治愈的嗓音让这首歌的韵味更加绵长，任前尘旧事慢慢在眼前浮现，人的心绪也随着歌声一同高低起伏，共一味人间的苦与痛。","publishtime":"2020-09-05 00:00:00","singername":"风小筝、吕宏斌","albumid":39139755,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200904/20200904152004379326.jpg"},{"albumname":"love you","intro":"由郑文作词作曲，和风小筝倾情演唱《love you》最新上线。","publishtime":"2020-08-04 00:00:00","singername":"风小筝、郑文","albumid":38731910,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200803/20200803155311270349.jpg"},{"albumname":"一线情长","intro":"余音不舍 琴弦中回荡 那曲离歌 而今犹绕梁 风小筝单曲《一线情长》全新上线 问君可念我一线情长 思念插上勇敢的翅膀 穿越爱恨悲喜的时光","publishtime":"2020-06-30 00:00:00","singername":"风小筝","albumid":38220917,"sum_ownercount":0,"songcount":3,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200623/20200623160006265303.jpg"},{"albumname":"如果是我该有多好","intro":"如果是我该多好，陪你经历所有快乐与不快乐， 多想陪在你身边，但我已成为一个匆匆过客。","publishtime":"2020-05-11 00:00:00","singername":"风小筝","albumid":37527059,"sum_ownercount":0,"songcount":3,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200507/20200507180013103646.jpg"},{"albumname":"穿透","intro":"我穿过你 去寻找梦中的自己 我透过你 只想与你紧扣十指 大电影《聊斋古卷 兰若之境》故事围绕着蒲松龄书里书外，亦真亦幻的情感动机开始！聊斋题材从古到今给了我们很多幻想，这次受邀演唱《穿透》的风小筝，把声音塑造柔情万千，若隐惹现，也让整个电影层次更丰富，让剧情更充满幻想！风小筝用情绪娓娓道来，用声音演绎穿越时空的爱恋。愿意和你去云游，愿意和你到白头！《聊斋古卷 兰若之境》让风小筝陪你穿透一场撕心裂肺的爱恨纠结！","publishtime":"2020-04-22 00:00:00","singername":"风小筝","albumid":37190674,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200420/20200420171706791166.jpg"},{"albumname":"归还","intro":"谈什么 儿女情长 泪沾湿 是谁的衣裳 这世间动荡 人事变幻无常 此生恐 不复归还","publishtime":"2020-03-31 00:00:00","singername":"风小筝","albumid":36842882,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200330/20200330153510709881.jpg"},{"albumname":"圆月光","intro":"所有的尘埃都有重量，所以，你需要一首《圆月光》。 拂去细尘，掸走过往，脚步走的太快，谁来温暖心房，谁来伴你成长，谁来把柔软换成坚强？ 其实都可以遗忘，圆月光还在耳外，宁静住进了心里，无风，也无浪。","publishtime":"2020-01-03 00:00:00","singername":"风小筝","albumid":35541766,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20191231/20191231183110819993.jpg"}],"total":57}
     * errcode : 0
     */

    private int status;
    private String error;
    private DataBean data;
    private int errcode;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public static class DataBean {
        /**
         * timestamp : 1644211991
         * info : [{"albumname":"想哭","intro":"\u201d我寄居回忆等一个相遇，把没说的话都附送给你\u201c不再联系，想哭却哭不出声音，对着影子 安慰着自己，风小筝新歌《想哭》发行上线，用心聆听！","publishtime":"2021-09-09 00:00:00","singername":"风小筝","albumid":48639853,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210908/20210908190810685165.jpg"},{"albumname":"《我们的黄金年代》电影原声","intro":"《我们的黄金年代》是由夏天、郑喜宁执导，安杰、阳知、张琛、琪格主演的青春校园爱情电影。插曲《好好陪着你》由玉璇玑演唱，片尾曲《如果看见流星》由风小筝演唱。","publishtime":"2021-09-06 00:00:00","singername":"风小筝、玉璇玑","albumid":48468581,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210831/20210831175019995616.jpg"},{"albumname":"风筝","intro":"\u201c风儿在打盹，云在窗外等，一晃几个春\u201d这一生就像是做了一场梦，路上开满了星辰。我就像是风筝，你就像是那条长绳，爱你是我的本能，因为有你，梦才完整。风小筝新歌《风筝》发行上线，用\u201c风筝\u201d讲述历程，握着彼此的线，不需要担心风的猛烈。用心聆听这首叫做\u201c我们\u201d的故事吧.......","publishtime":"2021-07-02 00:00:00","singername":"风小筝","albumid":46344662,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210701/20210701155511684345.jpg"},{"albumname":"南不懂北的夜","intro":"逃不过的情劫，终究成了一场恩怨，我这里白雪纷飞，你那边花开遍野，我们又怎样才能回到最初呢？从梦中惊醒后眼泪顺着脸颊滑落，想起了你说的离别，一个人在黑夜孤独的唱着悲伤，千言万语写不出我对你的思念，许过的誓约随着梦飘向远方，时过境迁，你终是我南不懂北的夜.......风小筝新歌《南不懂北的夜》，用歌声讲述缘起缘灭，用心聆听........","publishtime":"2021-06-17 00:00:00","singername":"风小筝","albumid":45678458,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210614/20210614165158411575.jpg"},{"albumname":"点绛唇","intro":"新歌发布哈，喜欢的朋友记得关注我风小筝哟！","publishtime":"2021-05-15 00:00:00","singername":"Cici、风小筝","albumid":44658802,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210515/20210515192447571796.jpg"},{"albumname":"书生醉","intro":"\u201c小小书生惭愧，没拭去你眼角泪，饮下爱恨一杯，就化作我的憔悴\u201d一杯小酒，十分相思，梦里与你再次相见，遥望月寒，深陷在你我的回忆，当琴声在岁月里流转，当相爱的人都离散，当恋人都忘了浪漫，当我将你的名字再次轻唤，饮下爱恨一杯，我用深情换长醉。风小筝新歌《书生醉》，一起描绘夏风青山，爱恨别离...","publishtime":"2021-04-23 00:00:00","singername":"风小筝","albumid":43748984,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210419/20210419165929866580.jpg"},{"albumname":"由你决定","intro":"最后是你做的决定 我不想听 我不相信 才回过神 你已经 在我的生命里出局 我们曾经那么好，现在连声问候都怕是打扰！ 不能再深究这段感情里的对与错，你决定要走，再多遗憾我也不再挽留...","publishtime":"2021-03-10 00:00:00","singername":"风小筝","albumid":42359833,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210309/20210309183302448677.jpg"},{"albumname":"故城","intro":"风小筝新歌《故城》\u201c曾梦回故城小巷，花树下的陈酿\u201d青石板路走一走，忽梦醒，余兴绵长......","publishtime":"2021-01-20 00:00:00","singername":"风小筝","albumid":41163345,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210114/20210114180102839666.jpg"},{"albumname":"千秋禄\u2022唐宴","intro":"《千秋录-唐宴》是以唐代为故事背景，由知名作家创作故事，由等什么君，回音哥，彭十六，草帽酱，风小筝，阿yueyue，曲肖冰，锦零等众多古风知名KOL以及抖音大V组成的阵容，以故事为主线，以音乐为主要表现载体，弘扬中国传统历史文化，打造新类型的\u201c听读一体\u201d国风企划 她，距长安城八百里外，留侠镇七星镖局的二小姐，一个不爱红装爱武装的女子。父亲平日对她管教严厉，不让她练武，心里希望她像姐姐一样，成为精通琴棋书画，温文尔雅的大家闺秀。愁着她这辈子嫁不出去，可她心中早已有了以心相许的如意郎君。   他，南野镇，屡次科举不中的落第秀才，镇里人眼中的\u201c大文豪\u201d，商贾眼中的穷酸秀才。十年如一日，寒窗苦读。他曾发过誓言，待考取功名，定十里红妆，八抬大轿，来娶他的心上人，七星镖局的二小姐。   他们的故事这就开始讲起了。 三十里外，南野镇打擂日。她又如往常一样\u201c梳洗打扮\u201d，这可不是寻常女子的施粉黛，别发簪，为掩人耳目，女扮男装，活脱脱一个帅气的书生，看起来比平常书生，还多了几分英气。 酒仙楼，他正和旁乡的好友斗诗，她在东南角看着两月一次的打擂台，看的她是内心澎湃，跃跃欲试。他斗诗惜败，被猛灌了一壶白酒，一个踉跄，身子翻过栏杆掉入了南运河里。好友也急的手忙脚乱，但不谙水性，也无从下手，眼看着河水湍急，便大声呼叫着人来帮忙。她，听到呼救声，不假思索几个箭步便冲到了河边，一下扎到了河里。他被救上来了，一口一个感谢，他也是没想到一个书生，竟然有这么好的水性。 他和她相谈甚欢，从诗词歌赋，武术拳脚，到人生理想。他认定她这个兄弟交定了，况且还是他的救命恩人。而她，听完他的\u201c长篇大论\u201d，没有对这个穷酸书生有一点反感，反倒多了几分倾慕，和惺惺相惜。二人喝的酣畅，一醉到天明。   翌日，他为表感激和尽地主之谊，带着她载小船听雨落，也教她吟诗作对。她在杨柳堤旁教他拳脚用来防身。在武艺切磋时，他不小心打掉了她的发簪，才知她是女儿身。 二人含情脉脉。她，红袖添香，为他洗笔研磨，活脱脱变了个人。他说待考取功名，定光明正大娶她回家。他知道她父亲喜欢习武的人，他便时常找她请教武学上的技巧要的。   时间如白马过隙，流水无形。到了进京赶考的时日。 他， 直奔梦想的天国长安城，夺金榜，得功名。 她， 回了镖局，做回了她的二小姐，整日茶饭不思，等着他凯旋而归。 到了发榜的时日，她听说他喜中状元，她内心何以用喜悦二字够形容。过了数月，也不见他归来，甚至连一封书信都没有。自从上次偷溜出去，父亲对她更是严加看管，不让她出镖局半步。她对他的思念犹如泉涌，不争气的从眼眶里流了出来。 又过半月，听姐姐说起，南野镇出了个新科状元，皇上将自己的妹妹许给了新科状元，成了当朝的驸马爷。她还不死心，追问姐姐这驸马爷姓甚名谁。她心如死灰，本就蜡黄消瘦的脸，没了一点精神。 她，为了忘记他，没日没夜的练武，显然成了一个武痴。父亲担心她是得了癔症，遍请名医来访，终不见成效。她说着自己没患病，让父亲不必担忧。   一月后，深夜，大雪纷飞。 她骑着白鬃马，遮着面纱，一袭红装，美艳动人。 她身背着长剑，直奔长安城\u2026\u2026","publishtime":"2021-01-12 00:00:00","singername":"草帽酱、回音哥、风小筝、曲肖冰、等什么君(邓寓君)","albumid":41050810,"sum_ownercount":0,"songcount":20,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20210313/20210313172902543988.jpg"},{"albumname":"雪花飘","intro":"风小筝最新单曲《雪花飘》，诉说一段离别的爱情，凄美了这个飘雪的冬季，\u201c漫天的雪花飘啊飘，飘到哪里寻不到，舍不得也忘不了，让时光带走你微笑...\u201d","publishtime":"2020-12-19 00:00:00","singername":"风小筝","albumid":40617031,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201216/20201216125104854117.jpg"},{"albumname":"是我太傻","intro":"风小筝全新伤感单曲《是我太傻》催泪来袭！这首歌曲展现了一个人在感情中越爱越伤的困顿和挣扎，伤感的旋律，句句戳心的歌词，歌手用温柔的声线对这首作品做了最好的诠释！作为风小筝年度的重磅歌曲，本次音乐制作同样是精心打造，力保音乐品质，绝对惊艳你的耳朵！","publishtime":"2020-11-13 00:00:00","singername":"风小筝","albumid":40111820,"sum_ownercount":0,"songcount":8,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201111/20201111150003755673.jpg"},{"albumname":"他爱你","intro":"世上有很多种遇见， 再见， 最后再见。 世上最遥远的距离， 是仅存的回忆， 「风小筝（覃沐曦）」最新单曲《他爱你》， 以第三视角倾心演绎一段铭心的回忆。","publishtime":"2020-10-25 00:00:00","singername":"风小筝","albumid":39661294,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20201022/20201022151015463470.jpg"},{"albumname":"再后来","intro":"再后来你没有我会不会真的快乐 再后来你是否还那样温柔和冷漠 再后来你是否小心取舍计算 值得不值得 后来的你是否还能再想起我","publishtime":"2020-09-30 00:00:00","singername":"风小筝","albumid":39406738,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200929/20200929162401232616.jpg"},{"albumname":"斩风刀 电影原声带","intro":"影片主要讲述了魔刀门徒鸦青卷入江湖\u201c蛇人\u201d阴谋，被迫逃亡过程中结识两位好友，三人共同挫败幕后黑手，揭秘一段父辈间宿怨恩仇的热血故事。 知名女歌手风小筝（覃沐曦）携手音乐人吕宏斌，联袂献唱电影宣传曲《三人行》，将现代rap与戏腔结合，碰撞出不一样的音乐火花，朗朗上口的旋律，铿锵有力的歌声，唱出一段江湖佳话。 电影插曲《无非陌路》由知名女歌手风小筝（覃沐曦）倾情献唱，深情又治愈的嗓音让这首歌的韵味更加绵长，任前尘旧事慢慢在眼前浮现，人的心绪也随着歌声一同高低起伏，共一味人间的苦与痛。","publishtime":"2020-09-05 00:00:00","singername":"风小筝、吕宏斌","albumid":39139755,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200904/20200904152004379326.jpg"},{"albumname":"love you","intro":"由郑文作词作曲，和风小筝倾情演唱《love you》最新上线。","publishtime":"2020-08-04 00:00:00","singername":"风小筝、郑文","albumid":38731910,"sum_ownercount":0,"songcount":4,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200803/20200803155311270349.jpg"},{"albumname":"一线情长","intro":"余音不舍 琴弦中回荡 那曲离歌 而今犹绕梁 风小筝单曲《一线情长》全新上线 问君可念我一线情长 思念插上勇敢的翅膀 穿越爱恨悲喜的时光","publishtime":"2020-06-30 00:00:00","singername":"风小筝","albumid":38220917,"sum_ownercount":0,"songcount":3,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200623/20200623160006265303.jpg"},{"albumname":"如果是我该有多好","intro":"如果是我该多好，陪你经历所有快乐与不快乐， 多想陪在你身边，但我已成为一个匆匆过客。","publishtime":"2020-05-11 00:00:00","singername":"风小筝","albumid":37527059,"sum_ownercount":0,"songcount":3,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200507/20200507180013103646.jpg"},{"albumname":"穿透","intro":"我穿过你 去寻找梦中的自己 我透过你 只想与你紧扣十指 大电影《聊斋古卷 兰若之境》故事围绕着蒲松龄书里书外，亦真亦幻的情感动机开始！聊斋题材从古到今给了我们很多幻想，这次受邀演唱《穿透》的风小筝，把声音塑造柔情万千，若隐惹现，也让整个电影层次更丰富，让剧情更充满幻想！风小筝用情绪娓娓道来，用声音演绎穿越时空的爱恋。愿意和你去云游，愿意和你到白头！《聊斋古卷 兰若之境》让风小筝陪你穿透一场撕心裂肺的爱恨纠结！","publishtime":"2020-04-22 00:00:00","singername":"风小筝","albumid":37190674,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200420/20200420171706791166.jpg"},{"albumname":"归还","intro":"谈什么 儿女情长 泪沾湿 是谁的衣裳 这世间动荡 人事变幻无常 此生恐 不复归还","publishtime":"2020-03-31 00:00:00","singername":"风小筝","albumid":36842882,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20200330/20200330153510709881.jpg"},{"albumname":"圆月光","intro":"所有的尘埃都有重量，所以，你需要一首《圆月光》。 拂去细尘，掸走过往，脚步走的太快，谁来温暖心房，谁来伴你成长，谁来把柔软换成坚强？ 其实都可以遗忘，圆月光还在耳外，宁静住进了心里，无风，也无浪。","publishtime":"2020-01-03 00:00:00","singername":"风小筝","albumid":35541766,"sum_ownercount":0,"songcount":2,"buycount":0,"privilege":0,"isfirst":0,"singerid":86747,"album_tag":[],"trans_param":{"special_tag":"0"},"imgurl":"http://imge.kugou.com/stdmusic/{size}/20191231/20191231183110819993.jpg"}]
         * total : 57
         */

        private int timestamp;
        private int total;
        private List<InfoBean> info;

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * albumname : 想哭
             * intro : ”我寄居回忆等一个相遇，把没说的话都附送给你“不再联系，想哭却哭不出声音，对着影子 安慰着自己，风小筝新歌《想哭》发行上线，用心聆听！
             * publishtime : 2021-09-09 00:00:00
             * singername : 风小筝
             * albumid : 48639853
             * sum_ownercount : 0
             * songcount : 2
             * buycount : 0
             * privilege : 0
             * isfirst : 0
             * singerid : 86747
             * album_tag : []
             * trans_param : {"special_tag":"0"}
             * imgurl : http://imge.kugou.com/stdmusic/{size}/20210908/20210908190810685165.jpg
             */

            private String albumname;
            private String intro;
            private String publishtime;
            private String singername;
            private int albumid;
            private int sum_ownercount;
            private int songcount;
            private int buycount;
            private int privilege;
            private int isfirst;
            private int singerid;
            private TransParamBean trans_param;
            private String imgurl;
            private List<?> album_tag;

            public String getAlbumname() {
                return albumname;
            }

            public void setAlbumname(String albumname) {
                this.albumname = albumname;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public String getPublishtime() {
                return publishtime;
            }

            public void setPublishtime(String publishtime) {
                this.publishtime = publishtime;
            }

            public String getSingername() {
                return singername;
            }

            public void setSingername(String singername) {
                this.singername = singername;
            }

            public int getAlbumid() {
                return albumid;
            }

            public void setAlbumid(int albumid) {
                this.albumid = albumid;
            }

            public int getSum_ownercount() {
                return sum_ownercount;
            }

            public void setSum_ownercount(int sum_ownercount) {
                this.sum_ownercount = sum_ownercount;
            }

            public int getSongcount() {
                return songcount;
            }

            public void setSongcount(int songcount) {
                this.songcount = songcount;
            }

            public int getBuycount() {
                return buycount;
            }

            public void setBuycount(int buycount) {
                this.buycount = buycount;
            }

            public int getPrivilege() {
                return privilege;
            }

            public void setPrivilege(int privilege) {
                this.privilege = privilege;
            }

            public int getIsfirst() {
                return isfirst;
            }

            public void setIsfirst(int isfirst) {
                this.isfirst = isfirst;
            }

            public int getSingerid() {
                return singerid;
            }

            public void setSingerid(int singerid) {
                this.singerid = singerid;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public String getImgurl() {
                return imgurl;
            }

            public void setImgurl(String imgurl) {
                this.imgurl = imgurl;
            }

            public List<?> getAlbum_tag() {
                return album_tag;
            }

            public void setAlbum_tag(List<?> album_tag) {
                this.album_tag = album_tag;
            }

            public static class TransParamBean {
                /**
                 * special_tag : 0
                 */

                private String special_tag;

                public String getSpecial_tag() {
                    return special_tag;
                }

                public void setSpecial_tag(String special_tag) {
                    this.special_tag = special_tag;
                }
            }
        }
    }
}
