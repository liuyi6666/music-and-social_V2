package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

/**
 * 酷狗音乐播放音乐的api
 */
public class KugouMusic implements Serializable {

    private static final long serialVersionUID = -8878564488532340465L;
    /**
     * status : 1
     * err_code : 0
     * data : {"hash":"3B15F1E26C4F8E24D5C7129FB61D487A","timelength":314000,"filesize":5020713,"audio_name":"周深 - 大鱼","have_album":1,"album_name":"大鱼","album_id":"1637713","img":"http://imge.kugou.com/stdmusic/20200909/20200909120403560507.jpg","have_mv":1,"video_id":"615668","author_name":"周深","song_name":"大鱼","lyrics":"﻿[id:$00000000]\r\n[ar:周深]\r\n[ti:大鱼]\r\n[by:]\r\n[hash:3b15f1e26c4f8e24d5c7129fb61d487a]\r\n[al:]\r\n[sign:]\r\n[qq:]\r\n[total:314000]\r\n[offset:0]\r\n[00:00.48]周深 - 大鱼\r\n[00:01.09]作词：尹约\r\n[00:01.24]作曲：钱雷\r\n[00:43.41]海浪无声将夜幕深深淹没\r\n[00:50.14]漫过天空尽头的角落\r\n[00:56.64]大鱼在梦境的缝隙里游过\r\n[01:03.76]凝望你沉睡的轮廓\r\n[01:09.78]看海天一色 听风起雨落\r\n[01:16.52]执子手吹散苍茫茫烟波\r\n[01:23.56]大鱼的翅膀 已经太辽阔\r\n[01:31.29]我松开时间的绳索\r\n[01:37.22]怕你飞远去\r\n[01:40.67]怕你离我而去\r\n[01:43.96]更怕你永远停留在这里\r\n[01:50.79]每一滴泪水\r\n[01:54.38]都向你流淌去\r\n[01:58.74]倒流进天空的海底\r\n[02:19.20]海浪无声将夜幕深深淹没\r\n[02:25.98]漫过天空尽头的角落\r\n[02:32.92]大鱼在梦境的缝隙里游过\r\n[02:39.70]凝望你沉睡的轮廓\r\n[02:45.68]看海天一色\r\n[02:49.03]听风起雨落\r\n[02:52.47]执子手吹散苍茫茫烟波\r\n[02:59.51]大鱼的翅膀\r\n[03:02.81]已经太辽阔\r\n[03:07.12]我松开时间的绳索\r\n[03:13.15]看你飞远去\r\n[03:16.50]看你离我而去\r\n[03:19.79]原来你生来就属于天际\r\n[03:26.73]每一滴泪水\r\n[03:30.22]都向你流淌去\r\n[03:34.62]倒流回最初的相遇\r\n[04:06.34]啊啊啊啊啊啊啊啊\r\n[04:28.13]啊啊啊\r\n","author_id":"169967","privilege":0,"privilege2":"0","play_url":"https://webfs.ali.kugou.com/202109201740/e920c82f99f1112ff1cb061ae50abd11/G063/M08/05/11/H5QEAFc-bc6AfD3uAEycKVKAkaw783.mp3","authors":[{"author_id":"169967","author_name":"周深","is_publish":"1","sizable_avatar":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg","avatar":"http://singerimg.kugou.com/uploadpic/softhead/400/20210616/20210616154403987.jpg"}],"is_free_part":0,"bitrate":128,"recommend_album_id":"1637713","audio_id":"21562630","has_privilege":true,"play_backup_url":"https://webfs.cloud.kugou.com/202109201740/ce1ac518ed96a1ab329f5ed09a7e1b97/G063/M08/05/11/H5QEAFc-bc6AfD3uAEycKVKAkaw783.mp3"}
     */

    private int status;
    private int err_code;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getErr_code() {
        return err_code;
    }

    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * hash : 3B15F1E26C4F8E24D5C7129FB61D487A
         * timelength : 314000
         * filesize : 5020713
         * audio_name : 周深 - 大鱼
         * have_album : 1
         * album_name : 大鱼
         * album_id : 1637713
         * img : http://imge.kugou.com/stdmusic/20200909/20200909120403560507.jpg
         * have_mv : 1
         * video_id : 615668
         * author_name : 周深
         * song_name : 大鱼
         * lyrics : ﻿[id:$00000000]
         * [ar:周深]
         * [ti:大鱼]
         * [by:]
         * [hash:3b15f1e26c4f8e24d5c7129fb61d487a]
         * [al:]
         * [sign:]
         * [qq:]
         * [total:314000]
         * [offset:0]
         * [00:00.48]周深 - 大鱼
         * [00:01.09]作词：尹约
         * [00:01.24]作曲：钱雷
         * [00:43.41]海浪无声将夜幕深深淹没
         * [00:50.14]漫过天空尽头的角落
         * [00:56.64]大鱼在梦境的缝隙里游过
         * [01:03.76]凝望你沉睡的轮廓
         * [01:09.78]看海天一色 听风起雨落
         * [01:16.52]执子手吹散苍茫茫烟波
         * [01:23.56]大鱼的翅膀 已经太辽阔
         * [01:31.29]我松开时间的绳索
         * [01:37.22]怕你飞远去
         * [01:40.67]怕你离我而去
         * [01:43.96]更怕你永远停留在这里
         * [01:50.79]每一滴泪水
         * [01:54.38]都向你流淌去
         * [01:58.74]倒流进天空的海底
         * [02:19.20]海浪无声将夜幕深深淹没
         * [02:25.98]漫过天空尽头的角落
         * [02:32.92]大鱼在梦境的缝隙里游过
         * [02:39.70]凝望你沉睡的轮廓
         * [02:45.68]看海天一色
         * [02:49.03]听风起雨落
         * [02:52.47]执子手吹散苍茫茫烟波
         * [02:59.51]大鱼的翅膀
         * [03:02.81]已经太辽阔
         * [03:07.12]我松开时间的绳索
         * [03:13.15]看你飞远去
         * [03:16.50]看你离我而去
         * [03:19.79]原来你生来就属于天际
         * [03:26.73]每一滴泪水
         * [03:30.22]都向你流淌去
         * [03:34.62]倒流回最初的相遇
         * [04:06.34]啊啊啊啊啊啊啊啊
         * [04:28.13]啊啊啊
         * author_id : 169967
         * privilege : 0
         * privilege2 : 0
         * play_url : https://webfs.ali.kugou.com/202109201740/e920c82f99f1112ff1cb061ae50abd11/G063/M08/05/11/H5QEAFc-bc6AfD3uAEycKVKAkaw783.mp3
         * authors : [{"author_id":"169967","author_name":"周深","is_publish":"1","sizable_avatar":"http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg","avatar":"http://singerimg.kugou.com/uploadpic/softhead/400/20210616/20210616154403987.jpg"}]
         * is_free_part : 0
         * bitrate : 128
         * recommend_album_id : 1637713
         * audio_id : 21562630
         * has_privilege : true
         * play_backup_url : https://webfs.cloud.kugou.com/202109201740/ce1ac518ed96a1ab329f5ed09a7e1b97/G063/M08/05/11/H5QEAFc-bc6AfD3uAEycKVKAkaw783.mp3
         */

        private String hash;
        private int timelength;
        private int filesize;
        private String audio_name;
        private int have_album;
        private String album_name;
        private String album_id;
        private String img;
        private int have_mv;
        private String video_id;
        private String author_name;
        private String song_name;
        private String lyrics;
        private String author_id;
        private int privilege;
        private String privilege2;
        private String play_url;
        private int is_free_part;
        private int bitrate;
        private String recommend_album_id;
        private String audio_id;
        private boolean has_privilege;
        private String play_backup_url;
        private List<AuthorsBean> authors;

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public int getTimelength() {
            return timelength;
        }

        public void setTimelength(int timelength) {
            this.timelength = timelength;
        }

        public int getFilesize() {
            return filesize;
        }

        public void setFilesize(int filesize) {
            this.filesize = filesize;
        }

        public String getAudio_name() {
            return audio_name;
        }

        public void setAudio_name(String audio_name) {
            this.audio_name = audio_name;
        }

        public int getHave_album() {
            return have_album;
        }

        public void setHave_album(int have_album) {
            this.have_album = have_album;
        }

        public String getAlbum_name() {
            return album_name;
        }

        public void setAlbum_name(String album_name) {
            this.album_name = album_name;
        }

        public String getAlbum_id() {
            return album_id;
        }

        public void setAlbum_id(String album_id) {
            this.album_id = album_id;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getHave_mv() {
            return have_mv;
        }

        public void setHave_mv(int have_mv) {
            this.have_mv = have_mv;
        }

        public String getVideo_id() {
            return video_id;
        }

        public void setVideo_id(String video_id) {
            this.video_id = video_id;
        }

        public String getAuthor_name() {
            return author_name;
        }

        public void setAuthor_name(String author_name) {
            this.author_name = author_name;
        }

        public String getSong_name() {
            return song_name;
        }

        public void setSong_name(String song_name) {
            this.song_name = song_name;
        }

        public String getLyrics() {
            return lyrics;
        }

        public void setLyrics(String lyrics) {
            this.lyrics = lyrics;
        }

        public String getAuthor_id() {
            return author_id;
        }

        public void setAuthor_id(String author_id) {
            this.author_id = author_id;
        }

        public int getPrivilege() {
            return privilege;
        }

        public void setPrivilege(int privilege) {
            this.privilege = privilege;
        }

        public String getPrivilege2() {
            return privilege2;
        }

        public void setPrivilege2(String privilege2) {
            this.privilege2 = privilege2;
        }

        public String getPlay_url() {
            return play_url;
        }

        public void setPlay_url(String play_url) {
            this.play_url = play_url;
        }

        public int getIs_free_part() {
            return is_free_part;
        }

        public void setIs_free_part(int is_free_part) {
            this.is_free_part = is_free_part;
        }

        public int getBitrate() {
            return bitrate;
        }

        public void setBitrate(int bitrate) {
            this.bitrate = bitrate;
        }

        public String getRecommend_album_id() {
            return recommend_album_id;
        }

        public void setRecommend_album_id(String recommend_album_id) {
            this.recommend_album_id = recommend_album_id;
        }

        public String getAudio_id() {
            return audio_id;
        }

        public void setAudio_id(String audio_id) {
            this.audio_id = audio_id;
        }

        public boolean isHas_privilege() {
            return has_privilege;
        }

        public void setHas_privilege(boolean has_privilege) {
            this.has_privilege = has_privilege;
        }

        public String getPlay_backup_url() {
            return play_backup_url;
        }

        public void setPlay_backup_url(String play_backup_url) {
            this.play_backup_url = play_backup_url;
        }

        public List<AuthorsBean> getAuthors() {
            return authors;
        }

        public void setAuthors(List<AuthorsBean> authors) {
            this.authors = authors;
        }

        public static class AuthorsBean {
            /**
             * author_id : 169967
             * author_name : 周深
             * is_publish : 1
             * sizable_avatar : http://singerimg.kugou.com/uploadpic/softhead/{size}/20210616/20210616154403987.jpg
             * avatar : http://singerimg.kugou.com/uploadpic/softhead/400/20210616/20210616154403987.jpg
             */

            private String author_id;
            private String author_name;
            private String is_publish;
            private String sizable_avatar;
            private String avatar;

            public String getAuthor_id() {
                return author_id;
            }

            public void setAuthor_id(String author_id) {
                this.author_id = author_id;
            }

            public String getAuthor_name() {
                return author_name;
            }

            public void setAuthor_name(String author_name) {
                this.author_name = author_name;
            }

            public String getIs_publish() {
                return is_publish;
            }

            public void setIs_publish(String is_publish) {
                this.is_publish = is_publish;
            }

            public String getSizable_avatar() {
                return sizable_avatar;
            }

            public void setSizable_avatar(String sizable_avatar) {
                this.sizable_avatar = sizable_avatar;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }
        }
    }
}
