package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

/**
 * 酷狗音乐搜索的api
 */
public class KugouSearch implements Serializable {
    private static final long serialVersionUID = -5923407411748738609L;

    /**
     * status : 1
     * data : {"aggregation":[{"key":"DJ","count":0},{"key":"现场","count":0},{"key":"广场舞","count":0},{"key":"伴奏","count":0},{"key":"铃声","count":0}],"searchfull":1,"total":463,"istagresult":0,"isshareresult":0,"page":1,"chinesecount":2,"lists":[{"Suffix":"","SongName":"大鱼","OwnerCount":18678,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":31584110,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":314,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21562630,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21562630,"HiFiQuality":2,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":1,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"评论过万","ResBitrate":0,"FoldType":0,"FileHash":"3B15F1E26C4F8E24D5C7129FB61D487A","A320Privilege":0,"SQPayType":0,"AlbumID":"1637713","AlbumName":"大鱼","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"38641536","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"38641536","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"FB863CA0763044280F1197CC05A23210","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"546C3C6CD0EBD887A11549D412D01D66","mvTotal":0,"PublishTime":"","MvHash":"94372BDC04D53DEFA0BA03BA2E9F4DF6","SQPrivilege":0,"SQBitrate":805,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5020713,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12551313,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":1,"FileName":"周深 - 大鱼","SourceID":0},{"Suffix":"(钢琴版)","SongName":"大鱼 (钢琴版)","OwnerCount":492,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69228386,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":69228386,"HiFiQuality":1,"Grp":[],"OriOtherName":"钢琴版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"0F4902715DC991BB9E131DF400F936C6","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252060937","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252060937","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"604EE5CCCDAD8316F9C57DD31FBAD3E2","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":140,"OtherName":"","FileSize":2242972,"SQPrice":0,"ResDuration":0,"SingerId":[87582],"Price":0,"Singers":[{"name":"纯音乐","id":87582}],"SingerName":"纯音乐","HQPayType":0,"HQFileSize":5606966,"HQPrice":0,"HQDuration":140,"PayType":0,"HasAlbum":0,"FileName":"纯音乐 - 大鱼 (钢琴版)","SourceID":0},{"Suffix":"(2017中国新歌声第二季第10期现场)","SongName":"大鱼 (2017中国新歌声第二季第10期现场)","OwnerCount":345,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":36288323,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":36288323,"HiFiQuality":0,"Grp":[],"OriOtherName":"2017中国新歌声第二季第10期现场","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":1,"classmap":{"attr0":8},"display":32,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"0A4F8291BD4462B7DA5A708C4EAE3B07","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"105265117","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"105265117","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"46C6947B709FBB39F722A7F5CCEDD95C","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":293,"OtherName":"","FileSize":4692069,"SQPrice":0,"ResDuration":0,"SingerId":[749377,169967],"Price":0,"Singers":[{"name":"郭沁","id":749377},{"name":"周深","id":169967}],"SingerName":"郭沁、周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"郭沁、周深 - 大鱼 (2017中国新歌声第二季第10期现场)","SourceID":0},{"Suffix":"(天籁女声版)","SongName":"大鱼 (天籁女声版)","OwnerCount":194,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":59623615,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":59623615,"HiFiQuality":1,"Grp":[],"OriOtherName":"天籁女声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"635222B84DA8997149336C831878A685","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"203732610","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"203732610","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"E174C06AEB50F3E0D9CAEA6F52A253BD","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5040327,"SQPrice":0,"ResDuration":0,"SingerId":[943690],"Price":0,"Singers":[{"name":"叶微凉","id":943690}],"SingerName":"叶微凉","HQPayType":0,"HQFileSize":12600423,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":0,"FileName":"叶微凉 - 大鱼 (天籁女声版)","SourceID":0},{"Suffix":"(KTV版伴奏)","SongName":"大鱼 (KTV版伴奏)","OwnerCount":1260,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":22696543,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":22696543,"HiFiQuality":1,"Grp":[{"Suffix":"(伴奏)","SongName":"大鱼 (伴奏)","OwnerCount":46,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":48820301,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":48820301,"HiFiQuality":0,"OriOtherName":"伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"596B91E753DCC921CA5E495957F866F4","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"129379071","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"129379071","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":324,"OtherName":"","FileSize":5191933,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (伴奏)","SourceID":0},{"Suffix":"(降调伴奏)","SongName":"大鱼 (降调伴奏)","OwnerCount":22,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":27532751,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":27532751,"HiFiQuality":0,"OriOtherName":"降调伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"FBEC52353D8E9164F0032456673D3483","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"68040977","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"68040977","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":324,"OtherName":"","FileSize":5191933,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (降调伴奏)","SourceID":0},{"Suffix":"(Live伴奏)","SongName":"大鱼 (Live伴奏)","OwnerCount":3,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":114642298,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":114642298,"HiFiQuality":0,"OriOtherName":"Live伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"358C3E056AF3A002990321BD837946A8","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"328579335","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"328579335","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":289,"OtherName":"","FileSize":4627797,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (Live伴奏)","SourceID":0},{"Suffix":"(升调版伴奏)","SongName":"大鱼 (升调版伴奏)","OwnerCount":3,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":21653194,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21653194,"HiFiQuality":1,"OriOtherName":"升调版伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"208B9AFC2AEF1028A1C81F1D8FF73C18","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"50129434","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"50129434","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"0B0D416BE2884036F900A5525C5C0262","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":320,"OtherName":"","FileSize":5120896,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12803266,"HQPrice":0,"HQDuration":320,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (升调版伴奏)","SourceID":0}],"OriOtherName":"KTV版伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":1,"FileHash":"3B5473D9CDEDB94B9DD3153A775272D1","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"50812628","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"50812628","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"E80E3BA7C3F50661ACFA09EB53C442A5","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5022241,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12555538,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (KTV版伴奏)","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":114,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"Grp":[{"Suffix":"","SongName":"大鱼","OwnerCount":5,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"F2B5941EEFE2A866D716A07CEC60C813","A320Privilege":0,"SQPayType":0,"AlbumID":"8505780","AlbumName":"大鱼(翻唱)","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"39672924","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"39672924","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5110579,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":5,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"DCC69469EEF35187DD78B30FF4275CBF","A320Privilege":0,"SQPayType":0,"AlbumID":"14471934","AlbumName":"默认专辑","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"128284033","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"128284033","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":317,"OtherName":"","FileSize":5076994,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0}],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":49839458,"pay_block_tpl":1,"classmap":{"attr0":8},"display_rate":0,"appid_block":"3124","display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"评论过万","ResBitrate":0,"FoldType":0,"FileHash":"F2B5941EEFE2A866D716A07CEC60C813","A320Privilege":0,"SQPayType":0,"AlbumID":"1672318","AlbumName":"翻唱歌曲合集","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"107653150","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"107653150","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5110579,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0},{"Suffix":"(2017中国新歌声第二季第十期现场伴奏)","SongName":"大鱼 (2017中国新歌声第二季第十期现场伴奏)","OwnerCount":197,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":28813881,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":300,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":28494698,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":28494698,"HiFiQuality":2,"Grp":[],"OriOtherName":"2017中国新歌声第二季第十期现场伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":1,"classmap":{"attr0":8},"display":32,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":1,"FileHash":"C359C5B41AA2342446459B567D6BEB2A","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"87973982","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"87973982","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"2906A83B78F57631DF0A7674D5DB5306","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"AC21D4A205081279194A4C347CBC1647","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":767,"PkgPrice":0,"M4aSize":0,"Duration":300,"OtherName":"","FileSize":4806323,"SQPrice":0,"ResDuration":0,"SingerId":[169967,749377],"Price":0,"Singers":[{"name":"周深","id":169967},{"name":"郭沁","id":749377}],"SingerName":"周深、郭沁","HQPayType":0,"HQFileSize":12015360,"HQPrice":0,"HQDuration":300,"PayType":0,"HasAlbum":0,"FileName":"周深、郭沁 - 大鱼 (2017中国新歌声第二季第十期现场伴奏)","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":39,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":30383562,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":320,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":23296661,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":23296661,"HiFiQuality":2,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"CC86798285FF1F9032D1F4424AEC4691","A320Privilege":0,"SQPayType":0,"AlbumID":"14015862","AlbumName":"冯提莫合集","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"53532590","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"53532590","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"7AC4A6667075AC74975FB7EDAC5A9F6C","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"F0E15DBADD267C88B0F2A7646B8ABCEC","mvTotal":0,"PublishTime":"","MvHash":"A3C0F3A9572FCE7CB22A8E65000A1A1B","SQPrivilege":0,"SQBitrate":757,"PkgPrice":0,"M4aSize":0,"Duration":321,"OtherName":"","FileSize":5136717,"SQPrice":0,"ResDuration":0,"SingerId":[552330],"Price":0,"Singers":[{"name":"冯提莫","id":552330}],"SingerName":"冯提莫","HQPayType":0,"HQFileSize":12842010,"HQPrice":0,"HQDuration":321,"PayType":0,"HasAlbum":1,"FileName":"冯提莫 - 大鱼","SourceID":0},{"Suffix":"(古筝版)","SongName":"大鱼 (古筝版)","OwnerCount":34,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":3,"Scid":80485521,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":80485521,"HiFiQuality":1,"Grp":[],"OriOtherName":"古筝版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":111725638,"pay_block_tpl":2,"musicpack_advance":0,"display_rate":8,"classmap":{"attr0":8},"display":64},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"55F85BD50CD0E839241F2E6BB5F7B5C7","A320Privilege":0,"SQPayType":0,"AlbumID":"37665462","AlbumName":"纯音乐洗礼-放松、舒适、升华","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"271669243","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"271669243","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"A341418FA79532A156D98FCD0A58EE30","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":304,"OtherName":"","FileSize":4865924,"SQPrice":0,"ResDuration":0,"SingerId":[991585],"Price":0,"Singers":[{"name":"大自然的声音","id":991585}],"SingerName":"大自然的声音","HQPayType":0,"HQFileSize":12182075,"HQPrice":0,"HQDuration":304,"PayType":0,"HasAlbum":1,"FileName":"大自然的声音 - 大鱼 (古筝版)","SourceID":0},{"Suffix":"(钢琴版)","SongName":"大鱼 (钢琴版)","OwnerCount":96,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":50734575,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":50734575,"HiFiQuality":1,"Grp":[],"OriOtherName":"钢琴版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"C7F9BCE1B1A89FE700ED297CEB5ECDE4","A320Privilege":0,"SQPayType":0,"AlbumID":"35841959","AlbumName":"夜色钢琴曲 改编作品","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"247400536","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"247400536","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"3C409D8E3415FD2CB136B58AA02BE23E","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":295,"OtherName":"","FileSize":4728834,"SQPrice":0,"ResDuration":0,"SingerId":[89493],"Price":0,"Singers":[{"name":"赵海洋","id":89493}],"SingerName":"赵海洋","HQPayType":0,"HQFileSize":11821485,"HQPrice":0,"HQDuration":295,"PayType":0,"HasAlbum":1,"FileName":"赵海洋 - 大鱼 (钢琴版)","SourceID":0},{"Suffix":"(温柔女声版)","SongName":"大鱼 (温柔女声版)","OwnerCount":7,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69558566,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":69558566,"HiFiQuality":0,"Grp":[],"OriOtherName":"温柔女声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"C4CB8BE85315AD7170A509B3FCE2846C","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252618084","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252618084","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":380,"OtherName":"","FileSize":6092608,"SQPrice":0,"ResDuration":0,"SingerId":[853620],"Price":0,"Singers":[{"name":"夏奈","id":853620}],"SingerName":"夏奈","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"夏奈 - 大鱼 (温柔女声版)","SourceID":0},{"Suffix":"(哼唱版片段)","SongName":"大鱼 (哼唱版片段)","OwnerCount":14,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":50225785,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":50225785,"HiFiQuality":1,"Grp":[],"OriOtherName":"哼唱版片段","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"9437EA99896BB4AFB6ADA863B74D9269","A320Privilege":0,"SQPayType":0,"AlbumID":"19214196","AlbumName":"大鱼哼唱","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"132278411","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"132278411","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"F9ED696A3A9B97786934BA48B78DECE1","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":34,"OtherName":"","FileSize":554257,"SQPrice":0,"ResDuration":0,"SingerId":[640260],"Price":0,"Singers":[{"name":"梨花少","id":640260}],"SingerName":"梨花少","HQPayType":0,"HQFileSize":1385578,"HQPrice":0,"HQDuration":34,"PayType":0,"HasAlbum":1,"FileName":"梨花少 - 大鱼 (哼唱版片段)","SourceID":0},{"Suffix":"(童声版)","SongName":"大鱼 (童声版)","OwnerCount":35,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":96612248,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":96612248,"HiFiQuality":1,"Grp":[],"OriOtherName":"童声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"E3417CB4FCB61EF3EFA4413DF67EECFB","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"297231501","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"297231501","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"宋小睿","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"CB970C628ADEFE6A3E559AEF2F7AE1A3","mvTotal":0,"PublishTime":"2021-02-23 16:02:22","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5025167,"SQPrice":0,"ResDuration":0,"SingerId":[825384],"Price":0,"Singers":[{"name":"宋小睿","id":825384}],"SingerName":"宋小睿","HQPayType":0,"HQFileSize":12549924,"HQPrice":0,"HQDuration":313,"PayType":0,"HasAlbum":0,"FileName":"宋小睿 - 大鱼 (童声版)","SourceID":0},{"Suffix":"","SongName":"大鱼(笛子版)","OwnerCount":14,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":79669705,"OriSongName":"大鱼(笛子版)","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":79669705,"HiFiQuality":1,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"863D617FED5AB08DC85223238B81ED3C","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"270420052","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"270420052","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"1459E85F28B6B5F57C797928557BFF23","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":110,"OtherName":"","FileSize":1772608,"SQPrice":0,"ResDuration":0,"SingerId":[0],"Price":0,"Singers":[{"name":"惜君qwq","id":0}],"SingerName":"惜君qwq","HQPayType":0,"HQFileSize":4431456,"HQPrice":0,"HQDuration":110,"PayType":0,"HasAlbum":0,"FileName":"惜君qwq - 大鱼(笛子版)","SourceID":0},{"Suffix":"(男声版)","SongName":"大鱼 (男声版)","OwnerCount":5,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69384073,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":69384073,"HiFiQuality":0,"Grp":[],"OriOtherName":"男声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"2B3FE854D5810549A1D2B1C5F4C0ECD2","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252317098","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252317098","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":311,"OtherName":"","FileSize":4977937,"SQPrice":0,"ResDuration":0,"SingerId":[958194],"Price":0,"Singers":[{"name":"子曦.星辰之下","id":958194}],"SingerName":"子曦.星辰之下","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"子曦.星辰之下 - 大鱼 (男声版)","SourceID":0},{"Suffix":"(Live)","SongName":"大鱼 (Live)","OwnerCount":60,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":108081716,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":108081716,"HiFiQuality":0,"Grp":[],"OriOtherName":"Live","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"875036F74CD5FFB7C66E1024DCAE9B48","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"314389547","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"314389547","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":83,"OtherName":"","FileSize":1334542,"SQPrice":0,"ResDuration":0,"SingerId":[550719],"Price":0,"Singers":[{"name":"马嘉祺","id":550719}],"SingerName":"马嘉祺","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"马嘉祺 - 大鱼 (Live)","SourceID":0},{"Suffix":"(Live)","SongName":"大鱼 (Live)","OwnerCount":29,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":117149764,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":117149764,"HiFiQuality":0,"Grp":[],"OriOtherName":"Live","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"FD858F6325C9BBD77C947ED875EAB60B","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"332633786","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"332633786","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"7D0BF5556ECD248CEDCBAD4EFEE175DC","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":194,"OtherName":"","FileSize":3113013,"SQPrice":0,"ResDuration":0,"SingerId":[169967,167164],"Price":0,"Singers":[{"name":"周深","id":169967},{"name":"方锦龙","id":167164}],"SingerName":"周深、方锦龙","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深、方锦龙 - 大鱼 (Live)","SourceID":0},{"Suffix":"(3D环绕版)","SongName":"大鱼 (3D环绕版)","OwnerCount":9,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":47708074,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":47708074,"HiFiQuality":1,"Grp":[],"OriOtherName":"3D环绕版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"F2C27200D31AAFD1D5C58F32F09906F2","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"177924187","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"177924187","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"5FAE4A3A4C99E5026FF5E438AAD0F16C","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":313,"OtherName":"","FileSize":5022258,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12556603,"HQPrice":0,"HQDuration":313,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (3D环绕版)","SourceID":0},{"Suffix":"(2017拜见小师父现场)","SongName":"大鱼 (2017拜见小师父现场)","OwnerCount":10,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":30209589,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":30209589,"HiFiQuality":0,"Grp":[],"OriOtherName":"2017拜见小师父现场","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"D50BB62AA14553F10DE0959061A3F5C1","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"91741875","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"91741875","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":270,"OtherName":"","FileSize":4331876,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (2017拜见小师父现场)","SourceID":0},{"Suffix":"(琵琶版)","SongName":"大鱼 (琵琶版)","OwnerCount":17,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":3,"Scid":81323008,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":81323008,"HiFiQuality":1,"Grp":[],"OriOtherName":"琵琶版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":112964752,"pay_block_tpl":2,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"234D80172ED5137F3D40D0786D4EF809","A320Privilege":0,"SQPayType":0,"AlbumID":"37736008","AlbumName":"清灵沉静的禅意音乐，唯美心静","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"273212977","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"273212977","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"EE43C7BF1706A2FBD1EFD8D36D2BD4F7","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5112405,"SQPrice":0,"ResDuration":0,"SingerId":[1024083],"Price":0,"Singers":[{"name":"矣微尘","id":1024083}],"SingerName":"矣微尘","HQPayType":0,"HQFileSize":13114824,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"矣微尘 - 大鱼 (琵琶版)","SourceID":0}],"correctiontype":0,"allowerr":0,"correctionsubject":"","subjecttype":0,"pagesize":20,"istag":0,"correctiontip":"","correctionforce":0,"sectag_info":{"is_sectag":0}}
     * error_code : 0
     * error_msg :
     */

    private int status;
    private DataBean data;
    private int error_code;
    private String error_msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public static class DataBean {
        /**
         * aggregation : [{"key":"DJ","count":0},{"key":"现场","count":0},{"key":"广场舞","count":0},{"key":"伴奏","count":0},{"key":"铃声","count":0}]
         * searchfull : 1
         * total : 463
         * istagresult : 0
         * isshareresult : 0
         * page : 1
         * chinesecount : 2
         * lists : [{"Suffix":"","SongName":"大鱼","OwnerCount":18678,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":31584110,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":314,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21562630,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21562630,"HiFiQuality":2,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":1,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"评论过万","ResBitrate":0,"FoldType":0,"FileHash":"3B15F1E26C4F8E24D5C7129FB61D487A","A320Privilege":0,"SQPayType":0,"AlbumID":"1637713","AlbumName":"大鱼","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"38641536","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"38641536","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"FB863CA0763044280F1197CC05A23210","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"546C3C6CD0EBD887A11549D412D01D66","mvTotal":0,"PublishTime":"","MvHash":"94372BDC04D53DEFA0BA03BA2E9F4DF6","SQPrivilege":0,"SQBitrate":805,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5020713,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12551313,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":1,"FileName":"周深 - 大鱼","SourceID":0},{"Suffix":"(钢琴版)","SongName":"大鱼 (钢琴版)","OwnerCount":492,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69228386,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":69228386,"HiFiQuality":1,"Grp":[],"OriOtherName":"钢琴版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"0F4902715DC991BB9E131DF400F936C6","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252060937","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252060937","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"604EE5CCCDAD8316F9C57DD31FBAD3E2","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":140,"OtherName":"","FileSize":2242972,"SQPrice":0,"ResDuration":0,"SingerId":[87582],"Price":0,"Singers":[{"name":"纯音乐","id":87582}],"SingerName":"纯音乐","HQPayType":0,"HQFileSize":5606966,"HQPrice":0,"HQDuration":140,"PayType":0,"HasAlbum":0,"FileName":"纯音乐 - 大鱼 (钢琴版)","SourceID":0},{"Suffix":"(2017中国新歌声第二季第10期现场)","SongName":"大鱼 (2017中国新歌声第二季第10期现场)","OwnerCount":345,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":36288323,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":36288323,"HiFiQuality":0,"Grp":[],"OriOtherName":"2017中国新歌声第二季第10期现场","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":1,"classmap":{"attr0":8},"display":32,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"0A4F8291BD4462B7DA5A708C4EAE3B07","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"105265117","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"105265117","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"46C6947B709FBB39F722A7F5CCEDD95C","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":293,"OtherName":"","FileSize":4692069,"SQPrice":0,"ResDuration":0,"SingerId":[749377,169967],"Price":0,"Singers":[{"name":"郭沁","id":749377},{"name":"周深","id":169967}],"SingerName":"郭沁、周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"郭沁、周深 - 大鱼 (2017中国新歌声第二季第10期现场)","SourceID":0},{"Suffix":"(天籁女声版)","SongName":"大鱼 (天籁女声版)","OwnerCount":194,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":59623615,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":59623615,"HiFiQuality":1,"Grp":[],"OriOtherName":"天籁女声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"635222B84DA8997149336C831878A685","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"203732610","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"203732610","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"E174C06AEB50F3E0D9CAEA6F52A253BD","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5040327,"SQPrice":0,"ResDuration":0,"SingerId":[943690],"Price":0,"Singers":[{"name":"叶微凉","id":943690}],"SingerName":"叶微凉","HQPayType":0,"HQFileSize":12600423,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":0,"FileName":"叶微凉 - 大鱼 (天籁女声版)","SourceID":0},{"Suffix":"(KTV版伴奏)","SongName":"大鱼 (KTV版伴奏)","OwnerCount":1260,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":22696543,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":22696543,"HiFiQuality":1,"Grp":[{"Suffix":"(伴奏)","SongName":"大鱼 (伴奏)","OwnerCount":46,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":48820301,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":48820301,"HiFiQuality":0,"OriOtherName":"伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"596B91E753DCC921CA5E495957F866F4","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"129379071","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"129379071","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":324,"OtherName":"","FileSize":5191933,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (伴奏)","SourceID":0},{"Suffix":"(降调伴奏)","SongName":"大鱼 (降调伴奏)","OwnerCount":22,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":27532751,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":27532751,"HiFiQuality":0,"OriOtherName":"降调伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"FBEC52353D8E9164F0032456673D3483","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"68040977","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"68040977","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":324,"OtherName":"","FileSize":5191933,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (降调伴奏)","SourceID":0},{"Suffix":"(Live伴奏)","SongName":"大鱼 (Live伴奏)","OwnerCount":3,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":114642298,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":114642298,"HiFiQuality":0,"OriOtherName":"Live伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"358C3E056AF3A002990321BD837946A8","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"328579335","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"328579335","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":289,"OtherName":"","FileSize":4627797,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (Live伴奏)","SourceID":0},{"Suffix":"(升调版伴奏)","SongName":"大鱼 (升调版伴奏)","OwnerCount":3,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":21653194,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21653194,"HiFiQuality":1,"OriOtherName":"升调版伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"208B9AFC2AEF1028A1C81F1D8FF73C18","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"50129434","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"50129434","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"0B0D416BE2884036F900A5525C5C0262","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":320,"OtherName":"","FileSize":5120896,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12803266,"HQPrice":0,"HQDuration":320,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (升调版伴奏)","SourceID":0}],"OriOtherName":"KTV版伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":1,"FileHash":"3B5473D9CDEDB94B9DD3153A775272D1","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"50812628","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"50812628","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"E80E3BA7C3F50661ACFA09EB53C442A5","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5022241,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12555538,"HQPrice":0,"HQDuration":314,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (KTV版伴奏)","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":114,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"Grp":[{"Suffix":"","SongName":"大鱼","OwnerCount":5,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"F2B5941EEFE2A866D716A07CEC60C813","A320Privilege":0,"SQPayType":0,"AlbumID":"8505780","AlbumName":"大鱼(翻唱)","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"39672924","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"39672924","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5110579,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":5,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":29721278,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":319,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":21976204,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":21976204,"HiFiQuality":2,"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"Privilege":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"TagContent":"","ResBitrate":0,"FileHash":"DCC69469EEF35187DD78B30FF4275CBF","A320Privilege":0,"SQPayType":0,"AlbumID":"14471934","AlbumName":"默认专辑","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"128284033","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"128284033","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":317,"OtherName":"","FileSize":5076994,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0}],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":49839458,"pay_block_tpl":1,"classmap":{"attr0":8},"display_rate":0,"appid_block":"3124","display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"评论过万","ResBitrate":0,"FoldType":0,"FileHash":"F2B5941EEFE2A866D716A07CEC60C813","A320Privilege":0,"SQPayType":0,"AlbumID":"1672318","AlbumName":"翻唱歌曲合集","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"107653150","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"107653150","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"F64612D998CE4BDAC298706E9E65BF68","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"B7E404744AF21D7BE8FA35E665E15BC2","mvTotal":0,"PublishTime":"","MvHash":"3D75E85E9BC896BBB169EFC942434C69","SQPrivilege":0,"SQBitrate":744,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5110579,"SQPrice":0,"ResDuration":0,"SingerId":[195541],"Price":0,"Singers":[{"name":"双笙(陈元汐)","id":195541}],"SingerName":"双笙(陈元汐)","HQPayType":0,"HQFileSize":12776159,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"双笙(陈元汐) - 大鱼","SourceID":0},{"Suffix":"(2017中国新歌声第二季第十期现场伴奏)","SongName":"大鱼 (2017中国新歌声第二季第十期现场伴奏)","OwnerCount":197,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":28813881,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":300,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":28494698,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":28494698,"HiFiQuality":2,"Grp":[],"OriOtherName":"2017中国新歌声第二季第十期现场伴奏","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":1,"classmap":{"attr0":8},"display":32,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":1,"FileHash":"C359C5B41AA2342446459B567D6BEB2A","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"87973982","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"87973982","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"2906A83B78F57631DF0A7674D5DB5306","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"AC21D4A205081279194A4C347CBC1647","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":767,"PkgPrice":0,"M4aSize":0,"Duration":300,"OtherName":"","FileSize":4806323,"SQPrice":0,"ResDuration":0,"SingerId":[169967,749377],"Price":0,"Singers":[{"name":"周深","id":169967},{"name":"郭沁","id":749377}],"SingerName":"周深、郭沁","HQPayType":0,"HQFileSize":12015360,"HQPrice":0,"HQDuration":300,"PayType":0,"HasAlbum":0,"FileName":"周深、郭沁 - 大鱼 (2017中国新歌声第二季第十期现场伴奏)","SourceID":0},{"Suffix":"","SongName":"大鱼","OwnerCount":39,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":30383562,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":320,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":23296661,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":23296661,"HiFiQuality":2,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"CC86798285FF1F9032D1F4424AEC4691","A320Privilege":0,"SQPayType":0,"AlbumID":"14015862","AlbumName":"冯提莫合集","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"53532590","SuperExtName":"","SQExtName":"flac","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"53532590","SuperFileSize":0,"QualityLevel":3,"SQFileHash":"7AC4A6667075AC74975FB7EDAC5A9F6C","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"F0E15DBADD267C88B0F2A7646B8ABCEC","mvTotal":0,"PublishTime":"","MvHash":"A3C0F3A9572FCE7CB22A8E65000A1A1B","SQPrivilege":0,"SQBitrate":757,"PkgPrice":0,"M4aSize":0,"Duration":321,"OtherName":"","FileSize":5136717,"SQPrice":0,"ResDuration":0,"SingerId":[552330],"Price":0,"Singers":[{"name":"冯提莫","id":552330}],"SingerName":"冯提莫","HQPayType":0,"HQFileSize":12842010,"HQPrice":0,"HQDuration":321,"PayType":0,"HasAlbum":1,"FileName":"冯提莫 - 大鱼","SourceID":0},{"Suffix":"(古筝版)","SongName":"大鱼 (古筝版)","OwnerCount":34,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":3,"Scid":80485521,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":80485521,"HiFiQuality":1,"Grp":[],"OriOtherName":"古筝版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":111725638,"pay_block_tpl":2,"musicpack_advance":0,"display_rate":8,"classmap":{"attr0":8},"display":64},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"55F85BD50CD0E839241F2E6BB5F7B5C7","A320Privilege":0,"SQPayType":0,"AlbumID":"37665462","AlbumName":"纯音乐洗礼-放松、舒适、升华","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"271669243","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"271669243","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"A341418FA79532A156D98FCD0A58EE30","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":304,"OtherName":"","FileSize":4865924,"SQPrice":0,"ResDuration":0,"SingerId":[991585],"Price":0,"Singers":[{"name":"大自然的声音","id":991585}],"SingerName":"大自然的声音","HQPayType":0,"HQFileSize":12182075,"HQPrice":0,"HQDuration":304,"PayType":0,"HasAlbum":1,"FileName":"大自然的声音 - 大鱼 (古筝版)","SourceID":0},{"Suffix":"(钢琴版)","SongName":"大鱼 (钢琴版)","OwnerCount":96,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":50734575,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":50734575,"HiFiQuality":1,"Grp":[],"OriOtherName":"钢琴版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"C7F9BCE1B1A89FE700ED297CEB5ECDE4","A320Privilege":0,"SQPayType":0,"AlbumID":"35841959","AlbumName":"夜色钢琴曲 改编作品","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"247400536","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"247400536","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"3C409D8E3415FD2CB136B58AA02BE23E","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":295,"OtherName":"","FileSize":4728834,"SQPrice":0,"ResDuration":0,"SingerId":[89493],"Price":0,"Singers":[{"name":"赵海洋","id":89493}],"SingerName":"赵海洋","HQPayType":0,"HQFileSize":11821485,"HQPrice":0,"HQDuration":295,"PayType":0,"HasAlbum":1,"FileName":"赵海洋 - 大鱼 (钢琴版)","SourceID":0},{"Suffix":"(温柔女声版)","SongName":"大鱼 (温柔女声版)","OwnerCount":7,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69558566,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":69558566,"HiFiQuality":0,"Grp":[],"OriOtherName":"温柔女声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"C4CB8BE85315AD7170A509B3FCE2846C","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252618084","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252618084","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":380,"OtherName":"","FileSize":6092608,"SQPrice":0,"ResDuration":0,"SingerId":[853620],"Price":0,"Singers":[{"name":"夏奈","id":853620}],"SingerName":"夏奈","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"夏奈 - 大鱼 (温柔女声版)","SourceID":0},{"Suffix":"(哼唱版片段)","SongName":"大鱼 (哼唱版片段)","OwnerCount":14,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":1,"Scid":50225785,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":50225785,"HiFiQuality":1,"Grp":[],"OriOtherName":"哼唱版片段","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"9437EA99896BB4AFB6ADA863B74D9269","A320Privilege":0,"SQPayType":0,"AlbumID":"19214196","AlbumName":"大鱼哼唱","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"132278411","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"132278411","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"F9ED696A3A9B97786934BA48B78DECE1","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":34,"OtherName":"","FileSize":554257,"SQPrice":0,"ResDuration":0,"SingerId":[640260],"Price":0,"Singers":[{"name":"梨花少","id":640260}],"SingerName":"梨花少","HQPayType":0,"HQFileSize":1385578,"HQPrice":0,"HQDuration":34,"PayType":0,"HasAlbum":1,"FileName":"梨花少 - 大鱼 (哼唱版片段)","SourceID":0},{"Suffix":"(童声版)","SongName":"大鱼 (童声版)","OwnerCount":35,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":96612248,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":96612248,"HiFiQuality":1,"Grp":[],"OriOtherName":"童声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"E3417CB4FCB61EF3EFA4413DF67EECFB","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"297231501","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"297231501","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"宋小睿","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"CB970C628ADEFE6A3E559AEF2F7AE1A3","mvTotal":0,"PublishTime":"2021-02-23 16:02:22","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":314,"OtherName":"","FileSize":5025167,"SQPrice":0,"ResDuration":0,"SingerId":[825384],"Price":0,"Singers":[{"name":"宋小睿","id":825384}],"SingerName":"宋小睿","HQPayType":0,"HQFileSize":12549924,"HQPrice":0,"HQDuration":313,"PayType":0,"HasAlbum":0,"FileName":"宋小睿 - 大鱼 (童声版)","SourceID":0},{"Suffix":"","SongName":"大鱼(笛子版)","OwnerCount":14,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":79669705,"OriSongName":"大鱼(笛子版)","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":79669705,"HiFiQuality":1,"Grp":[],"OriOtherName":"","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"863D617FED5AB08DC85223238B81ED3C","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"270420052","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"270420052","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"1459E85F28B6B5F57C797928557BFF23","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":110,"OtherName":"","FileSize":1772608,"SQPrice":0,"ResDuration":0,"SingerId":[0],"Price":0,"Singers":[{"name":"惜君qwq","id":0}],"SingerName":"惜君qwq","HQPayType":0,"HQFileSize":4431456,"HQPrice":0,"HQDuration":110,"PayType":0,"HasAlbum":0,"FileName":"惜君qwq - 大鱼(笛子版)","SourceID":0},{"Suffix":"(男声版)","SongName":"大鱼 (男声版)","OwnerCount":5,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":69384073,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":69384073,"HiFiQuality":0,"Grp":[],"OriOtherName":"男声版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"2B3FE854D5810549A1D2B1C5F4C0ECD2","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"252317098","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"252317098","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":311,"OtherName":"","FileSize":4977937,"SQPrice":0,"ResDuration":0,"SingerId":[958194],"Price":0,"Singers":[{"name":"子曦.星辰之下","id":958194}],"SingerName":"子曦.星辰之下","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"子曦.星辰之下 - 大鱼 (男声版)","SourceID":0},{"Suffix":"(Live)","SongName":"大鱼 (Live)","OwnerCount":60,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":108081716,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":108081716,"HiFiQuality":0,"Grp":[],"OriOtherName":"Live","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"875036F74CD5FFB7C66E1024DCAE9B48","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"314389547","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"314389547","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":83,"OtherName":"","FileSize":1334542,"SQPrice":0,"ResDuration":0,"SingerId":[550719],"Price":0,"Singers":[{"name":"马嘉祺","id":550719}],"SingerName":"马嘉祺","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"马嘉祺 - 大鱼 (Live)","SourceID":0},{"Suffix":"(Live)","SongName":"大鱼 (Live)","OwnerCount":29,"MvType":2,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":3,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":117149764,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":117149764,"HiFiQuality":0,"Grp":[],"OriOtherName":"Live","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"FD858F6325C9BBD77C947ED875EAB60B","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"332633786","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"332633786","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"7D0BF5556ECD248CEDCBAD4EFEE175DC","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":194,"OtherName":"","FileSize":3113013,"SQPrice":0,"ResDuration":0,"SingerId":[169967,167164],"Price":0,"Singers":[{"name":"周深","id":169967},{"name":"方锦龙","id":167164}],"SingerName":"周深、方锦龙","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深、方锦龙 - 大鱼 (Live)","SourceID":0},{"Suffix":"(3D环绕版)","SongName":"大鱼 (3D环绕版)","OwnerCount":9,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":47708074,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":47708074,"HiFiQuality":1,"Grp":[],"OriOtherName":"3D环绕版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"F2C27200D31AAFD1D5C58F32F09906F2","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"177924187","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"177924187","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"5FAE4A3A4C99E5026FF5E438AAD0F16C","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":313,"OtherName":"","FileSize":5022258,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":12556603,"HQPrice":0,"HQDuration":313,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (3D环绕版)","SourceID":0},{"Suffix":"(2017拜见小师父现场)","SongName":"大鱼 (2017拜见小师父现场)","OwnerCount":10,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"","SQFileSize":0,"Accompany":1,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":0,"Scid":30209589,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":0,"Audioid":30209589,"HiFiQuality":0,"Grp":[],"OriOtherName":"2017拜见小师父现场","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"D50BB62AA14553F10DE0959061A3F5C1","A320Privilege":0,"SQPayType":0,"AlbumID":"","AlbumName":"","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"91741875","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"91741875","SuperFileSize":0,"QualityLevel":1,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":270,"OtherName":"","FileSize":4331876,"SQPrice":0,"ResDuration":0,"SingerId":[169967],"Price":0,"Singers":[{"name":"周深","id":169967}],"SingerName":"周深","HQPayType":0,"HQFileSize":0,"HQPrice":0,"HQDuration":0,"PayType":0,"HasAlbum":0,"FileName":"周深 - 大鱼 (2017拜见小师父现场)","SourceID":0},{"Suffix":"(琵琶版)","SongName":"大鱼 (琵琶版)","OwnerCount":17,"MvType":0,"TopicRemark":"","SQFailProcess":0,"Source":"","Bitrate":128,"HQExtName":"mp3","SQFileSize":0,"Accompany":0,"AudioCdn":100,"MvTrac":0,"SQDuration":0,"recommend_type":0,"ExtName":"mp3","Auxiliary":"","SQPkgPrice":0,"Category":3,"Scid":81323008,"OriSongName":"大鱼","FailProcess":0,"HQPkgPrice":0,"HQBitrate":320,"Audioid":81323008,"HiFiQuality":1,"Grp":[],"OriOtherName":"琵琶版","AlbumPrivilege":0,"TopicUrl":"","SuperFileHash":"","ASQPrivilege":0,"OldCpy":1,"IsOriginal":0,"trans_param":{"cid":112964752,"pay_block_tpl":2,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0},"Privilege":0,"TagContent":"","ResBitrate":0,"FoldType":0,"FileHash":"234D80172ED5137F3D40D0786D4EF809","A320Privilege":0,"SQPayType":0,"AlbumID":"37736008","AlbumName":"清灵沉静的禅意音乐，唯美心静","MatchFlag":0,"vvid":"","Type":"audio","MixSongID":"273212977","SuperExtName":"","SQExtName":"","ResFileSize":0,"Publish":1,"SuperBitrate":0,"ID":"273212977","SuperFileSize":0,"QualityLevel":2,"SQFileHash":"","Uploader":"","HQPrivilege":0,"AlbumAux":"","SuperDuration":0,"SongLabel":"","ResFileHash":"","PublishAge":255,"HQFailProcess":0,"HQFileHash":"EE43C7BF1706A2FBD1EFD8D36D2BD4F7","mvTotal":0,"PublishTime":"","MvHash":"","SQPrivilege":0,"SQBitrate":0,"PkgPrice":0,"M4aSize":0,"Duration":319,"OtherName":"","FileSize":5112405,"SQPrice":0,"ResDuration":0,"SingerId":[1024083],"Price":0,"Singers":[{"name":"矣微尘","id":1024083}],"SingerName":"矣微尘","HQPayType":0,"HQFileSize":13114824,"HQPrice":0,"HQDuration":319,"PayType":0,"HasAlbum":1,"FileName":"矣微尘 - 大鱼 (琵琶版)","SourceID":0}]
         * correctiontype : 0
         * allowerr : 0
         * correctionsubject :
         * subjecttype : 0
         * pagesize : 20
         * istag : 0
         * correctiontip :
         * correctionforce : 0
         * sectag_info : {"is_sectag":0}
         */

        private int searchfull;
        private int total;
        private int istagresult;
        private int isshareresult;
        private int page;
        private int chinesecount;
        private int correctiontype;
        private int allowerr;
        private String correctionsubject;
        private int subjecttype;
        private int pagesize;
        private int istag;
        private String correctiontip;
        private int correctionforce;
        private SectagInfoBean sectag_info;
        private List<AggregationBean> aggregation;
        private List<ListsBean> lists;

        public int getSearchfull() {
            return searchfull;
        }

        public void setSearchfull(int searchfull) {
            this.searchfull = searchfull;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getIstagresult() {
            return istagresult;
        }

        public void setIstagresult(int istagresult) {
            this.istagresult = istagresult;
        }

        public int getIsshareresult() {
            return isshareresult;
        }

        public void setIsshareresult(int isshareresult) {
            this.isshareresult = isshareresult;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getChinesecount() {
            return chinesecount;
        }

        public void setChinesecount(int chinesecount) {
            this.chinesecount = chinesecount;
        }

        public int getCorrectiontype() {
            return correctiontype;
        }

        public void setCorrectiontype(int correctiontype) {
            this.correctiontype = correctiontype;
        }

        public int getAllowerr() {
            return allowerr;
        }

        public void setAllowerr(int allowerr) {
            this.allowerr = allowerr;
        }

        public String getCorrectionsubject() {
            return correctionsubject;
        }

        public void setCorrectionsubject(String correctionsubject) {
            this.correctionsubject = correctionsubject;
        }

        public int getSubjecttype() {
            return subjecttype;
        }

        public void setSubjecttype(int subjecttype) {
            this.subjecttype = subjecttype;
        }

        public int getPagesize() {
            return pagesize;
        }

        public void setPagesize(int pagesize) {
            this.pagesize = pagesize;
        }

        public int getIstag() {
            return istag;
        }

        public void setIstag(int istag) {
            this.istag = istag;
        }

        public String getCorrectiontip() {
            return correctiontip;
        }

        public void setCorrectiontip(String correctiontip) {
            this.correctiontip = correctiontip;
        }

        public int getCorrectionforce() {
            return correctionforce;
        }

        public void setCorrectionforce(int correctionforce) {
            this.correctionforce = correctionforce;
        }

        public SectagInfoBean getSectag_info() {
            return sectag_info;
        }

        public void setSectag_info(SectagInfoBean sectag_info) {
            this.sectag_info = sectag_info;
        }

        public List<AggregationBean> getAggregation() {
            return aggregation;
        }

        public void setAggregation(List<AggregationBean> aggregation) {
            this.aggregation = aggregation;
        }

        public List<ListsBean> getLists() {
            return lists;
        }

        public void setLists(List<ListsBean> lists) {
            this.lists = lists;
        }

        public static class SectagInfoBean {
            /**
             * is_sectag : 0
             */

            private int is_sectag;

            public int getIs_sectag() {
                return is_sectag;
            }

            public void setIs_sectag(int is_sectag) {
                this.is_sectag = is_sectag;
            }
        }

        public static class AggregationBean {
            /**
             * key : DJ
             * count : 0
             */

            private String key;
            private int count;

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }

        public static class ListsBean {
            /**
             * Suffix :
             * SongName : 大鱼
             * OwnerCount : 18678
             * MvType : 2
             * TopicRemark :
             * SQFailProcess : 0
             * Source :
             * Bitrate : 128
             * HQExtName : mp3
             * SQFileSize : 31584110
             * Accompany : 1
             * AudioCdn : 100
             * MvTrac : 3
             * SQDuration : 314
             * recommend_type : 0
             * ExtName : mp3
             * Auxiliary :
             * SQPkgPrice : 0
             * Category : 1
             * Scid : 21562630
             * OriSongName : 大鱼
             * FailProcess : 0
             * HQPkgPrice : 0
             * HQBitrate : 320
             * Audioid : 21562630
             * HiFiQuality : 2
             * Grp : []
             * OriOtherName :
             * AlbumPrivilege : 0
             * TopicUrl :
             * SuperFileHash :
             * ASQPrivilege : 0
             * OldCpy : 1
             * IsOriginal : 1
             * trans_param : {"cid":-1,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":8},"display":0,"cpy_attr0":0}
             * Privilege : 0
             * TagContent : 评论过万
             * ResBitrate : 0
             * FoldType : 0
             * FileHash : 3B15F1E26C4F8E24D5C7129FB61D487A
             * A320Privilege : 0
             * SQPayType : 0
             * AlbumID : 1637713
             * AlbumName : 大鱼
             * MatchFlag : 0
             * vvid :
             * Type : audio
             * MixSongID : 38641536
             * SuperExtName :
             * SQExtName : flac
             * ResFileSize : 0
             * Publish : 1
             * SuperBitrate : 0
             * ID : 38641536
             * SuperFileSize : 0
             * QualityLevel : 3
             * SQFileHash : FB863CA0763044280F1197CC05A23210
             * Uploader :
             * HQPrivilege : 0
             * AlbumAux :
             * SuperDuration : 0
             * SongLabel :
             * ResFileHash :
             * PublishAge : 255
             * HQFailProcess : 0
             * HQFileHash : 546C3C6CD0EBD887A11549D412D01D66
             * mvTotal : 0
             * PublishTime :
             * MvHash : 94372BDC04D53DEFA0BA03BA2E9F4DF6
             * SQPrivilege : 0
             * SQBitrate : 805
             * PkgPrice : 0
             * M4aSize : 0
             * Duration : 314
             * OtherName :
             * FileSize : 5020713
             * SQPrice : 0
             * ResDuration : 0
             * SingerId : [169967]
             * Price : 0
             * Singers : [{"name":"周深","id":169967}]
             * SingerName : 周深
             * HQPayType : 0
             * HQFileSize : 12551313
             * HQPrice : 0
             * HQDuration : 314
             * PayType : 0
             * HasAlbum : 1
             * FileName : 周深 - 大鱼
             * SourceID : 0
             */

            private String Suffix;
            private String SongName;
            private int OwnerCount;
            private int MvType;
            private String TopicRemark;
            private int SQFailProcess;
            private String Source;
            private int Bitrate;
            private String HQExtName;
            private int SQFileSize;
            private int Accompany;
            private int AudioCdn;
            private int MvTrac;
            private int SQDuration;
            private int recommend_type;
            private String ExtName;
            private String Auxiliary;
            private int SQPkgPrice;
            private int Category;
            private int Scid;
            private String OriSongName;
            private int FailProcess;
            private int HQPkgPrice;
            private int HQBitrate;
            private int Audioid;
            private int HiFiQuality;
            private String OriOtherName;
            private int AlbumPrivilege;
            private String TopicUrl;
            private String SuperFileHash;
            private int ASQPrivilege;
            private int OldCpy;
            private int IsOriginal;
            private TransParamBean trans_param;
            private int Privilege;
            private String TagContent;
            private int ResBitrate;
            private int FoldType;
            private String FileHash;
            private int A320Privilege;
            private int SQPayType;
            private String AlbumID;
            private String AlbumName;
            private int MatchFlag;
            private String vvid;
            private String Type;
            private String MixSongID;
            private String SuperExtName;
            private String SQExtName;
            private int ResFileSize;
            private int Publish;
            private int SuperBitrate;
            private String ID;
            private int SuperFileSize;
            private int QualityLevel;
            private String SQFileHash;
            private String Uploader;
            private int HQPrivilege;
            private String AlbumAux;
            private int SuperDuration;
            private String SongLabel;
            private String ResFileHash;
            private int PublishAge;
            private int HQFailProcess;
            private String HQFileHash;
            private int mvTotal;
            private String PublishTime;
            private String MvHash;
            private int SQPrivilege;
            private int SQBitrate;
            private int PkgPrice;
            private int M4aSize;
            private int Duration;
            private String OtherName;
            private int FileSize;
            private int SQPrice;
            private int ResDuration;
            private int Price;
            private String SingerName;
            private int HQPayType;
            private int HQFileSize;
            private int HQPrice;
            private int HQDuration;
            private int PayType;
            private int HasAlbum;
            private String FileName;
            private int SourceID;
            private List<?> Grp;
            private List<Integer> SingerId;
            private List<SingersBean> Singers;

            public String getSuffix() {
                return Suffix;
            }

            public void setSuffix(String Suffix) {
                this.Suffix = Suffix;
            }

            public String getSongName() {
                return SongName;
            }

            public void setSongName(String SongName) {
                this.SongName = SongName;
            }

            public int getOwnerCount() {
                return OwnerCount;
            }

            public void setOwnerCount(int OwnerCount) {
                this.OwnerCount = OwnerCount;
            }

            public int getMvType() {
                return MvType;
            }

            public void setMvType(int MvType) {
                this.MvType = MvType;
            }

            public String getTopicRemark() {
                return TopicRemark;
            }

            public void setTopicRemark(String TopicRemark) {
                this.TopicRemark = TopicRemark;
            }

            public int getSQFailProcess() {
                return SQFailProcess;
            }

            public void setSQFailProcess(int SQFailProcess) {
                this.SQFailProcess = SQFailProcess;
            }

            public String getSource() {
                return Source;
            }

            public void setSource(String Source) {
                this.Source = Source;
            }

            public int getBitrate() {
                return Bitrate;
            }

            public void setBitrate(int Bitrate) {
                this.Bitrate = Bitrate;
            }

            public String getHQExtName() {
                return HQExtName;
            }

            public void setHQExtName(String HQExtName) {
                this.HQExtName = HQExtName;
            }

            public int getSQFileSize() {
                return SQFileSize;
            }

            public void setSQFileSize(int SQFileSize) {
                this.SQFileSize = SQFileSize;
            }

            public int getAccompany() {
                return Accompany;
            }

            public void setAccompany(int Accompany) {
                this.Accompany = Accompany;
            }

            public int getAudioCdn() {
                return AudioCdn;
            }

            public void setAudioCdn(int AudioCdn) {
                this.AudioCdn = AudioCdn;
            }

            public int getMvTrac() {
                return MvTrac;
            }

            public void setMvTrac(int MvTrac) {
                this.MvTrac = MvTrac;
            }

            public int getSQDuration() {
                return SQDuration;
            }

            public void setSQDuration(int SQDuration) {
                this.SQDuration = SQDuration;
            }

            public int getRecommend_type() {
                return recommend_type;
            }

            public void setRecommend_type(int recommend_type) {
                this.recommend_type = recommend_type;
            }

            public String getExtName() {
                return ExtName;
            }

            public void setExtName(String ExtName) {
                this.ExtName = ExtName;
            }

            public String getAuxiliary() {
                return Auxiliary;
            }

            public void setAuxiliary(String Auxiliary) {
                this.Auxiliary = Auxiliary;
            }

            public int getSQPkgPrice() {
                return SQPkgPrice;
            }

            public void setSQPkgPrice(int SQPkgPrice) {
                this.SQPkgPrice = SQPkgPrice;
            }

            public int getCategory() {
                return Category;
            }

            public void setCategory(int Category) {
                this.Category = Category;
            }

            public int getScid() {
                return Scid;
            }

            public void setScid(int Scid) {
                this.Scid = Scid;
            }

            public String getOriSongName() {
                return OriSongName;
            }

            public void setOriSongName(String OriSongName) {
                this.OriSongName = OriSongName;
            }

            public int getFailProcess() {
                return FailProcess;
            }

            public void setFailProcess(int FailProcess) {
                this.FailProcess = FailProcess;
            }

            public int getHQPkgPrice() {
                return HQPkgPrice;
            }

            public void setHQPkgPrice(int HQPkgPrice) {
                this.HQPkgPrice = HQPkgPrice;
            }

            public int getHQBitrate() {
                return HQBitrate;
            }

            public void setHQBitrate(int HQBitrate) {
                this.HQBitrate = HQBitrate;
            }

            public int getAudioid() {
                return Audioid;
            }

            public void setAudioid(int Audioid) {
                this.Audioid = Audioid;
            }

            public int getHiFiQuality() {
                return HiFiQuality;
            }

            public void setHiFiQuality(int HiFiQuality) {
                this.HiFiQuality = HiFiQuality;
            }

            public String getOriOtherName() {
                return OriOtherName;
            }

            public void setOriOtherName(String OriOtherName) {
                this.OriOtherName = OriOtherName;
            }

            public int getAlbumPrivilege() {
                return AlbumPrivilege;
            }

            public void setAlbumPrivilege(int AlbumPrivilege) {
                this.AlbumPrivilege = AlbumPrivilege;
            }

            public String getTopicUrl() {
                return TopicUrl;
            }

            public void setTopicUrl(String TopicUrl) {
                this.TopicUrl = TopicUrl;
            }

            public String getSuperFileHash() {
                return SuperFileHash;
            }

            public void setSuperFileHash(String SuperFileHash) {
                this.SuperFileHash = SuperFileHash;
            }

            public int getASQPrivilege() {
                return ASQPrivilege;
            }

            public void setASQPrivilege(int ASQPrivilege) {
                this.ASQPrivilege = ASQPrivilege;
            }

            public int getOldCpy() {
                return OldCpy;
            }

            public void setOldCpy(int OldCpy) {
                this.OldCpy = OldCpy;
            }

            public int getIsOriginal() {
                return IsOriginal;
            }

            public void setIsOriginal(int IsOriginal) {
                this.IsOriginal = IsOriginal;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public int getPrivilege() {
                return Privilege;
            }

            public void setPrivilege(int Privilege) {
                this.Privilege = Privilege;
            }

            public String getTagContent() {
                return TagContent;
            }

            public void setTagContent(String TagContent) {
                this.TagContent = TagContent;
            }

            public int getResBitrate() {
                return ResBitrate;
            }

            public void setResBitrate(int ResBitrate) {
                this.ResBitrate = ResBitrate;
            }

            public int getFoldType() {
                return FoldType;
            }

            public void setFoldType(int FoldType) {
                this.FoldType = FoldType;
            }

            public String getFileHash() {
                return FileHash;
            }

            public void setFileHash(String FileHash) {
                this.FileHash = FileHash;
            }

            public int getA320Privilege() {
                return A320Privilege;
            }

            public void setA320Privilege(int A320Privilege) {
                this.A320Privilege = A320Privilege;
            }

            public int getSQPayType() {
                return SQPayType;
            }

            public void setSQPayType(int SQPayType) {
                this.SQPayType = SQPayType;
            }

            public String getAlbumID() {
                return AlbumID;
            }

            public void setAlbumID(String AlbumID) {
                this.AlbumID = AlbumID;
            }

            public String getAlbumName() {
                return AlbumName;
            }

            public void setAlbumName(String AlbumName) {
                this.AlbumName = AlbumName;
            }

            public int getMatchFlag() {
                return MatchFlag;
            }

            public void setMatchFlag(int MatchFlag) {
                this.MatchFlag = MatchFlag;
            }

            public String getVvid() {
                return vvid;
            }

            public void setVvid(String vvid) {
                this.vvid = vvid;
            }

            public String getType() {
                return Type;
            }

            public void setType(String Type) {
                this.Type = Type;
            }

            public String getMixSongID() {
                return MixSongID;
            }

            public void setMixSongID(String MixSongID) {
                this.MixSongID = MixSongID;
            }

            public String getSuperExtName() {
                return SuperExtName;
            }

            public void setSuperExtName(String SuperExtName) {
                this.SuperExtName = SuperExtName;
            }

            public String getSQExtName() {
                return SQExtName;
            }

            public void setSQExtName(String SQExtName) {
                this.SQExtName = SQExtName;
            }

            public int getResFileSize() {
                return ResFileSize;
            }

            public void setResFileSize(int ResFileSize) {
                this.ResFileSize = ResFileSize;
            }

            public int getPublish() {
                return Publish;
            }

            public void setPublish(int Publish) {
                this.Publish = Publish;
            }

            public int getSuperBitrate() {
                return SuperBitrate;
            }

            public void setSuperBitrate(int SuperBitrate) {
                this.SuperBitrate = SuperBitrate;
            }

            public String getID() {
                return ID;
            }

            public void setID(String ID) {
                this.ID = ID;
            }

            public int getSuperFileSize() {
                return SuperFileSize;
            }

            public void setSuperFileSize(int SuperFileSize) {
                this.SuperFileSize = SuperFileSize;
            }

            public int getQualityLevel() {
                return QualityLevel;
            }

            public void setQualityLevel(int QualityLevel) {
                this.QualityLevel = QualityLevel;
            }

            public String getSQFileHash() {
                return SQFileHash;
            }

            public void setSQFileHash(String SQFileHash) {
                this.SQFileHash = SQFileHash;
            }

            public String getUploader() {
                return Uploader;
            }

            public void setUploader(String Uploader) {
                this.Uploader = Uploader;
            }

            public int getHQPrivilege() {
                return HQPrivilege;
            }

            public void setHQPrivilege(int HQPrivilege) {
                this.HQPrivilege = HQPrivilege;
            }

            public String getAlbumAux() {
                return AlbumAux;
            }

            public void setAlbumAux(String AlbumAux) {
                this.AlbumAux = AlbumAux;
            }

            public int getSuperDuration() {
                return SuperDuration;
            }

            public void setSuperDuration(int SuperDuration) {
                this.SuperDuration = SuperDuration;
            }

            public String getSongLabel() {
                return SongLabel;
            }

            public void setSongLabel(String SongLabel) {
                this.SongLabel = SongLabel;
            }

            public String getResFileHash() {
                return ResFileHash;
            }

            public void setResFileHash(String ResFileHash) {
                this.ResFileHash = ResFileHash;
            }

            public int getPublishAge() {
                return PublishAge;
            }

            public void setPublishAge(int PublishAge) {
                this.PublishAge = PublishAge;
            }

            public int getHQFailProcess() {
                return HQFailProcess;
            }

            public void setHQFailProcess(int HQFailProcess) {
                this.HQFailProcess = HQFailProcess;
            }

            public String getHQFileHash() {
                return HQFileHash;
            }

            public void setHQFileHash(String HQFileHash) {
                this.HQFileHash = HQFileHash;
            }

            public int getMvTotal() {
                return mvTotal;
            }

            public void setMvTotal(int mvTotal) {
                this.mvTotal = mvTotal;
            }

            public String getPublishTime() {
                return PublishTime;
            }

            public void setPublishTime(String PublishTime) {
                this.PublishTime = PublishTime;
            }

            public String getMvHash() {
                return MvHash;
            }

            public void setMvHash(String MvHash) {
                this.MvHash = MvHash;
            }

            public int getSQPrivilege() {
                return SQPrivilege;
            }

            public void setSQPrivilege(int SQPrivilege) {
                this.SQPrivilege = SQPrivilege;
            }

            public int getSQBitrate() {
                return SQBitrate;
            }

            public void setSQBitrate(int SQBitrate) {
                this.SQBitrate = SQBitrate;
            }

            public int getPkgPrice() {
                return PkgPrice;
            }

            public void setPkgPrice(int PkgPrice) {
                this.PkgPrice = PkgPrice;
            }

            public int getM4aSize() {
                return M4aSize;
            }

            public void setM4aSize(int M4aSize) {
                this.M4aSize = M4aSize;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int Duration) {
                this.Duration = Duration;
            }

            public String getOtherName() {
                return OtherName;
            }

            public void setOtherName(String OtherName) {
                this.OtherName = OtherName;
            }

            public int getFileSize() {
                return FileSize;
            }

            public void setFileSize(int FileSize) {
                this.FileSize = FileSize;
            }

            public int getSQPrice() {
                return SQPrice;
            }

            public void setSQPrice(int SQPrice) {
                this.SQPrice = SQPrice;
            }

            public int getResDuration() {
                return ResDuration;
            }

            public void setResDuration(int ResDuration) {
                this.ResDuration = ResDuration;
            }

            public int getPrice() {
                return Price;
            }

            public void setPrice(int Price) {
                this.Price = Price;
            }

            public String getSingerName() {
                return SingerName;
            }

            public void setSingerName(String SingerName) {
                this.SingerName = SingerName;
            }

            public int getHQPayType() {
                return HQPayType;
            }

            public void setHQPayType(int HQPayType) {
                this.HQPayType = HQPayType;
            }

            public int getHQFileSize() {
                return HQFileSize;
            }

            public void setHQFileSize(int HQFileSize) {
                this.HQFileSize = HQFileSize;
            }

            public int getHQPrice() {
                return HQPrice;
            }

            public void setHQPrice(int HQPrice) {
                this.HQPrice = HQPrice;
            }

            public int getHQDuration() {
                return HQDuration;
            }

            public void setHQDuration(int HQDuration) {
                this.HQDuration = HQDuration;
            }

            public int getPayType() {
                return PayType;
            }

            public void setPayType(int PayType) {
                this.PayType = PayType;
            }

            public int getHasAlbum() {
                return HasAlbum;
            }

            public void setHasAlbum(int HasAlbum) {
                this.HasAlbum = HasAlbum;
            }

            public String getFileName() {
                return FileName;
            }

            public void setFileName(String FileName) {
                this.FileName = FileName;
            }

            public int getSourceID() {
                return SourceID;
            }

            public void setSourceID(int SourceID) {
                this.SourceID = SourceID;
            }

            public List<?> getGrp() {
                return Grp;
            }

            public void setGrp(List<?> Grp) {
                this.Grp = Grp;
            }

            public List<Integer> getSingerId() {
                return SingerId;
            }

            public void setSingerId(List<Integer> SingerId) {
                this.SingerId = SingerId;
            }

            public List<SingersBean> getSingers() {
                return Singers;
            }

            public void setSingers(List<SingersBean> Singers) {
                this.Singers = Singers;
            }

            public static class TransParamBean {
                /**
                 * cid : -1
                 * pay_block_tpl : 1
                 * musicpack_advance : 0
                 * display_rate : 0
                 * classmap : {"attr0":8}
                 * display : 0
                 * cpy_attr0 : 0
                 */

                private int cid;
                private int pay_block_tpl;
                private int musicpack_advance;
                private int display_rate;
                private ClassmapBean classmap;
                private int display;
                private int cpy_attr0;

                public int getCid() {
                    return cid;
                }

                public void setCid(int cid) {
                    this.cid = cid;
                }

                public int getPay_block_tpl() {
                    return pay_block_tpl;
                }

                public void setPay_block_tpl(int pay_block_tpl) {
                    this.pay_block_tpl = pay_block_tpl;
                }

                public int getMusicpack_advance() {
                    return musicpack_advance;
                }

                public void setMusicpack_advance(int musicpack_advance) {
                    this.musicpack_advance = musicpack_advance;
                }

                public int getDisplay_rate() {
                    return display_rate;
                }

                public void setDisplay_rate(int display_rate) {
                    this.display_rate = display_rate;
                }

                public ClassmapBean getClassmap() {
                    return classmap;
                }

                public void setClassmap(ClassmapBean classmap) {
                    this.classmap = classmap;
                }

                public int getDisplay() {
                    return display;
                }

                public void setDisplay(int display) {
                    this.display = display;
                }

                public int getCpy_attr0() {
                    return cpy_attr0;
                }

                public void setCpy_attr0(int cpy_attr0) {
                    this.cpy_attr0 = cpy_attr0;
                }

                public static class ClassmapBean {
                    /**
                     * attr0 : 8
                     */

                    private int attr0;

                    public int getAttr0() {
                        return attr0;
                    }

                    public void setAttr0(int attr0) {
                        this.attr0 = attr0;
                    }
                }
            }

            public static class SingersBean {
                /**
                 * name : 周深
                 * id : 169967
                 */

                private String name;
                private int id;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }
            }
        }
    }
}
