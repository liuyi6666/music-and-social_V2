package com.music.pojo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class KugouSingerAlbumMusic implements Serializable {
    private static final long serialVersionUID = 3619329326380820903L;

    /**
     * status : 1
     * error :
     * data : {"timestamp":1644249244,"info":[{"pay_type_320":2,"m4afilesize":0,"price_sq":300,"filesize":2961407,"bitrate":128,"trans_param":{"sort":1,"cpy_attr0":0,"cid":102463297,"ownercount":0,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":0},"display":0,"audio_privilege":10},"price":300,"inlist":1,"old_cpy":1,"fail_process_sq":4,"pay_type":2,"musical":null,"topic_url":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","fail_process_320":4,"pkg_price":0,"feetype":0,"filename":"周杰伦 - Mojito","price_320":300,"extname":"mp3","hash":"37FB544283B501DDC7D9CEE59FCC2B32","mvhash":"BC31D031AE3C698D7B767308CD26FC08","privilege":10,"album_id":"38080072","album_audio_id":260405908,"rp_type":"album","origin_hash":"37FB544283B501DDC7D9CEE59FCC2B32","audio_id":74516471,"320filesize":7403269,"rp_publish":1,"has_accompany":1,"topic_url_sq":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","pkg_price_sq":0,"remark":"","pkg_price_320":0,"320privilege":10,"sqhash":"3310B3FECE6BECCFC16B99A94A1772FA","fail_process":4,"duration":185,"pay_type_sq":2,"320hash":"E8D7BCB7CE26602811AA047EFD49E112","sqprivilege":10,"topic_url_320":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","sqfilesize":20850751}],"total":1}
     * errcode : 0
     */

    private int status;
    private String error;
    private DataBean data;
    private int errcode;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public static class DataBean {
        /**
         * timestamp : 1644249244
         * info : [{"pay_type_320":2,"m4afilesize":0,"price_sq":300,"filesize":2961407,"bitrate":128,"trans_param":{"sort":1,"cpy_attr0":0,"cid":102463297,"ownercount":0,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":0},"display":0,"audio_privilege":10},"price":300,"inlist":1,"old_cpy":1,"fail_process_sq":4,"pay_type":2,"musical":null,"topic_url":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","fail_process_320":4,"pkg_price":0,"feetype":0,"filename":"周杰伦 - Mojito","price_320":300,"extname":"mp3","hash":"37FB544283B501DDC7D9CEE59FCC2B32","mvhash":"BC31D031AE3C698D7B767308CD26FC08","privilege":10,"album_id":"38080072","album_audio_id":260405908,"rp_type":"album","origin_hash":"37FB544283B501DDC7D9CEE59FCC2B32","audio_id":74516471,"320filesize":7403269,"rp_publish":1,"has_accompany":1,"topic_url_sq":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","pkg_price_sq":0,"remark":"","pkg_price_320":0,"320privilege":10,"sqhash":"3310B3FECE6BECCFC16B99A94A1772FA","fail_process":4,"duration":185,"pay_type_sq":2,"320hash":"E8D7BCB7CE26602811AA047EFD49E112","sqprivilege":10,"topic_url_320":"https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37","sqfilesize":20850751}]
         * total : 1
         */

        private int timestamp;
        private int total;
        private List<InfoBean> info;

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * pay_type_320 : 2
             * m4afilesize : 0
             * price_sq : 300
             * filesize : 2961407
             * bitrate : 128
             * trans_param : {"sort":1,"cpy_attr0":0,"cid":102463297,"ownercount":0,"pay_block_tpl":1,"musicpack_advance":0,"display_rate":0,"classmap":{"attr0":0},"display":0,"audio_privilege":10}
             * price : 300
             * inlist : 1
             * old_cpy : 1
             * fail_process_sq : 4
             * pay_type : 2
             * musical : null
             * topic_url : https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37
             * fail_process_320 : 4
             * pkg_price : 0
             * feetype : 0
             * filename : 周杰伦 - Mojito
             * price_320 : 300
             * extname : mp3
             * hash : 37FB544283B501DDC7D9CEE59FCC2B32
             * mvhash : BC31D031AE3C698D7B767308CD26FC08
             * privilege : 10
             * album_id : 38080072
             * album_audio_id : 260405908
             * rp_type : album
             * origin_hash : 37FB544283B501DDC7D9CEE59FCC2B32
             * audio_id : 74516471
             * 320filesize : 7403269
             * rp_publish : 1
             * has_accompany : 1
             * topic_url_sq : https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37
             * pkg_price_sq : 0
             * remark :
             * pkg_price_320 : 0
             * 320privilege : 10
             * sqhash : 3310B3FECE6BECCFC16B99A94A1772FA
             * fail_process : 4
             * duration : 185
             * pay_type_sq : 2
             * 320hash : E8D7BCB7CE26602811AA047EFD49E112
             * sqprivilege : 10
             * topic_url_320 : https://ep.kugou.com/html/mobile_commonchargeV3/index_300434.html?is_go=1&hreffrom=37
             * sqfilesize : 20850751
             */

            private int pay_type_320;
            private int m4afilesize;
            private int price_sq;
            private int filesize;
            private int bitrate;
            private TransParamBean trans_param;
            private int price;
            private int inlist;
            private int old_cpy;
            private int fail_process_sq;
            private int pay_type;
            private Object musical;
            private String topic_url;
            private int fail_process_320;
            private int pkg_price;
            private int feetype;
            private String filename;
            private int price_320;
            private String extname;
            private String hash;
            private String mvhash;
            private int privilege;
            private String album_id;
            private int album_audio_id;
            private String rp_type;
            private String origin_hash;
            private int audio_id;
            @JSONField(name = "320filesize")
            private int _$320filesize;
            private int rp_publish;
            private int has_accompany;
            private String topic_url_sq;
            private int pkg_price_sq;
            private String remark;
            private int pkg_price_320;
            @JSONField(name = "320privilege")
            private int _$320privilege;
            private String sqhash;
            private int fail_process;
            private int duration;
            private int pay_type_sq;
            @JSONField(name = "320hash")
            private String _$320hash;
            private int sqprivilege;
            private String topic_url_320;
            private int sqfilesize;

            public int getPay_type_320() {
                return pay_type_320;
            }

            public void setPay_type_320(int pay_type_320) {
                this.pay_type_320 = pay_type_320;
            }

            public int getM4afilesize() {
                return m4afilesize;
            }

            public void setM4afilesize(int m4afilesize) {
                this.m4afilesize = m4afilesize;
            }

            public int getPrice_sq() {
                return price_sq;
            }

            public void setPrice_sq(int price_sq) {
                this.price_sq = price_sq;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public int getBitrate() {
                return bitrate;
            }

            public void setBitrate(int bitrate) {
                this.bitrate = bitrate;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getInlist() {
                return inlist;
            }

            public void setInlist(int inlist) {
                this.inlist = inlist;
            }

            public int getOld_cpy() {
                return old_cpy;
            }

            public void setOld_cpy(int old_cpy) {
                this.old_cpy = old_cpy;
            }

            public int getFail_process_sq() {
                return fail_process_sq;
            }

            public void setFail_process_sq(int fail_process_sq) {
                this.fail_process_sq = fail_process_sq;
            }

            public int getPay_type() {
                return pay_type;
            }

            public void setPay_type(int pay_type) {
                this.pay_type = pay_type;
            }

            public Object getMusical() {
                return musical;
            }

            public void setMusical(Object musical) {
                this.musical = musical;
            }

            public String getTopic_url() {
                return topic_url;
            }

            public void setTopic_url(String topic_url) {
                this.topic_url = topic_url;
            }

            public int getFail_process_320() {
                return fail_process_320;
            }

            public void setFail_process_320(int fail_process_320) {
                this.fail_process_320 = fail_process_320;
            }

            public int getPkg_price() {
                return pkg_price;
            }

            public void setPkg_price(int pkg_price) {
                this.pkg_price = pkg_price;
            }

            public int getFeetype() {
                return feetype;
            }

            public void setFeetype(int feetype) {
                this.feetype = feetype;
            }

            public String getFilename() {
                return filename;
            }

            public void setFilename(String filename) {
                this.filename = filename;
            }

            public int getPrice_320() {
                return price_320;
            }

            public void setPrice_320(int price_320) {
                this.price_320 = price_320;
            }

            public String getExtname() {
                return extname;
            }

            public void setExtname(String extname) {
                this.extname = extname;
            }

            public String getHash() {
                return hash;
            }

            public void setHash(String hash) {
                this.hash = hash;
            }

            public String getMvhash() {
                return mvhash;
            }

            public void setMvhash(String mvhash) {
                this.mvhash = mvhash;
            }

            public int getPrivilege() {
                return privilege;
            }

            public void setPrivilege(int privilege) {
                this.privilege = privilege;
            }

            public String getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(String album_id) {
                this.album_id = album_id;
            }

            public int getAlbum_audio_id() {
                return album_audio_id;
            }

            public void setAlbum_audio_id(int album_audio_id) {
                this.album_audio_id = album_audio_id;
            }

            public String getRp_type() {
                return rp_type;
            }

            public void setRp_type(String rp_type) {
                this.rp_type = rp_type;
            }

            public String getOrigin_hash() {
                return origin_hash;
            }

            public void setOrigin_hash(String origin_hash) {
                this.origin_hash = origin_hash;
            }

            public int getAudio_id() {
                return audio_id;
            }

            public void setAudio_id(int audio_id) {
                this.audio_id = audio_id;
            }

            public int get_$320filesize() {
                return _$320filesize;
            }

            public void set_$320filesize(int _$320filesize) {
                this._$320filesize = _$320filesize;
            }

            public int getRp_publish() {
                return rp_publish;
            }

            public void setRp_publish(int rp_publish) {
                this.rp_publish = rp_publish;
            }

            public int getHas_accompany() {
                return has_accompany;
            }

            public void setHas_accompany(int has_accompany) {
                this.has_accompany = has_accompany;
            }

            public String getTopic_url_sq() {
                return topic_url_sq;
            }

            public void setTopic_url_sq(String topic_url_sq) {
                this.topic_url_sq = topic_url_sq;
            }

            public int getPkg_price_sq() {
                return pkg_price_sq;
            }

            public void setPkg_price_sq(int pkg_price_sq) {
                this.pkg_price_sq = pkg_price_sq;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getPkg_price_320() {
                return pkg_price_320;
            }

            public void setPkg_price_320(int pkg_price_320) {
                this.pkg_price_320 = pkg_price_320;
            }

            public int get_$320privilege() {
                return _$320privilege;
            }

            public void set_$320privilege(int _$320privilege) {
                this._$320privilege = _$320privilege;
            }

            public String getSqhash() {
                return sqhash;
            }

            public void setSqhash(String sqhash) {
                this.sqhash = sqhash;
            }

            public int getFail_process() {
                return fail_process;
            }

            public void setFail_process(int fail_process) {
                this.fail_process = fail_process;
            }

            public int getDuration() {
                return duration;
            }

            public void setDuration(int duration) {
                this.duration = duration;
            }

            public int getPay_type_sq() {
                return pay_type_sq;
            }

            public void setPay_type_sq(int pay_type_sq) {
                this.pay_type_sq = pay_type_sq;
            }

            public String get_$320hash() {
                return _$320hash;
            }

            public void set_$320hash(String _$320hash) {
                this._$320hash = _$320hash;
            }

            public int getSqprivilege() {
                return sqprivilege;
            }

            public void setSqprivilege(int sqprivilege) {
                this.sqprivilege = sqprivilege;
            }

            public String getTopic_url_320() {
                return topic_url_320;
            }

            public void setTopic_url_320(String topic_url_320) {
                this.topic_url_320 = topic_url_320;
            }

            public int getSqfilesize() {
                return sqfilesize;
            }

            public void setSqfilesize(int sqfilesize) {
                this.sqfilesize = sqfilesize;
            }

            public static class TransParamBean {
                /**
                 * sort : 1
                 * cpy_attr0 : 0
                 * cid : 102463297
                 * ownercount : 0
                 * pay_block_tpl : 1
                 * musicpack_advance : 0
                 * display_rate : 0
                 * classmap : {"attr0":0}
                 * display : 0
                 * audio_privilege : 10
                 */

                private int sort;
                private int cpy_attr0;
                private int cid;
                private int ownercount;
                private int pay_block_tpl;
                private int musicpack_advance;
                private int display_rate;
                private ClassmapBean classmap;
                private int display;
                private int audio_privilege;

                public int getSort() {
                    return sort;
                }

                public void setSort(int sort) {
                    this.sort = sort;
                }

                public int getCpy_attr0() {
                    return cpy_attr0;
                }

                public void setCpy_attr0(int cpy_attr0) {
                    this.cpy_attr0 = cpy_attr0;
                }

                public int getCid() {
                    return cid;
                }

                public void setCid(int cid) {
                    this.cid = cid;
                }

                public int getOwnercount() {
                    return ownercount;
                }

                public void setOwnercount(int ownercount) {
                    this.ownercount = ownercount;
                }

                public int getPay_block_tpl() {
                    return pay_block_tpl;
                }

                public void setPay_block_tpl(int pay_block_tpl) {
                    this.pay_block_tpl = pay_block_tpl;
                }

                public int getMusicpack_advance() {
                    return musicpack_advance;
                }

                public void setMusicpack_advance(int musicpack_advance) {
                    this.musicpack_advance = musicpack_advance;
                }

                public int getDisplay_rate() {
                    return display_rate;
                }

                public void setDisplay_rate(int display_rate) {
                    this.display_rate = display_rate;
                }

                public ClassmapBean getClassmap() {
                    return classmap;
                }

                public void setClassmap(ClassmapBean classmap) {
                    this.classmap = classmap;
                }

                public int getDisplay() {
                    return display;
                }

                public void setDisplay(int display) {
                    this.display = display;
                }

                public int getAudio_privilege() {
                    return audio_privilege;
                }

                public void setAudio_privilege(int audio_privilege) {
                    this.audio_privilege = audio_privilege;
                }

                public static class ClassmapBean {
                    /**
                     * attr0 : 0
                     */

                    private int attr0;

                    public int getAttr0() {
                        return attr0;
                    }

                    public void setAttr0(int attr0) {
                        this.attr0 = attr0;
                    }
                }
            }
        }
    }
}
