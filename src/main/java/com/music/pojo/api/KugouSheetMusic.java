package com.music.pojo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class KugouSheetMusic implements Serializable {

    private static final long serialVersionUID = 5202607910135366504L;

    /**
     * JS_CSS_DATE : 20130320
     * kg_domain : https://m.kugou.com
     * src : http://downmobile.kugou.com/promote/package/download/channel=6
     * fr : null
     * ver : v3
     * list : {"list":{"timestamp":1642469473,"total":167,"info":[{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830421,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cid83.html"},{"hash":"D9AEF72926A682B65F58B6119886063D","sqfilesize":26856239,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F18FEE2562E500DD1CC3ABCE60C33548","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"24079251","mvhash":"595911438A1BC2DDD88449D129ADC85A","extname":"mp3","topic_url_sq":"","320hash":"33234C08DCE88059300629441FD5C6E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":7682065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"莫斯满、老猫 - 野花香 (DJ何鹏版)","old_cpy":1,"album_audio_id":171130466,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":72750712,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"46504232F2A329C1A8FAE5E1AE0FA20A","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":192,"audio_id":56810094,"topic_url":"","brief":"","rp_publish":1,"filesize":3072879,"remark":"野花香","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2tvx42c4.html"},{"hash":"FD025EC77FBA5BF518542549A810A6FF","sqfilesize":31976878,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"ED86EC015CDF234DA264BA57A4FD99D8","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"20635283","mvhash":"C894B2E860B8A4695A7D0D495527EBA4","extname":"mp3","topic_url_sq":"","320hash":"328CC13A721D4553A2657BE5578C7035","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843154,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"音萌萌、寂悸 - 愿为你跪六千年 (DJ何鹏版)","old_cpy":0,"album_audio_id":153334403,"fail_process_320":4,"trans_param":{"cpy_grade":6,"classmap":{"attr0":100663304},"cid":69687260,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"0E4061B450BC9B187C547744BB08744D","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":246,"audio_id":54820237,"topic_url":"","brief":"","rp_publish":1,"filesize":3937219,"remark":"愿为你跪六千年","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jahkzb8.html"},{"hash":"FA1018E8CD0B9B7C28F6018AF81CF251","sqfilesize":43241639,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"14C0BAFB7F7317AF33197D416ABDB682","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"12432890","mvhash":"B57E90910F956E702B51F86F81DDD7AD","extname":"mp3","topic_url_sq":"","320hash":"4B03E5911584C8BB82C8EF18EAD833E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12547348,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭聪明 - 你会遇见更好的人 (DJ阳少版)","old_cpy":0,"album_audio_id":121372145,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":50773717,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"3AA9B8DD7F607C345FF07A24AD0D7F38","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":313,"audio_id":49018116,"topic_url":"","brief":"","rp_publish":1,"filesize":5018897,"remark":"你会遇见更好的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/209fchbb.html"},{"hash":"3B08D35D317AEBBF90BF5193708CB12F","sqfilesize":46304973,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"02401887EC42C0AD7D0BA0B31BD70C14","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2417364","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA20571742A23DCB7CF85CEB5130E36B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":15019571,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"祁隆 - 唱着情歌流着泪 (DJ版)","old_cpy":0,"album_audio_id":62298255,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":22782096,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"A1973ED37F0F56BA5F9E566EA3452144","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":375,"audio_id":20770,"topic_url":"","brief":"","rp_publish":1,"filesize":6009832,"remark":"唱着情歌流着泪","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1139n36f.html"},{"hash":"D28D6091CFE2BF83DE1F7BB886639077","sqfilesize":35164339,"sqprivilege":5,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"B8865C9A44228A7C8598F5979BACD9CF","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"35709505","mvhash":"6ECEE711576CDDE25EF4D7EACFD15657","extname":"mp3","topic_url_sq":"","320hash":"3BDADA2DE3F0B4DD386A389018707BD2","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":10212876,"pkg_price_320":0,"privilege":5,"feetype":0,"price":0,"filename":"白小白 - 爱不得忘不舍 (DJ版)","old_cpy":1,"album_audio_id":246750484,"fail_process_320":0,"trans_param":{"cpy_grade":75,"classmap":{"attr0":100663304},"cid":176006448,"cpy_attr0":2,"hash_multitrack":"F7568F964514B92F36068569FB6F4E83","pay_block_tpl":1,"musicpack_advance":0,"display":1,"display_rate":0},"duration":255,"audio_id":87102146,"topic_url":"","brief":"","rp_publish":1,"filesize":4083087,"remark":"爱不得忘不舍","pkg_price":0,"320privilege":5,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/42wpw436.html"},{"hash":"525F90EF87AACEE5008703177F20B03E","sqfilesize":46790582,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F3B82FDE09F4E94A7C8DBDDE9568F396","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"15999493","mvhash":"818C1799FC27BEE7248936123D606215","extname":"mp3","topic_url_sq":"","320hash":"260C8925CA8677FDD8F8A7B7D1ED0F50","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13052575,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"大壮 - 伪装 (DJ小鱼儿版)","old_cpy":1,"album_audio_id":153369894,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":67108872},"cpy_level":1,"cid":69672404,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"480DD1ECDCA1CA5983EE0CB667F37703","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":326,"audio_id":54795157,"topic_url":"","brief":"","rp_publish":1,"filesize":5221190,"remark":"伪装","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jb8yud2.html"},{"hash":"E4753ED63B02ADEDDD14FC4EE882FB07","sqfilesize":18783028,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"6547A949A7DD9CBFEC6ECDB89BE65E6A","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"21345282","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"9026C6C5F660549EA6652884F71D221A","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":5566383,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"颜妹、苏天伦 - 渡我不渡她","old_cpy":1,"album_audio_id":156509900,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"EC2CA2968FC27295C3F4D5D91E5729DB","pay_block_tpl":1,"classmap":{"attr0":103809032},"display":64,"display_rate":5},"duration":139,"audio_id":54836961,"topic_url":"","brief":"","rp_publish":1,"filesize":2226094,"remark":"渡我不渡她DJ","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/2l6jt8dc.html"},{"hash":"BB4B788496B1F062D957F5E95D4F448F","sqfilesize":35307003,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F86DD9ABC534D41B8A02D741F1D681C6","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17959109","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"50100CE73D1F61005C00AF9C2E9C0189","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10642513,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"孙艺琪、崔伟立 - 怎么爱都爱不够 (DJ何鹏版)","old_cpy":0,"album_audio_id":140230779,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":66030173,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"30DFBB65321C27A41AEC7C1CF7B8E263","pay_block_tpl":1,"musicpack_advance":1,"display":32,"display_rate":1},"duration":266,"audio_id":52305374,"topic_url":"","brief":"","rp_publish":1,"filesize":4256958,"remark":"怎么爱都爱不够(DJ版)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bhmrf57.html"},{"hash":"3E91A006ADB0C81F3F719820E42CBAC5","sqfilesize":35316793,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"4E2CF8A0CF028FCD2833D0E3A92F17C4","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17892768","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8D889F65D56C1A9D61CA02AD7A550031","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10464593,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"吴官辉 - 错过花开错过爱 (DJ何鹏版)","old_cpy":0,"album_audio_id":139973328,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":65969771,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E5D5313D6E10D29C73E9569D5A25813C","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":261,"audio_id":52268804,"topic_url":"","brief":"","rp_publish":1,"filesize":4185905,"remark":"错过花开错过爱","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bc4406d.html"},{"hash":"359075417176CFA898C770EEC5140165","sqfilesize":29226468,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"727CE656045ADB4C241512CFB62EE488","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"27365590","mvhash":"07BFBB441B262BF6B5BFDF8A04C211A0","extname":"mp3","topic_url_sq":"","320hash":"F0B8DBA7A433E0A6985CA11F3DB38F80","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":8559974,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"雪十郎 - 谁 (DJ何鹏版)","old_cpy":0,"album_audio_id":199368286,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":75499739,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"85B3126FE294E52719B231635816D1F9","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":213,"audio_id":59202204,"topic_url":"","brief":"","rp_publish":1,"filesize":3423965,"remark":"谁","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/3ap5jy3c.html"},{"hash":"8F8B9D3EE4662BE76166377A13EF3F13","sqfilesize":46915452,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AF56AB39F9B0BBCBA3493B632EF1DE79","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"8304053","mvhash":"12F8765B7BB481384831717ED3E4D363","extname":"mp3","topic_url_sq":"","320hash":"CF522D62F8CC55C5EAA9239A81A6EA6B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14483524,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 牛在飞 (DJ阿远版)","old_cpy":0,"album_audio_id":105837366,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31988491,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A55950794C84E0D099CAEE7C1B0E6479","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":362,"audio_id":36698493,"topic_url":"","brief":"","rp_publish":1,"filesize":5793376,"remark":"牛在飞","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1r0gmuf2.html"},{"hash":"EA0FF533C09C1647AF8FB11B06A9058A","sqfilesize":39116181,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"204E82BB37FC08860BEDE91C179F9168","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"595197","mvhash":"80F2C0C09B9F7A3D78298BBD6C5590E6","extname":"mp3","topic_url_sq":"","320hash":"D2BFEE30C1E8E95EC5BC4C4D94A49C96","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11886949,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"智涛 - 我锁上爱情的门 (DJ何鹏版)","old_cpy":1,"album_audio_id":28587933,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":23004802,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"9DAB26A488361897EB90B896280E8B62","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":297,"audio_id":3926114,"topic_url":"","brief":"","rp_publish":1,"filesize":4762512,"remark":"为所欲为","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/h0ql946.html"},{"hash":"0E00136058F701B40862A1B01256ECEF","sqfilesize":0,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"34683080","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"370FE6D96B60199FD4B900DCC587AA99","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10945965,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"凉爽 - 根本你不懂得爱我 (DJ耀仔版)","old_cpy":1,"album_audio_id":241378799,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":82864516,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"52AFAAA10D9FEAFE8D7402796F214612","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":273,"audio_id":63950075,"topic_url":"","brief":"","rp_publish":1,"filesize":4378584,"remark":"Kok Aspan","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/3zpl2nc7.html"},{"hash":"71D4965A6B5BA48C8314FB6CD5DA2D29","sqfilesize":34527873,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"D5EE6C3F4164CDB41642A7C8DE770635","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14938759","mvhash":"8DE96E7201328EC36E70DDB72F25856F","extname":"mp3","topic_url_sq":"","320hash":"B8EF8287C90F62C2AB71187881ED5146","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9977934,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"百变二嫂 - 朋友这杯酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":129726054,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":57661165,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"D1B755D0C2BDE37A0F006BF5A14307B2","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":249,"audio_id":49791898,"topic_url":"","brief":"","rp_publish":1,"filesize":3991136,"remark":"朋友这杯酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258h9i81.html"},{"hash":"D1C8D835962B4FEF743F75541BEC9446","sqfilesize":36540868,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"26D9B44D7B1F78C90BF7F2CF85EA9BAE","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"35543348","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA16F3BE943E41E7BB479B1A6EDE9D32","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11219281,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅、李勇森 - 偷偷爱着你 (DJ何鹏版)","old_cpy":0,"album_audio_id":245621344,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":85396842,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AD33B3B303B484FBAE5813BD9966420D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":280,"audio_id":65339235,"topic_url":"","brief":"","rp_publish":1,"filesize":4487671,"remark":"偷偷爱着你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/428in48b.html"},{"hash":"6D0E6E5AC5D14D978320153BEF7A1D45","sqfilesize":31541133,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"482647816AEDEFBA55425D627B8FB2AB","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"10864457","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"2C20321F391181AC45E75ED829F9E22E","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9821038,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"姚大 - 那一刻是你 (DJ版)","old_cpy":0,"album_audio_id":115742361,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":49248287,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"020A63F480427CF285278A3AAB947BA1","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":44150426,"topic_url":"","brief":"","rp_publish":1,"filesize":3928519,"remark":"那一刻是你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1wwrdl61.html"},{"hash":"1C06541EF39A41DE1CB60E0668FA3F1F","sqfilesize":33919218,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F78FF5BA965085B05588F67CDD3CD826","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"23072478","mvhash":"E4388391A6DCBA5816E330E379D1039C","extname":"mp3","topic_url_sq":"","320hash":"1C34E697BAC76880985AA295CBE415EE","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9827309,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"大星 - 你是我的人 (DJ版)","old_cpy":0,"album_audio_id":164314016,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":75368319,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"5FA180677BB8E04CB3C530ECB51C7CEF","hash_offset":{"start_byte":0,"end_ms":60000,"end_byte":960931,"file_type":0,"start_ms":0,"offset_hash":"936926B7319175DD32F61D7A8C132D46"},"musicpack_advance":1,"display":0,"display_rate":0},"duration":245,"audio_id":56170669,"topic_url":"","brief":"","rp_publish":1,"filesize":3930950,"remark":"你是我的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2ptti82c.html"},{"hash":"5124AD603C6BAAF7A1606DBA01DC94BE","sqfilesize":34217386,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"7F45C223F7D34765CBF9360A89D4CDC7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"36841825","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"1EB1876F70E15998C7E00ADBC941C08F","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9935097,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"赵真 - 送我一杯忘情酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":252989575,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":94270867,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"C91A47A5F8023BBAF4854C31E95C317C","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":248,"audio_id":69834521,"topic_url":"","brief":"","rp_publish":1,"filesize":3973999,"remark":"送我一杯忘情酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/46mg0757.html"},{"hash":"69D5C768762ED868E9EFD11CC5306DC3","sqfilesize":59287596,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"FBA5B4F24AC588D5ADBA46557B6E6415","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"1673205","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"7A6A96A1C0F5F76AC50359CCB37A69E1","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":18121873,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"程响 - 新娘不是我 (DJ版)","old_cpy":1,"album_audio_id":101344691,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"2E60FE1C33F60BF73E50BD228321C41E","pay_block_tpl":1,"classmap":{"attr0":100663304},"display":0,"display_rate":0},"duration":451,"audio_id":86774056,"topic_url":"","brief":"","rp_publish":1,"filesize":7216482,"remark":"程式情歌 (DJ精选集)","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/1oc62bd0.html"},{"hash":"BB5B1D6904FB8BFF3906C749B3E6F772","sqfilesize":44853755,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"209A6C2E60F9A692CC81E9E56AD11D11","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"C78EE63327A4DFBEE2F92A754F71BDE9","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13300712,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ阿远版)","old_cpy":0,"album_audio_id":106664707,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":32434800,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AA707FE8EC951377F5D73EF1762ED076","pay_block_tpl":1,"musicpack_advance":1,"display":4,"display_rate":1},"duration":332,"audio_id":37350296,"topic_url":"","brief":"","rp_publish":1,"filesize":5320246,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1ri70jf3.html"},{"hash":"D0E335B3B5C45AC7B7A2A82F8866FA91","sqfilesize":35315914,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"C97C8CFF5AAF928B0D7F16923CF30947","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"11812620","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"DDCE56E8213AA222C83EBF2F7DB536E7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11027655,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"秋裤大叔 - 老地方的雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":119495095,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":50389529,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"266347DCBA08F41724BE1EAF82C8944D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":275,"audio_id":46071261,"topic_url":"","brief":"","rp_publish":1,"filesize":4411185,"remark":"老地方的雨","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1z5707fe.html"},{"hash":"23EDB24C697F41E8C955031457097D40","sqfilesize":39112007,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"6AA1B3F2B1FE602A79006C13822A174E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"061DDFF6BDAD4810F181147231F67368","extname":"mp3","topic_url_sq":"","320hash":"1034AA51CF690365A6214FF5B3B8DC42","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11778203,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 爱像一场纷飞雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690491,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6912008,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A097F1E436094551F55560FE2078DF51","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":296,"audio_id":3028629,"topic_url":"","brief":"","rp_publish":1,"filesize":4743880,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwsb1d.html"},{"hash":"0ECDF6CB280F245215E6117D703E3EED","sqfilesize":49464346,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"BB09372BD36875F4BE72E5C75A56F94C","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2300195","mvhash":"C8DFD98FF614719DD9109FF814F1BC47","extname":"mp3","topic_url_sq":"","320hash":"9707B35A610CADE8B834E7EFAE17035A","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":16031054,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭玲 - 爱上你我傻乎乎 (DJ阿圣版)","old_cpy":1,"album_audio_id":60021057,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911206,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"51325B2B0BD61EF1679EFC9273448887","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":401,"audio_id":174892,"topic_url":"","brief":"","rp_publish":1,"filesize":6421714,"remark":"DJ阿圣舞曲精选集(五)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/zqgjl89.html"},{"hash":"7FE1540CE8F05FBEC1819B42A2B43401","sqfilesize":32601892,"sqprivilege":10,"pay_type_sq":3,"bitrate":127,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"9BFF87478E8A7A862C63F74D8FFE2C83","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1627047","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"5E73BC4B2722040E1B5A6A711FC3A292","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅 - 套马杆 (DJ何鹏版)","old_cpy":1,"album_audio_id":105348887,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31254171,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AAC4076B4075C460F9CDBF2C384CAF14","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":21509826,"topic_url":"","brief":"","rp_publish":1,"filesize":3935001,"remark":"套马杆","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qpzpzfd.html"},{"hash":"50BF0BBE7ACDC7E484B6D9B8757849AE","sqfilesize":32558570,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"292A68A04D7887ED00DBDA5F26657454","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"16420788","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"A115950AB8D5ACF4412ABA30F1042244","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9378003,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"蓝琪儿 - 朋友如酒 (DJ何鹏版)","old_cpy":1,"album_audio_id":134619993,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":63757569,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"167FC1D6336A6FE60EF116DDF612F715","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":234,"audio_id":50912606,"topic_url":"","brief":"","rp_publish":1,"filesize":3751227,"remark":"朋友如酒DJ何鹏版","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/285dg972.html"},{"hash":"4894147E213EEC9217D0FD2BA30EC903","sqfilesize":32485022,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"600D06EC0B5BDA3509D6620079D8ECB7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"AE0E89D8EEE4D533E092B0A69F0E5D99","extname":"mp3","topic_url_sq":"","320hash":"536EEF63A0508B2D5B8C20F9CC354C84","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9911085,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 别把寂寞当缘分 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690504,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911995,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"800ED35B661EEBF8945A7079C88CB04B","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":247,"audio_id":6684218,"topic_url":"","brief":"","rp_publish":1,"filesize":3964386,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwso16.html"},{"hash":"052D118327C0060DA24B5BE24C7D91F6","sqfilesize":44985457,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"2E5F668B93C8D3EA9330379FCD8DB25E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14939069","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8067ED37A4484DDC5771F66EEBB0E73C","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14772975,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"龙奔 - 为你伤心为你哭 (DJ伟伟版)","old_cpy":1,"album_audio_id":129726872,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":57546330,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E1EAEBB82F1202DC0C26A8769A0AC91D","pay_block_tpl":1,"musicpack_advance":0,"display":4,"display_rate":1},"duration":369,"audio_id":49759499,"topic_url":"","brief":"","rp_publish":1,"filesize":5909151,"remark":"为你伤心为你哭","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258hw8d7.html"},{"hash":"29FA195C872778BEC8C3F556C553F9A3","sqfilesize":42767248,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"0CC91F417AB357C84BF9935B0D013391","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"D1D90EE07EC47FD0051FB89D730AF06B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14552500,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ版)","old_cpy":1,"album_audio_id":104961633,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31024258,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"82DD4A56320136EE3895EA93766B1143","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":364,"audio_id":23552165,"topic_url":"","brief":"","rp_publish":1,"filesize":5821044,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qhowx80.html"},{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830420,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cic6b.html"}]},"pagesize":30,"page":1}
     * info : {"list":{"specialid":2452799,"playcount":56207042,"songcount":167,"publishtime":"2020-04-30 00:00:00","user_type":0,"ugc_talent_review":1,"slid":297,"verified":0,"nickname":"☆冰雨欣☆🎸","commentcount":63,"singername":"","collectcount":50562,"trans_param":{"special_tag":0},"user_avatar":"http://imge.kugou.com/kugouicon/165/20170630/20170630115107548763.jpg","specialname":"车载劲爆DJ：提神醒脑 一路精神抖擞","tags":[{"tagid":17,"tagname":"DJ热碟"},{"tagid":63,"tagname":"车载"},{"tagid":577,"tagname":"轻松"}],"percount":0,"type":0,"plist":[],"imgurl":"http://c1.kgimg.com/custom/{size}/20200430/20200430102853651490.jpg","global_specialid":"collection_3_577614559_297_0","is_selected":0,"intro":"DJ音乐是旅途中的好伴侣，让你一路开车不再有倦意。","suid":577614559}}
     * pagesize : 30
     * __Tpl : plist/list.html
     */

    private int JS_CSS_DATE;
    private String kg_domain;
    private String src;
    private Object fr;
    private String ver;
    private ListBeanX list;
    private InfoBeanX info;
    private int pagesize;
    private String __Tpl;

    public int getJS_CSS_DATE() {
        return JS_CSS_DATE;
    }

    public void setJS_CSS_DATE(int JS_CSS_DATE) {
        this.JS_CSS_DATE = JS_CSS_DATE;
    }

    public String getKg_domain() {
        return kg_domain;
    }

    public void setKg_domain(String kg_domain) {
        this.kg_domain = kg_domain;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Object getFr() {
        return fr;
    }

    public void setFr(Object fr) {
        this.fr = fr;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public ListBeanX getList() {
        return list;
    }

    public void setList(ListBeanX list) {
        this.list = list;
    }

    public InfoBeanX getInfo() {
        return info;
    }

    public void setInfo(InfoBeanX info) {
        this.info = info;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public String get__Tpl() {
        return __Tpl;
    }

    public void set__Tpl(String __Tpl) {
        this.__Tpl = __Tpl;
    }

    public static class ListBeanX {
        /**
         * list : {"timestamp":1642469473,"total":167,"info":[{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830421,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cid83.html"},{"hash":"D9AEF72926A682B65F58B6119886063D","sqfilesize":26856239,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F18FEE2562E500DD1CC3ABCE60C33548","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"24079251","mvhash":"595911438A1BC2DDD88449D129ADC85A","extname":"mp3","topic_url_sq":"","320hash":"33234C08DCE88059300629441FD5C6E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":7682065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"莫斯满、老猫 - 野花香 (DJ何鹏版)","old_cpy":1,"album_audio_id":171130466,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":72750712,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"46504232F2A329C1A8FAE5E1AE0FA20A","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":192,"audio_id":56810094,"topic_url":"","brief":"","rp_publish":1,"filesize":3072879,"remark":"野花香","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2tvx42c4.html"},{"hash":"FD025EC77FBA5BF518542549A810A6FF","sqfilesize":31976878,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"ED86EC015CDF234DA264BA57A4FD99D8","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"20635283","mvhash":"C894B2E860B8A4695A7D0D495527EBA4","extname":"mp3","topic_url_sq":"","320hash":"328CC13A721D4553A2657BE5578C7035","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843154,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"音萌萌、寂悸 - 愿为你跪六千年 (DJ何鹏版)","old_cpy":0,"album_audio_id":153334403,"fail_process_320":4,"trans_param":{"cpy_grade":6,"classmap":{"attr0":100663304},"cid":69687260,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"0E4061B450BC9B187C547744BB08744D","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":246,"audio_id":54820237,"topic_url":"","brief":"","rp_publish":1,"filesize":3937219,"remark":"愿为你跪六千年","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jahkzb8.html"},{"hash":"FA1018E8CD0B9B7C28F6018AF81CF251","sqfilesize":43241639,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"14C0BAFB7F7317AF33197D416ABDB682","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"12432890","mvhash":"B57E90910F956E702B51F86F81DDD7AD","extname":"mp3","topic_url_sq":"","320hash":"4B03E5911584C8BB82C8EF18EAD833E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12547348,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭聪明 - 你会遇见更好的人 (DJ阳少版)","old_cpy":0,"album_audio_id":121372145,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":50773717,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"3AA9B8DD7F607C345FF07A24AD0D7F38","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":313,"audio_id":49018116,"topic_url":"","brief":"","rp_publish":1,"filesize":5018897,"remark":"你会遇见更好的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/209fchbb.html"},{"hash":"3B08D35D317AEBBF90BF5193708CB12F","sqfilesize":46304973,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"02401887EC42C0AD7D0BA0B31BD70C14","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2417364","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA20571742A23DCB7CF85CEB5130E36B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":15019571,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"祁隆 - 唱着情歌流着泪 (DJ版)","old_cpy":0,"album_audio_id":62298255,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":22782096,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"A1973ED37F0F56BA5F9E566EA3452144","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":375,"audio_id":20770,"topic_url":"","brief":"","rp_publish":1,"filesize":6009832,"remark":"唱着情歌流着泪","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1139n36f.html"},{"hash":"D28D6091CFE2BF83DE1F7BB886639077","sqfilesize":35164339,"sqprivilege":5,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"B8865C9A44228A7C8598F5979BACD9CF","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"35709505","mvhash":"6ECEE711576CDDE25EF4D7EACFD15657","extname":"mp3","topic_url_sq":"","320hash":"3BDADA2DE3F0B4DD386A389018707BD2","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":10212876,"pkg_price_320":0,"privilege":5,"feetype":0,"price":0,"filename":"白小白 - 爱不得忘不舍 (DJ版)","old_cpy":1,"album_audio_id":246750484,"fail_process_320":0,"trans_param":{"cpy_grade":75,"classmap":{"attr0":100663304},"cid":176006448,"cpy_attr0":2,"hash_multitrack":"F7568F964514B92F36068569FB6F4E83","pay_block_tpl":1,"musicpack_advance":0,"display":1,"display_rate":0},"duration":255,"audio_id":87102146,"topic_url":"","brief":"","rp_publish":1,"filesize":4083087,"remark":"爱不得忘不舍","pkg_price":0,"320privilege":5,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/42wpw436.html"},{"hash":"525F90EF87AACEE5008703177F20B03E","sqfilesize":46790582,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F3B82FDE09F4E94A7C8DBDDE9568F396","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"15999493","mvhash":"818C1799FC27BEE7248936123D606215","extname":"mp3","topic_url_sq":"","320hash":"260C8925CA8677FDD8F8A7B7D1ED0F50","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13052575,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"大壮 - 伪装 (DJ小鱼儿版)","old_cpy":1,"album_audio_id":153369894,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":67108872},"cpy_level":1,"cid":69672404,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"480DD1ECDCA1CA5983EE0CB667F37703","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":326,"audio_id":54795157,"topic_url":"","brief":"","rp_publish":1,"filesize":5221190,"remark":"伪装","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jb8yud2.html"},{"hash":"E4753ED63B02ADEDDD14FC4EE882FB07","sqfilesize":18783028,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"6547A949A7DD9CBFEC6ECDB89BE65E6A","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"21345282","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"9026C6C5F660549EA6652884F71D221A","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":5566383,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"颜妹、苏天伦 - 渡我不渡她","old_cpy":1,"album_audio_id":156509900,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"EC2CA2968FC27295C3F4D5D91E5729DB","pay_block_tpl":1,"classmap":{"attr0":103809032},"display":64,"display_rate":5},"duration":139,"audio_id":54836961,"topic_url":"","brief":"","rp_publish":1,"filesize":2226094,"remark":"渡我不渡她DJ","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/2l6jt8dc.html"},{"hash":"BB4B788496B1F062D957F5E95D4F448F","sqfilesize":35307003,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F86DD9ABC534D41B8A02D741F1D681C6","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17959109","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"50100CE73D1F61005C00AF9C2E9C0189","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10642513,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"孙艺琪、崔伟立 - 怎么爱都爱不够 (DJ何鹏版)","old_cpy":0,"album_audio_id":140230779,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":66030173,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"30DFBB65321C27A41AEC7C1CF7B8E263","pay_block_tpl":1,"musicpack_advance":1,"display":32,"display_rate":1},"duration":266,"audio_id":52305374,"topic_url":"","brief":"","rp_publish":1,"filesize":4256958,"remark":"怎么爱都爱不够(DJ版)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bhmrf57.html"},{"hash":"3E91A006ADB0C81F3F719820E42CBAC5","sqfilesize":35316793,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"4E2CF8A0CF028FCD2833D0E3A92F17C4","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17892768","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8D889F65D56C1A9D61CA02AD7A550031","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10464593,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"吴官辉 - 错过花开错过爱 (DJ何鹏版)","old_cpy":0,"album_audio_id":139973328,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":65969771,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E5D5313D6E10D29C73E9569D5A25813C","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":261,"audio_id":52268804,"topic_url":"","brief":"","rp_publish":1,"filesize":4185905,"remark":"错过花开错过爱","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bc4406d.html"},{"hash":"359075417176CFA898C770EEC5140165","sqfilesize":29226468,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"727CE656045ADB4C241512CFB62EE488","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"27365590","mvhash":"07BFBB441B262BF6B5BFDF8A04C211A0","extname":"mp3","topic_url_sq":"","320hash":"F0B8DBA7A433E0A6985CA11F3DB38F80","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":8559974,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"雪十郎 - 谁 (DJ何鹏版)","old_cpy":0,"album_audio_id":199368286,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":75499739,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"85B3126FE294E52719B231635816D1F9","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":213,"audio_id":59202204,"topic_url":"","brief":"","rp_publish":1,"filesize":3423965,"remark":"谁","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/3ap5jy3c.html"},{"hash":"8F8B9D3EE4662BE76166377A13EF3F13","sqfilesize":46915452,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AF56AB39F9B0BBCBA3493B632EF1DE79","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"8304053","mvhash":"12F8765B7BB481384831717ED3E4D363","extname":"mp3","topic_url_sq":"","320hash":"CF522D62F8CC55C5EAA9239A81A6EA6B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14483524,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 牛在飞 (DJ阿远版)","old_cpy":0,"album_audio_id":105837366,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31988491,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A55950794C84E0D099CAEE7C1B0E6479","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":362,"audio_id":36698493,"topic_url":"","brief":"","rp_publish":1,"filesize":5793376,"remark":"牛在飞","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1r0gmuf2.html"},{"hash":"EA0FF533C09C1647AF8FB11B06A9058A","sqfilesize":39116181,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"204E82BB37FC08860BEDE91C179F9168","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"595197","mvhash":"80F2C0C09B9F7A3D78298BBD6C5590E6","extname":"mp3","topic_url_sq":"","320hash":"D2BFEE30C1E8E95EC5BC4C4D94A49C96","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11886949,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"智涛 - 我锁上爱情的门 (DJ何鹏版)","old_cpy":1,"album_audio_id":28587933,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":23004802,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"9DAB26A488361897EB90B896280E8B62","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":297,"audio_id":3926114,"topic_url":"","brief":"","rp_publish":1,"filesize":4762512,"remark":"为所欲为","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/h0ql946.html"},{"hash":"0E00136058F701B40862A1B01256ECEF","sqfilesize":0,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"34683080","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"370FE6D96B60199FD4B900DCC587AA99","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10945965,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"凉爽 - 根本你不懂得爱我 (DJ耀仔版)","old_cpy":1,"album_audio_id":241378799,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":82864516,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"52AFAAA10D9FEAFE8D7402796F214612","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":273,"audio_id":63950075,"topic_url":"","brief":"","rp_publish":1,"filesize":4378584,"remark":"Kok Aspan","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/3zpl2nc7.html"},{"hash":"71D4965A6B5BA48C8314FB6CD5DA2D29","sqfilesize":34527873,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"D5EE6C3F4164CDB41642A7C8DE770635","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14938759","mvhash":"8DE96E7201328EC36E70DDB72F25856F","extname":"mp3","topic_url_sq":"","320hash":"B8EF8287C90F62C2AB71187881ED5146","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9977934,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"百变二嫂 - 朋友这杯酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":129726054,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":57661165,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"D1B755D0C2BDE37A0F006BF5A14307B2","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":249,"audio_id":49791898,"topic_url":"","brief":"","rp_publish":1,"filesize":3991136,"remark":"朋友这杯酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258h9i81.html"},{"hash":"D1C8D835962B4FEF743F75541BEC9446","sqfilesize":36540868,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"26D9B44D7B1F78C90BF7F2CF85EA9BAE","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"35543348","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA16F3BE943E41E7BB479B1A6EDE9D32","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11219281,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅、李勇森 - 偷偷爱着你 (DJ何鹏版)","old_cpy":0,"album_audio_id":245621344,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":85396842,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AD33B3B303B484FBAE5813BD9966420D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":280,"audio_id":65339235,"topic_url":"","brief":"","rp_publish":1,"filesize":4487671,"remark":"偷偷爱着你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/428in48b.html"},{"hash":"6D0E6E5AC5D14D978320153BEF7A1D45","sqfilesize":31541133,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"482647816AEDEFBA55425D627B8FB2AB","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"10864457","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"2C20321F391181AC45E75ED829F9E22E","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9821038,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"姚大 - 那一刻是你 (DJ版)","old_cpy":0,"album_audio_id":115742361,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":49248287,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"020A63F480427CF285278A3AAB947BA1","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":44150426,"topic_url":"","brief":"","rp_publish":1,"filesize":3928519,"remark":"那一刻是你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1wwrdl61.html"},{"hash":"1C06541EF39A41DE1CB60E0668FA3F1F","sqfilesize":33919218,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F78FF5BA965085B05588F67CDD3CD826","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"23072478","mvhash":"E4388391A6DCBA5816E330E379D1039C","extname":"mp3","topic_url_sq":"","320hash":"1C34E697BAC76880985AA295CBE415EE","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9827309,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"大星 - 你是我的人 (DJ版)","old_cpy":0,"album_audio_id":164314016,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":75368319,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"5FA180677BB8E04CB3C530ECB51C7CEF","hash_offset":{"start_byte":0,"end_ms":60000,"end_byte":960931,"file_type":0,"start_ms":0,"offset_hash":"936926B7319175DD32F61D7A8C132D46"},"musicpack_advance":1,"display":0,"display_rate":0},"duration":245,"audio_id":56170669,"topic_url":"","brief":"","rp_publish":1,"filesize":3930950,"remark":"你是我的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2ptti82c.html"},{"hash":"5124AD603C6BAAF7A1606DBA01DC94BE","sqfilesize":34217386,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"7F45C223F7D34765CBF9360A89D4CDC7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"36841825","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"1EB1876F70E15998C7E00ADBC941C08F","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9935097,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"赵真 - 送我一杯忘情酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":252989575,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":94270867,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"C91A47A5F8023BBAF4854C31E95C317C","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":248,"audio_id":69834521,"topic_url":"","brief":"","rp_publish":1,"filesize":3973999,"remark":"送我一杯忘情酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/46mg0757.html"},{"hash":"69D5C768762ED868E9EFD11CC5306DC3","sqfilesize":59287596,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"FBA5B4F24AC588D5ADBA46557B6E6415","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"1673205","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"7A6A96A1C0F5F76AC50359CCB37A69E1","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":18121873,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"程响 - 新娘不是我 (DJ版)","old_cpy":1,"album_audio_id":101344691,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"2E60FE1C33F60BF73E50BD228321C41E","pay_block_tpl":1,"classmap":{"attr0":100663304},"display":0,"display_rate":0},"duration":451,"audio_id":86774056,"topic_url":"","brief":"","rp_publish":1,"filesize":7216482,"remark":"程式情歌 (DJ精选集)","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/1oc62bd0.html"},{"hash":"BB5B1D6904FB8BFF3906C749B3E6F772","sqfilesize":44853755,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"209A6C2E60F9A692CC81E9E56AD11D11","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"C78EE63327A4DFBEE2F92A754F71BDE9","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13300712,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ阿远版)","old_cpy":0,"album_audio_id":106664707,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":32434800,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AA707FE8EC951377F5D73EF1762ED076","pay_block_tpl":1,"musicpack_advance":1,"display":4,"display_rate":1},"duration":332,"audio_id":37350296,"topic_url":"","brief":"","rp_publish":1,"filesize":5320246,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1ri70jf3.html"},{"hash":"D0E335B3B5C45AC7B7A2A82F8866FA91","sqfilesize":35315914,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"C97C8CFF5AAF928B0D7F16923CF30947","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"11812620","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"DDCE56E8213AA222C83EBF2F7DB536E7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11027655,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"秋裤大叔 - 老地方的雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":119495095,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":50389529,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"266347DCBA08F41724BE1EAF82C8944D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":275,"audio_id":46071261,"topic_url":"","brief":"","rp_publish":1,"filesize":4411185,"remark":"老地方的雨","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1z5707fe.html"},{"hash":"23EDB24C697F41E8C955031457097D40","sqfilesize":39112007,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"6AA1B3F2B1FE602A79006C13822A174E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"061DDFF6BDAD4810F181147231F67368","extname":"mp3","topic_url_sq":"","320hash":"1034AA51CF690365A6214FF5B3B8DC42","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11778203,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 爱像一场纷飞雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690491,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6912008,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A097F1E436094551F55560FE2078DF51","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":296,"audio_id":3028629,"topic_url":"","brief":"","rp_publish":1,"filesize":4743880,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwsb1d.html"},{"hash":"0ECDF6CB280F245215E6117D703E3EED","sqfilesize":49464346,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"BB09372BD36875F4BE72E5C75A56F94C","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2300195","mvhash":"C8DFD98FF614719DD9109FF814F1BC47","extname":"mp3","topic_url_sq":"","320hash":"9707B35A610CADE8B834E7EFAE17035A","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":16031054,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭玲 - 爱上你我傻乎乎 (DJ阿圣版)","old_cpy":1,"album_audio_id":60021057,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911206,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"51325B2B0BD61EF1679EFC9273448887","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":401,"audio_id":174892,"topic_url":"","brief":"","rp_publish":1,"filesize":6421714,"remark":"DJ阿圣舞曲精选集(五)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/zqgjl89.html"},{"hash":"7FE1540CE8F05FBEC1819B42A2B43401","sqfilesize":32601892,"sqprivilege":10,"pay_type_sq":3,"bitrate":127,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"9BFF87478E8A7A862C63F74D8FFE2C83","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1627047","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"5E73BC4B2722040E1B5A6A711FC3A292","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅 - 套马杆 (DJ何鹏版)","old_cpy":1,"album_audio_id":105348887,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31254171,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AAC4076B4075C460F9CDBF2C384CAF14","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":21509826,"topic_url":"","brief":"","rp_publish":1,"filesize":3935001,"remark":"套马杆","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qpzpzfd.html"},{"hash":"50BF0BBE7ACDC7E484B6D9B8757849AE","sqfilesize":32558570,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"292A68A04D7887ED00DBDA5F26657454","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"16420788","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"A115950AB8D5ACF4412ABA30F1042244","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9378003,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"蓝琪儿 - 朋友如酒 (DJ何鹏版)","old_cpy":1,"album_audio_id":134619993,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":63757569,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"167FC1D6336A6FE60EF116DDF612F715","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":234,"audio_id":50912606,"topic_url":"","brief":"","rp_publish":1,"filesize":3751227,"remark":"朋友如酒DJ何鹏版","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/285dg972.html"},{"hash":"4894147E213EEC9217D0FD2BA30EC903","sqfilesize":32485022,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"600D06EC0B5BDA3509D6620079D8ECB7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"AE0E89D8EEE4D533E092B0A69F0E5D99","extname":"mp3","topic_url_sq":"","320hash":"536EEF63A0508B2D5B8C20F9CC354C84","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9911085,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 别把寂寞当缘分 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690504,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911995,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"800ED35B661EEBF8945A7079C88CB04B","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":247,"audio_id":6684218,"topic_url":"","brief":"","rp_publish":1,"filesize":3964386,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwso16.html"},{"hash":"052D118327C0060DA24B5BE24C7D91F6","sqfilesize":44985457,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"2E5F668B93C8D3EA9330379FCD8DB25E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14939069","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8067ED37A4484DDC5771F66EEBB0E73C","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14772975,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"龙奔 - 为你伤心为你哭 (DJ伟伟版)","old_cpy":1,"album_audio_id":129726872,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":57546330,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E1EAEBB82F1202DC0C26A8769A0AC91D","pay_block_tpl":1,"musicpack_advance":0,"display":4,"display_rate":1},"duration":369,"audio_id":49759499,"topic_url":"","brief":"","rp_publish":1,"filesize":5909151,"remark":"为你伤心为你哭","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258hw8d7.html"},{"hash":"29FA195C872778BEC8C3F556C553F9A3","sqfilesize":42767248,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"0CC91F417AB357C84BF9935B0D013391","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"D1D90EE07EC47FD0051FB89D730AF06B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14552500,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ版)","old_cpy":1,"album_audio_id":104961633,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31024258,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"82DD4A56320136EE3895EA93766B1143","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":364,"audio_id":23552165,"topic_url":"","brief":"","rp_publish":1,"filesize":5821044,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qhowx80.html"},{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830420,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cic6b.html"}]}
         * pagesize : 30
         * page : 1
         */

        private ListBean list;
        private int pagesize;
        private int page;

        public ListBean getList() {
            return list;
        }

        public void setList(ListBean list) {
            this.list = list;
        }

        public int getPagesize() {
            return pagesize;
        }

        public void setPagesize(int pagesize) {
            this.pagesize = pagesize;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public static class ListBean {
            /**
             * timestamp : 1642469473
             * total : 167
             * info : [{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830421,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cid83.html"},{"hash":"D9AEF72926A682B65F58B6119886063D","sqfilesize":26856239,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F18FEE2562E500DD1CC3ABCE60C33548","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"24079251","mvhash":"595911438A1BC2DDD88449D129ADC85A","extname":"mp3","topic_url_sq":"","320hash":"33234C08DCE88059300629441FD5C6E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":7682065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"莫斯满、老猫 - 野花香 (DJ何鹏版)","old_cpy":1,"album_audio_id":171130466,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":72750712,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"46504232F2A329C1A8FAE5E1AE0FA20A","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":192,"audio_id":56810094,"topic_url":"","brief":"","rp_publish":1,"filesize":3072879,"remark":"野花香","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2tvx42c4.html"},{"hash":"FD025EC77FBA5BF518542549A810A6FF","sqfilesize":31976878,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"ED86EC015CDF234DA264BA57A4FD99D8","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"20635283","mvhash":"C894B2E860B8A4695A7D0D495527EBA4","extname":"mp3","topic_url_sq":"","320hash":"328CC13A721D4553A2657BE5578C7035","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843154,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"音萌萌、寂悸 - 愿为你跪六千年 (DJ何鹏版)","old_cpy":0,"album_audio_id":153334403,"fail_process_320":4,"trans_param":{"cpy_grade":6,"classmap":{"attr0":100663304},"cid":69687260,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"0E4061B450BC9B187C547744BB08744D","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":246,"audio_id":54820237,"topic_url":"","brief":"","rp_publish":1,"filesize":3937219,"remark":"愿为你跪六千年","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jahkzb8.html"},{"hash":"FA1018E8CD0B9B7C28F6018AF81CF251","sqfilesize":43241639,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"14C0BAFB7F7317AF33197D416ABDB682","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"12432890","mvhash":"B57E90910F956E702B51F86F81DDD7AD","extname":"mp3","topic_url_sq":"","320hash":"4B03E5911584C8BB82C8EF18EAD833E1","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12547348,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭聪明 - 你会遇见更好的人 (DJ阳少版)","old_cpy":0,"album_audio_id":121372145,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":50773717,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"3AA9B8DD7F607C345FF07A24AD0D7F38","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":313,"audio_id":49018116,"topic_url":"","brief":"","rp_publish":1,"filesize":5018897,"remark":"你会遇见更好的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/209fchbb.html"},{"hash":"3B08D35D317AEBBF90BF5193708CB12F","sqfilesize":46304973,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"02401887EC42C0AD7D0BA0B31BD70C14","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2417364","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA20571742A23DCB7CF85CEB5130E36B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":15019571,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"祁隆 - 唱着情歌流着泪 (DJ版)","old_cpy":0,"album_audio_id":62298255,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":22782096,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"A1973ED37F0F56BA5F9E566EA3452144","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":375,"audio_id":20770,"topic_url":"","brief":"","rp_publish":1,"filesize":6009832,"remark":"唱着情歌流着泪","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1139n36f.html"},{"hash":"D28D6091CFE2BF83DE1F7BB886639077","sqfilesize":35164339,"sqprivilege":5,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"B8865C9A44228A7C8598F5979BACD9CF","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"35709505","mvhash":"6ECEE711576CDDE25EF4D7EACFD15657","extname":"mp3","topic_url_sq":"","320hash":"3BDADA2DE3F0B4DD386A389018707BD2","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":10212876,"pkg_price_320":0,"privilege":5,"feetype":0,"price":0,"filename":"白小白 - 爱不得忘不舍 (DJ版)","old_cpy":1,"album_audio_id":246750484,"fail_process_320":0,"trans_param":{"cpy_grade":75,"classmap":{"attr0":100663304},"cid":176006448,"cpy_attr0":2,"hash_multitrack":"F7568F964514B92F36068569FB6F4E83","pay_block_tpl":1,"musicpack_advance":0,"display":1,"display_rate":0},"duration":255,"audio_id":87102146,"topic_url":"","brief":"","rp_publish":1,"filesize":4083087,"remark":"爱不得忘不舍","pkg_price":0,"320privilege":5,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/42wpw436.html"},{"hash":"525F90EF87AACEE5008703177F20B03E","sqfilesize":46790582,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F3B82FDE09F4E94A7C8DBDDE9568F396","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"15999493","mvhash":"818C1799FC27BEE7248936123D606215","extname":"mp3","topic_url_sq":"","320hash":"260C8925CA8677FDD8F8A7B7D1ED0F50","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13052575,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"大壮 - 伪装 (DJ小鱼儿版)","old_cpy":1,"album_audio_id":153369894,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":67108872},"cpy_level":1,"cid":69672404,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"480DD1ECDCA1CA5983EE0CB667F37703","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":326,"audio_id":54795157,"topic_url":"","brief":"","rp_publish":1,"filesize":5221190,"remark":"伪装","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2jb8yud2.html"},{"hash":"E4753ED63B02ADEDDD14FC4EE882FB07","sqfilesize":18783028,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"6547A949A7DD9CBFEC6ECDB89BE65E6A","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"21345282","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"9026C6C5F660549EA6652884F71D221A","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":5566383,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"颜妹、苏天伦 - 渡我不渡她","old_cpy":1,"album_audio_id":156509900,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"EC2CA2968FC27295C3F4D5D91E5729DB","pay_block_tpl":1,"classmap":{"attr0":103809032},"display":64,"display_rate":5},"duration":139,"audio_id":54836961,"topic_url":"","brief":"","rp_publish":1,"filesize":2226094,"remark":"渡我不渡她DJ","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/2l6jt8dc.html"},{"hash":"BB4B788496B1F062D957F5E95D4F448F","sqfilesize":35307003,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F86DD9ABC534D41B8A02D741F1D681C6","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17959109","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"50100CE73D1F61005C00AF9C2E9C0189","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10642513,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"孙艺琪、崔伟立 - 怎么爱都爱不够 (DJ何鹏版)","old_cpy":0,"album_audio_id":140230779,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":66030173,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"30DFBB65321C27A41AEC7C1CF7B8E263","pay_block_tpl":1,"musicpack_advance":1,"display":32,"display_rate":1},"duration":266,"audio_id":52305374,"topic_url":"","brief":"","rp_publish":1,"filesize":4256958,"remark":"怎么爱都爱不够(DJ版)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bhmrf57.html"},{"hash":"3E91A006ADB0C81F3F719820E42CBAC5","sqfilesize":35316793,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"4E2CF8A0CF028FCD2833D0E3A92F17C4","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"17892768","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8D889F65D56C1A9D61CA02AD7A550031","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10464593,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"吴官辉 - 错过花开错过爱 (DJ何鹏版)","old_cpy":0,"album_audio_id":139973328,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":65969771,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E5D5313D6E10D29C73E9569D5A25813C","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":261,"audio_id":52268804,"topic_url":"","brief":"","rp_publish":1,"filesize":4185905,"remark":"错过花开错过爱","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2bc4406d.html"},{"hash":"359075417176CFA898C770EEC5140165","sqfilesize":29226468,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"727CE656045ADB4C241512CFB62EE488","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"27365590","mvhash":"07BFBB441B262BF6B5BFDF8A04C211A0","extname":"mp3","topic_url_sq":"","320hash":"F0B8DBA7A433E0A6985CA11F3DB38F80","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":8559974,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"雪十郎 - 谁 (DJ何鹏版)","old_cpy":0,"album_audio_id":199368286,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":75499739,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"85B3126FE294E52719B231635816D1F9","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":213,"audio_id":59202204,"topic_url":"","brief":"","rp_publish":1,"filesize":3423965,"remark":"谁","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/3ap5jy3c.html"},{"hash":"8F8B9D3EE4662BE76166377A13EF3F13","sqfilesize":46915452,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AF56AB39F9B0BBCBA3493B632EF1DE79","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"8304053","mvhash":"12F8765B7BB481384831717ED3E4D363","extname":"mp3","topic_url_sq":"","320hash":"CF522D62F8CC55C5EAA9239A81A6EA6B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14483524,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 牛在飞 (DJ阿远版)","old_cpy":0,"album_audio_id":105837366,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31988491,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A55950794C84E0D099CAEE7C1B0E6479","pay_block_tpl":1,"musicpack_advance":1,"display":0,"display_rate":0},"duration":362,"audio_id":36698493,"topic_url":"","brief":"","rp_publish":1,"filesize":5793376,"remark":"牛在飞","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1r0gmuf2.html"},{"hash":"EA0FF533C09C1647AF8FB11B06A9058A","sqfilesize":39116181,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"204E82BB37FC08860BEDE91C179F9168","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"595197","mvhash":"80F2C0C09B9F7A3D78298BBD6C5590E6","extname":"mp3","topic_url_sq":"","320hash":"D2BFEE30C1E8E95EC5BC4C4D94A49C96","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11886949,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"智涛 - 我锁上爱情的门 (DJ何鹏版)","old_cpy":1,"album_audio_id":28587933,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":23004802,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"9DAB26A488361897EB90B896280E8B62","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":297,"audio_id":3926114,"topic_url":"","brief":"","rp_publish":1,"filesize":4762512,"remark":"为所欲为","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/h0ql946.html"},{"hash":"0E00136058F701B40862A1B01256ECEF","sqfilesize":0,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"34683080","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"370FE6D96B60199FD4B900DCC587AA99","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":10945965,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"凉爽 - 根本你不懂得爱我 (DJ耀仔版)","old_cpy":1,"album_audio_id":241378799,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":82864516,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"52AFAAA10D9FEAFE8D7402796F214612","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":273,"audio_id":63950075,"topic_url":"","brief":"","rp_publish":1,"filesize":4378584,"remark":"Kok Aspan","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/3zpl2nc7.html"},{"hash":"71D4965A6B5BA48C8314FB6CD5DA2D29","sqfilesize":34527873,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"D5EE6C3F4164CDB41642A7C8DE770635","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14938759","mvhash":"8DE96E7201328EC36E70DDB72F25856F","extname":"mp3","topic_url_sq":"","320hash":"B8EF8287C90F62C2AB71187881ED5146","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9977934,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"百变二嫂 - 朋友这杯酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":129726054,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":57661165,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"D1B755D0C2BDE37A0F006BF5A14307B2","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":249,"audio_id":49791898,"topic_url":"","brief":"","rp_publish":1,"filesize":3991136,"remark":"朋友这杯酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258h9i81.html"},{"hash":"D1C8D835962B4FEF743F75541BEC9446","sqfilesize":36540868,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"26D9B44D7B1F78C90BF7F2CF85EA9BAE","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"35543348","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"CA16F3BE943E41E7BB479B1A6EDE9D32","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11219281,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅、李勇森 - 偷偷爱着你 (DJ何鹏版)","old_cpy":0,"album_audio_id":245621344,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":85396842,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AD33B3B303B484FBAE5813BD9966420D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":280,"audio_id":65339235,"topic_url":"","brief":"","rp_publish":1,"filesize":4487671,"remark":"偷偷爱着你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/428in48b.html"},{"hash":"6D0E6E5AC5D14D978320153BEF7A1D45","sqfilesize":31541133,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"482647816AEDEFBA55425D627B8FB2AB","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"10864457","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"2C20321F391181AC45E75ED829F9E22E","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9821038,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"姚大 - 那一刻是你 (DJ版)","old_cpy":0,"album_audio_id":115742361,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":49248287,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"020A63F480427CF285278A3AAB947BA1","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":44150426,"topic_url":"","brief":"","rp_publish":1,"filesize":3928519,"remark":"那一刻是你","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1wwrdl61.html"},{"hash":"1C06541EF39A41DE1CB60E0668FA3F1F","sqfilesize":33919218,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"F78FF5BA965085B05588F67CDD3CD826","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"23072478","mvhash":"E4388391A6DCBA5816E330E379D1039C","extname":"mp3","topic_url_sq":"","320hash":"1C34E697BAC76880985AA295CBE415EE","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9827309,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"大星 - 你是我的人 (DJ版)","old_cpy":0,"album_audio_id":164314016,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":75368319,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"5FA180677BB8E04CB3C530ECB51C7CEF","hash_offset":{"start_byte":0,"end_ms":60000,"end_byte":960931,"file_type":0,"start_ms":0,"offset_hash":"936926B7319175DD32F61D7A8C132D46"},"musicpack_advance":1,"display":0,"display_rate":0},"duration":245,"audio_id":56170669,"topic_url":"","brief":"","rp_publish":1,"filesize":3930950,"remark":"你是我的人","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/2ptti82c.html"},{"hash":"5124AD603C6BAAF7A1606DBA01DC94BE","sqfilesize":34217386,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"7F45C223F7D34765CBF9360A89D4CDC7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"36841825","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"1EB1876F70E15998C7E00ADBC941C08F","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9935097,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"赵真 - 送我一杯忘情酒 (DJ何鹏版)","old_cpy":0,"album_audio_id":252989575,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":94270867,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"C91A47A5F8023BBAF4854C31E95C317C","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":248,"audio_id":69834521,"topic_url":"","brief":"","rp_publish":1,"filesize":3973999,"remark":"送我一杯忘情酒","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/46mg0757.html"},{"hash":"69D5C768762ED868E9EFD11CC5306DC3","sqfilesize":59287596,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"topic_url_320":"","sqhash":"FBA5B4F24AC588D5ADBA46557B6E6415","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"1673205","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"7A6A96A1C0F5F76AC50359CCB37A69E1","price_sq":0,"inlist":1,"m4afilesize":0,"pay_type_320":0,"320filesize":18121873,"pkg_price_320":0,"privilege":0,"feetype":0,"price":0,"filename":"程响 - 新娘不是我 (DJ版)","old_cpy":1,"album_audio_id":101344691,"fail_process_320":0,"trans_param":{"cid":-1,"musicpack_advance":0,"cpy_attr0":0,"hash_multitrack":"2E60FE1C33F60BF73E50BD228321C41E","pay_block_tpl":1,"classmap":{"attr0":100663304},"display":0,"display_rate":0},"duration":451,"audio_id":86774056,"topic_url":"","brief":"","rp_publish":1,"filesize":7216482,"remark":"程式情歌 (DJ精选集)","pkg_price":0,"320privilege":0,"price_320":0,"fail_process_sq":0,"song_url":"https://m.kugou.com/mixsong/1oc62bd0.html"},{"hash":"BB5B1D6904FB8BFF3906C749B3E6F772","sqfilesize":44853755,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"209A6C2E60F9A692CC81E9E56AD11D11","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"C78EE63327A4DFBEE2F92A754F71BDE9","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":13300712,"pkg_price_320":1,"privilege":10,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ阿远版)","old_cpy":0,"album_audio_id":106664707,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":32434800,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AA707FE8EC951377F5D73EF1762ED076","pay_block_tpl":1,"musicpack_advance":1,"display":4,"display_rate":1},"duration":332,"audio_id":37350296,"topic_url":"","brief":"","rp_publish":1,"filesize":5320246,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1ri70jf3.html"},{"hash":"D0E335B3B5C45AC7B7A2A82F8866FA91","sqfilesize":35315914,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"C97C8CFF5AAF928B0D7F16923CF30947","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"11812620","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"DDCE56E8213AA222C83EBF2F7DB536E7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11027655,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"秋裤大叔 - 老地方的雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":119495095,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":50389529,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"266347DCBA08F41724BE1EAF82C8944D","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":275,"audio_id":46071261,"topic_url":"","brief":"","rp_publish":1,"filesize":4411185,"remark":"老地方的雨","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1z5707fe.html"},{"hash":"23EDB24C697F41E8C955031457097D40","sqfilesize":39112007,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"6AA1B3F2B1FE602A79006C13822A174E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"061DDFF6BDAD4810F181147231F67368","extname":"mp3","topic_url_sq":"","320hash":"1034AA51CF690365A6214FF5B3B8DC42","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":11778203,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 爱像一场纷飞雨 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690491,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6912008,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"A097F1E436094551F55560FE2078DF51","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":296,"audio_id":3028629,"topic_url":"","brief":"","rp_publish":1,"filesize":4743880,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwsb1d.html"},{"hash":"0ECDF6CB280F245215E6117D703E3EED","sqfilesize":49464346,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"BB09372BD36875F4BE72E5C75A56F94C","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"2300195","mvhash":"C8DFD98FF614719DD9109FF814F1BC47","extname":"mp3","topic_url_sq":"","320hash":"9707B35A610CADE8B834E7EFAE17035A","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":16031054,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"郭玲 - 爱上你我傻乎乎 (DJ阿圣版)","old_cpy":1,"album_audio_id":60021057,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911206,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"51325B2B0BD61EF1679EFC9273448887","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":401,"audio_id":174892,"topic_url":"","brief":"","rp_publish":1,"filesize":6421714,"remark":"DJ阿圣舞曲精选集(五)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/zqgjl89.html"},{"hash":"7FE1540CE8F05FBEC1819B42A2B43401","sqfilesize":32601892,"sqprivilege":10,"pay_type_sq":3,"bitrate":127,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"9BFF87478E8A7A862C63F74D8FFE2C83","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1627047","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"5E73BC4B2722040E1B5A6A711FC3A292","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9843065,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"乌兰托娅 - 套马杆 (DJ何鹏版)","old_cpy":1,"album_audio_id":105348887,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31254171,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"AAC4076B4075C460F9CDBF2C384CAF14","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":245,"audio_id":21509826,"topic_url":"","brief":"","rp_publish":1,"filesize":3935001,"remark":"套马杆","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qpzpzfd.html"},{"hash":"50BF0BBE7ACDC7E484B6D9B8757849AE","sqfilesize":32558570,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"292A68A04D7887ED00DBDA5F26657454","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"16420788","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"A115950AB8D5ACF4412ABA30F1042244","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9378003,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"蓝琪儿 - 朋友如酒 (DJ何鹏版)","old_cpy":1,"album_audio_id":134619993,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":63757569,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"167FC1D6336A6FE60EF116DDF612F715","pay_block_tpl":1,"musicpack_advance":0,"display":32,"display_rate":1},"duration":234,"audio_id":50912606,"topic_url":"","brief":"","rp_publish":1,"filesize":3751227,"remark":"朋友如酒DJ何鹏版","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/285dg972.html"},{"hash":"4894147E213EEC9217D0FD2BA30EC903","sqfilesize":32485022,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"600D06EC0B5BDA3509D6620079D8ECB7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"3642999","mvhash":"AE0E89D8EEE4D533E092B0A69F0E5D99","extname":"mp3","topic_url_sq":"","320hash":"536EEF63A0508B2D5B8C20F9CC354C84","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":9911085,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"冷漠 - 别把寂寞当缘分 (DJ何鹏版)","old_cpy":1,"album_audio_id":81690504,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":6911995,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"800ED35B661EEBF8945A7079C88CB04B","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":247,"audio_id":6684218,"topic_url":"","brief":"","rp_publish":1,"filesize":3964386,"remark":"DJ何鹏舞曲精选集(七)","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1cmwso16.html"},{"hash":"052D118327C0060DA24B5BE24C7D91F6","sqfilesize":44985457,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"2E5F668B93C8D3EA9330379FCD8DB25E","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"14939069","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"8067ED37A4484DDC5771F66EEBB0E73C","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14772975,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"龙奔 - 为你伤心为你哭 (DJ伟伟版)","old_cpy":1,"album_audio_id":129726872,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663808},"cid":57546330,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"E1EAEBB82F1202DC0C26A8769A0AC91D","pay_block_tpl":1,"musicpack_advance":0,"display":4,"display_rate":1},"duration":369,"audio_id":49759499,"topic_url":"","brief":"","rp_publish":1,"filesize":5909151,"remark":"为你伤心为你哭","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/258hw8d7.html"},{"hash":"29FA195C872778BEC8C3F556C553F9A3","sqfilesize":42767248,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"0CC91F417AB357C84BF9935B0D013391","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1746876","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"D1D90EE07EC47FD0051FB89D730AF06B","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":14552500,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"张冬玲 - 人的命天注定 (DJ版)","old_cpy":1,"album_audio_id":104961633,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cid":31024258,"cpy_level":1,"cpy_attr0":0,"hash_multitrack":"82DD4A56320136EE3895EA93766B1143","pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"duration":364,"audio_id":23552165,"topic_url":"","brief":"","rp_publish":1,"filesize":5821044,"remark":"人的命天注定","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1qhowx80.html"},{"hash":"24C9EBA886B95BB13D45C67B8877CB45","sqfilesize":40415643,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"AEF60FE4723F3280DD208B38E567F5E7","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"1987936","mvhash":"","extname":"mp3","topic_url_sq":"","320hash":"B411A92C75B57421C2E83E4DD2865EE7","price_sq":200,"inlist":1,"m4afilesize":0,"pay_type_320":3,"320filesize":12924342,"pkg_price_320":1,"privilege":8,"feetype":0,"price":200,"filename":"白小白 - 最美情侣 (DJ版)","old_cpy":0,"album_audio_id":112830420,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0},"duration":323,"audio_id":26710829,"topic_url":"","brief":"","rp_publish":1,"filesize":5169232,"remark":"最美情侣","pkg_price":1,"320privilege":10,"price_320":200,"fail_process_sq":4,"song_url":"https://m.kugou.com/mixsong/1v6cic6b.html"}]
             */

            private int timestamp;
            private int total;
            private List<InfoBean> info;

            public int getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(int timestamp) {
                this.timestamp = timestamp;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<InfoBean> getInfo() {
                return info;
            }

            public void setInfo(List<InfoBean> info) {
                this.info = info;
            }

            public static class InfoBean {
                /**
                 * hash : 24C9EBA886B95BB13D45C67B8877CB45
                 * sqfilesize : 40415643
                 * sqprivilege : 10
                 * pay_type_sq : 3
                 * bitrate : 128
                 * pkg_price_sq : 1
                 * has_accompany : 1
                 * topic_url_320 :
                 * sqhash : AEF60FE4723F3280DD208B38E567F5E7
                 * fail_process : 4
                 * pay_type : 3
                 * rp_type : audio
                 * album_id : 1987936
                 * mvhash :
                 * extname : mp3
                 * topic_url_sq :
                 * 320hash : B411A92C75B57421C2E83E4DD2865EE7
                 * price_sq : 200
                 * inlist : 1
                 * m4afilesize : 0
                 * pay_type_320 : 3
                 * 320filesize : 12924342
                 * pkg_price_320 : 1
                 * privilege : 8
                 * feetype : 0
                 * price : 200
                 * filename : 白小白 - 最美情侣 (DJ版)
                 * old_cpy : 0
                 * album_audio_id : 112830421
                 * fail_process_320 : 4
                 * trans_param : {"cpy_grade":5,"classmap":{"attr0":100663304},"cpy_level":1,"cid":67097320,"pay_block_tpl":1,"cpy_attr0":0,"hash_multitrack":"7694D9B16840D41BBA3400074B8CBACA","appid_block":"3124","musicpack_advance":0,"display":0,"display_rate":0}
                 * duration : 323
                 * audio_id : 26710829
                 * topic_url :
                 * brief :
                 * rp_publish : 1
                 * filesize : 5169232
                 * remark : 最美情侣
                 * pkg_price : 1
                 * 320privilege : 10
                 * price_320 : 200
                 * fail_process_sq : 4
                 * song_url : https://m.kugou.com/mixsong/1v6cid83.html
                 */

                private String hash;
                private int sqfilesize;
                private int sqprivilege;
                private int pay_type_sq;
                private int bitrate;
                private int pkg_price_sq;
                private int has_accompany;
                private String topic_url_320;
                private String sqhash;
                private int fail_process;
                private int pay_type;
                private String rp_type;
                private String album_id;
                private String mvhash;
                private String extname;
                private String topic_url_sq;
                @JSONField(name = "320hash")
                private String _$320hash;
                private int price_sq;
                private int inlist;
                private int m4afilesize;
                private int pay_type_320;
                @JSONField(name = "320filesize")
                private int _$320filesize;
                private int pkg_price_320;
                private int privilege;
                private int feetype;
                private int price;
                private String filename;
                private int old_cpy;
                private int album_audio_id;
                private int fail_process_320;
                private TransParamBean trans_param;
                private int duration;
                private int audio_id;
                private String topic_url;
                private String brief;
                private int rp_publish;
                private int filesize;
                private String remark;
                private int pkg_price;
                @JSONField(name = "320privilege")
                private int _$320privilege;
                private int price_320;
                private int fail_process_sq;
                private String song_url;

                public String getHash() {
                    return hash;
                }

                public void setHash(String hash) {
                    this.hash = hash;
                }

                public int getSqfilesize() {
                    return sqfilesize;
                }

                public void setSqfilesize(int sqfilesize) {
                    this.sqfilesize = sqfilesize;
                }

                public int getSqprivilege() {
                    return sqprivilege;
                }

                public void setSqprivilege(int sqprivilege) {
                    this.sqprivilege = sqprivilege;
                }

                public int getPay_type_sq() {
                    return pay_type_sq;
                }

                public void setPay_type_sq(int pay_type_sq) {
                    this.pay_type_sq = pay_type_sq;
                }

                public int getBitrate() {
                    return bitrate;
                }

                public void setBitrate(int bitrate) {
                    this.bitrate = bitrate;
                }

                public int getPkg_price_sq() {
                    return pkg_price_sq;
                }

                public void setPkg_price_sq(int pkg_price_sq) {
                    this.pkg_price_sq = pkg_price_sq;
                }

                public int getHas_accompany() {
                    return has_accompany;
                }

                public void setHas_accompany(int has_accompany) {
                    this.has_accompany = has_accompany;
                }

                public String getTopic_url_320() {
                    return topic_url_320;
                }

                public void setTopic_url_320(String topic_url_320) {
                    this.topic_url_320 = topic_url_320;
                }

                public String getSqhash() {
                    return sqhash;
                }

                public void setSqhash(String sqhash) {
                    this.sqhash = sqhash;
                }

                public int getFail_process() {
                    return fail_process;
                }

                public void setFail_process(int fail_process) {
                    this.fail_process = fail_process;
                }

                public int getPay_type() {
                    return pay_type;
                }

                public void setPay_type(int pay_type) {
                    this.pay_type = pay_type;
                }

                public String getRp_type() {
                    return rp_type;
                }

                public void setRp_type(String rp_type) {
                    this.rp_type = rp_type;
                }

                public String getAlbum_id() {
                    return album_id;
                }

                public void setAlbum_id(String album_id) {
                    this.album_id = album_id;
                }

                public String getMvhash() {
                    return mvhash;
                }

                public void setMvhash(String mvhash) {
                    this.mvhash = mvhash;
                }

                public String getExtname() {
                    return extname;
                }

                public void setExtname(String extname) {
                    this.extname = extname;
                }

                public String getTopic_url_sq() {
                    return topic_url_sq;
                }

                public void setTopic_url_sq(String topic_url_sq) {
                    this.topic_url_sq = topic_url_sq;
                }

                public String get_$320hash() {
                    return _$320hash;
                }

                public void set_$320hash(String _$320hash) {
                    this._$320hash = _$320hash;
                }

                public int getPrice_sq() {
                    return price_sq;
                }

                public void setPrice_sq(int price_sq) {
                    this.price_sq = price_sq;
                }

                public int getInlist() {
                    return inlist;
                }

                public void setInlist(int inlist) {
                    this.inlist = inlist;
                }

                public int getM4afilesize() {
                    return m4afilesize;
                }

                public void setM4afilesize(int m4afilesize) {
                    this.m4afilesize = m4afilesize;
                }

                public int getPay_type_320() {
                    return pay_type_320;
                }

                public void setPay_type_320(int pay_type_320) {
                    this.pay_type_320 = pay_type_320;
                }

                public int get_$320filesize() {
                    return _$320filesize;
                }

                public void set_$320filesize(int _$320filesize) {
                    this._$320filesize = _$320filesize;
                }

                public int getPkg_price_320() {
                    return pkg_price_320;
                }

                public void setPkg_price_320(int pkg_price_320) {
                    this.pkg_price_320 = pkg_price_320;
                }

                public int getPrivilege() {
                    return privilege;
                }

                public void setPrivilege(int privilege) {
                    this.privilege = privilege;
                }

                public int getFeetype() {
                    return feetype;
                }

                public void setFeetype(int feetype) {
                    this.feetype = feetype;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public String getFilename() {
                    return filename;
                }

                public void setFilename(String filename) {
                    this.filename = filename;
                }

                public int getOld_cpy() {
                    return old_cpy;
                }

                public void setOld_cpy(int old_cpy) {
                    this.old_cpy = old_cpy;
                }

                public int getAlbum_audio_id() {
                    return album_audio_id;
                }

                public void setAlbum_audio_id(int album_audio_id) {
                    this.album_audio_id = album_audio_id;
                }

                public int getFail_process_320() {
                    return fail_process_320;
                }

                public void setFail_process_320(int fail_process_320) {
                    this.fail_process_320 = fail_process_320;
                }

                public TransParamBean getTrans_param() {
                    return trans_param;
                }

                public void setTrans_param(TransParamBean trans_param) {
                    this.trans_param = trans_param;
                }

                public int getDuration() {
                    return duration;
                }

                public void setDuration(int duration) {
                    this.duration = duration;
                }

                public int getAudio_id() {
                    return audio_id;
                }

                public void setAudio_id(int audio_id) {
                    this.audio_id = audio_id;
                }

                public String getTopic_url() {
                    return topic_url;
                }

                public void setTopic_url(String topic_url) {
                    this.topic_url = topic_url;
                }

                public String getBrief() {
                    return brief;
                }

                public void setBrief(String brief) {
                    this.brief = brief;
                }

                public int getRp_publish() {
                    return rp_publish;
                }

                public void setRp_publish(int rp_publish) {
                    this.rp_publish = rp_publish;
                }

                public int getFilesize() {
                    return filesize;
                }

                public void setFilesize(int filesize) {
                    this.filesize = filesize;
                }

                public String getRemark() {
                    return remark;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }

                public int getPkg_price() {
                    return pkg_price;
                }

                public void setPkg_price(int pkg_price) {
                    this.pkg_price = pkg_price;
                }

                public int get_$320privilege() {
                    return _$320privilege;
                }

                public void set_$320privilege(int _$320privilege) {
                    this._$320privilege = _$320privilege;
                }

                public int getPrice_320() {
                    return price_320;
                }

                public void setPrice_320(int price_320) {
                    this.price_320 = price_320;
                }

                public int getFail_process_sq() {
                    return fail_process_sq;
                }

                public void setFail_process_sq(int fail_process_sq) {
                    this.fail_process_sq = fail_process_sq;
                }

                public String getSong_url() {
                    return song_url;
                }

                public void setSong_url(String song_url) {
                    this.song_url = song_url;
                }

                public static class TransParamBean {
                    /**
                     * cpy_grade : 5
                     * classmap : {"attr0":100663304}
                     * cpy_level : 1
                     * cid : 67097320
                     * pay_block_tpl : 1
                     * cpy_attr0 : 0
                     * hash_multitrack : 7694D9B16840D41BBA3400074B8CBACA
                     * appid_block : 3124
                     * musicpack_advance : 0
                     * display : 0
                     * display_rate : 0
                     */

                    private int cpy_grade;
                    private ClassmapBean classmap;
                    private int cpy_level;
                    private int cid;
                    private int pay_block_tpl;
                    private int cpy_attr0;
                    private String hash_multitrack;
                    private String appid_block;
                    private int musicpack_advance;
                    private int display;
                    private int display_rate;

                    public int getCpy_grade() {
                        return cpy_grade;
                    }

                    public void setCpy_grade(int cpy_grade) {
                        this.cpy_grade = cpy_grade;
                    }

                    public ClassmapBean getClassmap() {
                        return classmap;
                    }

                    public void setClassmap(ClassmapBean classmap) {
                        this.classmap = classmap;
                    }

                    public int getCpy_level() {
                        return cpy_level;
                    }

                    public void setCpy_level(int cpy_level) {
                        this.cpy_level = cpy_level;
                    }

                    public int getCid() {
                        return cid;
                    }

                    public void setCid(int cid) {
                        this.cid = cid;
                    }

                    public int getPay_block_tpl() {
                        return pay_block_tpl;
                    }

                    public void setPay_block_tpl(int pay_block_tpl) {
                        this.pay_block_tpl = pay_block_tpl;
                    }

                    public int getCpy_attr0() {
                        return cpy_attr0;
                    }

                    public void setCpy_attr0(int cpy_attr0) {
                        this.cpy_attr0 = cpy_attr0;
                    }

                    public String getHash_multitrack() {
                        return hash_multitrack;
                    }

                    public void setHash_multitrack(String hash_multitrack) {
                        this.hash_multitrack = hash_multitrack;
                    }

                    public String getAppid_block() {
                        return appid_block;
                    }

                    public void setAppid_block(String appid_block) {
                        this.appid_block = appid_block;
                    }

                    public int getMusicpack_advance() {
                        return musicpack_advance;
                    }

                    public void setMusicpack_advance(int musicpack_advance) {
                        this.musicpack_advance = musicpack_advance;
                    }

                    public int getDisplay() {
                        return display;
                    }

                    public void setDisplay(int display) {
                        this.display = display;
                    }

                    public int getDisplay_rate() {
                        return display_rate;
                    }

                    public void setDisplay_rate(int display_rate) {
                        this.display_rate = display_rate;
                    }

                    public static class ClassmapBean {
                        /**
                         * attr0 : 100663304
                         */

                        private int attr0;

                        public int getAttr0() {
                            return attr0;
                        }

                        public void setAttr0(int attr0) {
                            this.attr0 = attr0;
                        }
                    }
                }
            }
        }
    }

    public static class InfoBeanX {
        /**
         * list : {"specialid":2452799,"playcount":56207042,"songcount":167,"publishtime":"2020-04-30 00:00:00","user_type":0,"ugc_talent_review":1,"slid":297,"verified":0,"nickname":"☆冰雨欣☆🎸","commentcount":63,"singername":"","collectcount":50562,"trans_param":{"special_tag":0},"user_avatar":"http://imge.kugou.com/kugouicon/165/20170630/20170630115107548763.jpg","specialname":"车载劲爆DJ：提神醒脑 一路精神抖擞","tags":[{"tagid":17,"tagname":"DJ热碟"},{"tagid":63,"tagname":"车载"},{"tagid":577,"tagname":"轻松"}],"percount":0,"type":0,"plist":[],"imgurl":"http://c1.kgimg.com/custom/{size}/20200430/20200430102853651490.jpg","global_specialid":"collection_3_577614559_297_0","is_selected":0,"intro":"DJ音乐是旅途中的好伴侣，让你一路开车不再有倦意。","suid":577614559}
         */

        private ListBeanXX list;

        public ListBeanXX getList() {
            return list;
        }

        public void setList(ListBeanXX list) {
            this.list = list;
        }

        public static class ListBeanXX {
            /**
             * specialid : 2452799
             * playcount : 56207042
             * songcount : 167
             * publishtime : 2020-04-30 00:00:00
             * user_type : 0
             * ugc_talent_review : 1
             * slid : 297
             * verified : 0
             * nickname : ☆冰雨欣☆🎸
             * commentcount : 63
             * singername :
             * collectcount : 50562
             * trans_param : {"special_tag":0}
             * user_avatar : http://imge.kugou.com/kugouicon/165/20170630/20170630115107548763.jpg
             * specialname : 车载劲爆DJ：提神醒脑 一路精神抖擞
             * tags : [{"tagid":17,"tagname":"DJ热碟"},{"tagid":63,"tagname":"车载"},{"tagid":577,"tagname":"轻松"}]
             * percount : 0
             * type : 0
             * plist : []
             * imgurl : http://c1.kgimg.com/custom/{size}/20200430/20200430102853651490.jpg
             * global_specialid : collection_3_577614559_297_0
             * is_selected : 0
             * intro : DJ音乐是旅途中的好伴侣，让你一路开车不再有倦意。
             * suid : 577614559
             */

            private int specialid;
            private int playcount;
            private int songcount;
            private String publishtime;
            private int user_type;
            private int ugc_talent_review;
            private int slid;
            private int verified;
            private String nickname;
            private int commentcount;
            private String singername;
            private int collectcount;
            private TransParamBeanX trans_param;
            private String user_avatar;
            private String specialname;
            private int percount;
            private int type;
            private String imgurl;
            private String global_specialid;
            private int is_selected;
            private String intro;
            private int suid;
            private List<TagsBean> tags;
            private List<?> plist;

            public int getSpecialid() {
                return specialid;
            }

            public void setSpecialid(int specialid) {
                this.specialid = specialid;
            }

            public int getPlaycount() {
                return playcount;
            }

            public void setPlaycount(int playcount) {
                this.playcount = playcount;
            }

            public int getSongcount() {
                return songcount;
            }

            public void setSongcount(int songcount) {
                this.songcount = songcount;
            }

            public String getPublishtime() {
                return publishtime;
            }

            public void setPublishtime(String publishtime) {
                this.publishtime = publishtime;
            }

            public int getUser_type() {
                return user_type;
            }

            public void setUser_type(int user_type) {
                this.user_type = user_type;
            }

            public int getUgc_talent_review() {
                return ugc_talent_review;
            }

            public void setUgc_talent_review(int ugc_talent_review) {
                this.ugc_talent_review = ugc_talent_review;
            }

            public int getSlid() {
                return slid;
            }

            public void setSlid(int slid) {
                this.slid = slid;
            }

            public int getVerified() {
                return verified;
            }

            public void setVerified(int verified) {
                this.verified = verified;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public int getCommentcount() {
                return commentcount;
            }

            public void setCommentcount(int commentcount) {
                this.commentcount = commentcount;
            }

            public String getSingername() {
                return singername;
            }

            public void setSingername(String singername) {
                this.singername = singername;
            }

            public int getCollectcount() {
                return collectcount;
            }

            public void setCollectcount(int collectcount) {
                this.collectcount = collectcount;
            }

            public TransParamBeanX getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBeanX trans_param) {
                this.trans_param = trans_param;
            }

            public String getUser_avatar() {
                return user_avatar;
            }

            public void setUser_avatar(String user_avatar) {
                this.user_avatar = user_avatar;
            }

            public String getSpecialname() {
                return specialname;
            }

            public void setSpecialname(String specialname) {
                this.specialname = specialname;
            }

            public int getPercount() {
                return percount;
            }

            public void setPercount(int percount) {
                this.percount = percount;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getImgurl() {
                return imgurl;
            }

            public void setImgurl(String imgurl) {
                this.imgurl = imgurl;
            }

            public String getGlobal_specialid() {
                return global_specialid;
            }

            public void setGlobal_specialid(String global_specialid) {
                this.global_specialid = global_specialid;
            }

            public int getIs_selected() {
                return is_selected;
            }

            public void setIs_selected(int is_selected) {
                this.is_selected = is_selected;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public int getSuid() {
                return suid;
            }

            public void setSuid(int suid) {
                this.suid = suid;
            }

            public List<TagsBean> getTags() {
                return tags;
            }

            public void setTags(List<TagsBean> tags) {
                this.tags = tags;
            }

            public List<?> getPlist() {
                return plist;
            }

            public void setPlist(List<?> plist) {
                this.plist = plist;
            }

            public static class TransParamBeanX {
                /**
                 * special_tag : 0
                 */

                private int special_tag;

                public int getSpecial_tag() {
                    return special_tag;
                }

                public void setSpecial_tag(int special_tag) {
                    this.special_tag = special_tag;
                }
            }

            public static class TagsBean {
                /**
                 * tagid : 17
                 * tagname : DJ热碟
                 */

                private int tagid;
                private String tagname;

                public int getTagid() {
                    return tagid;
                }

                public void setTagid(int tagid) {
                    this.tagid = tagid;
                }

                public String getTagname() {
                    return tagname;
                }

                public void setTagname(String tagname) {
                    this.tagname = tagname;
                }
            }
        }
    }
}
