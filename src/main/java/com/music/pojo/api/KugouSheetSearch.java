package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

public class KugouSheetSearch implements Serializable {
    private static final long serialVersionUID = 400249678151911552L;

    /**
     * status : 1
     * error :
     * data : {"timestamp":1650121963,"info":[{"specialname":"燃脂电音：好曲只为俘获你的双耳","nickname":"安稳余生","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":40133009,"singername":"","verified":0,"contain":"OMFG - Hello (你好<\/em>)","songcount":145,"imgurl":"http://c1.kgimg.com/custom/150/20210301/20210301192824487734.jpg","gid":"collection_3_622553891_359_0","iscustom":0,"isperiodical":0,"specialid":3586976,"suid":622553891,"publishtime":"2021-02-21 15:27:54"},{"specialname":"【高潮片段】又不是没你不行 只是有你更好.","nickname":"嘤嘤子","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":145624100,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":203,"imgurl":"http://c1.kgimg.com/custom/150/20210815/20210815112737233571.jpg","gid":"collection_3_1352732629_104_0","iscustom":0,"isperiodical":0,"specialid":4096048,"suid":1352732629,"publishtime":"2021-08-11 12:01:19"},{"specialname":"温柔片段\u2016你逆光而来 配得上这世间所有的美好","nickname":"ccc","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":18940997,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":311,"imgurl":"http://c1.kgimg.com/v2/custom/8fff3e6c2c258c223851d1d681db5f8f.png","gid":"collection_3_1860203666_270_0","iscustom":0,"isperiodical":0,"specialid":4661827,"suid":1860203666,"publishtime":"2022-01-13 13:16:48"},{"specialname":"轻快的纯音乐带给你好<\/em>心情","nickname":"清晖轻乐","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":194302,"singername":"","verified":0,"contain":"","songcount":51,"imgurl":"http://c1.kgimg.com/custom/150/20200214/20200214161412156916.jpg","gid":"collection_3_638705104_46_0","iscustom":0,"isperiodical":0,"specialid":2069674,"suid":638705104,"publishtime":"2020-02-14 16:14:41"},{"specialname":"《我很好我从来没想过你好<\/em>吧》","nickname":"爱笑的人","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":1203410,"singername":"","verified":0,"contain":"","songcount":128,"imgurl":"http://c1.kgimg.com/custom/150/20220213/20220213181449682383.jpg","gid":"collection_3_1674745225_7_0","iscustom":0,"isperiodical":0,"specialid":4842630,"suid":1674745225,"publishtime":"2022-02-13 18:15:43"},{"specialname":"催泪伤感：我好像只是偶尔被你需要","nickname":"顾子霖","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":28860073,"singername":"","verified":0,"contain":"Eric周兴哲 - 你,好不好?","songcount":71,"imgurl":"http://c1.kgimg.com/custom/150/20210221/20210221222410520116.jpg","gid":"collection_3_1518000243_592_0","iscustom":0,"isperiodical":0,"specialid":3588448,"suid":1518000243,"publishtime":"2021-02-21 22:49:04"},{"specialname":"嘻嘻\u2026我知道\u2026你\u2026很爱我\u2026但\u2026你去好好爱","nickname":"神明可信吗囍","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":12246350,"singername":"","verified":0,"contain":"姜鹏Calm - 再见，你好<\/em> (Live片段)","songcount":377,"imgurl":"http://c1.kgimg.com/custom/150/20211015/20211015183806977887.jpg","gid":"collection_3_1804999317_38_0","iscustom":0,"isperiodical":0,"specialid":4313516,"suid":1804999317,"publishtime":"2021-10-15 18:42:18"},{"specialname":"三无Marblue合集~喜欢的宝速速抱走","nickname":"厌战","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":133891,"singername":"","verified":0,"contain":"三无Marblue - 你好<\/em>","songcount":277,"imgurl":"http://c1.kgimg.com/custom/150/20211108/20211108133633932706.jpg","gid":"collection_3_1346553610_162_0","iscustom":0,"isperiodical":0,"specialid":4389605,"suid":1346553610,"publishtime":"2021-11-08 13:38:07"},{"specialname":"好听的DJ舞曲戴上耳机整个世界都是你的","nickname":"风停了","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":65852,"singername":"","verified":0,"contain":"阳山伟伟 - Hello friend (你好<\/em>朋友)(电音版)","songcount":303,"imgurl":"http://c1.kgimg.com/custom/150/20210208/20210208214358538337.jpg","gid":"collection_3_1162273119_8_0","iscustom":0,"isperiodical":0,"specialid":3546957,"suid":1162273119,"publishtime":"2021-02-08 21:44:41"},{"specialname":"Muse Dash（喵斯快跑）","nickname":"巅峰少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":34800,"singername":"","verified":0,"contain":"初音ミク - 你好<\/em>","songcount":210,"imgurl":"http://c1.kgimg.com/custom/150/20211127/20211127093853851042.jpg","gid":"collection_3_1398923985_93_0","iscustom":0,"isperiodical":0,"specialid":4450510,"suid":1398923985,"publishtime":"2021-11-27 00:00:01"},{"specialname":"别回头，离开你的都不是什么好人.","nickname":"原耽永远的神","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":1950812,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":692,"imgurl":"http://c1.kgimg.com/custom/150/20220201/20220201150646625763.jpg","gid":"collection_3_1634871791_1545_0","iscustom":0,"isperiodical":0,"specialid":4775436,"suid":1634871791,"publishtime":"2022-02-01 15:29:18"},{"specialname":"最火儿歌大全：宝宝听了会乖乖","nickname":"歌单美少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":6313788,"singername":"","verified":0,"contain":"贝瓦儿歌 - 你好<\/em>","songcount":133,"imgurl":"http://c1.kgimg.com/custom/150/20201016/20201016233823582809.jpg","gid":"collection_3_1401373129_107_0","iscustom":0,"isperiodical":0,"specialid":3177569,"suid":1401373129,"publishtime":"2020-10-16 00:00:01"},{"specialname":"喵斯快跑（Muse Dash）基础包","nickname":"斑驳","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":93774,"singername":"","verified":0,"contain":"初音ミク - 你好<\/em>","songcount":37,"imgurl":"http://c1.kgimg.com/custom/150/20200303/20200303200540856352.jpg","gid":"collection_3_1572190732_59_0","iscustom":0,"isperiodical":0,"specialid":2287948,"suid":1572190732,"publishtime":"2020-03-30 18:18:15"},{"specialname":"你好<\/em>DJ，来个串烧套餐好伐？","nickname":"圣","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":11379505,"singername":"","verified":0,"contain":"","songcount":19,"imgurl":"http://imge.kugou.com/soft/collection/{size}/20200928/20200928112850220178.jpg","gid":"collection_3_509005055_36_0","iscustom":0,"isperiodical":0,"specialid":3117485,"suid":509005055,"publishtime":"2020-09-28 11:30:14"},{"specialname":"牛奶咖啡：离开，只是想说明天你好<\/em>","nickname":"花少識","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":44450,"singername":"","verified":0,"contain":"","songcount":38,"imgurl":"http://c1.kgimg.com/custom/150/20220102/20220102185842770923.jpg","gid":"collection_3_1189577316_241_0","iscustom":0,"isperiodical":0,"specialid":4611877,"suid":1189577316,"publishtime":"2022-01-02 19:22:09"},{"specialname":"催泪伤感：我好像只是偶尔的被你需要","nickname":"兔叽我吃惹","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":4514368,"singername":"","verified":0,"contain":"Eric周兴哲 - 你,好不好?","songcount":70,"imgurl":"http://c1.kgimg.com/custom/150/20211109/20211109193319997232.jpg","gid":"collection_3_1745966958_539_0","iscustom":0,"isperiodical":0,"specialid":4393369,"suid":1745966958,"publishtime":"2021-11-09 19:47:07"},{"specialname":"那些惹你看哭电视剧的超好听主题曲","nickname":"冯海毓","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":6406705,"singername":"","verified":0,"contain":"周深 - 说声你好<\/em>","songcount":250,"imgurl":"http://c1.kgimg.com/custom/150/20211224/20211224152954836324.jpg","gid":"collection_3_731474874_783_0","iscustom":0,"isperiodical":0,"specialid":4568288,"suid":731474874,"publishtime":"2021-12-24 00:00:01"},{"specialname":"振华三部曲（你好<\/em>旧时光/最好的我们/暗恋橘生淮南）原声合辑","nickname":"影视小编-栗子","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":702117,"singername":"","verified":0,"contain":"","songcount":20,"imgurl":"http://imge.kugou.com/soft/collection/{size}/20190605/20190605141806996870.jpg","gid":"collection_1_1081536272_709578_0","iscustom":0,"isperiodical":0,"specialid":709578,"suid":1081536272,"publishtime":"2019-06-05 14:18:09"},{"specialname":"全网最好听的咚鼓版歌曲，咚进你心❤","nickname":"歌单美少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":16280260,"singername":"","verified":0,"contain":"德芙 - 请先说你好<\/em> (咕咚版)","songcount":118,"imgurl":"http://c1.kgimg.com/custom/150/20210326/20210326105333645325.jpg","gid":"collection_3_1401373129_295_0","iscustom":0,"isperiodical":0,"specialid":3679866,"suid":1401373129,"publishtime":"2021-03-26 00:00:01"},{"specialname":"我喜欢你好<\/em>明显，你的不喜欢也是","nickname":"林晚","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":14867090,"singername":"","verified":0,"contain":"","songcount":43,"imgurl":"http://c1.kgimg.com/custom/150/20210411/20210411123134860922.jpg","gid":"collection_3_1673099072_183_0","iscustom":0,"isperiodical":0,"specialid":3725212,"suid":1673099072,"publishtime":"2021-04-11 12:34:40"}],"total":1000}
     * errcode : 0
     */

    private int status;
    private String error;
    private DataBean data;
    private int errcode;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public static class DataBean {
        /**
         * timestamp : 1650121963
         * info : [{"specialname":"燃脂电音：好曲只为俘获你的双耳","nickname":"安稳余生","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":40133009,"singername":"","verified":0,"contain":"OMFG - Hello (你好<\/em>)","songcount":145,"imgurl":"http://c1.kgimg.com/custom/150/20210301/20210301192824487734.jpg","gid":"collection_3_622553891_359_0","iscustom":0,"isperiodical":0,"specialid":3586976,"suid":622553891,"publishtime":"2021-02-21 15:27:54"},{"specialname":"【高潮片段】又不是没你不行 只是有你更好.","nickname":"嘤嘤子","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":145624100,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":203,"imgurl":"http://c1.kgimg.com/custom/150/20210815/20210815112737233571.jpg","gid":"collection_3_1352732629_104_0","iscustom":0,"isperiodical":0,"specialid":4096048,"suid":1352732629,"publishtime":"2021-08-11 12:01:19"},{"specialname":"温柔片段\u2016你逆光而来 配得上这世间所有的美好","nickname":"ccc","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":18940997,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":311,"imgurl":"http://c1.kgimg.com/v2/custom/8fff3e6c2c258c223851d1d681db5f8f.png","gid":"collection_3_1860203666_270_0","iscustom":0,"isperiodical":0,"specialid":4661827,"suid":1860203666,"publishtime":"2022-01-13 13:16:48"},{"specialname":"轻快的纯音乐带给你好<\/em>心情","nickname":"清晖轻乐","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":194302,"singername":"","verified":0,"contain":"","songcount":51,"imgurl":"http://c1.kgimg.com/custom/150/20200214/20200214161412156916.jpg","gid":"collection_3_638705104_46_0","iscustom":0,"isperiodical":0,"specialid":2069674,"suid":638705104,"publishtime":"2020-02-14 16:14:41"},{"specialname":"《我很好我从来没想过你好<\/em>吧》","nickname":"爱笑的人","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":1203410,"singername":"","verified":0,"contain":"","songcount":128,"imgurl":"http://c1.kgimg.com/custom/150/20220213/20220213181449682383.jpg","gid":"collection_3_1674745225_7_0","iscustom":0,"isperiodical":0,"specialid":4842630,"suid":1674745225,"publishtime":"2022-02-13 18:15:43"},{"specialname":"催泪伤感：我好像只是偶尔被你需要","nickname":"顾子霖","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":28860073,"singername":"","verified":0,"contain":"Eric周兴哲 - 你,好不好?","songcount":71,"imgurl":"http://c1.kgimg.com/custom/150/20210221/20210221222410520116.jpg","gid":"collection_3_1518000243_592_0","iscustom":0,"isperiodical":0,"specialid":3588448,"suid":1518000243,"publishtime":"2021-02-21 22:49:04"},{"specialname":"嘻嘻\u2026我知道\u2026你\u2026很爱我\u2026但\u2026你去好好爱","nickname":"神明可信吗囍","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":12246350,"singername":"","verified":0,"contain":"姜鹏Calm - 再见，你好<\/em> (Live片段)","songcount":377,"imgurl":"http://c1.kgimg.com/custom/150/20211015/20211015183806977887.jpg","gid":"collection_3_1804999317_38_0","iscustom":0,"isperiodical":0,"specialid":4313516,"suid":1804999317,"publishtime":"2021-10-15 18:42:18"},{"specialname":"三无Marblue合集~喜欢的宝速速抱走","nickname":"厌战","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":133891,"singername":"","verified":0,"contain":"三无Marblue - 你好<\/em>","songcount":277,"imgurl":"http://c1.kgimg.com/custom/150/20211108/20211108133633932706.jpg","gid":"collection_3_1346553610_162_0","iscustom":0,"isperiodical":0,"specialid":4389605,"suid":1346553610,"publishtime":"2021-11-08 13:38:07"},{"specialname":"好听的DJ舞曲戴上耳机整个世界都是你的","nickname":"风停了","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":65852,"singername":"","verified":0,"contain":"阳山伟伟 - Hello friend (你好<\/em>朋友)(电音版)","songcount":303,"imgurl":"http://c1.kgimg.com/custom/150/20210208/20210208214358538337.jpg","gid":"collection_3_1162273119_8_0","iscustom":0,"isperiodical":0,"specialid":3546957,"suid":1162273119,"publishtime":"2021-02-08 21:44:41"},{"specialname":"Muse Dash（喵斯快跑）","nickname":"巅峰少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":34800,"singername":"","verified":0,"contain":"初音ミク - 你好<\/em>","songcount":210,"imgurl":"http://c1.kgimg.com/custom/150/20211127/20211127093853851042.jpg","gid":"collection_3_1398923985_93_0","iscustom":0,"isperiodical":0,"specialid":4450510,"suid":1398923985,"publishtime":"2021-11-27 00:00:01"},{"specialname":"别回头，离开你的都不是什么好人.","nickname":"原耽永远的神","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":1950812,"singername":"","verified":0,"contain":"王贰浪 - 再见你好<\/em> (片段)","songcount":692,"imgurl":"http://c1.kgimg.com/custom/150/20220201/20220201150646625763.jpg","gid":"collection_3_1634871791_1545_0","iscustom":0,"isperiodical":0,"specialid":4775436,"suid":1634871791,"publishtime":"2022-02-01 15:29:18"},{"specialname":"最火儿歌大全：宝宝听了会乖乖","nickname":"歌单美少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":6313788,"singername":"","verified":0,"contain":"贝瓦儿歌 - 你好<\/em>","songcount":133,"imgurl":"http://c1.kgimg.com/custom/150/20201016/20201016233823582809.jpg","gid":"collection_3_1401373129_107_0","iscustom":0,"isperiodical":0,"specialid":3177569,"suid":1401373129,"publishtime":"2020-10-16 00:00:01"},{"specialname":"喵斯快跑（Muse Dash）基础包","nickname":"斑驳","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":93774,"singername":"","verified":0,"contain":"初音ミク - 你好<\/em>","songcount":37,"imgurl":"http://c1.kgimg.com/custom/150/20200303/20200303200540856352.jpg","gid":"collection_3_1572190732_59_0","iscustom":0,"isperiodical":0,"specialid":2287948,"suid":1572190732,"publishtime":"2020-03-30 18:18:15"},{"specialname":"你好<\/em>DJ，来个串烧套餐好伐？","nickname":"圣","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":11379505,"singername":"","verified":0,"contain":"","songcount":19,"imgurl":"http://imge.kugou.com/soft/collection/{size}/20200928/20200928112850220178.jpg","gid":"collection_3_509005055_36_0","iscustom":0,"isperiodical":0,"specialid":3117485,"suid":509005055,"publishtime":"2020-09-28 11:30:14"},{"specialname":"牛奶咖啡：离开，只是想说明天你好<\/em>","nickname":"花少識","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":44450,"singername":"","verified":0,"contain":"","songcount":38,"imgurl":"http://c1.kgimg.com/custom/150/20220102/20220102185842770923.jpg","gid":"collection_3_1189577316_241_0","iscustom":0,"isperiodical":0,"specialid":4611877,"suid":1189577316,"publishtime":"2022-01-02 19:22:09"},{"specialname":"催泪伤感：我好像只是偶尔的被你需要","nickname":"兔叽我吃惹","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":4514368,"singername":"","verified":0,"contain":"Eric周兴哲 - 你,好不好?","songcount":70,"imgurl":"http://c1.kgimg.com/custom/150/20211109/20211109193319997232.jpg","gid":"collection_3_1745966958_539_0","iscustom":0,"isperiodical":0,"specialid":4393369,"suid":1745966958,"publishtime":"2021-11-09 19:47:07"},{"specialname":"那些惹你看哭电视剧的超好听主题曲","nickname":"冯海毓","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":6406705,"singername":"","verified":0,"contain":"周深 - 说声你好<\/em>","songcount":250,"imgurl":"http://c1.kgimg.com/custom/150/20211224/20211224152954836324.jpg","gid":"collection_3_731474874_783_0","iscustom":0,"isperiodical":0,"specialid":4568288,"suid":731474874,"publishtime":"2021-12-24 00:00:01"},{"specialname":"振华三部曲（你好<\/em>旧时光/最好的我们/暗恋橘生淮南）原声合辑","nickname":"影视小编-栗子","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":702117,"singername":"","verified":0,"contain":"","songcount":20,"imgurl":"http://imge.kugou.com/soft/collection/{size}/20190605/20190605141806996870.jpg","gid":"collection_1_1081536272_709578_0","iscustom":0,"isperiodical":0,"specialid":709578,"suid":1081536272,"publishtime":"2019-06-05 14:18:09"},{"specialname":"全网最好听的咚鼓版歌曲，咚进你心❤","nickname":"歌单美少年","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":16280260,"singername":"","verified":0,"contain":"德芙 - 请先说你好<\/em> (咕咚版)","songcount":118,"imgurl":"http://c1.kgimg.com/custom/150/20210326/20210326105333645325.jpg","gid":"collection_3_1401373129_295_0","iscustom":0,"isperiodical":0,"specialid":3679866,"suid":1401373129,"publishtime":"2021-03-26 00:00:01"},{"specialname":"我喜欢你好<\/em>明显，你的不喜欢也是","nickname":"林晚","intro":"","collectcount":0,"slid":0,"trans_param":{"special_tag":0},"nper":0,"playcount":14867090,"singername":"","verified":0,"contain":"","songcount":43,"imgurl":"http://c1.kgimg.com/custom/150/20210411/20210411123134860922.jpg","gid":"collection_3_1673099072_183_0","iscustom":0,"isperiodical":0,"specialid":3725212,"suid":1673099072,"publishtime":"2021-04-11 12:34:40"}]
         * total : 1000
         */

        private int timestamp;
        private int total;
        private List<InfoBean> info;

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * specialname : 燃脂电音：好曲只为俘获你的双耳
             * nickname : 安稳余生
             * intro :
             * collectcount : 0
             * slid : 0
             * trans_param : {"special_tag":0}
             * nper : 0
             * playcount : 40133009
             * singername :
             * verified : 0
             * contain : OMFG - Hello (你好</em>)
             * songcount : 145
             * imgurl : http://c1.kgimg.com/custom/150/20210301/20210301192824487734.jpg
             * gid : collection_3_622553891_359_0
             * iscustom : 0
             * isperiodical : 0
             * specialid : 3586976
             * suid : 622553891
             * publishtime : 2021-02-21 15:27:54
             */

            private String specialname;
            private String nickname;
            private String intro;
            private int collectcount;
            private int slid;
            private TransParamBean trans_param;
            private int nper;
            private int playcount;
            private String singername;
            private int verified;
            private String contain;
            private int songcount;
            private String imgurl;
            private String gid;
            private int iscustom;
            private int isperiodical;
            private int specialid;
            private int suid;
            private String publishtime;

            public String getSpecialname() {
                return specialname;
            }

            public void setSpecialname(String specialname) {
                this.specialname = specialname;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public int getCollectcount() {
                return collectcount;
            }

            public void setCollectcount(int collectcount) {
                this.collectcount = collectcount;
            }

            public int getSlid() {
                return slid;
            }

            public void setSlid(int slid) {
                this.slid = slid;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public int getNper() {
                return nper;
            }

            public void setNper(int nper) {
                this.nper = nper;
            }

            public int getPlaycount() {
                return playcount;
            }

            public void setPlaycount(int playcount) {
                this.playcount = playcount;
            }

            public String getSingername() {
                return singername;
            }

            public void setSingername(String singername) {
                this.singername = singername;
            }

            public int getVerified() {
                return verified;
            }

            public void setVerified(int verified) {
                this.verified = verified;
            }

            public String getContain() {
                return contain;
            }

            public void setContain(String contain) {
                this.contain = contain;
            }

            public int getSongcount() {
                return songcount;
            }

            public void setSongcount(int songcount) {
                this.songcount = songcount;
            }

            public String getImgurl() {
                return imgurl;
            }

            public void setImgurl(String imgurl) {
                this.imgurl = imgurl;
            }

            public String getGid() {
                return gid;
            }

            public void setGid(String gid) {
                this.gid = gid;
            }

            public int getIscustom() {
                return iscustom;
            }

            public void setIscustom(int iscustom) {
                this.iscustom = iscustom;
            }

            public int getIsperiodical() {
                return isperiodical;
            }

            public void setIsperiodical(int isperiodical) {
                this.isperiodical = isperiodical;
            }

            public int getSpecialid() {
                return specialid;
            }

            public void setSpecialid(int specialid) {
                this.specialid = specialid;
            }

            public int getSuid() {
                return suid;
            }

            public void setSuid(int suid) {
                this.suid = suid;
            }

            public String getPublishtime() {
                return publishtime;
            }

            public void setPublishtime(String publishtime) {
                this.publishtime = publishtime;
            }

            public static class TransParamBean {
                /**
                 * special_tag : 0
                 */

                private int special_tag;

                public int getSpecial_tag() {
                    return special_tag;
                }

                public void setSpecial_tag(int special_tag) {
                    this.special_tag = special_tag;
                }
            }
        }
    }
}
