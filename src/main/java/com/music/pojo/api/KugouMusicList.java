package com.music.pojo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class KugouMusicList implements Serializable {
    private static final long serialVersionUID = -6657109824671636859L;

    private int JS_CSS_DATE;
    private String kg_domain;
    private String src;
    private Object fr;
    private String ver;
    private PlistBean plist;
    private int pagesize;
    private String __Tpl;

    public int getJS_CSS_DATE() {
        return JS_CSS_DATE;
    }

    public void setJS_CSS_DATE(int JS_CSS_DATE) {
        this.JS_CSS_DATE = JS_CSS_DATE;
    }

    public String getKg_domain() {
        return kg_domain;
    }

    public void setKg_domain(String kg_domain) {
        this.kg_domain = kg_domain;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Object getFr() {
        return fr;
    }

    public void setFr(Object fr) {
        this.fr = fr;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public PlistBean getPlist() {
        return plist;
    }

    public void setPlist(PlistBean plist) {
        this.plist = plist;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public String get__Tpl() {
        return __Tpl;
    }

    public void set__Tpl(String __Tpl) {
        this.__Tpl = __Tpl;
    }

    public static class PlistBean {


        private ListBean list;
        private int pagesize;

        public ListBean getList() {
            return list;
        }

        public void setList(ListBean list) {
            this.list = list;
        }

        public int getPagesize() {
            return pagesize;
        }

        public void setPagesize(int pagesize) {
            this.pagesize = pagesize;
        }

        public static class ListBean {

            private int timestamp;
            private int total;
            private int has_next;
            private List<InfoBean> info;

            public int getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(int timestamp) {
                this.timestamp = timestamp;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public int getHas_next() {
                return has_next;
            }

            public void setHas_next(int has_next) {
                this.has_next = has_next;
            }

            public List<InfoBean> getInfo() {
                return info;
            }

            public void setInfo(List<InfoBean> info) {
                this.info = info;
            }

            public static class InfoBean {
                /**
                 * specialid : 3995633
                 * playcount : 332141
                 * songcount : 20
                 * publishtime : 2021-07-16 00:00:00
                 * songs : [{"hash":"89C687A86B99C8EACC3730116C71736D","sqfilesize":0,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":0,"topic_url_320":"","sqhash":"","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"44475403","mvhash":"","duration":236,"topic_url_sq":"","320hash":"00679F80939A8111318CC74C2EEB709D","price_sq":0,"inlist":1,"m4afilesize":0,"old_cpy":0,"320filesize":9443831,"pkg_price_320":1,"price_320":200,"feetype":0,"price":200,"filename":"治愈--萌芽熊 - Healing Bear","extname":"mp3","pkg_price":1,"fail_process_320":4,"trans_param":{"cpy_grade":22,"classmap":{"attr0":8},"cid":148667285,"cpy_attr0":0,"display_rate":0,"pay_block_tpl":1,"musicpack_advance":0,"display":0,"cpy_level":1},"remark":"疗愈熊","filesize":3777559,"album_audio_id":312492081,"brief":"","rp_publish":1,"privilege":8,"topic_url":"","audio_id":106913503,"320privilege":10,"pay_type_320":3,"fail_process_sq":0},{"hash":"5B4600FE0251FA1729704A3F39C85E63","sqfilesize":18553043,"sqprivilege":10,"pay_type_sq":3,"bitrate":128,"pkg_price_sq":1,"has_accompany":1,"topic_url_320":"","sqhash":"32DCB68A8721C30651672C9EDE612E17","fail_process":4,"pay_type":3,"rp_type":"audio","album_id":"587762","mvhash":"F0ECC1353238346489C20A867BE12647","duration":257,"topic_url_sq":"","320hash":"AC1703DE4C2DB55BCC3812B04715A006","price_sq":200,"inlist":1,"m4afilesize":0,"old_cpy":0,"320filesize":10319658,"pkg_price_320":1,"price_320":200,"feetype":0,"price":200,"filename":"Piano Master - Bamboo","extname":"mp3","pkg_price":1,"fail_process_320":4,"trans_param":{"cpy_grade":5,"classmap":{"attr0":8},"cid":17810470,"cpy_attr0":0,"display_rate":0,"pay_block_tpl":1,"musicpack_advance":1,"display":0,"cpy_level":1},"remark":"竹","filesize":4128010,"album_audio_id":28508786,"brief":"","rp_publish":1,"privilege":10,"topic_url":"","audio_id":2880839,"320privilege":10,"pay_type_320":3,"fail_process_sq":4},{"hash":"9D0E9A4D11B496E102A062FD21AE6FCB","sqfilesize":20050405,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":0,"topic_url_320":"","sqhash":"1415EF213BC584425A7C6352A991A393","fail_process":0,"pay_type":0,"rp_type":"audio","album_id":"8833561","mvhash":"","duration":223,"topic_url_sq":"","320hash":"E3225BB604FD686358DE73C73A6364EA","price_sq":0,"inlist":1,"m4afilesize":0,"old_cpy":1,"320filesize":8943400,"pkg_price_320":0,"price_320":0,"feetype":0,"price":0,"filename":"Bernward Koch - Quiet Day","extname":"mp3","pkg_price":0,"fail_process_320":0,"trans_param":{"cid":-1,"cpy_attr0":0,"classmap":{"attr0":8},"pay_block_tpl":1,"musicpack_advance":0,"display":0,"display_rate":0},"remark":"安静的一天","filesize":3577566,"album_audio_id":110405142,"brief":"","rp_publish":1,"privilege":0,"topic_url":"","audio_id":39624189,"320privilege":0,"pay_type_320":0,"fail_process_sq":0}]
                 * suid : 1288019698
                 * url :
                 * type : 0
                 * slid : 716
                 * verified : 0
                 * global_specialid : collection_3_1288019698_716_0
                 * selected_reason :
                 * tags : []
                 * collectcount : 615
                 * trans_param : {"special_tag":0}
                 * user_type : 1
                 * username : 治愈--萌芽熊
                 * singername :
                 * percount : 0
                 * recommendfirst : 0
                 * ugc_talent_review : 1
                 * specialname : 森系轻音：感受自然の慰藉音符
                 * user_avatar : http://imge.kugou.com/kugouicon/165/20210509/20210509111419285852.jpg
                 * is_selected : 0
                 * intro : 森系轻音：感受自然の慰藉音符绿山环绕，自然の美······【轻音乐专区】
                 * imgurl : http://c1.kgimg.com/custom/{size}/20210716/20210716092250395036.jpg
                 */

                private int specialid;
                private int playcount;
                private int songcount;
                private String publishtime;
                private int suid;
                private String url;
                private int type;
                private int slid;
                private int verified;
                private String global_specialid;
                private String selected_reason;
                private int collectcount;
                private TransParamBean trans_param;
                private int user_type;
                private String username;
                private String singername;
                private int percount;
                private int recommendfirst;
                private int ugc_talent_review;
                private String specialname;
                private String user_avatar;
                private int is_selected;
                private String intro;
                private String imgurl;
                private List<SongsBean> songs;
                private List<?> tags;

                public int getSpecialid() {
                    return specialid;
                }

                public void setSpecialid(int specialid) {
                    this.specialid = specialid;
                }

                public int getPlaycount() {
                    return playcount;
                }

                public void setPlaycount(int playcount) {
                    this.playcount = playcount;
                }

                public int getSongcount() {
                    return songcount;
                }

                public void setSongcount(int songcount) {
                    this.songcount = songcount;
                }

                public String getPublishtime() {
                    return publishtime;
                }

                public void setPublishtime(String publishtime) {
                    this.publishtime = publishtime;
                }

                public int getSuid() {
                    return suid;
                }

                public void setSuid(int suid) {
                    this.suid = suid;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public int getSlid() {
                    return slid;
                }

                public void setSlid(int slid) {
                    this.slid = slid;
                }

                public int getVerified() {
                    return verified;
                }

                public void setVerified(int verified) {
                    this.verified = verified;
                }

                public String getGlobal_specialid() {
                    return global_specialid;
                }

                public void setGlobal_specialid(String global_specialid) {
                    this.global_specialid = global_specialid;
                }

                public String getSelected_reason() {
                    return selected_reason;
                }

                public void setSelected_reason(String selected_reason) {
                    this.selected_reason = selected_reason;
                }

                public int getCollectcount() {
                    return collectcount;
                }

                public void setCollectcount(int collectcount) {
                    this.collectcount = collectcount;
                }

                public TransParamBean getTrans_param() {
                    return trans_param;
                }

                public void setTrans_param(TransParamBean trans_param) {
                    this.trans_param = trans_param;
                }

                public int getUser_type() {
                    return user_type;
                }

                public void setUser_type(int user_type) {
                    this.user_type = user_type;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getSingername() {
                    return singername;
                }

                public void setSingername(String singername) {
                    this.singername = singername;
                }

                public int getPercount() {
                    return percount;
                }

                public void setPercount(int percount) {
                    this.percount = percount;
                }

                public int getRecommendfirst() {
                    return recommendfirst;
                }

                public void setRecommendfirst(int recommendfirst) {
                    this.recommendfirst = recommendfirst;
                }

                public int getUgc_talent_review() {
                    return ugc_talent_review;
                }

                public void setUgc_talent_review(int ugc_talent_review) {
                    this.ugc_talent_review = ugc_talent_review;
                }

                public String getSpecialname() {
                    return specialname;
                }

                public void setSpecialname(String specialname) {
                    this.specialname = specialname;
                }

                public String getUser_avatar() {
                    return user_avatar;
                }

                public void setUser_avatar(String user_avatar) {
                    this.user_avatar = user_avatar;
                }

                public int getIs_selected() {
                    return is_selected;
                }

                public void setIs_selected(int is_selected) {
                    this.is_selected = is_selected;
                }

                public String getIntro() {
                    return intro;
                }

                public void setIntro(String intro) {
                    this.intro = intro;
                }

                public String getImgurl() {
                    return imgurl;
                }

                public void setImgurl(String imgurl) {
                    this.imgurl = imgurl;
                }

                public List<SongsBean> getSongs() {
                    return songs;
                }

                public void setSongs(List<SongsBean> songs) {
                    this.songs = songs;
                }

                public List<?> getTags() {
                    return tags;
                }

                public void setTags(List<?> tags) {
                    this.tags = tags;
                }

                public static class TransParamBean {
                    /**
                     * special_tag : 0
                     */

                    private int special_tag;

                    public int getSpecial_tag() {
                        return special_tag;
                    }

                    public void setSpecial_tag(int special_tag) {
                        this.special_tag = special_tag;
                    }
                }

                public static class SongsBean {
                    /**
                     * hash : 89C687A86B99C8EACC3730116C71736D
                     * sqfilesize : 0
                     * sqprivilege : 0
                     * pay_type_sq : 0
                     * bitrate : 128
                     * pkg_price_sq : 0
                     * has_accompany : 0
                     * topic_url_320 :
                     * sqhash :
                     * fail_process : 4
                     * pay_type : 3
                     * rp_type : audio
                     * album_id : 44475403
                     * mvhash :
                     * duration : 236
                     * topic_url_sq :
                     * 320hash : 00679F80939A8111318CC74C2EEB709D
                     * price_sq : 0
                     * inlist : 1
                     * m4afilesize : 0
                     * old_cpy : 0
                     * 320filesize : 9443831
                     * pkg_price_320 : 1
                     * price_320 : 200
                     * feetype : 0
                     * price : 200
                     * filename : 治愈--萌芽熊 - Healing Bear
                     * extname : mp3
                     * pkg_price : 1
                     * fail_process_320 : 4
                     * trans_param : {"cpy_grade":22,"classmap":{"attr0":8},"cid":148667285,"cpy_attr0":0,"display_rate":0,"pay_block_tpl":1,"musicpack_advance":0,"display":0,"cpy_level":1}
                     * remark : 疗愈熊
                     * filesize : 3777559
                     * album_audio_id : 312492081
                     * brief :
                     * rp_publish : 1
                     * privilege : 8
                     * topic_url :
                     * audio_id : 106913503
                     * 320privilege : 10
                     * pay_type_320 : 3
                     * fail_process_sq : 0
                     */

                    private String hash;
                    private int sqfilesize;
                    private int sqprivilege;
                    private int pay_type_sq;
                    private int bitrate;
                    private int pkg_price_sq;
                    private int has_accompany;
                    private String topic_url_320;
                    private String sqhash;
                    private int fail_process;
                    private int pay_type;
                    private String rp_type;
                    private String album_id;
                    private String mvhash;
                    private int duration;
                    private String topic_url_sq;
                    @JSONField(name = "320hash")
                    private String _$320hash;
                    private int price_sq;
                    private int inlist;
                    private int m4afilesize;
                    private int old_cpy;
                    @JSONField(name = "320filesize")
                    private int _$320filesize;
                    private int pkg_price_320;
                    private int price_320;
                    private int feetype;
                    private int price;
                    private String filename;
                    private String extname;
                    private int pkg_price;
                    private int fail_process_320;
                    private TransParamBeanX trans_param;
                    private String remark;
                    private int filesize;
                    private int album_audio_id;
                    private String brief;
                    private int rp_publish;
                    private int privilege;
                    private String topic_url;
                    private int audio_id;
                    @JSONField(name = "320privilege")
                    private int _$320privilege;
                    private int pay_type_320;
                    private int fail_process_sq;

                    public String getHash() {
                        return hash;
                    }

                    public void setHash(String hash) {
                        this.hash = hash;
                    }

                    public int getSqfilesize() {
                        return sqfilesize;
                    }

                    public void setSqfilesize(int sqfilesize) {
                        this.sqfilesize = sqfilesize;
                    }

                    public int getSqprivilege() {
                        return sqprivilege;
                    }

                    public void setSqprivilege(int sqprivilege) {
                        this.sqprivilege = sqprivilege;
                    }

                    public int getPay_type_sq() {
                        return pay_type_sq;
                    }

                    public void setPay_type_sq(int pay_type_sq) {
                        this.pay_type_sq = pay_type_sq;
                    }

                    public int getBitrate() {
                        return bitrate;
                    }

                    public void setBitrate(int bitrate) {
                        this.bitrate = bitrate;
                    }

                    public int getPkg_price_sq() {
                        return pkg_price_sq;
                    }

                    public void setPkg_price_sq(int pkg_price_sq) {
                        this.pkg_price_sq = pkg_price_sq;
                    }

                    public int getHas_accompany() {
                        return has_accompany;
                    }

                    public void setHas_accompany(int has_accompany) {
                        this.has_accompany = has_accompany;
                    }

                    public String getTopic_url_320() {
                        return topic_url_320;
                    }

                    public void setTopic_url_320(String topic_url_320) {
                        this.topic_url_320 = topic_url_320;
                    }

                    public String getSqhash() {
                        return sqhash;
                    }

                    public void setSqhash(String sqhash) {
                        this.sqhash = sqhash;
                    }

                    public int getFail_process() {
                        return fail_process;
                    }

                    public void setFail_process(int fail_process) {
                        this.fail_process = fail_process;
                    }

                    public int getPay_type() {
                        return pay_type;
                    }

                    public void setPay_type(int pay_type) {
                        this.pay_type = pay_type;
                    }

                    public String getRp_type() {
                        return rp_type;
                    }

                    public void setRp_type(String rp_type) {
                        this.rp_type = rp_type;
                    }

                    public String getAlbum_id() {
                        return album_id;
                    }

                    public void setAlbum_id(String album_id) {
                        this.album_id = album_id;
                    }

                    public String getMvhash() {
                        return mvhash;
                    }

                    public void setMvhash(String mvhash) {
                        this.mvhash = mvhash;
                    }

                    public int getDuration() {
                        return duration;
                    }

                    public void setDuration(int duration) {
                        this.duration = duration;
                    }

                    public String getTopic_url_sq() {
                        return topic_url_sq;
                    }

                    public void setTopic_url_sq(String topic_url_sq) {
                        this.topic_url_sq = topic_url_sq;
                    }

                    public String get_$320hash() {
                        return _$320hash;
                    }

                    public void set_$320hash(String _$320hash) {
                        this._$320hash = _$320hash;
                    }

                    public int getPrice_sq() {
                        return price_sq;
                    }

                    public void setPrice_sq(int price_sq) {
                        this.price_sq = price_sq;
                    }

                    public int getInlist() {
                        return inlist;
                    }

                    public void setInlist(int inlist) {
                        this.inlist = inlist;
                    }

                    public int getM4afilesize() {
                        return m4afilesize;
                    }

                    public void setM4afilesize(int m4afilesize) {
                        this.m4afilesize = m4afilesize;
                    }

                    public int getOld_cpy() {
                        return old_cpy;
                    }

                    public void setOld_cpy(int old_cpy) {
                        this.old_cpy = old_cpy;
                    }

                    public int get_$320filesize() {
                        return _$320filesize;
                    }

                    public void set_$320filesize(int _$320filesize) {
                        this._$320filesize = _$320filesize;
                    }

                    public int getPkg_price_320() {
                        return pkg_price_320;
                    }

                    public void setPkg_price_320(int pkg_price_320) {
                        this.pkg_price_320 = pkg_price_320;
                    }

                    public int getPrice_320() {
                        return price_320;
                    }

                    public void setPrice_320(int price_320) {
                        this.price_320 = price_320;
                    }

                    public int getFeetype() {
                        return feetype;
                    }

                    public void setFeetype(int feetype) {
                        this.feetype = feetype;
                    }

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public String getFilename() {
                        return filename;
                    }

                    public void setFilename(String filename) {
                        this.filename = filename;
                    }

                    public String getExtname() {
                        return extname;
                    }

                    public void setExtname(String extname) {
                        this.extname = extname;
                    }

                    public int getPkg_price() {
                        return pkg_price;
                    }

                    public void setPkg_price(int pkg_price) {
                        this.pkg_price = pkg_price;
                    }

                    public int getFail_process_320() {
                        return fail_process_320;
                    }

                    public void setFail_process_320(int fail_process_320) {
                        this.fail_process_320 = fail_process_320;
                    }

                    public TransParamBeanX getTrans_param() {
                        return trans_param;
                    }

                    public void setTrans_param(TransParamBeanX trans_param) {
                        this.trans_param = trans_param;
                    }

                    public String getRemark() {
                        return remark;
                    }

                    public void setRemark(String remark) {
                        this.remark = remark;
                    }

                    public int getFilesize() {
                        return filesize;
                    }

                    public void setFilesize(int filesize) {
                        this.filesize = filesize;
                    }

                    public int getAlbum_audio_id() {
                        return album_audio_id;
                    }

                    public void setAlbum_audio_id(int album_audio_id) {
                        this.album_audio_id = album_audio_id;
                    }

                    public String getBrief() {
                        return brief;
                    }

                    public void setBrief(String brief) {
                        this.brief = brief;
                    }

                    public int getRp_publish() {
                        return rp_publish;
                    }

                    public void setRp_publish(int rp_publish) {
                        this.rp_publish = rp_publish;
                    }

                    public int getPrivilege() {
                        return privilege;
                    }

                    public void setPrivilege(int privilege) {
                        this.privilege = privilege;
                    }

                    public String getTopic_url() {
                        return topic_url;
                    }

                    public void setTopic_url(String topic_url) {
                        this.topic_url = topic_url;
                    }

                    public int getAudio_id() {
                        return audio_id;
                    }

                    public void setAudio_id(int audio_id) {
                        this.audio_id = audio_id;
                    }

                    public int get_$320privilege() {
                        return _$320privilege;
                    }

                    public void set_$320privilege(int _$320privilege) {
                        this._$320privilege = _$320privilege;
                    }

                    public int getPay_type_320() {
                        return pay_type_320;
                    }

                    public void setPay_type_320(int pay_type_320) {
                        this.pay_type_320 = pay_type_320;
                    }

                    public int getFail_process_sq() {
                        return fail_process_sq;
                    }

                    public void setFail_process_sq(int fail_process_sq) {
                        this.fail_process_sq = fail_process_sq;
                    }

                    public static class TransParamBeanX {
                        /**
                         * cpy_grade : 22
                         * classmap : {"attr0":8}
                         * cid : 148667285
                         * cpy_attr0 : 0
                         * display_rate : 0
                         * pay_block_tpl : 1
                         * musicpack_advance : 0
                         * display : 0
                         * cpy_level : 1
                         */

                        private int cpy_grade;
                        private ClassmapBean classmap;
                        private int cid;
                        private int cpy_attr0;
                        private int display_rate;
                        private int pay_block_tpl;
                        private int musicpack_advance;
                        private int display;
                        private int cpy_level;

                        public int getCpy_grade() {
                            return cpy_grade;
                        }

                        public void setCpy_grade(int cpy_grade) {
                            this.cpy_grade = cpy_grade;
                        }

                        public ClassmapBean getClassmap() {
                            return classmap;
                        }

                        public void setClassmap(ClassmapBean classmap) {
                            this.classmap = classmap;
                        }

                        public int getCid() {
                            return cid;
                        }

                        public void setCid(int cid) {
                            this.cid = cid;
                        }

                        public int getCpy_attr0() {
                            return cpy_attr0;
                        }

                        public void setCpy_attr0(int cpy_attr0) {
                            this.cpy_attr0 = cpy_attr0;
                        }

                        public int getDisplay_rate() {
                            return display_rate;
                        }

                        public void setDisplay_rate(int display_rate) {
                            this.display_rate = display_rate;
                        }

                        public int getPay_block_tpl() {
                            return pay_block_tpl;
                        }

                        public void setPay_block_tpl(int pay_block_tpl) {
                            this.pay_block_tpl = pay_block_tpl;
                        }

                        public int getMusicpack_advance() {
                            return musicpack_advance;
                        }

                        public void setMusicpack_advance(int musicpack_advance) {
                            this.musicpack_advance = musicpack_advance;
                        }

                        public int getDisplay() {
                            return display;
                        }

                        public void setDisplay(int display) {
                            this.display = display;
                        }

                        public int getCpy_level() {
                            return cpy_level;
                        }

                        public void setCpy_level(int cpy_level) {
                            this.cpy_level = cpy_level;
                        }

                        public static class ClassmapBean {
                            /**
                             * attr0 : 8
                             */

                            private int attr0;

                            public int getAttr0() {
                                return attr0;
                            }

                            public void setAttr0(int attr0) {
                                this.attr0 = attr0;
                            }
                        }
                    }
                }
            }
        }
    }
}
