package com.music.pojo.api;

import java.io.Serializable;
import java.util.List;

public class KugouMusicTop500 implements Serializable {

    private static final long serialVersionUID = -4502484923936375791L;
    /**
     * JS_CSS_DATE : 20130320
     * kg_domain : https://m.kugou.com
     * src : http://downmobile.kugou.com/promote/package/download/channel=6
     * fr : null
     * ver : v3
     * info : {"rank_cid":55787,"id":2,"rankname":"TOP500","jump_url":"","banner_9":"http://imge.kugou.com/mcommon/{size}/20210618/20210618123113781989.png","rankid":8888,"bannerurl":"http://imge.kugou.com/mcommonbanner/{size}/20181019/20181019122517263545.jpg","isvol":1,"zone":"tx6_gz_kmr","play_times":38087748,"img_9":"","custom_type":0,"ranktype":2,"img_cover":"http://imge.kugou.com/mcommon/{size}/20211117/20211117115003451494.png","issue":345,"banner7url":"http://imge.kugou.com/mcommon/{size}/20181019/20181019122516438289.jpg","show_play_button":0,"jump_title":"","update_frequency":"每天","classify":1,"album_img_9":"http://imge.kugou.com/stdmusic/{size}/20191213/20191213210932485332.jpg","intro":"数据来源：全曲库歌曲 排序方式：按歌曲喜爱用户数的总量排序 更新频率：每天","imgurl":"http://imge.kugou.com/mcommon/{size}/20181019/20181019122513972113.jpg"}
     * songs : {"total":4,"page":1,"pagesize":30,"timestamp":1639214653,"list":[{"last_sort":276,"topic_url_320":"","sqhash":"A54CE9DE067FA7CF5D55CB60C12D23A3","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:03","rp_type":"audio","album_id":"15117855","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":11097034,"pkg_price_320":0,"feetype":0,"filename":"李常超(Lao乾妈) - 盗墓笔记·十年人间","duration_high":0,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"A1A0393A7375783E29BBB3B3486C6B29","sqfilesize":34471088,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6797713154255495168","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"","duration":277,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20191213/20191213210932485332.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":0,"320privilege":0,"remark":"盗墓笔记·十年人间","isfirst":0,"privilege":0,"320hash":"1AF882B5A0F106437DDB8DFA214C28AE","price":0,"extname":"mp3","price_320":0,"mvhash":"86910CD88196B3A9A7A34048919EF09E","sort":95,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"all_quality_free":1,"cid":83786626,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"B83D88C1E5A29A82D71C8BB2D53B31BC","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":111937430,"bitrate_high":0,"issue":345,"pay_type":0,"filesize":4438944,"hash_super":"","audio_id":40949274,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":445,"topic_url_320":"","sqhash":"2E4E22B4A17C482FEFE9DEC4376474C9","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:10","rp_type":"audio","album_id":"49421475","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":8069947,"pkg_price_320":0,"feetype":0,"filename":"张杰 - 少年中国说","duration_high":201,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"6642026538E01B183807745DCA8E3570","sqfilesize":25291096,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"DA79A25278A1F9FE50310B8619B4F406","duration":201,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20211009/20211009212901818356.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":41521569,"320privilege":0,"remark":"少年中国说","isfirst":0,"privilege":0,"320hash":"F323C4C8A83627C91182706034061650","price":0,"extname":"mp3","price_320":0,"mvhash":"D04FB522FD675A4558DEA96D94C60330","sort":392,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100671496},"cid":173638959,"display_rate":2,"cpy_attr0":0,"hash_multitrack":"86817DB26EFF3EDCD0D5696921D3878D","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":341910557,"bitrate_high":1646,"issue":345,"pay_type":0,"filesize":3228098,"hash_super":"","audio_id":123137688,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":285,"topic_url_320":"","sqhash":"A22FF18FB141CBF87D108B9DD463E8C9","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:12","rp_type":"audio","album_id":"964178","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":9496236,"pkg_price_320":0,"feetype":0,"filename":"孙楠 - 留什么给你","duration_high":0,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"32856ED5E34CFE975149EAD254A38528","sqfilesize":29177645,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6736906865484750848","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"","duration":237,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20150718/20150718150204815336.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":0,"320privilege":0,"remark":"我们都是伤心人","isfirst":0,"privilege":0,"320hash":"D3A8C417F7DB45F82860723C9981B4C3","price":0,"extname":"mp3","price_320":0,"mvhash":"B94D6586C36E7052651528AF1A89BECD","sort":443,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":35018062,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"CD7DC0E40E48AD1013146D1843869309","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":32077008,"bitrate_high":0,"issue":345,"pay_type":0,"filesize":3798617,"hash_super":"","audio_id":249315,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":480,"topic_url_320":"","sqhash":"403048691D6A5BBDD5D1D056C5BA5C8F","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:12","rp_type":"audio","album_id":"14456909","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":10452304,"pkg_price_320":0,"feetype":0,"filename":"薛之谦 - 演员","duration_high":261,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"219DCC6167EBF64EAA187414CA58E836","sqfilesize":25495240,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6688372764355067904","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"46C014840BB4DC1B79D1E14A493DF844","duration":261,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20160907/20160907192633928797.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":48374383,"320privilege":0,"remark":"绅士","isfirst":0,"privilege":0,"320hash":"66B8A0A70E703EDA81B58145329F75A8","price":0,"extname":"mp3","price_320":0,"mvhash":"4F5DAE4814BFAF9C556ABCD91EBF7FC5","sort":457,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"pay_block_tpl":1,"cid":2481985,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"91C3E2B118140ECD782C665339D270D4","appid_block":"3124","musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":128180951,"bitrate_high":1481,"issue":345,"pay_type":0,"filesize":4181035,"hash_super":"","audio_id":15327235,"first":0,"privilege_super":0,"fail_process_sq":0}]}
     * pagesize : 30
     * __Tpl : rank/info.html
     */

    private int JS_CSS_DATE;
    private String kg_domain;
    private String src;
    private Object fr;
    private String ver;
    private InfoBean info;
    private SongsBean songs;
    private int pagesize;
    private String __Tpl;

    public int getJS_CSS_DATE() {
        return JS_CSS_DATE;
    }

    public void setJS_CSS_DATE(int JS_CSS_DATE) {
        this.JS_CSS_DATE = JS_CSS_DATE;
    }

    public String getKg_domain() {
        return kg_domain;
    }

    public void setKg_domain(String kg_domain) {
        this.kg_domain = kg_domain;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Object getFr() {
        return fr;
    }

    public void setFr(Object fr) {
        this.fr = fr;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public SongsBean getSongs() {
        return songs;
    }

    public void setSongs(SongsBean songs) {
        this.songs = songs;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public String get__Tpl() {
        return __Tpl;
    }

    public void set__Tpl(String __Tpl) {
        this.__Tpl = __Tpl;
    }

    public static class InfoBean {
        /**
         * rank_cid : 55787
         * id : 2
         * rankname : TOP500
         * jump_url :
         * banner_9 : http://imge.kugou.com/mcommon/{size}/20210618/20210618123113781989.png
         * rankid : 8888
         * bannerurl : http://imge.kugou.com/mcommonbanner/{size}/20181019/20181019122517263545.jpg
         * isvol : 1
         * zone : tx6_gz_kmr
         * play_times : 38087748
         * img_9 :
         * custom_type : 0
         * ranktype : 2
         * img_cover : http://imge.kugou.com/mcommon/{size}/20211117/20211117115003451494.png
         * issue : 345
         * banner7url : http://imge.kugou.com/mcommon/{size}/20181019/20181019122516438289.jpg
         * show_play_button : 0
         * jump_title :
         * update_frequency : 每天
         * classify : 1
         * album_img_9 : http://imge.kugou.com/stdmusic/{size}/20191213/20191213210932485332.jpg
         * intro : 数据来源：全曲库歌曲 排序方式：按歌曲喜爱用户数的总量排序 更新频率：每天
         * imgurl : http://imge.kugou.com/mcommon/{size}/20181019/20181019122513972113.jpg
         */

        private int rank_cid;
        private int id;
        private String rankname;
        private String jump_url;
        private String banner_9;
        private int rankid;
        private String bannerurl;
        private int isvol;
        private String zone;
        private int play_times;
        private String img_9;
        private int custom_type;
        private int ranktype;
        private String img_cover;
        private int issue;
        private String banner7url;
        private int show_play_button;
        private String jump_title;
        private String update_frequency;
        private int classify;
        private String album_img_9;
        private String intro;
        private String imgurl;

        public int getRank_cid() {
            return rank_cid;
        }

        public void setRank_cid(int rank_cid) {
            this.rank_cid = rank_cid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRankname() {
            return rankname;
        }

        public void setRankname(String rankname) {
            this.rankname = rankname;
        }

        public String getJump_url() {
            return jump_url;
        }

        public void setJump_url(String jump_url) {
            this.jump_url = jump_url;
        }

        public String getBanner_9() {
            return banner_9;
        }

        public void setBanner_9(String banner_9) {
            this.banner_9 = banner_9;
        }

        public int getRankid() {
            return rankid;
        }

        public void setRankid(int rankid) {
            this.rankid = rankid;
        }

        public String getBannerurl() {
            return bannerurl;
        }

        public void setBannerurl(String bannerurl) {
            this.bannerurl = bannerurl;
        }

        public int getIsvol() {
            return isvol;
        }

        public void setIsvol(int isvol) {
            this.isvol = isvol;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public int getPlay_times() {
            return play_times;
        }

        public void setPlay_times(int play_times) {
            this.play_times = play_times;
        }

        public String getImg_9() {
            return img_9;
        }

        public void setImg_9(String img_9) {
            this.img_9 = img_9;
        }

        public int getCustom_type() {
            return custom_type;
        }

        public void setCustom_type(int custom_type) {
            this.custom_type = custom_type;
        }

        public int getRanktype() {
            return ranktype;
        }

        public void setRanktype(int ranktype) {
            this.ranktype = ranktype;
        }

        public String getImg_cover() {
            return img_cover;
        }

        public void setImg_cover(String img_cover) {
            this.img_cover = img_cover;
        }

        public int getIssue() {
            return issue;
        }

        public void setIssue(int issue) {
            this.issue = issue;
        }

        public String getBanner7url() {
            return banner7url;
        }

        public void setBanner7url(String banner7url) {
            this.banner7url = banner7url;
        }

        public int getShow_play_button() {
            return show_play_button;
        }

        public void setShow_play_button(int show_play_button) {
            this.show_play_button = show_play_button;
        }

        public String getJump_title() {
            return jump_title;
        }

        public void setJump_title(String jump_title) {
            this.jump_title = jump_title;
        }

        public String getUpdate_frequency() {
            return update_frequency;
        }

        public void setUpdate_frequency(String update_frequency) {
            this.update_frequency = update_frequency;
        }

        public int getClassify() {
            return classify;
        }

        public void setClassify(int classify) {
            this.classify = classify;
        }

        public String getAlbum_img_9() {
            return album_img_9;
        }

        public void setAlbum_img_9(String album_img_9) {
            this.album_img_9 = album_img_9;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }
    }

    public static class SongsBean {
        /**
         * total : 4
         * page : 1
         * pagesize : 30
         * timestamp : 1639214653
         * list : [{"last_sort":276,"topic_url_320":"","sqhash":"A54CE9DE067FA7CF5D55CB60C12D23A3","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:03","rp_type":"audio","album_id":"15117855","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":11097034,"pkg_price_320":0,"feetype":0,"filename":"李常超(Lao乾妈) - 盗墓笔记·十年人间","duration_high":0,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"A1A0393A7375783E29BBB3B3486C6B29","sqfilesize":34471088,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6797713154255495168","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"","duration":277,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20191213/20191213210932485332.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":0,"320privilege":0,"remark":"盗墓笔记·十年人间","isfirst":0,"privilege":0,"320hash":"1AF882B5A0F106437DDB8DFA214C28AE","price":0,"extname":"mp3","price_320":0,"mvhash":"86910CD88196B3A9A7A34048919EF09E","sort":95,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"all_quality_free":1,"cid":83786626,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"B83D88C1E5A29A82D71C8BB2D53B31BC","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":111937430,"bitrate_high":0,"issue":345,"pay_type":0,"filesize":4438944,"hash_super":"","audio_id":40949274,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":445,"topic_url_320":"","sqhash":"2E4E22B4A17C482FEFE9DEC4376474C9","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:10","rp_type":"audio","album_id":"49421475","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":8069947,"pkg_price_320":0,"feetype":0,"filename":"张杰 - 少年中国说","duration_high":201,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"6642026538E01B183807745DCA8E3570","sqfilesize":25291096,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"DA79A25278A1F9FE50310B8619B4F406","duration":201,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20211009/20211009212901818356.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":41521569,"320privilege":0,"remark":"少年中国说","isfirst":0,"privilege":0,"320hash":"F323C4C8A83627C91182706034061650","price":0,"extname":"mp3","price_320":0,"mvhash":"D04FB522FD675A4558DEA96D94C60330","sort":392,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100671496},"cid":173638959,"display_rate":2,"cpy_attr0":0,"hash_multitrack":"86817DB26EFF3EDCD0D5696921D3878D","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":341910557,"bitrate_high":1646,"issue":345,"pay_type":0,"filesize":3228098,"hash_super":"","audio_id":123137688,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":285,"topic_url_320":"","sqhash":"A22FF18FB141CBF87D108B9DD463E8C9","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:12","rp_type":"audio","album_id":"964178","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":9496236,"pkg_price_320":0,"feetype":0,"filename":"孙楠 - 留什么给你","duration_high":0,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"32856ED5E34CFE975149EAD254A38528","sqfilesize":29177645,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6736906865484750848","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"","duration":237,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20150718/20150718150204815336.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":0,"320privilege":0,"remark":"我们都是伤心人","isfirst":0,"privilege":0,"320hash":"D3A8C417F7DB45F82860723C9981B4C3","price":0,"extname":"mp3","price_320":0,"mvhash":"B94D6586C36E7052651528AF1A89BECD","sort":443,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"cid":35018062,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"CD7DC0E40E48AD1013146D1843869309","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":32077008,"bitrate_high":0,"issue":345,"pay_type":0,"filesize":3798617,"hash_super":"","audio_id":249315,"first":0,"privilege_super":0,"fail_process_sq":0},{"last_sort":480,"topic_url_320":"","sqhash":"403048691D6A5BBDD5D1D056C5BA5C8F","fail_process":0,"pkg_price":0,"addtime":"2021-12-11 09:30:12","rp_type":"audio","album_id":"14456909","privilege_high":0,"topic_url_sq":"","rank_cid":55787,"inlist":1,"320filesize":10452304,"pkg_price_320":0,"feetype":0,"filename":"薛之谦 - 演员","duration_high":261,"fail_process_320":0,"zone":"tx6_gz_kmr","topic_url":"","rp_publish":1,"trans_obj":{"rank_show_sort":1},"hash":"219DCC6167EBF64EAA187414CA58E836","sqfilesize":25495240,"sqprivilege":0,"pay_type_sq":0,"bitrate":128,"pkg_price_sq":0,"has_accompany":1,"musical":null,"pay_type_320":0,"vstr_id":"6688372764355067904","extname_super":"","duration_super":0,"bitrate_super":0,"hash_high":"46C014840BB4DC1B79D1E14A493DF844","duration":261,"album_sizable_cover":"http://imge.kugou.com/stdmusic/{size}/20160907/20160907192633928797.jpg","price_sq":0,"old_cpy":1,"filesize_super":0,"m4afilesize":0,"filesize_high":48374383,"320privilege":0,"remark":"绅士","isfirst":0,"privilege":0,"320hash":"66B8A0A70E703EDA81B58145329F75A8","price":0,"extname":"mp3","price_320":0,"mvhash":"4F5DAE4814BFAF9C556ABCD91EBF7FC5","sort":457,"trans_param":{"cpy_grade":5,"classmap":{"attr0":100667400},"pay_block_tpl":1,"cid":2481985,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"91C3E2B118140ECD782C665339D270D4","appid_block":"3124","musicpack_advance":0,"display":32,"cpy_level":1},"recommend_reason":"","album_audio_id":128180951,"bitrate_high":1481,"issue":345,"pay_type":0,"filesize":4181035,"hash_super":"","audio_id":15327235,"first":0,"privilege_super":0,"fail_process_sq":0}]
         */

        private int total;
        private int page;
        private int pagesize;
        private int timestamp;
        private List<ListBean> list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPagesize() {
            return pagesize;
        }

        public void setPagesize(int pagesize) {
            this.pagesize = pagesize;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * last_sort : 276
             * topic_url_320 :
             * sqhash : A54CE9DE067FA7CF5D55CB60C12D23A3
             * fail_process : 0
             * pkg_price : 0
             * addtime : 2021-12-11 09:30:03
             * rp_type : audio
             * album_id : 15117855
             * privilege_high : 0
             * topic_url_sq :
             * rank_cid : 55787
             * inlist : 1
             * 320filesize : 11097034
             * pkg_price_320 : 0
             * feetype : 0
             * filename : 李常超(Lao乾妈) - 盗墓笔记·十年人间
             * duration_high : 0
             * fail_process_320 : 0
             * zone : tx6_gz_kmr
             * topic_url :
             * rp_publish : 1
             * trans_obj : {"rank_show_sort":1}
             * hash : A1A0393A7375783E29BBB3B3486C6B29
             * sqfilesize : 34471088
             * sqprivilege : 0
             * pay_type_sq : 0
             * bitrate : 128
             * pkg_price_sq : 0
             * has_accompany : 1
             * musical : null
             * pay_type_320 : 0
             * vstr_id : 6797713154255495168
             * extname_super :
             * duration_super : 0
             * bitrate_super : 0
             * hash_high :
             * duration : 277
             * album_sizable_cover : http://imge.kugou.com/stdmusic/{size}/20191213/20191213210932485332.jpg
             * price_sq : 0
             * old_cpy : 1
             * filesize_super : 0
             * m4afilesize : 0
             * filesize_high : 0
             * 320privilege : 0
             * remark : 盗墓笔记·十年人间
             * isfirst : 0
             * privilege : 0
             * 320hash : 1AF882B5A0F106437DDB8DFA214C28AE
             * price : 0
             * extname : mp3
             * price_320 : 0
             * mvhash : 86910CD88196B3A9A7A34048919EF09E
             * sort : 95
             * trans_param : {"cpy_grade":5,"classmap":{"attr0":100667400},"all_quality_free":1,"cid":83786626,"display_rate":1,"cpy_attr0":0,"hash_multitrack":"B83D88C1E5A29A82D71C8BB2D53B31BC","pay_block_tpl":1,"musicpack_advance":0,"display":32,"cpy_level":1}
             * recommend_reason :
             * album_audio_id : 111937430
             * bitrate_high : 0
             * issue : 345
             * pay_type : 0
             * filesize : 4438944
             * hash_super :
             * audio_id : 40949274
             * first : 0
             * privilege_super : 0
             * fail_process_sq : 0
             */

            private int last_sort;
            private String topic_url_320;
            private String sqhash;
            private int fail_process;
            private int pkg_price;
            private String addtime;
            private String rp_type;
            private String album_id;
            private int privilege_high;
            private String topic_url_sq;
            private int rank_cid;
            private int inlist;
            @com.google.gson.annotations.SerializedName("320filesize")
            private int _$320filesize;
            private int pkg_price_320;
            private int feetype;
            private String filename;
            private int duration_high;
            private int fail_process_320;
            private String zone;
            private String topic_url;
            private int rp_publish;
            private TransObjBean trans_obj;
            private String hash;
            private int sqfilesize;
            private int sqprivilege;
            private int pay_type_sq;
            private int bitrate;
            private int pkg_price_sq;
            private int has_accompany;
            private Object musical;
            private int pay_type_320;
            private String vstr_id;
            private String extname_super;
            private int duration_super;
            private int bitrate_super;
            private String hash_high;
            private int duration;
            private String album_sizable_cover;
            private int price_sq;
            private int old_cpy;
            private int filesize_super;
            private int m4afilesize;
            private int filesize_high;
            @com.google.gson.annotations.SerializedName("320privilege")
            private int _$320privilege;
            private String remark;
            private int isfirst;
            private int privilege;
            @com.google.gson.annotations.SerializedName("320hash")
            private String _$320hash;
            private int price;
            private String extname;
            private int price_320;
            private String mvhash;
            private int sort;
            private TransParamBean trans_param;
            private String recommend_reason;
            private int album_audio_id;
            private int bitrate_high;
            private int issue;
            private int pay_type;
            private int filesize;
            private String hash_super;
            private int audio_id;
            private int first;
            private int privilege_super;
            private int fail_process_sq;

            public int getLast_sort() {
                return last_sort;
            }

            public void setLast_sort(int last_sort) {
                this.last_sort = last_sort;
            }

            public String getTopic_url_320() {
                return topic_url_320;
            }

            public void setTopic_url_320(String topic_url_320) {
                this.topic_url_320 = topic_url_320;
            }

            public String getSqhash() {
                return sqhash;
            }

            public void setSqhash(String sqhash) {
                this.sqhash = sqhash;
            }

            public int getFail_process() {
                return fail_process;
            }

            public void setFail_process(int fail_process) {
                this.fail_process = fail_process;
            }

            public int getPkg_price() {
                return pkg_price;
            }

            public void setPkg_price(int pkg_price) {
                this.pkg_price = pkg_price;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getRp_type() {
                return rp_type;
            }

            public void setRp_type(String rp_type) {
                this.rp_type = rp_type;
            }

            public String getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(String album_id) {
                this.album_id = album_id;
            }

            public int getPrivilege_high() {
                return privilege_high;
            }

            public void setPrivilege_high(int privilege_high) {
                this.privilege_high = privilege_high;
            }

            public String getTopic_url_sq() {
                return topic_url_sq;
            }

            public void setTopic_url_sq(String topic_url_sq) {
                this.topic_url_sq = topic_url_sq;
            }

            public int getRank_cid() {
                return rank_cid;
            }

            public void setRank_cid(int rank_cid) {
                this.rank_cid = rank_cid;
            }

            public int getInlist() {
                return inlist;
            }

            public void setInlist(int inlist) {
                this.inlist = inlist;
            }

            public int get_$320filesize() {
                return _$320filesize;
            }

            public void set_$320filesize(int _$320filesize) {
                this._$320filesize = _$320filesize;
            }

            public int getPkg_price_320() {
                return pkg_price_320;
            }

            public void setPkg_price_320(int pkg_price_320) {
                this.pkg_price_320 = pkg_price_320;
            }

            public int getFeetype() {
                return feetype;
            }

            public void setFeetype(int feetype) {
                this.feetype = feetype;
            }

            public String getFilename() {
                return filename;
            }

            public void setFilename(String filename) {
                this.filename = filename;
            }

            public int getDuration_high() {
                return duration_high;
            }

            public void setDuration_high(int duration_high) {
                this.duration_high = duration_high;
            }

            public int getFail_process_320() {
                return fail_process_320;
            }

            public void setFail_process_320(int fail_process_320) {
                this.fail_process_320 = fail_process_320;
            }

            public String getZone() {
                return zone;
            }

            public void setZone(String zone) {
                this.zone = zone;
            }

            public String getTopic_url() {
                return topic_url;
            }

            public void setTopic_url(String topic_url) {
                this.topic_url = topic_url;
            }

            public int getRp_publish() {
                return rp_publish;
            }

            public void setRp_publish(int rp_publish) {
                this.rp_publish = rp_publish;
            }

            public TransObjBean getTrans_obj() {
                return trans_obj;
            }

            public void setTrans_obj(TransObjBean trans_obj) {
                this.trans_obj = trans_obj;
            }

            public String getHash() {
                return hash;
            }

            public void setHash(String hash) {
                this.hash = hash;
            }

            public int getSqfilesize() {
                return sqfilesize;
            }

            public void setSqfilesize(int sqfilesize) {
                this.sqfilesize = sqfilesize;
            }

            public int getSqprivilege() {
                return sqprivilege;
            }

            public void setSqprivilege(int sqprivilege) {
                this.sqprivilege = sqprivilege;
            }

            public int getPay_type_sq() {
                return pay_type_sq;
            }

            public void setPay_type_sq(int pay_type_sq) {
                this.pay_type_sq = pay_type_sq;
            }

            public int getBitrate() {
                return bitrate;
            }

            public void setBitrate(int bitrate) {
                this.bitrate = bitrate;
            }

            public int getPkg_price_sq() {
                return pkg_price_sq;
            }

            public void setPkg_price_sq(int pkg_price_sq) {
                this.pkg_price_sq = pkg_price_sq;
            }

            public int getHas_accompany() {
                return has_accompany;
            }

            public void setHas_accompany(int has_accompany) {
                this.has_accompany = has_accompany;
            }

            public Object getMusical() {
                return musical;
            }

            public void setMusical(Object musical) {
                this.musical = musical;
            }

            public int getPay_type_320() {
                return pay_type_320;
            }

            public void setPay_type_320(int pay_type_320) {
                this.pay_type_320 = pay_type_320;
            }

            public String getVstr_id() {
                return vstr_id;
            }

            public void setVstr_id(String vstr_id) {
                this.vstr_id = vstr_id;
            }

            public String getExtname_super() {
                return extname_super;
            }

            public void setExtname_super(String extname_super) {
                this.extname_super = extname_super;
            }

            public int getDuration_super() {
                return duration_super;
            }

            public void setDuration_super(int duration_super) {
                this.duration_super = duration_super;
            }

            public int getBitrate_super() {
                return bitrate_super;
            }

            public void setBitrate_super(int bitrate_super) {
                this.bitrate_super = bitrate_super;
            }

            public String getHash_high() {
                return hash_high;
            }

            public void setHash_high(String hash_high) {
                this.hash_high = hash_high;
            }

            public int getDuration() {
                return duration;
            }

            public void setDuration(int duration) {
                this.duration = duration;
            }

            public String getAlbum_sizable_cover() {
                return album_sizable_cover;
            }

            public void setAlbum_sizable_cover(String album_sizable_cover) {
                this.album_sizable_cover = album_sizable_cover;
            }

            public int getPrice_sq() {
                return price_sq;
            }

            public void setPrice_sq(int price_sq) {
                this.price_sq = price_sq;
            }

            public int getOld_cpy() {
                return old_cpy;
            }

            public void setOld_cpy(int old_cpy) {
                this.old_cpy = old_cpy;
            }

            public int getFilesize_super() {
                return filesize_super;
            }

            public void setFilesize_super(int filesize_super) {
                this.filesize_super = filesize_super;
            }

            public int getM4afilesize() {
                return m4afilesize;
            }

            public void setM4afilesize(int m4afilesize) {
                this.m4afilesize = m4afilesize;
            }

            public int getFilesize_high() {
                return filesize_high;
            }

            public void setFilesize_high(int filesize_high) {
                this.filesize_high = filesize_high;
            }

            public int get_$320privilege() {
                return _$320privilege;
            }

            public void set_$320privilege(int _$320privilege) {
                this._$320privilege = _$320privilege;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getIsfirst() {
                return isfirst;
            }

            public void setIsfirst(int isfirst) {
                this.isfirst = isfirst;
            }

            public int getPrivilege() {
                return privilege;
            }

            public void setPrivilege(int privilege) {
                this.privilege = privilege;
            }

            public String get_$320hash() {
                return _$320hash;
            }

            public void set_$320hash(String _$320hash) {
                this._$320hash = _$320hash;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public String getExtname() {
                return extname;
            }

            public void setExtname(String extname) {
                this.extname = extname;
            }

            public int getPrice_320() {
                return price_320;
            }

            public void setPrice_320(int price_320) {
                this.price_320 = price_320;
            }

            public String getMvhash() {
                return mvhash;
            }

            public void setMvhash(String mvhash) {
                this.mvhash = mvhash;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public String getRecommend_reason() {
                return recommend_reason;
            }

            public void setRecommend_reason(String recommend_reason) {
                this.recommend_reason = recommend_reason;
            }

            public int getAlbum_audio_id() {
                return album_audio_id;
            }

            public void setAlbum_audio_id(int album_audio_id) {
                this.album_audio_id = album_audio_id;
            }

            public int getBitrate_high() {
                return bitrate_high;
            }

            public void setBitrate_high(int bitrate_high) {
                this.bitrate_high = bitrate_high;
            }

            public int getIssue() {
                return issue;
            }

            public void setIssue(int issue) {
                this.issue = issue;
            }

            public int getPay_type() {
                return pay_type;
            }

            public void setPay_type(int pay_type) {
                this.pay_type = pay_type;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public String getHash_super() {
                return hash_super;
            }

            public void setHash_super(String hash_super) {
                this.hash_super = hash_super;
            }

            public int getAudio_id() {
                return audio_id;
            }

            public void setAudio_id(int audio_id) {
                this.audio_id = audio_id;
            }

            public int getFirst() {
                return first;
            }

            public void setFirst(int first) {
                this.first = first;
            }

            public int getPrivilege_super() {
                return privilege_super;
            }

            public void setPrivilege_super(int privilege_super) {
                this.privilege_super = privilege_super;
            }

            public int getFail_process_sq() {
                return fail_process_sq;
            }

            public void setFail_process_sq(int fail_process_sq) {
                this.fail_process_sq = fail_process_sq;
            }

            public static class TransObjBean {
                /**
                 * rank_show_sort : 1
                 */

                private int rank_show_sort;

                public int getRank_show_sort() {
                    return rank_show_sort;
                }

                public void setRank_show_sort(int rank_show_sort) {
                    this.rank_show_sort = rank_show_sort;
                }
            }

            public static class TransParamBean {
                /**
                 * cpy_grade : 5
                 * classmap : {"attr0":100667400}
                 * all_quality_free : 1
                 * cid : 83786626
                 * display_rate : 1
                 * cpy_attr0 : 0
                 * hash_multitrack : B83D88C1E5A29A82D71C8BB2D53B31BC
                 * pay_block_tpl : 1
                 * musicpack_advance : 0
                 * display : 32
                 * cpy_level : 1
                 */

                private int cpy_grade;
                private ClassmapBean classmap;
                private int all_quality_free;
                private int cid;
                private int display_rate;
                private int cpy_attr0;
                private String hash_multitrack;
                private int pay_block_tpl;
                private int musicpack_advance;
                private int display;
                private int cpy_level;

                public int getCpy_grade() {
                    return cpy_grade;
                }

                public void setCpy_grade(int cpy_grade) {
                    this.cpy_grade = cpy_grade;
                }

                public ClassmapBean getClassmap() {
                    return classmap;
                }

                public void setClassmap(ClassmapBean classmap) {
                    this.classmap = classmap;
                }

                public int getAll_quality_free() {
                    return all_quality_free;
                }

                public void setAll_quality_free(int all_quality_free) {
                    this.all_quality_free = all_quality_free;
                }

                public int getCid() {
                    return cid;
                }

                public void setCid(int cid) {
                    this.cid = cid;
                }

                public int getDisplay_rate() {
                    return display_rate;
                }

                public void setDisplay_rate(int display_rate) {
                    this.display_rate = display_rate;
                }

                public int getCpy_attr0() {
                    return cpy_attr0;
                }

                public void setCpy_attr0(int cpy_attr0) {
                    this.cpy_attr0 = cpy_attr0;
                }

                public String getHash_multitrack() {
                    return hash_multitrack;
                }

                public void setHash_multitrack(String hash_multitrack) {
                    this.hash_multitrack = hash_multitrack;
                }

                public int getPay_block_tpl() {
                    return pay_block_tpl;
                }

                public void setPay_block_tpl(int pay_block_tpl) {
                    this.pay_block_tpl = pay_block_tpl;
                }

                public int getMusicpack_advance() {
                    return musicpack_advance;
                }

                public void setMusicpack_advance(int musicpack_advance) {
                    this.musicpack_advance = musicpack_advance;
                }

                public int getDisplay() {
                    return display;
                }

                public void setDisplay(int display) {
                    this.display = display;
                }

                public int getCpy_level() {
                    return cpy_level;
                }

                public void setCpy_level(int cpy_level) {
                    this.cpy_level = cpy_level;
                }

                public static class ClassmapBean {
                    /**
                     * attr0 : 100667400
                     */

                    private int attr0;

                    public int getAttr0() {
                        return attr0;
                    }

                    public void setAttr0(int attr0) {
                        this.attr0 = attr0;
                    }
                }
            }
        }
    }
}
