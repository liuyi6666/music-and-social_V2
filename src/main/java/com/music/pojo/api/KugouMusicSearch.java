package com.music.pojo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class KugouMusicSearch implements Serializable {

    private static final long serialVersionUID = 1082905988359070144L;
    /**
     * error :
     * data : {"aggregation":[],"tab":"","info":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3614717,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":99076859,"pay_block_tpl":1,"hash_multitrack":"5DEEAA48FCCE6986F9AA8686385B2694","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Rentz","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Rentz - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"709287edb9a974559d5f35d7087320ba","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":256930565,"rp_publish":1,"album_id":"37486055","ownercount":1225684,"fold_type":0,"audio_id":72084935,"pkg_price_sq":1,"320filesize":9036483,"isnew":0,"duration":225,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":30328649,"fail_process":4,"320hash":"398d00ae35554541f19580f92e14352a","extname":"mp3","sqhash":"173b2ae3bfb666c54fb6980df2b33a0e","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3305483,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":71940519,"pay_block_tpl":1,"hash_multitrack":"5710175045106284E295B29DE893ADF5","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"나혼자 (Alone) (我一个人)","singername":"SISTAR","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"SISTAR - 나혼자 (Alone) (我一个人)","price_320":200,"songname":"나혼자 (Alone)","group":[{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3305487,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":105957821,"pay_block_tpl":1,"hash_multitrack":"84D27F209E0CAAB0F3293C2141B63F2D","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"나혼자 (Alone) (我一个人)","singername":"SISTAR","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"SISTAR - 나혼자 (Alone) (我一个人)","price_320":200,"songname":"나혼자 (Alone)","hash":"74b1ce9aa7f318ad54549478ff7208bc","mvhash":"f00e729257380a4c532abebd4c40394e","rp_type":"audio","privilege":8,"album_audio_id":264717346,"rp_publish":1,"album_id":"38305786","ownercount":2345,"fold_type":0,"audio_id":202908,"pkg_price_sq":1,"320filesize":8263319,"isnew":0,"duration":206,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27442581,"fail_process":4,"320hash":"63cdb89228c8c26986552c0cea736d34","extname":"mp3","sqhash":"a53b1747241d53c5ce152c7234188894","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"DJ Central Vol. 11 KPOP","othername":""}],"hash":"86546f08a254a5714146c742e787d1e3","mvhash":"f00e729257380a4c532abebd4c40394e","rp_type":"audio","privilege":8,"album_audio_id":164864141,"rp_publish":1,"album_id":"23195715","ownercount":30840,"fold_type":0,"audio_id":202908,"pkg_price_sq":1,"320filesize":8263315,"isnew":0,"duration":206,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27796702,"fail_process":4,"320hash":"0f7dcde6bea59135c3b53b52b6ddb24e","extname":"mp3","sqhash":"e90ee85845a614fd19fc39199bf7d7ea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"DJ Central KPOP Vol. 11","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":1,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6071280,"pay_block_tpl":1,"hash_multitrack":"8409A2EF4040824A5A17B44C7DD9986B","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":1,"musicpack_advance":0,"cpy_level":1,"cid":54320718,"pay_block_tpl":1,"hash_multitrack":"723A3F7F25797C13A45E17D2E74345E2","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":125351874,"rp_publish":1,"album_id":"13727483","ownercount":13713,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Different World","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":22452137,"pay_block_tpl":1,"display_rate":1,"classmap":{"attr0":100667400},"display":32,"musicpack_advance":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":87891088,"rp_publish":1,"album_id":"3991455","ownercount":7754,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Ultra Music Festival China 2017","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2544636,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"hash_multitrack":"B1FDE7C64F0ECC897263E74A9AF30349","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"9ffe5aeee1c5708df0f9f6f0fa9721f6","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":53401024,"rp_publish":1,"album_id":"","ownercount":6998,"fold_type":0,"audio_id":24738847,"pkg_price_sq":0,"320filesize":6534832,"isnew":0,"duration":159,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"05948750eb596cac0d8ac7d5cfc352bd","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""}],"hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":51877495,"rp_publish":1,"album_id":"1857732","ownercount":2837239,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4134496,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"80C3F7A8ED5BA935821BE06487D3EF04","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Nzi Jian","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Nzi Jian - Alone<\/em> (Remix)","price_320":0,"songname":"Alone<\/em> (Remix)","group":[],"hash":"94cba54570989eaeb34946a1d9cb6021","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":380432895,"rp_publish":1,"album_id":"54570458","ownercount":90330,"fold_type":0,"audio_id":148536147,"pkg_price_sq":0,"320filesize":10336174,"isnew":0,"duration":258,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":28466599,"fail_process":0,"320hash":"f42814ebb7bc2f492d22933d4063bed4","extname":"mp3","sqhash":"a48b4cdda1cad0e02073322b6c843ce9","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3273238,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":187492141,"pay_block_tpl":1,"hash_multitrack":"3AB9182D0A174A2F2D3A48AC6CD2D53C","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"嘉滢","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"嘉滢 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"6126ec3b1a4a51a6242a3120f75eb958","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":360569138,"rp_publish":1,"album_id":"51608919","ownercount":372148,"fold_type":0,"audio_id":136575169,"pkg_price_sq":1,"320filesize":8182796,"isnew":0,"duration":204,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21655274,"fail_process":4,"320hash":"6ca20075d48461fc3c92ec099c5b280f","extname":"mp3","sqhash":"78da9ae5af2c64d27414f3002996b64c","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":1,"filesize":4382100,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":32251919,"pay_block_tpl":1,"hash_multitrack":"214E8357D4B0E2FF8AD74F56FD8ED6CB","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382142,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":32251393,"pay_block_tpl":1,"hash_multitrack":"4ABF9E969405F46B8B90F6A28B570ED5","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"eb006c793d4a85e0ef4568e9c3e88b35","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":39841025,"rp_publish":1,"album_id":"1767661","ownercount":43312,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954744,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"e8afcd59e93756c9050e60477f44982d","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat 027 - Cataclysm","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382140,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":37646072,"pay_block_tpl":1,"hash_multitrack":"0E5D963C93FB3BF3D19F0178FBDFE549","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"49fa48e993d84437fdd1812760c51a25","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":66798341,"rp_publish":1,"album_id":"2783717","ownercount":28265,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954665,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"d1b0feec5499f8c9a7cd9a542f13bf5c","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat - Best of Trap","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382140,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":37646146,"pay_block_tpl":1,"hash_multitrack":"881C7E5D9C2242A5CC1672012F47AD88","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"f70512e8c7fa3f4cde3236b2618cc8f4","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":109532246,"rp_publish":1,"album_id":"8710022","ownercount":365,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954966,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"39f181c570ccf16d339cbff60fbff088","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat - Best of 2016","othername":""}],"hash":"c9c1f7887bd1cfb268e64523567e696a","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":40118863,"rp_publish":1,"album_id":"1802713","ownercount":653977,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954847,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"94269582ac4e417f902b66a2c4b5535b","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2876237,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":136475913,"pay_block_tpl":1,"hash_multitrack":"D46C46DB2B727762E0B054EDF0DFB164","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"Tommo、MELISA","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Tommo、MELISA - I'm Alone<\/em>","price_320":200,"songname":"I'm Alone<\/em>","group":[],"hash":"43c0a5800193daae71133201927ba96d","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":298317791,"rp_publish":1,"album_id":"42210725","ownercount":3264111,"fold_type":0,"audio_id":97298290,"pkg_price_sq":1,"320filesize":7190203,"isnew":0,"duration":179,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21296033,"fail_process":4,"320hash":"f75495460a7bb43dcb8ecb96aac30d8e","extname":"mp3","sqhash":"818db26f06290163b54f449b0e090dc7","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"I'm Alone","othername":""},{"othername_original":"Lost Stories Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3878705,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":1,"classmap":{"attr0":100667400},"display":32,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Lost Stories、Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Lost Stories、Alan Walker - Alone<\/em> (Lost Stories Remix)","price_320":0,"songname":"Alone<\/em> (Lost Stories Remix)","group":[],"hash":"b2fef2c0e5783ac5a0e3ce9530840983","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":57540951,"rp_publish":1,"album_id":"2138503","ownercount":72045,"fold_type":0,"audio_id":26399162,"pkg_price_sq":0,"320filesize":9696697,"isnew":0,"duration":242,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":32880624,"fail_process":0,"320hash":"fdf92b500f63b510363898502ae019e2","extname":"mp3","sqhash":"d5d9380eaaf779baefbdd65826905b5a","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Lost Stories Remix)","othername":""},{"othername_original":"Restrung","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2967310,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6205725,"pay_block_tpl":1,"hash_multitrack":"2B6821C7053F96EE3DDF48AF38F9B662","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em> (Restrung)","price_320":200,"songname":"Alone<\/em> (Restrung)","group":[],"hash":"9c13c704c099600cb6856a95ad5b4114","mvhash":"f3318802d5acd7466abf9a1c35d4ccee","rp_type":"audio","privilege":8,"album_audio_id":53779586,"rp_publish":1,"album_id":"1857732","ownercount":26465,"fold_type":0,"audio_id":25087851,"pkg_price_sq":1,"320filesize":7417271,"isnew":0,"duration":185,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":20223646,"fail_process":4,"320hash":"ead85489b7a713e32e2b97f06675f281","extname":"mp3","sqhash":"b304cb285d5e2c27bc21ae3bece4886a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":0,"isoriginal":1,"filesize":3276170,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":31990365,"pay_block_tpl":1,"hash_multitrack":"47567A43531A8DD8380D329EAE91DF5D","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"26F27CE542BC42980EC6C571F103FC22","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960129}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Bazzi","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Bazzi - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3143933,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"544BFE55B7341B41FD6ACF6286910E4A","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Bazzi","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Bazzi - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"282cce735729b74802ddd662e821dd00","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":119211956,"rp_publish":1,"album_id":"","ownercount":6305,"fold_type":0,"audio_id":25870965,"pkg_price_sq":0,"320filesize":7859425,"isnew":0,"duration":196,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"37b4e0dfda967bacd8df25355ae703d9","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""}],"hash":"b60ebd7e858b731b759cdf21ecb0a623","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":105855915,"rp_publish":1,"album_id":"8305866","ownercount":28364,"fold_type":0,"audio_id":36707045,"pkg_price_sq":0,"320filesize":8190116,"isnew":0,"duration":204,"pkg_price_320":1,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":4,"320hash":"25dce0dfbc93e10345379bfaa3fb4e5f","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":10,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174728,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":55035721,"pay_block_tpl":1,"hash_multitrack":"9E2F8D4BF1DCD7A219F53B5CBA02836E","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174694,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":81559494,"pay_block_tpl":1,"hash_multitrack":"838592610F3AF7E1EE58E91F818FD8E2","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"49326484665b0786b61a2b746eb6d4dd","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":146360690,"rp_publish":1,"album_id":"19277328","ownercount":14550,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936294,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25780639,"fail_process":4,"320hash":"f91fa28069a80b73a2d45c806583d64f","extname":"mp3","sqhash":"be6006f26350ca48a2b14a8495750078","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Trance 100 - Best Of 2014","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174744,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":81551469,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"8551b248e96215ac7256fb55686fe171","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":145215134,"rp_publish":1,"album_id":"19024990","ownercount":185,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936012,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26032240,"fail_process":4,"320hash":"ed206816aa3a2835e3877f9ee3e8d2a5","extname":"mp3","sqhash":"f91703ae571c20cc56d71161c2d07eea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems Top 100 (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174728,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":168269247,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"249ba70a9280e3d69d1298e628d7495a","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":336030458,"rp_publish":1,"album_id":"48614128","ownercount":5,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7935848,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25780653,"fail_process":4,"320hash":"855fed68fa4e78b033ca2bcebef8ed9e","extname":"mp3","sqhash":"fa2910281b4f4cc28af69ac9610111b6","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174736,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":81547109,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"5cb5af2b79fe3563125abcdea45f4890","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":146345161,"rp_publish":1,"album_id":"19275489","ownercount":5,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7935936,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26032240,"fail_process":4,"320hash":"1d0417ccd513dad8cf4bf33cf655f109","extname":"mp3","sqhash":"f91703ae571c20cc56d71161c2d07eea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armada Stream 40 - Best Of 2014 - Armada Music","othername":""}],"hash":"5bf20276f9c9a8fe96ea253e65dfc4d5","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":31741257,"rp_publish":1,"album_id":"930477","ownercount":32163,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936328,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25829563,"fail_process":4,"320hash":"cb5e66b1a279b677276593712481dd7c","extname":"mp3","sqhash":"03c982c18e41a431fa6c15d9bb5dfb37","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2830463,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"I'm Alone<\/em>","singername":"Melisa (259601382)、Tommo","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Melisa (259601382)、Tommo - I'm Alone<\/em>","price_320":0,"songname":"I'm Alone<\/em>","group":[],"hash":"f41e45213eceefccf7d102ea5a87b917","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":365303359,"rp_publish":1,"album_id":"","ownercount":357169,"fold_type":0,"audio_id":140022218,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":176,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4227885,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":101139509,"pay_block_tpl":1,"hash_multitrack":"9701358BB842E27D579CA0AF34F7F6A9","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"DJ SK、Bayza","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"DJ SK、Bayza - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"180c5ac09383aa0afdcf489f6ca124ee","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":258963262,"rp_publish":1,"album_id":"37776583","ownercount":61568,"fold_type":0,"audio_id":73400664,"pkg_price_sq":1,"320filesize":10569371,"isnew":0,"duration":264,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25619349,"fail_process":4,"320hash":"9cc98b15f5940cd4c3c31253c15484b2","extname":"mp3","sqhash":"6f4bdabfe45ee60021f3eef82095ff26","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3444920,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":4442150,"pay_block_tpl":1,"hash_multitrack":"2046A64F915D7606FD2DEBDB167EF669","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"黄子韬","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"黄子韬 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"c57f25ee7c9f46b415cc98fd6b4f0c4c","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":28051991,"rp_publish":1,"album_id":"550756","ownercount":59489,"fold_type":0,"audio_id":16713976,"pkg_price_sq":1,"320filesize":8601766,"isnew":0,"duration":215,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27541124,"fail_process":4,"320hash":"6f1c7b68796ef28cb7d6631a9047f720","extname":"mp3","sqhash":"6cd6c800df325215e65c9f131438cdcb","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Z.TAO","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5216768,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":69410164,"pay_block_tpl":1,"hash_multitrack":"6411128AC2CB65959FC700C2218ED446","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"A7437AC7DEFF67B22D6C3217B92D076A","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960143}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5281608,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":6835084,"pay_block_tpl":1,"hash_multitrack":"AB7388B13D8AFBEAAB4392CE349FDE38","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"443CEE5117DE5D7D6C47123556A9BF9F","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960199}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"b4f4d3922b7a2374ae00d9c2d4cc8b74","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":10,"album_audio_id":64388539,"rp_publish":1,"album_id":"1989662","ownercount":183,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13203606,"isnew":0,"duration":330,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35164198,"fail_process":4,"320hash":"088d07344dce939e600b6d73ff243d4d","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"At Last...The Duets Album / Breathless","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5249369,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6872991,"pay_block_tpl":1,"hash_multitrack":"7B372FC606AB38F3C04ED4C21B4A2F55","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"759aebeb472097115f0ce54b274ea5f9","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":8,"album_audio_id":28178401,"rp_publish":1,"album_id":"560498","ownercount":36073,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13122743,"isnew":0,"duration":328,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35002096,"fail_process":4,"320hash":"d10136b16804b894202d68a8dcae038d","extname":"mp3","sqhash":"298d60e9360db4d58a1ad8ceda243c41","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Breathless","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":5252158,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"ABA10E65D238F6659ABFBE8880A4938D","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"2759f1f786790f88d9dedf5e163ca775","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":0,"album_audio_id":146796837,"rp_publish":1,"album_id":"19336439","ownercount":183,"fold_type":0,"audio_id":520413,"pkg_price_sq":0,"320filesize":13165706,"isnew":0,"duration":328,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":35164198,"fail_process":0,"320hash":"0efffd7655647a5b1465629e6666437b","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Going Home","othername":""}],"hash":"e813d32a35a8cb07cc78a155c5de6052","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":10,"album_audio_id":150190713,"rp_publish":1,"album_id":"20010551","ownercount":3783,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13041591,"isnew":0,"duration":326,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35164198,"fail_process":4,"320hash":"7ed549c75dc7e07d2f2be9e90bc4d9cc","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Breathless","othername":""},{"othername_original":"Live","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":1267880,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":190506139,"pay_block_tpl":1,"hash_multitrack":"1D1936AC981D60B19DE050A14624D600","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"音质推荐","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"音质推荐 - I'm Alone<\/em> (Live)","price_320":200,"songname":"I'm Alone<\/em> (Live)","group":[],"hash":"32291d344c1c71bca399618ea367249b","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":365998931,"rp_publish":1,"album_id":"52202915","ownercount":34467,"fold_type":0,"audio_id":140495319,"pkg_price_sq":1,"320filesize":3169385,"isnew":0,"duration":79,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":5895876,"fail_process":4,"320hash":"e026eb5a62e46d28fbd558755472f771","extname":"mp3","sqhash":"6b6612126d8bf8d15ace1e054dbd1051","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"治愈旋律","othername":""},{"othername_original":"DJ版","pay_type_320":3,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2168166,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":200067531,"pay_block_tpl":1,"hash_multitrack":"7B72273B9D76E926FEA5E7CC6B655352","display_rate":4,"classmap":{"attr0":102760456},"display":64,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"水牛哥","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"水牛哥 - I'm Alone<\/em> (DJ版)","price_320":200,"songname":"I'm Alone<\/em> (DJ版)","group":[],"hash":"b71e97bd2e0240eadb0f77a7205c6506","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":386598999,"rp_publish":1,"album_id":"55380555","ownercount":27943,"fold_type":0,"audio_id":151981079,"pkg_price_sq":0,"320filesize":5420097,"isnew":0,"duration":135,"pkg_price_320":1,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":4,"320hash":"0702ff9831299eae4432757aefbe9de2","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":10,"sqprivilege":0,"album_name":"Temple","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2488317,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":80330461,"pay_block_tpl":1,"hash_multitrack":"23636538DCDF00B39C939404217DF02E","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"Alone<\/em> (孤独)","singername":"ＴＲＡ＄Ｈ","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"ＴＲＡ＄Ｈ - Alone<\/em> (孤独)","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2488573,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"CEC0479CBD375D026FD987E4F5D67C7D","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"ALONE<\/em> (孤独)","singername":"ＴＲＡ＄Ｈ","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"ＴＲＡ＄Ｈ - ALONE<\/em> (孤独)","price_320":0,"songname":"ALONE<\/em>","hash":"c9ade69315619b78339fe31826e461c7","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":119204526,"rp_publish":1,"album_id":"15389225","ownercount":27308,"fold_type":0,"audio_id":35666033,"pkg_price_sq":0,"320filesize":6295253,"isnew":0,"duration":155,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":9582188,"fail_process":0,"320hash":"8010130a5e9445380998abd9f0da18cb","extname":"mp3","sqhash":"24626803f92e2b24ffae374481c0b34a","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Awake Vol. 1","othername":""}],"hash":"12b7ff0c24221dbb8afad3ef15dcfd58","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":230080234,"rp_publish":1,"album_id":"32726914","ownercount":12402,"fold_type":0,"audio_id":35666033,"pkg_price_sq":1,"320filesize":6220428,"isnew":0,"duration":155,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":9582188,"fail_process":4,"320hash":"106a90a7d3c0f57cbf7db2f4f809dcaf","extname":"mp3","sqhash":"24626803f92e2b24ffae374481c0b34a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Awake","othername":""},{"othername_original":"Gammer Flip","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2593480,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"C69FCE292D89D97A1C2198DC656AF517","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Marshmello - Alone<\/em> (Gammer Flip)","price_320":0,"songname":"Alone<\/em> (Gammer Flip)","group":[],"hash":"6c827842cf5b4da5b3861dd1efd06b07","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":96859921,"rp_publish":1,"album_id":"18108831","ownercount":24387,"fold_type":0,"audio_id":27451685,"pkg_price_sq":0,"320filesize":6483636,"isnew":0,"duration":162,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":19155154,"fail_process":0,"320hash":"42c1ecbab23548e6c173f37f3aab7131","extname":"mp3","sqhash":"b41b680fdf972d7a38442bc4b0605b08","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Gammer Flip)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2935569,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"9B58CAB00F6A604A21676058FCBEC5BA","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"alone<\/em>","singername":"中川幸太郎","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"中川幸太郎 - alone<\/em>","price_320":0,"songname":"alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2935081,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"alone<\/em>","singername":"中川幸太郎","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"中川幸太郎 - alone<\/em>","price_320":0,"songname":"alone<\/em>","hash":"9ca5ca2f96ff0aa0d8eec7ee9e34c976","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":143751091,"rp_publish":1,"album_id":"18750675","ownercount":183,"fold_type":0,"audio_id":7639709,"pkg_price_sq":0,"320filesize":7337445,"isnew":0,"duration":183,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":14729973,"fail_process":0,"320hash":"9ed06a15d0356cdb506cf38aa2085e92","extname":"mp3","sqhash":"dff58712681286e0174130e781c7ecb7","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"妖狐×仆SS 2","othername":""}],"hash":"7dfb6441af17857d6bd1d4b99d77b76c","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":28245531,"rp_publish":1,"album_id":"565863","ownercount":20883,"fold_type":0,"audio_id":7639709,"pkg_price_sq":0,"320filesize":7338560,"isnew":0,"duration":183,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":14729973,"fail_process":0,"320hash":"0084fe1c3e7b947f235b96c1956cf7f2","extname":"mp3","sqhash":"dff58712681286e0174130e781c7ecb7","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"妖狐×僕SS 2","othername":""},{"othername_original":"Reimx","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4437181,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Marshmello - Alone<\/em> (Reimx)","price_320":0,"songname":"Alone<\/em> (Reimx)","group":[],"hash":"58d0998ac7547f791872bea687c0f94e","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":50149861,"rp_publish":1,"album_id":"","ownercount":19624,"fold_type":0,"audio_id":21694823,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":277,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"Remix","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2413920,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":21569369,"pay_block_tpl":1,"hash_multitrack":"E5DC160BDE4C552BB01D5DAD5F2808F5","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"AnJuy","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"AnJuy - Alone<\/em> (Remix)","price_320":200,"songname":"Alone<\/em> (Remix)","group":[],"hash":"9dcea8ca279e907804e811190ba0134e","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":56985289,"rp_publish":1,"album_id":"2086609","ownercount":19020,"fold_type":0,"audio_id":24752123,"pkg_price_sq":1,"320filesize":6034491,"isnew":0,"duration":150,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21179685,"fail_process":4,"320hash":"02bfb66588ba1ed721cd72dcf909394f","extname":"mp3","sqhash":"4e1af32a380741f3338b02a3b6d610d9","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4544514,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"C70325A54EA06AF28BAAB225859B8D32","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Wisp X、初音ミク","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Wisp X、初音ミク - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","group":[],"hash":"7153ca0d02de503eeeae40e0c15297d0","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":54110297,"rp_publish":1,"album_id":"1978950","ownercount":18004,"fold_type":0,"audio_id":16408688,"pkg_price_sq":0,"320filesize":11361219,"isnew":0,"duration":284,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"d6f71a770f6d9f48bae0cbca6bf6ca23","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"2016世界巡回演唱会挪威站","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3478807,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"78B80FE02DBA338C57EAD259B8A97AC9","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em> (2016世界巡回演唱会挪威站)","price_320":0,"songname":"Alone<\/em> (2016世界巡回演唱会挪威站)","group":[],"hash":"def4b1fc4974b7692a4bb94c61e1c446","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":56084851,"rp_publish":1,"album_id":"","ownercount":17208,"fold_type":0,"audio_id":26316047,"pkg_price_sq":0,"320filesize":8696785,"isnew":0,"duration":217,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"c6dd6e6a0fea8f34761236499009821a","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"Raspo Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2659518,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"B3537C4023EC04834DC71B8069553C57","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em> (Raspo Remix)","price_320":0,"songname":"Alone<\/em> (Raspo Remix)","group":[],"hash":"06ec186cd44c3e6dc4e700126394507c","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":65675841,"rp_publish":1,"album_id":"2692422","ownercount":14126,"fold_type":0,"audio_id":26605817,"pkg_price_sq":0,"320filesize":6648931,"isnew":0,"duration":166,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"3f8b9eeda7ae3bb97a712a8512ce64eb","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Raspo Remix)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4512925,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"E662325A003F9F726F9543E1FD31BA7E","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"苏可可、7snxx","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"苏可可、7snxx - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","group":[],"hash":"15ecb5560d37aeda312b99cdf2bcbd65","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":375337172,"rp_publish":1,"album_id":"53608628","ownercount":12063,"fold_type":0,"audio_id":146342819,"pkg_price_sq":0,"320filesize":11281983,"isnew":0,"duration":282,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":31657928,"fail_process":0,"320hash":"2310f051ab819fce4ee912912259f78e","extname":"mp3","sqhash":"ca3954ecf15b08ced7ef3bcf232e3f70","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Payphone","othername":""},{"othername_original":"Mesto Remix Radio Edit","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3166816,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":36372041,"pay_block_tpl":1,"hash_multitrack":"B2C8ADB2C35968F6F74CA1C4B99CA250","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"E8FF68873CF5F5174BAECA19E0BCDEB7","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960281}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"NERVO、Askery、Brielle Von Hugel","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"NERVO、Askery、Brielle Von Hugel - Alone<\/em> (Mesto Remix Radio Edit)","price_320":200,"songname":"Alone<\/em> (Mesto Remix Radio Edit)","group":[],"hash":"22e758038178646a73d9158d9fec40cf","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":143373936,"rp_publish":1,"album_id":"18679034","ownercount":11194,"fold_type":0,"audio_id":24591679,"pkg_price_sq":1,"320filesize":7916077,"isnew":0,"duration":197,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26611668,"fail_process":4,"320hash":"adb916c8abe382e0f62c2e356e59dcef","extname":"mp3","sqhash":"01bd4dd22adad0956570029109501fba","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone (feat. Brielle Von Hugel)","othername":""},{"othername_original":"DJ版","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2558966,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"I'm alone<\/em>","singername":"大雅、杳葵","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"大雅、杳葵 - I'm alone<\/em> (DJ版)","price_320":0,"songname":"I'm alone<\/em> (DJ版)","group":[],"hash":"65ce05b58bf515ffe9ffe03e8e8dade2","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":371865259,"rp_publish":1,"album_id":"53101541","ownercount":10231,"fold_type":0,"audio_id":143977777,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":159,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Everyday","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3456720,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":26042577,"pay_block_tpl":1,"hash_multitrack":"2CAF05E161977FD6C022E9D991E0FE31","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"4CCB58836830775853773329DBAFC260","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960121}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"林在莞","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"林在莞 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"5e550b3d0104900b471e576f1c4fb6db","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":64546550,"rp_publish":1,"album_id":"557391","ownercount":8824,"fold_type":0,"audio_id":2718884,"pkg_price_sq":1,"320filesize":8641072,"isnew":0,"duration":216,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":24927168,"fail_process":4,"320hash":"1d9ad33f340294d1c35a777104f917e3","extname":"mp3","sqhash":"a17e06de9a375e2ae9ece1184714f350","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"마이걸 (我的女孩)","othername":""},{"othername_original":"Remix","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4263389,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":140574964,"pay_block_tpl":1,"hash_multitrack":"9F636DB75460DD0E356A763E2AA8B451","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Y2002","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Y2002 - Alone<\/em> (Remix)","price_320":200,"songname":"Alone<\/em> (Remix)","group":[],"hash":"ae871608cd29f2eed650c74509659105","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":304703350,"rp_publish":1,"album_id":"43047438","ownercount":8823,"fold_type":0,"audio_id":102171542,"pkg_price_sq":1,"320filesize":10657826,"isnew":0,"duration":266,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":32406877,"fail_process":4,"320hash":"2101c88edc4579967ebbd88341f30e35","extname":"mp3","sqhash":"70bdf1e5cc99023c0d5bed6db2570b2a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"电瓶车之歌","othername":""}],"correctiontype":0,"timestamp":1650006412,"allowerr":0,"total":347,"istag":0,"istagresult":0,"forcecorrection":0,"correctiontip":""}
     * errcode : 0
     * status : 1
     * relative : {"priortype":1,"singer":[{"songcount":5,"imgurl":"http://singerimg.kugou.com/uploadpic/softhead/240/20210911/20210911175501753405.jpg","correctiontip":"","mvcount":0,"singername":"Alone","singerid":6999408,"albumcount":2}]}
     * black : {"isblock":1,"type":0}
     * correct : null
     */

    private String error;
    private DataBean data;
    private int errcode;
    private int status;
    private RelativeBean relative;
    private BlackBean black;
    private Object correct;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RelativeBean getRelative() {
        return relative;
    }

    public void setRelative(RelativeBean relative) {
        this.relative = relative;
    }

    public BlackBean getBlack() {
        return black;
    }

    public void setBlack(BlackBean black) {
        this.black = black;
    }

    public Object getCorrect() {
        return correct;
    }

    public void setCorrect(Object correct) {
        this.correct = correct;
    }

    public static class DataBean {
        /**
         * aggregation : []
         * tab :
         * info : [{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3614717,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":99076859,"pay_block_tpl":1,"hash_multitrack":"5DEEAA48FCCE6986F9AA8686385B2694","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Rentz","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Rentz - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"709287edb9a974559d5f35d7087320ba","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":256930565,"rp_publish":1,"album_id":"37486055","ownercount":1225684,"fold_type":0,"audio_id":72084935,"pkg_price_sq":1,"320filesize":9036483,"isnew":0,"duration":225,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":30328649,"fail_process":4,"320hash":"398d00ae35554541f19580f92e14352a","extname":"mp3","sqhash":"173b2ae3bfb666c54fb6980df2b33a0e","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3305483,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":71940519,"pay_block_tpl":1,"hash_multitrack":"5710175045106284E295B29DE893ADF5","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"나혼자 (Alone) (我一个人)","singername":"SISTAR","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"SISTAR - 나혼자 (Alone) (我一个人)","price_320":200,"songname":"나혼자 (Alone)","group":[{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3305487,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":105957821,"pay_block_tpl":1,"hash_multitrack":"84D27F209E0CAAB0F3293C2141B63F2D","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"나혼자 (Alone) (我一个人)","singername":"SISTAR","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"SISTAR - 나혼자 (Alone) (我一个人)","price_320":200,"songname":"나혼자 (Alone)","hash":"74b1ce9aa7f318ad54549478ff7208bc","mvhash":"f00e729257380a4c532abebd4c40394e","rp_type":"audio","privilege":8,"album_audio_id":264717346,"rp_publish":1,"album_id":"38305786","ownercount":2345,"fold_type":0,"audio_id":202908,"pkg_price_sq":1,"320filesize":8263319,"isnew":0,"duration":206,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27442581,"fail_process":4,"320hash":"63cdb89228c8c26986552c0cea736d34","extname":"mp3","sqhash":"a53b1747241d53c5ce152c7234188894","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"DJ Central Vol. 11 KPOP","othername":""}],"hash":"86546f08a254a5714146c742e787d1e3","mvhash":"f00e729257380a4c532abebd4c40394e","rp_type":"audio","privilege":8,"album_audio_id":164864141,"rp_publish":1,"album_id":"23195715","ownercount":30840,"fold_type":0,"audio_id":202908,"pkg_price_sq":1,"320filesize":8263315,"isnew":0,"duration":206,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27796702,"fail_process":4,"320hash":"0f7dcde6bea59135c3b53b52b6ddb24e","extname":"mp3","sqhash":"e90ee85845a614fd19fc39199bf7d7ea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"DJ Central KPOP Vol. 11","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":1,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6071280,"pay_block_tpl":1,"hash_multitrack":"8409A2EF4040824A5A17B44C7DD9986B","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":1,"musicpack_advance":0,"cpy_level":1,"cid":54320718,"pay_block_tpl":1,"hash_multitrack":"723A3F7F25797C13A45E17D2E74345E2","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":125351874,"rp_publish":1,"album_id":"13727483","ownercount":13713,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Different World","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2580332,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":22452137,"pay_block_tpl":1,"display_rate":1,"classmap":{"attr0":100667400},"display":32,"musicpack_advance":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":87891088,"rp_publish":1,"album_id":"3991455","ownercount":7754,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Ultra Music Festival China 2017","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2544636,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"hash_multitrack":"B1FDE7C64F0ECC897263E74A9AF30349","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"9ffe5aeee1c5708df0f9f6f0fa9721f6","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":53401024,"rp_publish":1,"album_id":"","ownercount":6998,"fold_type":0,"audio_id":24738847,"pkg_price_sq":0,"320filesize":6534832,"isnew":0,"duration":159,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"05948750eb596cac0d8ac7d5cfc352bd","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""}],"hash":"89f83b5ad5b6b56b76a12cc8786d0b64","mvhash":"0f6b8d122d2f738508af440f3db4ea52","rp_type":"audio","privilege":8,"album_audio_id":51877495,"rp_publish":1,"album_id":"1857732","ownercount":2837239,"fold_type":0,"audio_id":24172255,"pkg_price_sq":1,"320filesize":6450425,"isnew":0,"duration":161,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21504006,"fail_process":4,"320hash":"c6ee7ae4f3502cb23ddd9fbcd21c80f0","extname":"mp3","sqhash":"2cc44a2413415cea0cb3c71cc7968f29","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4134496,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"80C3F7A8ED5BA935821BE06487D3EF04","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Nzi Jian","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Nzi Jian - Alone<\/em> (Remix)","price_320":0,"songname":"Alone<\/em> (Remix)","group":[],"hash":"94cba54570989eaeb34946a1d9cb6021","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":380432895,"rp_publish":1,"album_id":"54570458","ownercount":90330,"fold_type":0,"audio_id":148536147,"pkg_price_sq":0,"320filesize":10336174,"isnew":0,"duration":258,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":28466599,"fail_process":0,"320hash":"f42814ebb7bc2f492d22933d4063bed4","extname":"mp3","sqhash":"a48b4cdda1cad0e02073322b6c843ce9","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3273238,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":187492141,"pay_block_tpl":1,"hash_multitrack":"3AB9182D0A174A2F2D3A48AC6CD2D53C","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"嘉滢","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"嘉滢 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"6126ec3b1a4a51a6242a3120f75eb958","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":360569138,"rp_publish":1,"album_id":"51608919","ownercount":372148,"fold_type":0,"audio_id":136575169,"pkg_price_sq":1,"320filesize":8182796,"isnew":0,"duration":204,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21655274,"fail_process":4,"320hash":"6ca20075d48461fc3c92ec099c5b280f","extname":"mp3","sqhash":"78da9ae5af2c64d27414f3002996b64c","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":1,"filesize":4382100,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":32251919,"pay_block_tpl":1,"hash_multitrack":"214E8357D4B0E2FF8AD74F56FD8ED6CB","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382142,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":32251393,"pay_block_tpl":1,"hash_multitrack":"4ABF9E969405F46B8B90F6A28B570ED5","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"eb006c793d4a85e0ef4568e9c3e88b35","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":39841025,"rp_publish":1,"album_id":"1767661","ownercount":43312,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954744,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"e8afcd59e93756c9050e60477f44982d","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat 027 - Cataclysm","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382140,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":37646072,"pay_block_tpl":1,"hash_multitrack":"0E5D963C93FB3BF3D19F0178FBDFE549","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"49fa48e993d84437fdd1812760c51a25","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":66798341,"rp_publish":1,"album_id":"2783717","ownercount":28265,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954665,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"d1b0feec5499f8c9a7cd9a542f13bf5c","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat - Best of Trap","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4382140,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":37646146,"pay_block_tpl":1,"hash_multitrack":"881C7E5D9C2242A5CC1672012F47AD88","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Marshmello - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"f70512e8c7fa3f4cde3236b2618cc8f4","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":109532246,"rp_publish":1,"album_id":"8710022","ownercount":365,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954966,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"39f181c570ccf16d339cbff60fbff088","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Monstercat - Best of 2016","othername":""}],"hash":"c9c1f7887bd1cfb268e64523567e696a","mvhash":"51811305220c016362399740af0d32ca","rp_type":"audio","privilege":8,"album_audio_id":40118863,"rp_publish":1,"album_id":"1802713","ownercount":653977,"fold_type":0,"audio_id":21523168,"pkg_price_sq":1,"320filesize":10954847,"isnew":0,"duration":273,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":34830024,"fail_process":4,"320hash":"94269582ac4e417f902b66a2c4b5535b","extname":"mp3","sqhash":"594b96c409afd5cc3e6fa376c07563c0","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2876237,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":136475913,"pay_block_tpl":1,"hash_multitrack":"D46C46DB2B727762E0B054EDF0DFB164","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"Tommo、MELISA","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Tommo、MELISA - I'm Alone<\/em>","price_320":200,"songname":"I'm Alone<\/em>","group":[],"hash":"43c0a5800193daae71133201927ba96d","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":298317791,"rp_publish":1,"album_id":"42210725","ownercount":3264111,"fold_type":0,"audio_id":97298290,"pkg_price_sq":1,"320filesize":7190203,"isnew":0,"duration":179,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21296033,"fail_process":4,"320hash":"f75495460a7bb43dcb8ecb96aac30d8e","extname":"mp3","sqhash":"818db26f06290163b54f449b0e090dc7","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"I'm Alone","othername":""},{"othername_original":"Lost Stories Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3878705,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":1,"classmap":{"attr0":100667400},"display":32,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Lost Stories、Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Lost Stories、Alan Walker - Alone<\/em> (Lost Stories Remix)","price_320":0,"songname":"Alone<\/em> (Lost Stories Remix)","group":[],"hash":"b2fef2c0e5783ac5a0e3ce9530840983","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":57540951,"rp_publish":1,"album_id":"2138503","ownercount":72045,"fold_type":0,"audio_id":26399162,"pkg_price_sq":0,"320filesize":9696697,"isnew":0,"duration":242,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":32880624,"fail_process":0,"320hash":"fdf92b500f63b510363898502ae019e2","extname":"mp3","sqhash":"d5d9380eaaf779baefbdd65826905b5a","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Lost Stories Remix)","othername":""},{"othername_original":"Restrung","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2967310,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6205725,"pay_block_tpl":1,"hash_multitrack":"2B6821C7053F96EE3DDF48AF38F9B662","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Alan Walker - Alone<\/em> (Restrung)","price_320":200,"songname":"Alone<\/em> (Restrung)","group":[],"hash":"9c13c704c099600cb6856a95ad5b4114","mvhash":"f3318802d5acd7466abf9a1c35d4ccee","rp_type":"audio","privilege":8,"album_audio_id":53779586,"rp_publish":1,"album_id":"1857732","ownercount":26465,"fold_type":0,"audio_id":25087851,"pkg_price_sq":1,"320filesize":7417271,"isnew":0,"duration":185,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":20223646,"fail_process":4,"320hash":"ead85489b7a713e32e2b97f06675f281","extname":"mp3","sqhash":"b304cb285d5e2c27bc21ae3bece4886a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":0,"isoriginal":1,"filesize":3276170,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":31990365,"pay_block_tpl":1,"hash_multitrack":"47567A43531A8DD8380D329EAE91DF5D","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"26F27CE542BC42980EC6C571F103FC22","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960129}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Bazzi","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Bazzi - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3143933,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"544BFE55B7341B41FD6ACF6286910E4A","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Bazzi","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Bazzi - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"282cce735729b74802ddd662e821dd00","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":119211956,"rp_publish":1,"album_id":"","ownercount":6305,"fold_type":0,"audio_id":25870965,"pkg_price_sq":0,"320filesize":7859425,"isnew":0,"duration":196,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"37b4e0dfda967bacd8df25355ae703d9","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""}],"hash":"b60ebd7e858b731b759cdf21ecb0a623","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":105855915,"rp_publish":1,"album_id":"8305866","ownercount":28364,"fold_type":0,"audio_id":36707045,"pkg_price_sq":0,"320filesize":8190116,"isnew":0,"duration":204,"pkg_price_320":1,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":4,"320hash":"25dce0dfbc93e10345379bfaa3fb4e5f","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":10,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174728,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":55035721,"pay_block_tpl":1,"hash_multitrack":"9E2F8D4BF1DCD7A219F53B5CBA02836E","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174694,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":81559494,"pay_block_tpl":1,"hash_multitrack":"838592610F3AF7E1EE58E91F818FD8E2","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"49326484665b0786b61a2b746eb6d4dd","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":146360690,"rp_publish":1,"album_id":"19277328","ownercount":14550,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936294,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25780639,"fail_process":4,"320hash":"f91fa28069a80b73a2d45c806583d64f","extname":"mp3","sqhash":"be6006f26350ca48a2b14a8495750078","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Trance 100 - Best Of 2014","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174744,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":81551469,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"8551b248e96215ac7256fb55686fe171","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":145215134,"rp_publish":1,"album_id":"19024990","ownercount":185,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936012,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26032240,"fail_process":4,"320hash":"ed206816aa3a2835e3877f9ee3e8d2a5","extname":"mp3","sqhash":"f91703ae571c20cc56d71161c2d07eea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems Top 100 (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174728,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":168269247,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"249ba70a9280e3d69d1298e628d7495a","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":336030458,"rp_publish":1,"album_id":"48614128","ownercount":5,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7935848,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25780653,"fail_process":4,"320hash":"855fed68fa4e78b033ca2bcebef8ed9e","extname":"mp3","sqhash":"fa2910281b4f4cc28af69ac9610111b6","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3174736,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":81547109,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Lauren Evans、Armin van Buuren","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Lauren Evans、Armin van Buuren - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"5cb5af2b79fe3563125abcdea45f4890","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":146345161,"rp_publish":1,"album_id":"19275489","ownercount":5,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7935936,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26032240,"fail_process":4,"320hash":"1d0417ccd513dad8cf4bf33cf655f109","extname":"mp3","sqhash":"f91703ae571c20cc56d71161c2d07eea","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armada Stream 40 - Best Of 2014 - Armada Music","othername":""}],"hash":"5bf20276f9c9a8fe96ea253e65dfc4d5","mvhash":"5b428da108128a0751d67d9397d1d400","rp_type":"audio","privilege":8,"album_audio_id":31741257,"rp_publish":1,"album_id":"930477","ownercount":32163,"fold_type":0,"audio_id":2525276,"pkg_price_sq":1,"320filesize":7936328,"isnew":0,"duration":198,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25829563,"fail_process":4,"320hash":"cb5e66b1a279b677276593712481dd7c","extname":"mp3","sqhash":"03c982c18e41a431fa6c15d9bb5dfb37","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Armin Anthems (Ultimate Singles Collected)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2830463,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"I'm Alone<\/em>","singername":"Melisa (259601382)、Tommo","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Melisa (259601382)、Tommo - I'm Alone<\/em>","price_320":0,"songname":"I'm Alone<\/em>","group":[],"hash":"f41e45213eceefccf7d102ea5a87b917","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":365303359,"rp_publish":1,"album_id":"","ownercount":357169,"fold_type":0,"audio_id":140022218,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":176,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"","pay_type_320":1,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4227885,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":101139509,"pay_block_tpl":1,"hash_multitrack":"9701358BB842E27D579CA0AF34F7F6A9","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"DJ SK、Bayza","pay_type":1,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"DJ SK、Bayza - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"180c5ac09383aa0afdcf489f6ca124ee","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":258963262,"rp_publish":1,"album_id":"37776583","ownercount":61568,"fold_type":0,"audio_id":73400664,"pkg_price_sq":1,"320filesize":10569371,"isnew":0,"duration":264,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":25619349,"fail_process":4,"320hash":"9cc98b15f5940cd4c3c31253c15484b2","extname":"mp3","sqhash":"6f4bdabfe45ee60021f3eef82095ff26","pay_type_sq":1,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3444920,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":4442150,"pay_block_tpl":1,"hash_multitrack":"2046A64F915D7606FD2DEBDB167EF669","display_rate":1,"classmap":{"attr0":100667400},"display":32,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"黄子韬","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"黄子韬 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"c57f25ee7c9f46b415cc98fd6b4f0c4c","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":28051991,"rp_publish":1,"album_id":"550756","ownercount":59489,"fold_type":0,"audio_id":16713976,"pkg_price_sq":1,"320filesize":8601766,"isnew":0,"duration":215,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":27541124,"fail_process":4,"320hash":"6f1c7b68796ef28cb7d6631a9047f720","extname":"mp3","sqhash":"6cd6c800df325215e65c9f131438cdcb","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Z.TAO","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5216768,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":69410164,"pay_block_tpl":1,"hash_multitrack":"6411128AC2CB65959FC700C2218ED446","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"A7437AC7DEFF67B22D6C3217B92D076A","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960143}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5281608,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":6835084,"pay_block_tpl":1,"hash_multitrack":"AB7388B13D8AFBEAAB4392CE349FDE38","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"443CEE5117DE5D7D6C47123556A9BF9F","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960199}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"b4f4d3922b7a2374ae00d9c2d4cc8b74","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":10,"album_audio_id":64388539,"rp_publish":1,"album_id":"1989662","ownercount":183,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13203606,"isnew":0,"duration":330,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35164198,"fail_process":4,"320hash":"088d07344dce939e600b6d73ff243d4d","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"At Last...The Duets Album / Breathless","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":5249369,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":6872991,"pay_block_tpl":1,"hash_multitrack":"7B372FC606AB38F3C04ED4C21B4A2F55","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","hash":"759aebeb472097115f0ce54b274ea5f9","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":8,"album_audio_id":28178401,"rp_publish":1,"album_id":"560498","ownercount":36073,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13122743,"isnew":0,"duration":328,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35002096,"fail_process":4,"320hash":"d10136b16804b894202d68a8dcae038d","extname":"mp3","sqhash":"298d60e9360db4d58a1ad8ceda243c41","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Breathless","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":5252158,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"ABA10E65D238F6659ABFBE8880A4938D","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Kenny G","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Kenny G - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","hash":"2759f1f786790f88d9dedf5e163ca775","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":0,"album_audio_id":146796837,"rp_publish":1,"album_id":"19336439","ownercount":183,"fold_type":0,"audio_id":520413,"pkg_price_sq":0,"320filesize":13165706,"isnew":0,"duration":328,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":35164198,"fail_process":0,"320hash":"0efffd7655647a5b1465629e6666437b","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Going Home","othername":""}],"hash":"e813d32a35a8cb07cc78a155c5de6052","mvhash":"3e2ad08725d94c41ee0406394d8323e7","rp_type":"audio","privilege":10,"album_audio_id":150190713,"rp_publish":1,"album_id":"20010551","ownercount":3783,"fold_type":0,"audio_id":520413,"pkg_price_sq":1,"320filesize":13041591,"isnew":0,"duration":326,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":35164198,"fail_process":4,"320hash":"7ed549c75dc7e07d2f2be9e90bc4d9cc","extname":"mp3","sqhash":"ffef00f475781f88e4f9ee25a1454ca3","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Breathless","othername":""},{"othername_original":"Live","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":1267880,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":190506139,"pay_block_tpl":1,"hash_multitrack":"1D1936AC981D60B19DE050A14624D600","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"音质推荐","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"音质推荐 - I'm Alone<\/em> (Live)","price_320":200,"songname":"I'm Alone<\/em> (Live)","group":[],"hash":"32291d344c1c71bca399618ea367249b","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":365998931,"rp_publish":1,"album_id":"52202915","ownercount":34467,"fold_type":0,"audio_id":140495319,"pkg_price_sq":1,"320filesize":3169385,"isnew":0,"duration":79,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":5895876,"fail_process":4,"320hash":"e026eb5a62e46d28fbd558755472f771","extname":"mp3","sqhash":"6b6612126d8bf8d15ace1e054dbd1051","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"治愈旋律","othername":""},{"othername_original":"DJ版","pay_type_320":3,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2168166,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":200067531,"pay_block_tpl":1,"hash_multitrack":"7B72273B9D76E926FEA5E7CC6B655352","display_rate":4,"classmap":{"attr0":102760456},"display":64,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"I'm Alone<\/em>","singername":"水牛哥","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"水牛哥 - I'm Alone<\/em> (DJ版)","price_320":200,"songname":"I'm Alone<\/em> (DJ版)","group":[],"hash":"b71e97bd2e0240eadb0f77a7205c6506","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":386598999,"rp_publish":1,"album_id":"55380555","ownercount":27943,"fold_type":0,"audio_id":151981079,"pkg_price_sq":0,"320filesize":5420097,"isnew":0,"duration":135,"pkg_price_320":1,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":4,"320hash":"0702ff9831299eae4432757aefbe9de2","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":10,"sqprivilege":0,"album_name":"Temple","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2488317,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":80330461,"pay_block_tpl":1,"hash_multitrack":"23636538DCDF00B39C939404217DF02E","display_rate":0,"classmap":{"attr0":100663304},"display":0,"cpy_attr0":0},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"Alone<\/em> (孤独)","singername":"ＴＲＡ＄Ｈ","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"ＴＲＡ＄Ｈ - Alone<\/em> (孤独)","price_320":200,"songname":"Alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2488573,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"CEC0479CBD375D026FD987E4F5D67C7D","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"ALONE<\/em> (孤独)","singername":"ＴＲＡ＄Ｈ","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"ＴＲＡ＄Ｈ - ALONE<\/em> (孤独)","price_320":0,"songname":"ALONE<\/em>","hash":"c9ade69315619b78339fe31826e461c7","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":119204526,"rp_publish":1,"album_id":"15389225","ownercount":27308,"fold_type":0,"audio_id":35666033,"pkg_price_sq":0,"320filesize":6295253,"isnew":0,"duration":155,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":9582188,"fail_process":0,"320hash":"8010130a5e9445380998abd9f0da18cb","extname":"mp3","sqhash":"24626803f92e2b24ffae374481c0b34a","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Awake Vol. 1","othername":""}],"hash":"12b7ff0c24221dbb8afad3ef15dcfd58","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":230080234,"rp_publish":1,"album_id":"32726914","ownercount":12402,"fold_type":0,"audio_id":35666033,"pkg_price_sq":1,"320filesize":6220428,"isnew":0,"duration":155,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":9582188,"fail_process":4,"320hash":"106a90a7d3c0f57cbf7db2f4f809dcaf","extname":"mp3","sqhash":"24626803f92e2b24ffae374481c0b34a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Awake","othername":""},{"othername_original":"Gammer Flip","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2593480,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"C69FCE292D89D97A1C2198DC656AF517","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Marshmello - Alone<\/em> (Gammer Flip)","price_320":0,"songname":"Alone<\/em> (Gammer Flip)","group":[],"hash":"6c827842cf5b4da5b3861dd1efd06b07","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":96859921,"rp_publish":1,"album_id":"18108831","ownercount":24387,"fold_type":0,"audio_id":27451685,"pkg_price_sq":0,"320filesize":6483636,"isnew":0,"duration":162,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":19155154,"fail_process":0,"320hash":"42c1ecbab23548e6c173f37f3aab7131","extname":"mp3","sqhash":"b41b680fdf972d7a38442bc4b0605b08","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Gammer Flip)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2935569,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"9B58CAB00F6A604A21676058FCBEC5BA","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"alone<\/em>","singername":"中川幸太郎","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"中川幸太郎 - alone<\/em>","price_320":0,"songname":"alone<\/em>","group":[{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2935081,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"alone<\/em>","singername":"中川幸太郎","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"中川幸太郎 - alone<\/em>","price_320":0,"songname":"alone<\/em>","hash":"9ca5ca2f96ff0aa0d8eec7ee9e34c976","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":143751091,"rp_publish":1,"album_id":"18750675","ownercount":183,"fold_type":0,"audio_id":7639709,"pkg_price_sq":0,"320filesize":7337445,"isnew":0,"duration":183,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":14729973,"fail_process":0,"320hash":"9ed06a15d0356cdb506cf38aa2085e92","extname":"mp3","sqhash":"dff58712681286e0174130e781c7ecb7","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"妖狐×仆SS 2","othername":""}],"hash":"7dfb6441af17857d6bd1d4b99d77b76c","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":28245531,"rp_publish":1,"album_id":"565863","ownercount":20883,"fold_type":0,"audio_id":7639709,"pkg_price_sq":0,"320filesize":7338560,"isnew":0,"duration":183,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":14729973,"fail_process":0,"320hash":"0084fe1c3e7b947f235b96c1956cf7f2","extname":"mp3","sqhash":"dff58712681286e0174130e781c7ecb7","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"妖狐×僕SS 2","othername":""},{"othername_original":"Reimx","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4437181,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Marshmello","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Marshmello - Alone<\/em> (Reimx)","price_320":0,"songname":"Alone<\/em> (Reimx)","group":[],"hash":"58d0998ac7547f791872bea687c0f94e","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":50149861,"rp_publish":1,"album_id":"","ownercount":19624,"fold_type":0,"audio_id":21694823,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":277,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"Remix","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":2413920,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":21569369,"pay_block_tpl":1,"hash_multitrack":"E5DC160BDE4C552BB01D5DAD5F2808F5","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"AnJuy","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"AnJuy - Alone<\/em> (Remix)","price_320":200,"songname":"Alone<\/em> (Remix)","group":[],"hash":"9dcea8ca279e907804e811190ba0134e","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":56985289,"rp_publish":1,"album_id":"2086609","ownercount":19020,"fold_type":0,"audio_id":24752123,"pkg_price_sq":1,"320filesize":6034491,"isnew":0,"duration":150,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":21179685,"fail_process":4,"320hash":"02bfb66588ba1ed721cd72dcf909394f","extname":"mp3","sqhash":"4e1af32a380741f3338b02a3b6d610d9","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4544514,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"C70325A54EA06AF28BAAB225859B8D32","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Wisp X、初音ミク","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Wisp X、初音ミク - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","group":[],"hash":"7153ca0d02de503eeeae40e0c15297d0","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":54110297,"rp_publish":1,"album_id":"1978950","ownercount":18004,"fold_type":0,"audio_id":16408688,"pkg_price_sq":0,"320filesize":11361219,"isnew":0,"duration":284,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"d6f71a770f6d9f48bae0cbca6bf6ca23","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone","othername":""},{"othername_original":"2016世界巡回演唱会挪威站","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":3478807,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"78B80FE02DBA338C57EAD259B8A97AC9","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em> (2016世界巡回演唱会挪威站)","price_320":0,"songname":"Alone<\/em> (2016世界巡回演唱会挪威站)","group":[],"hash":"def4b1fc4974b7692a4bb94c61e1c446","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":56084851,"rp_publish":1,"album_id":"","ownercount":17208,"fold_type":0,"audio_id":26316047,"pkg_price_sq":0,"320filesize":8696785,"isnew":0,"duration":217,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"c6dd6e6a0fea8f34761236499009821a","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"","othername":""},{"othername_original":"Raspo Remix","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2659518,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"B3537C4023EC04834DC71B8069553C57","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":1,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"Alan Walker","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"Alan Walker - Alone<\/em> (Raspo Remix)","price_320":0,"songname":"Alone<\/em> (Raspo Remix)","group":[],"hash":"06ec186cd44c3e6dc4e700126394507c","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":65675841,"rp_publish":1,"album_id":"2692422","ownercount":14126,"fold_type":0,"audio_id":26605817,"pkg_price_sq":0,"320filesize":6648931,"isnew":0,"duration":166,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"3f8b9eeda7ae3bb97a712a8512ce64eb","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Alone (Raspo Remix)","othername":""},{"othername_original":"","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":4512925,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"musicpack_advance":0,"hash_multitrack":"E662325A003F9F726F9543E1FD31BA7E","cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"pay_block_tpl":1},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"Alone<\/em>","singername":"苏可可、7snxx","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"苏可可、7snxx - Alone<\/em>","price_320":0,"songname":"Alone<\/em>","group":[],"hash":"15ecb5560d37aeda312b99cdf2bcbd65","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":375337172,"rp_publish":1,"album_id":"53608628","ownercount":12063,"fold_type":0,"audio_id":146342819,"pkg_price_sq":0,"320filesize":11281983,"isnew":0,"duration":282,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":31657928,"fail_process":0,"320hash":"2310f051ab819fce4ee912912259f78e","extname":"mp3","sqhash":"ca3954ecf15b08ced7ef3bcf232e3f70","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Payphone","othername":""},{"othername_original":"Mesto Remix Radio Edit","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3166816,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":36372041,"pay_block_tpl":1,"hash_multitrack":"B2C8ADB2C35968F6F74CA1C4B99CA250","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"E8FF68873CF5F5174BAECA19E0BCDEB7","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960281}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"NERVO、Askery、Brielle Von Hugel","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"NERVO、Askery、Brielle Von Hugel - Alone<\/em> (Mesto Remix Radio Edit)","price_320":200,"songname":"Alone<\/em> (Mesto Remix Radio Edit)","group":[],"hash":"22e758038178646a73d9158d9fec40cf","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":143373936,"rp_publish":1,"album_id":"18679034","ownercount":11194,"fold_type":0,"audio_id":24591679,"pkg_price_sq":1,"320filesize":7916077,"isnew":0,"duration":197,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":26611668,"fail_process":4,"320hash":"adb916c8abe382e0f62c2e356e59dcef","extname":"mp3","sqhash":"01bd4dd22adad0956570029109501fba","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"Alone (feat. Brielle Von Hugel)","othername":""},{"othername_original":"DJ版","pay_type_320":0,"m4afilesize":0,"price_sq":0,"isoriginal":0,"filesize":2558966,"source":"","bitrate":128,"topic":"","trans_param":{"cid":-1,"pay_block_tpl":1,"cpy_attr0":0,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"musicpack_advance":0},"price":0,"Accompany":0,"old_cpy":1,"songname_original":"I'm alone<\/em>","singername":"大雅、杳葵","pay_type":0,"sourceid":0,"topic_url":"","fail_process_320":0,"pkg_price":0,"feetype":0,"filename":"大雅、杳葵 - I'm alone<\/em> (DJ版)","price_320":0,"songname":"I'm alone<\/em> (DJ版)","group":[],"hash":"65ce05b58bf515ffe9ffe03e8e8dade2","mvhash":"","rp_type":"audio","privilege":0,"album_audio_id":371865259,"rp_publish":1,"album_id":"53101541","ownercount":10231,"fold_type":0,"audio_id":143977777,"pkg_price_sq":0,"320filesize":0,"isnew":0,"duration":159,"pkg_price_320":0,"srctype":1,"fail_process_sq":0,"sqfilesize":0,"fail_process":0,"320hash":"","extname":"mp3","sqhash":"","pay_type_sq":0,"320privilege":0,"sqprivilege":0,"album_name":"Everyday","othername":""},{"othername_original":"","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":3456720,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":26042577,"pay_block_tpl":1,"hash_multitrack":"2CAF05E161977FD6C022E9D991E0FE31","musicpack_advance":1,"display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_offset":{"offset_hash":"4CCB58836830775853773329DBAFC260","start_byte":0,"start_ms":0,"file_type":0,"end_ms":60000,"end_byte":960121}},"price":200,"Accompany":1,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"林在莞","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"林在莞 - Alone<\/em>","price_320":200,"songname":"Alone<\/em>","group":[],"hash":"5e550b3d0104900b471e576f1c4fb6db","mvhash":"","rp_type":"audio","privilege":10,"album_audio_id":64546550,"rp_publish":1,"album_id":"557391","ownercount":8824,"fold_type":0,"audio_id":2718884,"pkg_price_sq":1,"320filesize":8641072,"isnew":0,"duration":216,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":24927168,"fail_process":4,"320hash":"1d9ad33f340294d1c35a777104f917e3","extname":"mp3","sqhash":"a17e06de9a375e2ae9ece1184714f350","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"마이걸 (我的女孩)","othername":""},{"othername_original":"Remix","pay_type_320":3,"m4afilesize":0,"price_sq":200,"isoriginal":0,"filesize":4263389,"source":"","bitrate":128,"topic":"","trans_param":{"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":140574964,"pay_block_tpl":1,"hash_multitrack":"9F636DB75460DD0E356A763E2AA8B451","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}},"price":200,"Accompany":0,"old_cpy":0,"songname_original":"Alone<\/em>","singername":"Y2002","pay_type":3,"sourceid":0,"topic_url":"","fail_process_320":4,"pkg_price":1,"feetype":0,"filename":"Y2002 - Alone<\/em> (Remix)","price_320":200,"songname":"Alone<\/em> (Remix)","group":[],"hash":"ae871608cd29f2eed650c74509659105","mvhash":"","rp_type":"audio","privilege":8,"album_audio_id":304703350,"rp_publish":1,"album_id":"43047438","ownercount":8823,"fold_type":0,"audio_id":102171542,"pkg_price_sq":1,"320filesize":10657826,"isnew":0,"duration":266,"pkg_price_320":1,"srctype":1,"fail_process_sq":4,"sqfilesize":32406877,"fail_process":4,"320hash":"2101c88edc4579967ebbd88341f30e35","extname":"mp3","sqhash":"70bdf1e5cc99023c0d5bed6db2570b2a","pay_type_sq":3,"320privilege":10,"sqprivilege":10,"album_name":"电瓶车之歌","othername":""}]
         * correctiontype : 0
         * timestamp : 1650006412
         * allowerr : 0
         * total : 347
         * istag : 0
         * istagresult : 0
         * forcecorrection : 0
         * correctiontip :
         */

        private String tab;
        private int correctiontype;
        private int timestamp;
        private int allowerr;
        private int total;
        private int istag;
        private int istagresult;
        private int forcecorrection;
        private String correctiontip;
        private List<?> aggregation;
        private List<InfoBean> info;

        public String getTab() {
            return tab;
        }

        public void setTab(String tab) {
            this.tab = tab;
        }

        public int getCorrectiontype() {
            return correctiontype;
        }

        public void setCorrectiontype(int correctiontype) {
            this.correctiontype = correctiontype;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getAllowerr() {
            return allowerr;
        }

        public void setAllowerr(int allowerr) {
            this.allowerr = allowerr;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getIstag() {
            return istag;
        }

        public void setIstag(int istag) {
            this.istag = istag;
        }

        public int getIstagresult() {
            return istagresult;
        }

        public void setIstagresult(int istagresult) {
            this.istagresult = istagresult;
        }

        public int getForcecorrection() {
            return forcecorrection;
        }

        public void setForcecorrection(int forcecorrection) {
            this.forcecorrection = forcecorrection;
        }

        public String getCorrectiontip() {
            return correctiontip;
        }

        public void setCorrectiontip(String correctiontip) {
            this.correctiontip = correctiontip;
        }

        public List<?> getAggregation() {
            return aggregation;
        }

        public void setAggregation(List<?> aggregation) {
            this.aggregation = aggregation;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * othername_original :
             * pay_type_320 : 3
             * m4afilesize : 0
             * price_sq : 200
             * isoriginal : 0
             * filesize : 3614717
             * source :
             * bitrate : 128
             * topic :
             * trans_param : {"cpy_grade":5,"cpy_attr0":0,"cpy_level":1,"cid":99076859,"pay_block_tpl":1,"hash_multitrack":"5DEEAA48FCCE6986F9AA8686385B2694","musicpack_advance":0,"display_rate":0,"appid_block":"3124","display":0,"classmap":{"attr0":100663304}}
             * price : 200
             * Accompany : 1
             * old_cpy : 0
             * songname_original : Alone</em>
             * singername : Rentz
             * pay_type : 3
             * sourceid : 0
             * topic_url :
             * fail_process_320 : 4
             * pkg_price : 1
             * feetype : 0
             * filename : Rentz - Alone</em>
             * price_320 : 200
             * songname : Alone</em>
             * group : []
             * hash : 709287edb9a974559d5f35d7087320ba
             * mvhash :
             * rp_type : audio
             * privilege : 8
             * album_audio_id : 256930565
             * rp_publish : 1
             * album_id : 37486055
             * ownercount : 1225684
             * fold_type : 0
             * audio_id : 72084935
             * pkg_price_sq : 1
             * 320filesize : 9036483
             * isnew : 0
             * duration : 225
             * pkg_price_320 : 1
             * srctype : 1
             * fail_process_sq : 4
             * sqfilesize : 30328649
             * fail_process : 4
             * 320hash : 398d00ae35554541f19580f92e14352a
             * extname : mp3
             * sqhash : 173b2ae3bfb666c54fb6980df2b33a0e
             * pay_type_sq : 3
             * 320privilege : 10
             * sqprivilege : 10
             * album_name : Alone
             * othername :
             */

            private String othername_original;
            private int pay_type_320;
            private int m4afilesize;
            private int price_sq;
            private int isoriginal;
            private int filesize;
            private String source;
            private int bitrate;
            private String topic;
            private TransParamBean trans_param;
            private int price;
            private int Accompany;
            private int old_cpy;
            private String songname_original;
            private String singername;
            private int pay_type;
            private int sourceid;
            private String topic_url;
            private int fail_process_320;
            private int pkg_price;
            private int feetype;
            private String filename;
            private int price_320;
            private String songname;
            private String hash;
            private String mvhash;
            private String rp_type;
            private int privilege;
            private int album_audio_id;
            private int rp_publish;
            private String album_id;
            private int ownercount;
            private int fold_type;
            private int audio_id;
            private int pkg_price_sq;
            @JSONField(name = "320filesize")
            private int _$320filesize;
            private int isnew;
            private int duration;
            private int pkg_price_320;
            private int srctype;
            private int fail_process_sq;
            private int sqfilesize;
            private int fail_process;
            @JSONField(name = "320hash")
            private String _$320hash;
            private String extname;
            private String sqhash;
            private int pay_type_sq;
            @JSONField(name = "320privilege")
            private int _$320privilege;
            private int sqprivilege;
            private String album_name;
            private String othername;
            private List<?> group;

            public String getOthername_original() {
                return othername_original;
            }

            public void setOthername_original(String othername_original) {
                this.othername_original = othername_original;
            }

            public int getPay_type_320() {
                return pay_type_320;
            }

            public void setPay_type_320(int pay_type_320) {
                this.pay_type_320 = pay_type_320;
            }

            public int getM4afilesize() {
                return m4afilesize;
            }

            public void setM4afilesize(int m4afilesize) {
                this.m4afilesize = m4afilesize;
            }

            public int getPrice_sq() {
                return price_sq;
            }

            public void setPrice_sq(int price_sq) {
                this.price_sq = price_sq;
            }

            public int getIsoriginal() {
                return isoriginal;
            }

            public void setIsoriginal(int isoriginal) {
                this.isoriginal = isoriginal;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public int getBitrate() {
                return bitrate;
            }

            public void setBitrate(int bitrate) {
                this.bitrate = bitrate;
            }

            public String getTopic() {
                return topic;
            }

            public void setTopic(String topic) {
                this.topic = topic;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getAccompany() {
                return Accompany;
            }

            public void setAccompany(int Accompany) {
                this.Accompany = Accompany;
            }

            public int getOld_cpy() {
                return old_cpy;
            }

            public void setOld_cpy(int old_cpy) {
                this.old_cpy = old_cpy;
            }

            public String getSongname_original() {
                return songname_original;
            }

            public void setSongname_original(String songname_original) {
                this.songname_original = songname_original;
            }

            public String getSingername() {
                return singername;
            }

            public void setSingername(String singername) {
                this.singername = singername;
            }

            public int getPay_type() {
                return pay_type;
            }

            public void setPay_type(int pay_type) {
                this.pay_type = pay_type;
            }

            public int getSourceid() {
                return sourceid;
            }

            public void setSourceid(int sourceid) {
                this.sourceid = sourceid;
            }

            public String getTopic_url() {
                return topic_url;
            }

            public void setTopic_url(String topic_url) {
                this.topic_url = topic_url;
            }

            public int getFail_process_320() {
                return fail_process_320;
            }

            public void setFail_process_320(int fail_process_320) {
                this.fail_process_320 = fail_process_320;
            }

            public int getPkg_price() {
                return pkg_price;
            }

            public void setPkg_price(int pkg_price) {
                this.pkg_price = pkg_price;
            }

            public int getFeetype() {
                return feetype;
            }

            public void setFeetype(int feetype) {
                this.feetype = feetype;
            }

            public String getFilename() {
                return filename;
            }

            public void setFilename(String filename) {
                this.filename = filename;
            }

            public int getPrice_320() {
                return price_320;
            }

            public void setPrice_320(int price_320) {
                this.price_320 = price_320;
            }

            public String getSongname() {
                return songname;
            }

            public void setSongname(String songname) {
                this.songname = songname;
            }

            public String getHash() {
                return hash;
            }

            public void setHash(String hash) {
                this.hash = hash;
            }

            public String getMvhash() {
                return mvhash;
            }

            public void setMvhash(String mvhash) {
                this.mvhash = mvhash;
            }

            public String getRp_type() {
                return rp_type;
            }

            public void setRp_type(String rp_type) {
                this.rp_type = rp_type;
            }

            public int getPrivilege() {
                return privilege;
            }

            public void setPrivilege(int privilege) {
                this.privilege = privilege;
            }

            public int getAlbum_audio_id() {
                return album_audio_id;
            }

            public void setAlbum_audio_id(int album_audio_id) {
                this.album_audio_id = album_audio_id;
            }

            public int getRp_publish() {
                return rp_publish;
            }

            public void setRp_publish(int rp_publish) {
                this.rp_publish = rp_publish;
            }

            public String getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(String album_id) {
                this.album_id = album_id;
            }

            public int getOwnercount() {
                return ownercount;
            }

            public void setOwnercount(int ownercount) {
                this.ownercount = ownercount;
            }

            public int getFold_type() {
                return fold_type;
            }

            public void setFold_type(int fold_type) {
                this.fold_type = fold_type;
            }

            public int getAudio_id() {
                return audio_id;
            }

            public void setAudio_id(int audio_id) {
                this.audio_id = audio_id;
            }

            public int getPkg_price_sq() {
                return pkg_price_sq;
            }

            public void setPkg_price_sq(int pkg_price_sq) {
                this.pkg_price_sq = pkg_price_sq;
            }

            public int get_$320filesize() {
                return _$320filesize;
            }

            public void set_$320filesize(int _$320filesize) {
                this._$320filesize = _$320filesize;
            }

            public int getIsnew() {
                return isnew;
            }

            public void setIsnew(int isnew) {
                this.isnew = isnew;
            }

            public int getDuration() {
                return duration;
            }

            public void setDuration(int duration) {
                this.duration = duration;
            }

            public int getPkg_price_320() {
                return pkg_price_320;
            }

            public void setPkg_price_320(int pkg_price_320) {
                this.pkg_price_320 = pkg_price_320;
            }

            public int getSrctype() {
                return srctype;
            }

            public void setSrctype(int srctype) {
                this.srctype = srctype;
            }

            public int getFail_process_sq() {
                return fail_process_sq;
            }

            public void setFail_process_sq(int fail_process_sq) {
                this.fail_process_sq = fail_process_sq;
            }

            public int getSqfilesize() {
                return sqfilesize;
            }

            public void setSqfilesize(int sqfilesize) {
                this.sqfilesize = sqfilesize;
            }

            public int getFail_process() {
                return fail_process;
            }

            public void setFail_process(int fail_process) {
                this.fail_process = fail_process;
            }

            public String get_$320hash() {
                return _$320hash;
            }

            public void set_$320hash(String _$320hash) {
                this._$320hash = _$320hash;
            }

            public String getExtname() {
                return extname;
            }

            public void setExtname(String extname) {
                this.extname = extname;
            }

            public String getSqhash() {
                return sqhash;
            }

            public void setSqhash(String sqhash) {
                this.sqhash = sqhash;
            }

            public int getPay_type_sq() {
                return pay_type_sq;
            }

            public void setPay_type_sq(int pay_type_sq) {
                this.pay_type_sq = pay_type_sq;
            }

            public int get_$320privilege() {
                return _$320privilege;
            }

            public void set_$320privilege(int _$320privilege) {
                this._$320privilege = _$320privilege;
            }

            public int getSqprivilege() {
                return sqprivilege;
            }

            public void setSqprivilege(int sqprivilege) {
                this.sqprivilege = sqprivilege;
            }

            public String getAlbum_name() {
                return album_name;
            }

            public void setAlbum_name(String album_name) {
                this.album_name = album_name;
            }

            public String getOthername() {
                return othername;
            }

            public void setOthername(String othername) {
                this.othername = othername;
            }

            public List<?> getGroup() {
                return group;
            }

            public void setGroup(List<?> group) {
                this.group = group;
            }

            public static class TransParamBean {
                /**
                 * cpy_grade : 5
                 * cpy_attr0 : 0
                 * cpy_level : 1
                 * cid : 99076859
                 * pay_block_tpl : 1
                 * hash_multitrack : 5DEEAA48FCCE6986F9AA8686385B2694
                 * musicpack_advance : 0
                 * display_rate : 0
                 * appid_block : 3124
                 * display : 0
                 * classmap : {"attr0":100663304}
                 */

                private int cpy_grade;
                private int cpy_attr0;
                private int cpy_level;
                private int cid;
                private int pay_block_tpl;
                private String hash_multitrack;
                private int musicpack_advance;
                private int display_rate;
                private String appid_block;
                private int display;
                private ClassmapBean classmap;

                public int getCpy_grade() {
                    return cpy_grade;
                }

                public void setCpy_grade(int cpy_grade) {
                    this.cpy_grade = cpy_grade;
                }

                public int getCpy_attr0() {
                    return cpy_attr0;
                }

                public void setCpy_attr0(int cpy_attr0) {
                    this.cpy_attr0 = cpy_attr0;
                }

                public int getCpy_level() {
                    return cpy_level;
                }

                public void setCpy_level(int cpy_level) {
                    this.cpy_level = cpy_level;
                }

                public int getCid() {
                    return cid;
                }

                public void setCid(int cid) {
                    this.cid = cid;
                }

                public int getPay_block_tpl() {
                    return pay_block_tpl;
                }

                public void setPay_block_tpl(int pay_block_tpl) {
                    this.pay_block_tpl = pay_block_tpl;
                }

                public String getHash_multitrack() {
                    return hash_multitrack;
                }

                public void setHash_multitrack(String hash_multitrack) {
                    this.hash_multitrack = hash_multitrack;
                }

                public int getMusicpack_advance() {
                    return musicpack_advance;
                }

                public void setMusicpack_advance(int musicpack_advance) {
                    this.musicpack_advance = musicpack_advance;
                }

                public int getDisplay_rate() {
                    return display_rate;
                }

                public void setDisplay_rate(int display_rate) {
                    this.display_rate = display_rate;
                }

                public String getAppid_block() {
                    return appid_block;
                }

                public void setAppid_block(String appid_block) {
                    this.appid_block = appid_block;
                }

                public int getDisplay() {
                    return display;
                }

                public void setDisplay(int display) {
                    this.display = display;
                }

                public ClassmapBean getClassmap() {
                    return classmap;
                }

                public void setClassmap(ClassmapBean classmap) {
                    this.classmap = classmap;
                }

                public static class ClassmapBean {
                    /**
                     * attr0 : 100663304
                     */

                    private int attr0;

                    public int getAttr0() {
                        return attr0;
                    }

                    public void setAttr0(int attr0) {
                        this.attr0 = attr0;
                    }
                }
            }
        }
    }

    public static class RelativeBean {
        /**
         * priortype : 1
         * singer : [{"songcount":5,"imgurl":"http://singerimg.kugou.com/uploadpic/softhead/240/20210911/20210911175501753405.jpg","correctiontip":"","mvcount":0,"singername":"Alone","singerid":6999408,"albumcount":2}]
         */

        private int priortype;
        private List<SingerBean> singer;

        public int getPriortype() {
            return priortype;
        }

        public void setPriortype(int priortype) {
            this.priortype = priortype;
        }

        public List<SingerBean> getSinger() {
            return singer;
        }

        public void setSinger(List<SingerBean> singer) {
            this.singer = singer;
        }

        public static class SingerBean {
            /**
             * songcount : 5
             * imgurl : http://singerimg.kugou.com/uploadpic/softhead/240/20210911/20210911175501753405.jpg
             * correctiontip :
             * mvcount : 0
             * singername : Alone
             * singerid : 6999408
             * albumcount : 2
             */

            private int songcount;
            private String imgurl;
            private String correctiontip;
            private int mvcount;
            private String singername;
            private int singerid;
            private int albumcount;

            public int getSongcount() {
                return songcount;
            }

            public void setSongcount(int songcount) {
                this.songcount = songcount;
            }

            public String getImgurl() {
                return imgurl;
            }

            public void setImgurl(String imgurl) {
                this.imgurl = imgurl;
            }

            public String getCorrectiontip() {
                return correctiontip;
            }

            public void setCorrectiontip(String correctiontip) {
                this.correctiontip = correctiontip;
            }

            public int getMvcount() {
                return mvcount;
            }

            public void setMvcount(int mvcount) {
                this.mvcount = mvcount;
            }

            public String getSingername() {
                return singername;
            }

            public void setSingername(String singername) {
                this.singername = singername;
            }

            public int getSingerid() {
                return singerid;
            }

            public void setSingerid(int singerid) {
                this.singerid = singerid;
            }

            public int getAlbumcount() {
                return albumcount;
            }

            public void setAlbumcount(int albumcount) {
                this.albumcount = albumcount;
            }
        }
    }

    public static class BlackBean {
        /**
         * isblock : 1
         * type : 0
         */

        private int isblock;
        private int type;

        public int getIsblock() {
            return isblock;
        }

        public void setIsblock(int isblock) {
            this.isblock = isblock;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
