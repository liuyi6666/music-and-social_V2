package com.music.pojo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;

public class KugouMusicListForRandId implements Serializable {
    private static final long serialVersionUID = 9159640922816006874L;


    private int status;
    private String error;
    private DataBean data;
    private int errcode;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public static class DataBean {


        private int timestamp;
        private int total;
        private List<InfoBean> info;

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<InfoBean> getInfo() {
            return info;
        }

        public void setInfo(List<InfoBean> info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * rank_cid : 0
             * bitrate_super : 0
             * pay_type_320 : 0
             * m4afilesize : 0
             * hash_super :
             * zone : bj_kmr
             * price_sq : 0
             * first : 0
             * privilege_high : 0
             * filesize : 3468901
             * topic_url_320 :
             * bitrate : 128
             * duration_super : 0
             * pkg_price_320 : 0
             * 320filesize : 8671439
             * album_sizable_cover : http://imge.kugou.com/stdmusic/{size}/20200812/20200812114233288886.jpg
             * trans_param : {"cpy_grade":5,"musicpack_advance":0,"cpy_level":1,"cid":55165919,"cpy_attr0":0,"pay_block_tpl":1,"appid_block":"3124","display_rate":0,"classmap":{"attr0":100663304},"display":0,"hash_multitrack":"3B4345C0CC664385E0D8E5268625D03E"}
             * price : 0
             * inlist : 1
             * hash_high :
             * pkg_price_sq : 0
             * bitrate_high : 0
             * pay_type : 0
             * topic_url_sq :
             * fail_process : 0
             * musical : null
             * topic_url :
             * fail_process_320 : 0
             * sqhash : BD99FA0E3223A73B3ECB3BD62DA1CBDE
             * remark : 圣诞节的时候
             * pkg_price : 0
             * recommend_reason :
             * filename : 张艺兴 - When It's Christmas
             * trans_obj : {"rank_show_sort":1}
             * extname : mp3
             * last_sort : 0
             * filesize_super : 0
             * 320hash : F39FEAC482A4993F2D3400437262DFC3
             * audio_id : 48605017
             * hash : 2C04B97A5C0E2080E70C38B946A4E0FF
             * privilege : 0
             * duration : 216
             * addtime : 2018-12-25 14:22:32
             * album_audio_id : 126379948
             * mvhash : E7374E40A605052B45E5BC68EFFBF6E2
             * album_id : 14001283
             * fail_process_sq : 0
             * sqfilesize : 24697986
             * sort : 1
             * rp_publish : 1
             * has_accompany : 1
             * duration_high : 0
             * extname_super :
             * isfirst : 0
             * price_320 : 0
             * 320privilege : 0
             * feetype : 0
             * rp_type : audio
             * filesize_high : 0
             * pay_type_sq : 0
             * old_cpy : 1
             * sqprivilege : 0
             * privilege_super : 0
             * issue : 359
             * vstr_id : 6744814198288044032
             */

            private int rank_cid;
            private int bitrate_super;
            private int pay_type_320;
            private int m4afilesize;
            private String hash_super;
            private String zone;
            private int price_sq;
            private int first;
            private int privilege_high;
            private int filesize;
            private String topic_url_320;
            private int bitrate;
            private int duration_super;
            private int pkg_price_320;
            @JSONField(name = "320filesize")
            private int _$320filesize;
            private String album_sizable_cover;
            private TransParamBean trans_param;
            private int price;
            private int inlist;
            private String hash_high;
            private int pkg_price_sq;
            private int bitrate_high;
            private int pay_type;
            private String topic_url_sq;
            private int fail_process;
            private Object musical;
            private String topic_url;
            private int fail_process_320;
            private String sqhash;
            private String remark;
            private int pkg_price;
            private String recommend_reason;
            private String filename;
            private TransObjBean trans_obj;
            private String extname;
            private int last_sort;
            private int filesize_super;
            @JSONField(name = "320hash")
            private String _$320hash;
            private int audio_id;
            private String hash;
            private int privilege;
            private int duration;
            private String addtime;
            private int album_audio_id;
            private String mvhash;
            private String album_id;
            private int fail_process_sq;
            private int sqfilesize;
            private int sort;
            private int rp_publish;
            private int has_accompany;
            private int duration_high;
            private String extname_super;
            private int isfirst;
            private int price_320;
            @JSONField(name = "320privilege")
            private int _$320privilege;
            private int feetype;
            private String rp_type;
            private int filesize_high;
            private int pay_type_sq;
            private int old_cpy;
            private int sqprivilege;
            private int privilege_super;
            private int issue;
            private String vstr_id;

            public int getRank_cid() {
                return rank_cid;
            }

            public void setRank_cid(int rank_cid) {
                this.rank_cid = rank_cid;
            }

            public int getBitrate_super() {
                return bitrate_super;
            }

            public void setBitrate_super(int bitrate_super) {
                this.bitrate_super = bitrate_super;
            }

            public int getPay_type_320() {
                return pay_type_320;
            }

            public void setPay_type_320(int pay_type_320) {
                this.pay_type_320 = pay_type_320;
            }

            public int getM4afilesize() {
                return m4afilesize;
            }

            public void setM4afilesize(int m4afilesize) {
                this.m4afilesize = m4afilesize;
            }

            public String getHash_super() {
                return hash_super;
            }

            public void setHash_super(String hash_super) {
                this.hash_super = hash_super;
            }

            public String getZone() {
                return zone;
            }

            public void setZone(String zone) {
                this.zone = zone;
            }

            public int getPrice_sq() {
                return price_sq;
            }

            public void setPrice_sq(int price_sq) {
                this.price_sq = price_sq;
            }

            public int getFirst() {
                return first;
            }

            public void setFirst(int first) {
                this.first = first;
            }

            public int getPrivilege_high() {
                return privilege_high;
            }

            public void setPrivilege_high(int privilege_high) {
                this.privilege_high = privilege_high;
            }

            public int getFilesize() {
                return filesize;
            }

            public void setFilesize(int filesize) {
                this.filesize = filesize;
            }

            public String getTopic_url_320() {
                return topic_url_320;
            }

            public void setTopic_url_320(String topic_url_320) {
                this.topic_url_320 = topic_url_320;
            }

            public int getBitrate() {
                return bitrate;
            }

            public void setBitrate(int bitrate) {
                this.bitrate = bitrate;
            }

            public int getDuration_super() {
                return duration_super;
            }

            public void setDuration_super(int duration_super) {
                this.duration_super = duration_super;
            }

            public int getPkg_price_320() {
                return pkg_price_320;
            }

            public void setPkg_price_320(int pkg_price_320) {
                this.pkg_price_320 = pkg_price_320;
            }

            public int get_$320filesize() {
                return _$320filesize;
            }

            public void set_$320filesize(int _$320filesize) {
                this._$320filesize = _$320filesize;
            }

            public String getAlbum_sizable_cover() {
                return album_sizable_cover;
            }

            public void setAlbum_sizable_cover(String album_sizable_cover) {
                this.album_sizable_cover = album_sizable_cover;
            }

            public TransParamBean getTrans_param() {
                return trans_param;
            }

            public void setTrans_param(TransParamBean trans_param) {
                this.trans_param = trans_param;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getInlist() {
                return inlist;
            }

            public void setInlist(int inlist) {
                this.inlist = inlist;
            }

            public String getHash_high() {
                return hash_high;
            }

            public void setHash_high(String hash_high) {
                this.hash_high = hash_high;
            }

            public int getPkg_price_sq() {
                return pkg_price_sq;
            }

            public void setPkg_price_sq(int pkg_price_sq) {
                this.pkg_price_sq = pkg_price_sq;
            }

            public int getBitrate_high() {
                return bitrate_high;
            }

            public void setBitrate_high(int bitrate_high) {
                this.bitrate_high = bitrate_high;
            }

            public int getPay_type() {
                return pay_type;
            }

            public void setPay_type(int pay_type) {
                this.pay_type = pay_type;
            }

            public String getTopic_url_sq() {
                return topic_url_sq;
            }

            public void setTopic_url_sq(String topic_url_sq) {
                this.topic_url_sq = topic_url_sq;
            }

            public int getFail_process() {
                return fail_process;
            }

            public void setFail_process(int fail_process) {
                this.fail_process = fail_process;
            }

            public Object getMusical() {
                return musical;
            }

            public void setMusical(Object musical) {
                this.musical = musical;
            }

            public String getTopic_url() {
                return topic_url;
            }

            public void setTopic_url(String topic_url) {
                this.topic_url = topic_url;
            }

            public int getFail_process_320() {
                return fail_process_320;
            }

            public void setFail_process_320(int fail_process_320) {
                this.fail_process_320 = fail_process_320;
            }

            public String getSqhash() {
                return sqhash;
            }

            public void setSqhash(String sqhash) {
                this.sqhash = sqhash;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public int getPkg_price() {
                return pkg_price;
            }

            public void setPkg_price(int pkg_price) {
                this.pkg_price = pkg_price;
            }

            public String getRecommend_reason() {
                return recommend_reason;
            }

            public void setRecommend_reason(String recommend_reason) {
                this.recommend_reason = recommend_reason;
            }

            public String getFilename() {
                return filename;
            }

            public void setFilename(String filename) {
                this.filename = filename;
            }

            public TransObjBean getTrans_obj() {
                return trans_obj;
            }

            public void setTrans_obj(TransObjBean trans_obj) {
                this.trans_obj = trans_obj;
            }

            public String getExtname() {
                return extname;
            }

            public void setExtname(String extname) {
                this.extname = extname;
            }

            public int getLast_sort() {
                return last_sort;
            }

            public void setLast_sort(int last_sort) {
                this.last_sort = last_sort;
            }

            public int getFilesize_super() {
                return filesize_super;
            }

            public void setFilesize_super(int filesize_super) {
                this.filesize_super = filesize_super;
            }

            public String get_$320hash() {
                return _$320hash;
            }

            public void set_$320hash(String _$320hash) {
                this._$320hash = _$320hash;
            }

            public int getAudio_id() {
                return audio_id;
            }

            public void setAudio_id(int audio_id) {
                this.audio_id = audio_id;
            }

            public String getHash() {
                return hash;
            }

            public void setHash(String hash) {
                this.hash = hash;
            }

            public int getPrivilege() {
                return privilege;
            }

            public void setPrivilege(int privilege) {
                this.privilege = privilege;
            }

            public int getDuration() {
                return duration;
            }

            public void setDuration(int duration) {
                this.duration = duration;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public int getAlbum_audio_id() {
                return album_audio_id;
            }

            public void setAlbum_audio_id(int album_audio_id) {
                this.album_audio_id = album_audio_id;
            }

            public String getMvhash() {
                return mvhash;
            }

            public void setMvhash(String mvhash) {
                this.mvhash = mvhash;
            }

            public String getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(String album_id) {
                this.album_id = album_id;
            }

            public int getFail_process_sq() {
                return fail_process_sq;
            }

            public void setFail_process_sq(int fail_process_sq) {
                this.fail_process_sq = fail_process_sq;
            }

            public int getSqfilesize() {
                return sqfilesize;
            }

            public void setSqfilesize(int sqfilesize) {
                this.sqfilesize = sqfilesize;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getRp_publish() {
                return rp_publish;
            }

            public void setRp_publish(int rp_publish) {
                this.rp_publish = rp_publish;
            }

            public int getHas_accompany() {
                return has_accompany;
            }

            public void setHas_accompany(int has_accompany) {
                this.has_accompany = has_accompany;
            }

            public int getDuration_high() {
                return duration_high;
            }

            public void setDuration_high(int duration_high) {
                this.duration_high = duration_high;
            }

            public String getExtname_super() {
                return extname_super;
            }

            public void setExtname_super(String extname_super) {
                this.extname_super = extname_super;
            }

            public int getIsfirst() {
                return isfirst;
            }

            public void setIsfirst(int isfirst) {
                this.isfirst = isfirst;
            }

            public int getPrice_320() {
                return price_320;
            }

            public void setPrice_320(int price_320) {
                this.price_320 = price_320;
            }

            public int get_$320privilege() {
                return _$320privilege;
            }

            public void set_$320privilege(int _$320privilege) {
                this._$320privilege = _$320privilege;
            }

            public int getFeetype() {
                return feetype;
            }

            public void setFeetype(int feetype) {
                this.feetype = feetype;
            }

            public String getRp_type() {
                return rp_type;
            }

            public void setRp_type(String rp_type) {
                this.rp_type = rp_type;
            }

            public int getFilesize_high() {
                return filesize_high;
            }

            public void setFilesize_high(int filesize_high) {
                this.filesize_high = filesize_high;
            }

            public int getPay_type_sq() {
                return pay_type_sq;
            }

            public void setPay_type_sq(int pay_type_sq) {
                this.pay_type_sq = pay_type_sq;
            }

            public int getOld_cpy() {
                return old_cpy;
            }

            public void setOld_cpy(int old_cpy) {
                this.old_cpy = old_cpy;
            }

            public int getSqprivilege() {
                return sqprivilege;
            }

            public void setSqprivilege(int sqprivilege) {
                this.sqprivilege = sqprivilege;
            }

            public int getPrivilege_super() {
                return privilege_super;
            }

            public void setPrivilege_super(int privilege_super) {
                this.privilege_super = privilege_super;
            }

            public int getIssue() {
                return issue;
            }

            public void setIssue(int issue) {
                this.issue = issue;
            }

            public String getVstr_id() {
                return vstr_id;
            }

            public void setVstr_id(String vstr_id) {
                this.vstr_id = vstr_id;
            }

            public static class TransParamBean {
                /**
                 * cpy_grade : 5
                 * musicpack_advance : 0
                 * cpy_level : 1
                 * cid : 55165919
                 * cpy_attr0 : 0
                 * pay_block_tpl : 1
                 * appid_block : 3124
                 * display_rate : 0
                 * classmap : {"attr0":100663304}
                 * display : 0
                 * hash_multitrack : 3B4345C0CC664385E0D8E5268625D03E
                 */

                private int cpy_grade;
                private int musicpack_advance;
                private int cpy_level;
                private int cid;
                private int cpy_attr0;
                private int pay_block_tpl;
                private String appid_block;
                private int display_rate;
                private ClassmapBean classmap;
                private int display;
                private String hash_multitrack;

                public int getCpy_grade() {
                    return cpy_grade;
                }

                public void setCpy_grade(int cpy_grade) {
                    this.cpy_grade = cpy_grade;
                }

                public int getMusicpack_advance() {
                    return musicpack_advance;
                }

                public void setMusicpack_advance(int musicpack_advance) {
                    this.musicpack_advance = musicpack_advance;
                }

                public int getCpy_level() {
                    return cpy_level;
                }

                public void setCpy_level(int cpy_level) {
                    this.cpy_level = cpy_level;
                }

                public int getCid() {
                    return cid;
                }

                public void setCid(int cid) {
                    this.cid = cid;
                }

                public int getCpy_attr0() {
                    return cpy_attr0;
                }

                public void setCpy_attr0(int cpy_attr0) {
                    this.cpy_attr0 = cpy_attr0;
                }

                public int getPay_block_tpl() {
                    return pay_block_tpl;
                }

                public void setPay_block_tpl(int pay_block_tpl) {
                    this.pay_block_tpl = pay_block_tpl;
                }

                public String getAppid_block() {
                    return appid_block;
                }

                public void setAppid_block(String appid_block) {
                    this.appid_block = appid_block;
                }

                public int getDisplay_rate() {
                    return display_rate;
                }

                public void setDisplay_rate(int display_rate) {
                    this.display_rate = display_rate;
                }

                public ClassmapBean getClassmap() {
                    return classmap;
                }

                public void setClassmap(ClassmapBean classmap) {
                    this.classmap = classmap;
                }

                public int getDisplay() {
                    return display;
                }

                public void setDisplay(int display) {
                    this.display = display;
                }

                public String getHash_multitrack() {
                    return hash_multitrack;
                }

                public void setHash_multitrack(String hash_multitrack) {
                    this.hash_multitrack = hash_multitrack;
                }

                public static class ClassmapBean {
                    /**
                     * attr0 : 100663304
                     */

                    private int attr0;

                    public int getAttr0() {
                        return attr0;
                    }

                    public void setAttr0(int attr0) {
                        this.attr0 = attr0;
                    }
                }
            }

            public static class TransObjBean {
                /**
                 * rank_show_sort : 1
                 */

                private int rank_show_sort;

                public int getRank_show_sort() {
                    return rank_show_sort;
                }

                public void setRank_show_sort(int rank_show_sort) {
                    this.rank_show_sort = rank_show_sort;
                }
            }
        }
    }
}
