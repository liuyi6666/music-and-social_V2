package com.music.pojo.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class SingerAlbumDTO implements Serializable {
    private static final long serialVersionUID = -5405223512305738289L;
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 专辑名
     */
    private String albumName;

    /**
     * 专辑信息
     */
    private String intro;

    /**
     * 添加时间
     */
    private String publishTime;

    /**
     * 歌手名称
     */
    private String singerName;

    /**
     * 专辑id
     */
    private Integer albumId;

    /**
     * 歌手id
     */
    private Integer singerId;

    /**
     * 专辑图片
     */
    private String imgUrl;
}
