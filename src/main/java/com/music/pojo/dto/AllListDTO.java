package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AllListDTO implements Serializable {
    private static final long serialVersionUID = 6704718248656014148L;

    private Integer id;

    //榜单id
    private Integer rankId;

    //榜单热度
    private Integer playTimes;

    //榜单信息
    private String intro;

    //榜单名
    private String rankName;

    //最新期数的volid值
    private Integer rankCid;

    //榜单图片
    private String img;

    //添加日期
    private Date addTime;
}
