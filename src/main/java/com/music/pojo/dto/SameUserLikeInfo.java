package com.music.pojo.dto;

import com.music.pojo.entity.UserLikeMusic;
import com.music.pojo.entity.UserLikeSinger;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Data
public class SameUserLikeInfo implements Serializable {
    private static final long serialVersionUID = 2386189832244231412L;

    //用户信息
    private List<UserDTO> userDTO;

    //相同喜欢音乐
    private HashMap<Integer,Object> userLikeSameMusicMap;

    //相同喜欢歌手
    private HashMap<Integer,Object> userLikeSameSingerMap;

    //相同兴趣标签
    private HashMap<Integer,Object> userSameHobbyTabsMap;

}
