package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonMusicDTO implements Serializable {
    private static final long serialVersionUID = -5405582400447744554L;

    private Integer id;

    /**
     * hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 歌曲时长
     */
    private String timeLength;

    /**
     * 歌曲大小
     */
    private Integer fileSize;

    /**
     * 歌曲名
     */
    private String audioName;

    /**
     * 专辑名
     */
    private String albumName;

    /**
     * 歌曲封面
     */
    private String img;

    /**
     * 歌手名
     */
    private String authorName;

    /**
     * 歌曲名
     */
    private String songName;

    /**
     * 歌词
     */
    private String lyrics;

    /**
     * 歌手id
     */
    private String authorId;

    /**
     * 播放地址
     */
    private String playUrl;

    /**
     * 歌曲id
     */
    private String audioId;

    /**
     * 播放备用地址
     */
    private String playBackupUrl;

    /**
     * 用户喜欢标识
     */
    private Integer likeFlag;

    /**
     * 已经播放次数
     */
    private Integer playCount;
}
