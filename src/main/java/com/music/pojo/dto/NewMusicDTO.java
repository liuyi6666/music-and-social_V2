package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class NewMusicDTO implements Serializable {
    private static final long serialVersionUID = 7308307337028760902L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 文件大小
     */
    private Integer fileSize;

    /**
     * 歌曲名字
     */
    private String fileName;

    /**
     * 歌曲id
     */
    private Integer audioId;

    /**
     * 专辑图片
     */
    private String albumCover;

    /**
     * hash值
     */
    private String hash;

    /**
     * 添加时间
     */
    private String addTime;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 新歌类型 1华语 2欧美 3日韩
     */
    private Integer type;

    /**
     * 歌曲时长
     */
    private String timeLength;

    /**
     * 给前端的排行
     */
    private Integer sortNum;

}
