package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 简化音乐实体
 */
@Data
@Accessors(chain = true)
public class SimpleMusicDTO implements Serializable {

    private static final long serialVersionUID = 4629259558064930180L;

    private String hash;
    private String albumId;
    private String songName;
    private String timeLength;
}
