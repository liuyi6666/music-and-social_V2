package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RankingListMusicDTO implements Serializable {
    private static final long serialVersionUID = -5295802569660524819L;
    private Integer id;

    /**
     * 文件大小
     */
    private Integer fileSize;

    /**
     * 专辑封面
     */
    private String albumSizableCover;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 歌曲名
     */
    private String fileName;

    /**
     * 歌曲hash值
     */
    private String hash;

    /**
     * 添加到排行榜的事件
     */
    private String addTime;

    /**
     * 专辑ID
     */
    private String albumId;


    /**
     * 最新期数
     */
    private Integer rankCid;

    /**
     * 排行榜Id
     */
    private Integer rankId;

    /**
     * 音乐时长
     */
    private String timeLength;

    /**
     * 前端判断是否为用户喜欢的音乐
     */
    private Integer likeFlag;
}
