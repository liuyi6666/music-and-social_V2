package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MusicSheetDTO implements Serializable {
    private static final long serialVersionUID = 5529880651751990824L;

    private Integer id;

    //歌单id
    private Integer specialId;

    //播放数量
    private Integer playCount;

    //歌单提交时间
    private String publishTime;

    //创建歌单用户Id
    private Integer userId;

    //创建歌单用户
    private String userName;

    //歌单名
    private String specialName;

    //用户头像地址
    private String userAvatar;

    //歌单简介
    private String intro;

    //歌单图片地址
    private String imgUrl;

    //是否为酷狗用户提交的歌单
    private Integer isKugouUser;

    //用户收藏歌单标识
    private Integer likeFlag;

}
