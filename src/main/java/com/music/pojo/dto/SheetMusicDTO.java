package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SheetMusicDTO implements Serializable {
    private static final long serialVersionUID = 6440899302532119795L;

    private Integer id;

    /**
     * 歌曲hash值
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 歌曲名
     */
    private String fileName;

    /**
     * 歌曲id
     */
    private Integer audioId;

    /**
     * 歌曲文件大小
     */
    private Integer fileSize;

    /**
     * 专辑名
     */
    private String remark;

    /**
     * 歌单id
     */
    private Integer specialId;

    /**
     * 音乐时长
     */
    private String timeLength;

    /**
     * 用户喜欢标识
     */
    private Integer likeFlag;
}
