package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class SingerDTO implements Serializable {
    private static final long serialVersionUID = 5476752322297713582L;
    private Integer id;

    //粉丝数
    private Integer fansCount;

    //歌手ID
    private Integer singerId;

    //歌手名字
    private String singerName;

    //歌手热度
    private Integer heat;

    //歌手头像
    private String imgUrl;

    //性别类型
    private Integer sexType;

    //歌手类型
    private Integer type;

    //歌手简介
    private String intro;

    //mv数量
    private Integer mvCount;

    //歌曲数量
    private  Integer songCount;

    //专辑数量
    private Integer albumCount;

    //用户喜欢标识
    private Integer likeFlag;
}
