package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ChatInfo implements Serializable {

    private static final long serialVersionUID = -2099659640942105471L;

    //消息用户id
    private Integer userId;
    //消息内容
    private String message;
    //消息类型 0普通消息 1分享歌曲消息
    private Integer msgType;

}
