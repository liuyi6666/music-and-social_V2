package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MyMessage implements Serializable {

    private static final long serialVersionUID = 9157319660491775383L;

    private Integer userId;
    private Integer toUserId;
    private String message;
    private Integer messageType;
    //标识聊天从那个入口进入，0匹配 1相互关注
    private Integer chatType;

}
