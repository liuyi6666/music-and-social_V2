package com.music.pojo.dto;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class AdminUserDTO implements Serializable {
    private static final long serialVersionUID = -7665128361392663349L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String adminName;

    private String headerImg;
}
