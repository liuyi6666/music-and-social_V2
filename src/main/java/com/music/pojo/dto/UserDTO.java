package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class UserDTO implements Serializable {
    private static final long serialVersionUID = -8006781045121603799L;
    private Integer id;


    private String username;

    /**
     *
     */
    private String email;


    /**
     *
     */
    private String sex;

    /**
     *
     */
    private Integer age;


    private String headerImg;

    /**
     *
     */
    private List<String> musicHobby;

    /**
     *
     */
    private Integer status;

    /**
     *
     */
    private String birthday;

    /**
     * 主页背景
     */
    private String personalCenterImg;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 个性签名
     */
    private String personalSignature;

    /**
     * 喜欢相同音乐数量
     */
    private Integer sameUserLikeMusicCount;

    /**
     * 喜欢相同歌手数量
     */
    private Integer sameUserLikeSingerCount;

    /**
     * 相同标签
     */
    private List<String> sameUserHobby;

    /**
     * 聊天最后显示消息
     */
    private String lastMessage;

    /**
     * 违规次数
     */
    private Integer vioNum;

}
