package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserRelationshipDTO implements Serializable {

    private static final long serialVersionUID = 1899506204650655750L;

    /**
     * 状态 0为关注 1关注 2互相关注
     */
    private Integer status;

}
