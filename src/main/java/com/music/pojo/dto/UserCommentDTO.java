package com.music.pojo.dto;

import com.music.pojo.entity.UserComment;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class UserCommentDTO implements Serializable {
    private static final long serialVersionUID = 5120606632090385746L;

    /**
     * 评论id
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 上级级评论id
     */
    private Integer parentId;

    /**
     * 上一级评论用户id
     */
    private Integer parentUserId;

    /**
     * 上一级评论用户名
     */
    private String parentUserName;

    /**
     * 动态或者歌曲下id
     */
    private Long articleId;

    /**
     * 动态或者歌曲名
     */
    private String articleTitle;

    /**
     * 被回复的评论id
     */
    private Integer replyCommentId;

    /**
     * 被回复的评论的用户id
     */
    private Integer replyCommentUserId;

    /**
     * 被回复的评论的用户名
     */
    private String replyCommentUserName;

    /**
     * 评论级别，回复的歌曲或者动态的是一级评论，其他的都是2级
     */
    private Integer commentLevel;

    /**
     * 0是歌曲评论，1是动态评论
     */
    private Integer commentType;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论状态
     */
    private Integer status;

    /**
     * 评论点赞数
     */
    private Integer likeNum;

    /**
     * 评论时间
     */
    private Date createTime;

    /**
     * 评论更新时间比如点赞
     */
    private Date updateTime;

    /**
     * 歌曲hash
     */
    private String hash;

    /**
     * 专辑id
     */
    private String albumId;

    /**
     * 下级评论
     */
    private List<UserComment> userCommentList;

    //用户头像
    private String userHeaderImg;

    //用户是否喜欢
    private Integer isUserLike;

    //歌曲信息
    private CommonMusicDTO commonMusicDTO;

    /*
    ========================前端需要参数=========================
     */
    /**
     * 评论功能是否显示
     */
    private String isShowComment = "none";

    /**
     * 输入框
     */
    private String inputText= "";


}
