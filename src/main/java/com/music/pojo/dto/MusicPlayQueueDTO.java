package com.music.pojo.dto;

import lombok.Data;

import java.io.Serializable;

//播放队列
@Data
public class MusicPlayQueueDTO implements Serializable {
    private static final long serialVersionUID = 7466839339006518546L;

    private String hash;
    private String albumId;

}
