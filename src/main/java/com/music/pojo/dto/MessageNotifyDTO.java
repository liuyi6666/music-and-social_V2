package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class MessageNotifyDTO implements Serializable {
    private static final long serialVersionUID = -6697987894203258045L;

    //通知指定用户id
    private Integer toUserId;
    //通知指定用户名称
    private String toUserName;
    //发送通知用户id
    private Integer userId;
    //发送通知用户名
    private String userName;
    //通知标题
    private String messageTitle;
    //通知内容
    private String messageContext;
    //是否已读
    private Integer status;
    //通知时间
    private Date messageTime;
}
