package com.music.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class MusicListForRandIdDTO implements Serializable {
    private static final long serialVersionUID = -8314680490355230481L;

    private Integer rank_cid;
    private Integer filesize;
    private String sqhash;
    private String remark;
    private String filename;
    private String extname;
    private Integer audio_id;
    private String hash;
    private String addtime;
    private Integer album_audio_id;
    private String mvhash;
    private String album_id;
    private String timeLength;
}
