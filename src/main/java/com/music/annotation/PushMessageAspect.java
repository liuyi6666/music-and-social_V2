package com.music.annotation;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.music.constant.SystemMessageConstant;
import com.music.enums.StatusEnum;
import com.music.pojo.dto.UserDTO;
import com.music.user.service.UserRelationshipService;
import com.music.user.service.UserService;
import com.music.user.service.impl.MessageNotifyTemplate;
import com.music.util.MyJWTUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 推送/判断用户是否能评论切点
 */
@Aspect
@Component
public class PushMessageAspect {

    @Autowired
    private MessageNotifyTemplate messageNotifyTemplate;

    @Autowired
    private UserRelationshipService userRelationshipService;

    @Autowired
    private UserService userService;

    private void pushMessage(JoinPoint joinPoint, String messageType) {
        Object[] args = joinPoint.getArgs();
        JSONObject jsonObject = JSONUtil.parseObj(args[0]);
        Integer toUserId = (Integer) jsonObject.get("replyCommentUserId");
        String content = (String) jsonObject.get("content");
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo((String) args[1]);
        Integer userId = (Integer) jwtInfo.get("userId");
        if (!userId.equals(toUserId)) {
            messageNotifyTemplate.pushMessageNotify(toUserId, userId, messageType, content);
        }
    }

    /**
     * 回复歌手评论
     *
     * @param joinPoint
     */
    @AfterReturning(value = "execution(* com.music.user.service.UserCommentService.commitMusicCommentV1(..))")
    public void pushMusicMessageAfter(JoinPoint joinPoint) {
        pushMessage(joinPoint, SystemMessageConstant.REPLY_MUSIC_COMMENT);
    }

    /**
     * 回复动态
     *
     * @param joinPoint
     */
    @AfterReturning(value = "execution(* com.music.user.service.UserCommentService.commentUserIssueLevel1(..))")
    public void pushIssueMessageAfter(JoinPoint joinPoint) {
        pushMessage(joinPoint, SystemMessageConstant.REPLY_ISSUE_COMMENT);
    }

    /**
     * 关注
     *
     * @param joinPoint
     */
    @AfterReturning(value = "execution(* com.music.user.service.UserRelationshipService.updateUserRelationship(..))")
    public void pushFollowMessageAfter(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Integer id = (Integer) args[0];
        String token = (String) args[1];
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo((String) args[1]);
        Integer userId = (Integer) jwtInfo.get("userId");
        System.out.println("toUserId=" + id + "=====userId=" + userId);
        HashMap<String, Object> userRelationshipById = userRelationshipService.getUserRelationshipById(id, token);
        Integer relationStatus = (Integer) userRelationshipById.get("relationStatus");
        System.out.println("relationStatus=" + userRelationshipById);
        if (relationStatus != 0) {
            messageNotifyTemplate.pushMessageNotify(id, userId, SystemMessageConstant.FOLLOW, null);
        }

    }

    /**
     * 评论前判断是否被禁言
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* com.music.user.service.UserCommentService.commitMusicCommentV1(..))")
    public Object judgeUserMusicCommentV1Status(ProceedingJoinPoint point) throws Throwable {
        return userAllowComment(point);
    }

    /**
     * 是否能评论动态
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* com.music.user.service.UserCommentService.commentUserIssueLevel1(..))")
    public Object judgeUserIssueCommentStatus(ProceedingJoinPoint point) throws Throwable {
        return userAllowComment(point);
    }

    /**
     * 是否发布动态
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* com.music.user.service.UserCommentService.releaseIssue(..))")
    public Object judgeReleaseUserIssueCommentStatus(ProceedingJoinPoint point) throws Throwable {
        return userAllowComment(point);
    }

    /**
     * 是否能评论音乐
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around(value = "execution(* com.music.user.service.UserCommentService.commitMusicCommentV0(..))")
    public Object judgeUserMusicCommentV0Status(ProceedingJoinPoint point) throws Throwable {
        return userAllowComment(point);
    }

    private Object userAllowComment(ProceedingJoinPoint point) throws Throwable {
        Object[] args = point.getArgs();
        String authentication = (String) args[1];
        System.out.println("authentication=========="+authentication);
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        UserDTO userDTO = userService.getUserInfoById(userId);
        Object proceed = null;
        if (userDTO.getStatus() == 2) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("statusCode", StatusEnum.USER_NOT_ALLOW_COMMENT.getStatusCode());
            proceed=map;
        } else {
            proceed = point.proceed(args);
        }
        return proceed;
    }
}
