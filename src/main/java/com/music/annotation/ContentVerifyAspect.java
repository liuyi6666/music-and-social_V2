package com.music.annotation;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.music.constant.CommentFromConstant;
import com.music.enums.StatusEnum;
import com.music.pojo.entity.UserCommentCheck;
import com.music.util.AliyunContextVerifyUtil;
import com.music.util.MyJWTUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Aspect
@Component
public class ContentVerifyAspect {

    //一级歌曲评论切点
    @Pointcut("execution(* com.music.user.service.UserCommentService.commitMusicCommentV0(..))")
    public void musicCommentV0() {
    }

    //二级歌曲评论切点
    @Pointcut("execution(* com.music.user.service.UserCommentService.commitMusicCommentV1(..))")
    public void musicCommentV1() {
    }

    //发布动态切点
    @Pointcut("execution(* com.music.user.service.UserCommentService.releaseIssue(..))")
    public void userIssueCommentV0() {
    }

    //评论动态切点
    @Pointcut("execution(* com.music.user.service.UserCommentService.commentUserIssueLevel1(..))")
    public void userIssueCommentV1() {
    }

    @Around("musicCommentV0()")
    public Object musicCommentV0Verify(ProceedingJoinPoint point) throws Throwable {
        return verifyComment(point, CommentFromConstant.COMMENT_MUSIC_V0);
    }

    @Around("musicCommentV1()")
    public Object musicCommentV1Verify(ProceedingJoinPoint point) throws Throwable {
        return verifyComment(point, CommentFromConstant.COMMENT_MUSIC_V1);
    }

    @Around("userIssueCommentV0()")
    public Object userIssueCommentV0Verify(ProceedingJoinPoint point) throws Throwable {
        return verifyComment(point, CommentFromConstant.COMMENT_ISSUE_V0);
    }

    @Around("userIssueCommentV1()")
    public Object userIssueCommentV1Verify(ProceedingJoinPoint point) throws Throwable {
        return verifyComment(point, CommentFromConstant.COMMENT_ISSUE_V1);
    }

    private Object verifyComment(ProceedingJoinPoint point, Integer commentFromFlag) throws Throwable {
        Object[] args = point.getArgs();
        String commentInfo = (String) args[0];
        JSONObject jsonObject = JSONUtil.parseObj(commentInfo);
        String content = (String) jsonObject.get("content");
        String authentication = (String) args[1];
        HashMap<String, Object> jwtInfo = MyJWTUtil.getJWTInfo(authentication);
        Integer userId = (Integer) jwtInfo.get("userId");
        String userName = (String) jwtInfo.get("userName");
        UserCommentCheck userCommentCheck = new UserCommentCheck();
        userCommentCheck.setUserId(userId)
                .setUsername(userName)
                .setContext(content)
                .setContextInfo(commentInfo)
                .setAuthentication(authentication)
                .setCommentFromFlag(commentFromFlag);
        String verify = AliyunContextVerifyUtil.verify(userCommentCheck);
        Object proceed = null;
        HashMap<String, Object> map = new HashMap<>();
        if ("review".equals(verify)) {
            map.put("statusCode", StatusEnum.OTHER_COMMENT_CHECK_REVIEW.getStatusCode());
            proceed = map;
        } else if ("block".equals(verify)) {
            map.put("statusCode", StatusEnum.OTHER_COMMENT_CHECK_BLOCK.getStatusCode());
            proceed = map;
        } else {
            proceed = point.proceed(args);
        }
        return proceed;
    }
}
