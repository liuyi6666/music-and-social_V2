package com.music.admin.controller;

import com.music.admin.service.AdminUserService;
import com.music.enums.StatusEnum;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@RequestMapping("/adminUser")
@RestController
@CrossOrigin("*")
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;

    @PostMapping("/before/login")
    public ResultVO login(@RequestBody String loginJson) {
        HashMap<String, Object> map = adminUserService.login(loginJson);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    @GetMapping("/before/imgCode")
    public void imgCode(HttpServletRequest request, HttpServletResponse response) {
        adminUserService.imgCode(request, response);
    }

    @PostMapping("/verifyToken")
    public ResultVO verifyToken() {
        return ResultVO.result(200, "登录成功", null);
    }
}
