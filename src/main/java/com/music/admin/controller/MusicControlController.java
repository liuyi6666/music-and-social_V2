package com.music.admin.controller;

import com.music.admin.service.KugouInfoSynchronizationService;
import com.music.enums.StatusEnum;
import com.music.pojo.entity.CommonMusic;
import com.music.pojo.entity.MusicSheet;
import com.music.pojo.entity.UserCommentCheck;
import com.music.user.service.*;
import com.music.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequestMapping("/musicControl")
@RestController
@CrossOrigin("*")
public class MusicControlController {
    @Autowired
    private KugouInfoSynchronizationService kugouInfoSynchronizationService;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private MusicSheetService musicSheetService;

    @Autowired
    private SheetMusicService sheetMusicService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserCommentService userCommentService;

    @Autowired
    private SingerService singerService;

    @Autowired
    private WalkingService walkingService;

    @Autowired
    private UserTabService userTabService;

    @Autowired
    private UserCommentCheckService userCommentCheckService;

    @Autowired
    private UserProblemService userProblemService;


    /**
     * 同步期数表
     *
     * @return
     */
    @PostMapping("/updateVolid")
    public ResultVO updateVolidForRankId() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncVolidByRankId();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(0, "更新失败", null);
        }
    }

    /**
     * 同步各大榜单
     *
     * @return
     */
    @PostMapping("/updateAllMusicList")
    public ResultVO updateAllMusicList() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncAllMusicList();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(0, "更新失败", null);
        }
    }

    /**
     * 同步酷狗歌单
     *
     * @return
     */
    @PostMapping("/updateMusicSheet")
    public ResultVO updateMusicSheet() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncMusicSheet();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(-1, "更新失败", null);
        }
    }

    /**
     * 同步歌手
     *
     * @return
     */
    @PostMapping("/updateSinger")
    public ResultVO updateSinger() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncMusicSinger();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(0, "更新失败", null);
        }
    }

    /**
     * 同步榜单中的音乐
     * 例如top500里的音乐
     *
     * @return
     */
    @PostMapping("/updateListMusic")
    public ResultVO updateListMusic() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncListMusic();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(0, "更新失败", null);
        }
    }

    @PostMapping("/updateNewMusic")
    public ResultVO updateNewMusic() {
        Boolean aBoolean = kugouInfoSynchronizationService.syncNewMusic();
        if (aBoolean) {
            return ResultVO.result(200, "更新成功", null);
        } else {
            return ResultVO.result(0, "更新失败", null);
        }
    }

    /**
     * 同步歌单里的音乐
     *
     * @return
     */
    @PostMapping("/updateSheetMusic")
    public ResultVO updateSheetMusic() {
        kugouInfoSynchronizationService.syncSheetMusic();
        return ResultVO.result(200, "更新成功", null);
    }

    @PostMapping("/updateSingerAlbum")
    public ResultVO updateSingerAlbum() {
        kugouInfoSynchronizationService.syncSingerAlbum();
        return ResultVO.result(200, "更新成功", null);
    }

    @PostMapping("/updateSingerAlbumMusic")
    public ResultVO updateSingerAlbumMusic() {
        kugouInfoSynchronizationService.syncSingerAlbumMusic();
        return ResultVO.result(200, "更新成功", null);
    }

    /**
     * 分页获取全部歌曲
     *
     * @param start
     * @param size
     * @return
     */
    @PostMapping("/getAllMusic/{start}/{size}")
    public ResultVO getAllMusic(@PathVariable Integer start, @PathVariable Integer size, @RequestBody String musicSearchKey) {
        System.out.println("musicSearchKey=" + musicSearchKey);
        HashMap<String, Object> map = commonMusicService.findAll(start, size, musicSearchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 同步音乐
     *
     * @param hash
     * @param albumId
     * @return
     */
    @PostMapping("/updateMusic/{hash}/{albumId}")
    public ResultVO updateMusic(@PathVariable String hash, @PathVariable String albumId) {
        HashMap<String, Object> map = kugouInfoSynchronizationService.updateMusic(hash, albumId);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找在线音乐
     *
     * @param musicSearchKey
     * @return
     */
    @PostMapping("/onlineMusicSearch")
    public ResultVO onlineMusicSearch(@RequestBody String musicSearchKey) {
        HashMap<String, Object> map = kugouInfoSynchronizationService.onlineMusicSearch(musicSearchKey);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 导入歌曲
     *
     * @param musicList
     * @return
     */
    @PostMapping("/importMusic")
    public ResultVO importMusic(@RequestBody String musicList) {
        HashMap<String, Object> map = commonMusicService.importMusic(musicList);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 分页获取全部歌单
     *
     * @param start
     * @param size
     * @param sheetSearchKey
     * @return
     */
    @PostMapping("/getAllSheet/{start}/{size}")
    public ResultVO getAllSheet(@PathVariable Integer start, @PathVariable Integer size, @RequestBody String sheetSearchKey) {
        HashMap<String, Object> map = musicSheetService.findAll(start, size, sheetSearchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取歌单歌曲
     *
     * @param specialId
     * @return
     */
    @PostMapping("/getSheetMusic/{specialId}")
    public ResultVO getSheetMusic(@PathVariable Integer specialId) {
        HashMap<String, Object> map = sheetMusicService.getSheetMusicBySpecialId(specialId);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 删除歌曲
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteMusic/{id}")
    public ResultVO deleteMusic(@PathVariable Integer id) {
        HashMap<String, Object> map = commonMusicService.deleteMusic(id);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 删除歌单
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteSheet/{id}")
    public ResultVO deleteSheet(@PathVariable Integer id) {
        HashMap<String, Object> map = musicSheetService.deleteSheet(id);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 在线查找歌单
     *
     * @param sheetSearchKey
     * @return
     */
    @PostMapping("/onlineSheetSearch")
    public ResultVO onlineSheetSearch(@RequestBody String sheetSearchKey) {
        HashMap<String, Object> map = kugouInfoSynchronizationService.onlineSheetSearch(sheetSearchKey);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 导入歌单
     *
     * @param sheetList
     * @return
     */
    @PostMapping("/importSheet")
    public ResultVO importSheet(@RequestBody String sheetList) {
        HashMap<String, Object> map = musicSheetService.importSheet(sheetList);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找用户
     *
     * @param start
     * @param size
     * @param userSearchKey
     * @return
     */
    @PostMapping("/getAllUser/{start}/{size}")
    public ResultVO getAllUser(@PathVariable Integer start, @PathVariable Integer size, @RequestBody String userSearchKey) {
        HashMap<String, Object> map = userService.findAllLimit(start, size, userSearchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找歌手
     *
     * @param start
     * @param size
     * @param singerSearchKey
     * @return
     */
    @PostMapping("/getAllSinger/{start}/{size}")
    public ResultVO getAllSinger(@PathVariable Integer start, @PathVariable Integer size, @RequestBody String singerSearchKey) {
        HashMap<String, Object> map = singerService.findAll(start, size, singerSearchKey);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取用户评论
     *
     * @param userId
     * @return
     */
    @PostMapping("/getUserComment/{userId}")
    public ResultVO getComment(@PathVariable Integer userId) {
        HashMap<String, Object> map = userCommentService.findUserCommentByUserId(userId);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }


    /**
     * 删除评论或动态
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteComment/{id}")
    public ResultVO deleteComment(@PathVariable Integer id, @RequestHeader String authentication) {
        HashMap<String, Object> map = userCommentService.deleteComment(id, authentication);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 更新用户状态（禁言）
     *
     * @param userId
     * @param status
     * @return
     */
    @PostMapping("/updateUserStatus/{userId}/{status}")
    public ResultVO updateUserStatus(@PathVariable Integer userId, @PathVariable Integer status) {
        HashMap<String, Object> map = userService.updateUserStatus(userId, status);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 歌手信息自动补充
     *
     * @param singerId
     * @return
     */
    @PostMapping("/updateOneSingerDetail/{singerId}")
    public ResultVO updateOneSingerDetail(@PathVariable Integer singerId) {
        HashMap<String, Object> map = kugouInfoSynchronizationService.updateOneSingerDetail(singerId);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 删除Banner
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteBanner/{id}")
    public ResultVO deleteBanner(@PathVariable Integer id) {
        HashMap<String, Object> map = walkingService.deleteBanner(id);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 增加首页图片
     *
     * @param data
     * @return
     */
    @PostMapping("/addBanner")
    public ResultVO addBanner(@RequestBody String data) {
        HashMap<String, Object> map = walkingService.addBanner(data);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 增加兴趣标签
     *
     * @param tabName
     * @return
     */
    @PostMapping("/addTab/{tabName}")
    public ResultVO addTab(@PathVariable String tabName) {
        HashMap<String, Object> map = userTabService.addTab(tabName);
        Integer code = (Integer) map.get("statusCode");
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 查找违规记录
     *
     * @param userId
     * @return
     */
    @PostMapping("/showUserCommentCheck/{userId}")
    public ResultVO showUserCommentCheck(@PathVariable Integer userId) {
        HashMap<String, Object> map = userCommentCheckService.showUserCommentCheck(userId);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }

    /**
     * 获取用户反馈
     *
     * @param start
     * @param size
     * @return
     */
    @PostMapping("/getUserProblem/{start}/{size}")
    public ResultVO getUserProblem(@PathVariable Integer start, @PathVariable Integer size) {
        HashMap<String, Object> map = userProblemService.getUserProblem(start, size);
        Integer code = StatusEnum.OTHER_SUCCESS.getStatusCode();
        return ResultVO.result(code, StatusEnum.getMsg(code), map);
    }
}
