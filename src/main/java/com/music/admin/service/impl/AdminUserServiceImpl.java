package com.music.admin.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.admin.service.AdminUserService;
import com.music.enums.StatusEnum;
import com.music.mapper.AdminUserMapper;
import com.music.pojo.entity.AdminUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Autowired
    private AdminUserMapper adminUserMapper;

    //登陆验证码
    private ShearCaptcha captcha;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    //盐值
    private static final String SECRET = "liuyi";
    /**
     * 登录
     *
     * @param loginJson
     * @return
     */
    @Override
    public HashMap<String, Object> login(String loginJson) {
        HashMap<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONUtil.parseObj(loginJson);
        String username = (String)jsonObject.get("username");
        String password = (String)jsonObject.get("password");
        String code = (String)jsonObject.get("code");
        if (captcha == null) {
            map.put("statusCode", StatusEnum.CODE_NULL.getStatusCode());
            return map;
        }
        if (!captcha.verify(code)) {
            map.put("statusCode", StatusEnum.CODE_ERROR.getStatusCode());
            return map;
        }
        QueryWrapper<AdminUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("admin_name", username);
        AdminUser adminUser = adminUserMapper.selectOne(queryWrapper);
        if (!ObjectUtil.isNotEmpty(adminUser)) {
            map.put("statusCode", StatusEnum.USER_PASSWORD_ERROR.getStatusCode());
        }else {
            if (bCryptPasswordEncoder.matches(password, adminUser.getPassword())) {
                map.put("statusCode", StatusEnum.LOGIN_SUCCESS.getStatusCode());
                HashMap<String, Object> jwtPayLoad = new HashMap<>();
                jwtPayLoad.put("adminUsername", adminUser.getAdminName());
                jwtPayLoad.put("userId", adminUser.getId());
                String token = JWTUtil.createToken(jwtPayLoad, SECRET.getBytes());
                DateTime iatDate = DateUtil.date();
                token = JWTUtil.parseToken(token)
                        .setExpiresAt(DateUtil.offsetDay(iatDate, 1))
                        .setSigner("HS256", SECRET.getBytes())
                        .sign();
                map.put("token", token);
            }else {
                map.put("statusCode", StatusEnum.USER_PASSWORD_ERROR.getStatusCode());
            }
        }
        return map;
    }

    /**
     * 获取图片验证码
     *
     * @return
     */
    @Override
    public void imgCode(HttpServletRequest request, HttpServletResponse response) {
        captcha = CaptchaUtil.createShearCaptcha(100, 40, 4, 4);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(new MathGenerator());
        // 重新生成code
        captcha.createCode();
        try {
            request.getSession().setAttribute("CAPTCHA_KEY", captcha.getCode());
            response.setContentType("image/png");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);
            captcha.write(response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
