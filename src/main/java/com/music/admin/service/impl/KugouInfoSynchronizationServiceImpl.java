package com.music.admin.service.impl;

import cn.hutool.Hutool;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.music.enums.StatusEnum;
import com.music.mapper.*;
import com.music.admin.service.KugouInfoSynchronizationService;
import com.music.pojo.entity.*;
import com.music.pojo.api.*;
import com.music.user.service.CommonMusicService;
import com.music.user.service.NewMusicService;
import com.music.user.service.SingerAlbumMusicService;
import com.music.user.service.impl.KugouServiceImpl;
import com.music.util.KuGouUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * 同步酷狗api数据到本地数据库中
 */
@Service
@Slf4j
public class KugouInfoSynchronizationServiceImpl implements KugouInfoSynchronizationService {

    @Autowired
    private VolidMapper volidMapper;

    @Autowired
    private AllMusicListMapper allMusicListMapper;

    @Autowired
    private MusicSheetMapper musicSheetMapper;

    @Autowired
    private SingerMapper singerMapper;

    @Autowired
    private RankingListMusicMapper rankingListMusicMapper;

    @Autowired
    private CommonMusicService commonMusicService;

    @Autowired
    private NewMusicService newMusicService;

    @Autowired
    private NewMusicMapper newMusicMapper;

    @Autowired
    private SheetMusicMapper sheetMusicMapper;

    @Autowired
    private MusicSingerDetailMapper musicSingerDetailMapper;

    @Autowired
    private SingerAlbumMapper singerAlbumMapper;

    @Autowired
    private SingerAlbumMusicMapper singerAlbumMusicMapper;

    /**
     * 同步期数表(最新)
     *
     * @param
     * @return
     */
    @Transactional
    @Override
    public Boolean syncVolidByRankId() {
        List<AllList> allMusicList = allMusicListMapper.findAll();
        for (AllList allList : allMusicList) {
            String s = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/vol?ranktype=2&plat=0&rankid=" + allList.getRankId() + "&with_res_tag=1");
            s = KuGouUtil.relieveKuGouUrlLimit(s);
            KugouVolid kugouVolid = JSON.parseObject(s, KugouVolid.class);
            if (kugouVolid.getData().getInfo().isEmpty()) {
                continue;
            }
            Volid volid = new Volid();
            volid.setVolid(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVolid());
            volid.setVolName(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVolname());
            volid.setRank_cid(kugouVolid.getData().getRank_cid());
            volid.setRankId(allList.getRankId());
            volid.setVolTitle(kugouVolid.getData().getInfo().get(0).getVols().get(0).getVoltitle());
            volid.setYear(kugouVolid.getData().getInfo().get(0).getYear());
            UpdateWrapper<Volid> updateWrapper = new UpdateWrapper<>();
            if (CollUtil.isNotEmpty(volidMapper.findVolid(volid.getVolid()))) {
                volid.setUpdateTime(new Date());
                updateWrapper.eq("volid", volid.getVolid());
                volidMapper.update(volid, updateWrapper);
                updateWrapper.clear();
            } else {
                volid.setUpdateTime(new Date());
                volid.setCreateTime(new Date());
                volidMapper.insert(volid);
            }
        }
        return true;
    }

    /**
     * 同步顺序 1
     * 同步各大榜单
     *
     * @return
     */
    @Override
    @Transactional
    public Boolean syncAllMusicList() {
        String url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/list?version=9108&plat=0&showtype=2&parentid=0&apiver=6&area_code=1&withsong=1&with_res_tag=1");
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        KugouAllList kugouAllList = JSON.parseObject(url, KugouAllList.class);
        AllList allList = new AllList();
        UpdateWrapper<AllList> updateWrapper = new UpdateWrapper<>();
        for (KugouAllList.DataBean.InfoBean infoBean : kugouAllList.getData().getInfo()) {
            allList.setRankCid(infoBean.getRank_cid());
            allList.setRankName(infoBean.getRankname());
            allList.setPlayTimes(infoBean.getPlay_times());
            allList.setRankId(infoBean.getRankid());
            allList.setImg(infoBean.getBanner_9().replaceAll("\\{size}/", ""));
            allList.setIntro(infoBean.getIntro());
            if (CollUtil.isNotEmpty(allMusicListMapper.findAllListDTOByRankId(allList.getRankId()))) {
                updateWrapper.eq("rank_id", allList.getRankId());
                allList.setUpdateTime(new Date());
                allMusicListMapper.update(allList, updateWrapper);
                updateWrapper.clear();
            } else {
                allList.setUpdateTime(new Date());
                allList.setCreateTime(new Date());
                allMusicListMapper.insert(allList);
            }
        }
        syncVolidByRankId();
        syncListMusic();
        return true;
    }

    /**
     * 同步酷狗歌单，并同步歌单中的歌曲
     *
     * @return
     */
    @Override
    @Transactional
    public Boolean syncMusicSheet() {
        String url = HttpUtil.get("http://m.kugou.com/plist/index&json=true");
        KugouMusicList kugouMusicList = JSON.parseObject(url, KugouMusicList.class);
        MusicSheet musicSheet = new MusicSheet();
        UpdateWrapper<MusicSheet> updateWrapper = new UpdateWrapper<>();
        for (KugouMusicList.PlistBean.ListBean.InfoBean infoBean : kugouMusicList.getPlist().getList().getInfo()) {
            musicSheet.setUserName(infoBean.getUsername());
            musicSheet.setImgUrl(infoBean.getImgurl().replaceAll("\\{size}/", ""));
            musicSheet.setIntro(infoBean.getIntro());
            musicSheet.setPlayCount(infoBean.getPlaycount());
            musicSheet.setPublishTime(infoBean.getPublishtime());
            musicSheet.setSpecialId(infoBean.getSpecialid());
            musicSheet.setSpecialName(infoBean.getSpecialname());
            musicSheet.setUserAvatar(infoBean.getUser_avatar());
            musicSheet.setIsKugouUser(1);
            if (CollUtil.isNotEmpty(musicSheetMapper.findMusicSheetBySpecialId(musicSheet.getSpecialId()))) {
                updateWrapper.eq("special_id", musicSheet.getSpecialId());
                musicSheet.setUpdateTime(new Date());
                musicSheetMapper.update(musicSheet, updateWrapper);
                updateWrapper.clear();
            } else {
                musicSheet.setUpdateTime(new Date());
                musicSheet.setCreateTime(new Date());
                musicSheetMapper.insert(musicSheet);
            }
        }
        syncSheetMusic();
        return true;
    }

    /**
     * 同步全部类型的歌手，并同步歌手的细节
     * 歌手头像需要将{size}替换成240以显示高清头像
     * sextypes:
     * [{key: 2,value: "女"},
     * {key: 3,value: "组合"},
     * {key: 1,value: "男"},
     * {key: 0,value: "全部"}],
     * types:
     * [{key: 4,value: "其他"},
     * {key: 3,value: "日韩"},
     * {key: 5,value: "日本"},
     * {key: 2,value: "欧美"},
     * {key: 6,value: "韩国"},
     * {key: 1,value: "华语"},
     * {key: 0,value: "全部"}]
     * 在进行到5、6时会覆盖3的值，所以只查到4
     *
     * @return
     */
    @Override
    @Transactional
    public Boolean syncMusicSinger() {
        Integer[] sexTypes = new Integer[]{1, 2, 3};
        Integer[] types = new Integer[]{1, 2, 3, 4};
        String url = "";
        for (Integer type : types) {
            for (Integer sexType : sexTypes) {
                url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v5/singer/list?version=9108&showtype=1&plat=0&sextype=" +
                        sexType + "&sort=0&pagesize=100&type=" +
                        type + "&page=1&musician=0");
                log.info("url========={}", url);
                url = KuGouUtil.relieveKuGouUrlLimit(url);
                KugouSingerV2 kugouSingerV2 = JSON.parseObject(url, KugouSingerV2.class);
                Singer singer = new Singer();
                UpdateWrapper<Singer> updateWrapper = new UpdateWrapper<>();
                for (KugouSingerV2.DataBean.InfoBean infoBean : kugouSingerV2.getData().getInfo()) {
                    singer.setFansCount(infoBean.getFanscount());
                    singer.setImgUrl(infoBean.getImgurl().replaceAll("\\{size}/", "400/"));
                    singer.setSingerName(infoBean.getSingername());
                    singer.setHeat(infoBean.getHeat());
                    singer.setSexType(sexType);
                    singer.setSingerId(infoBean.getSingerid());
                    singer.setType(type);
                    if (Objects.isNull(singerMapper.findSingerBySingerId(singer.getSingerId()))) {
                        updateWrapper.eq("singer_id", singer.getSingerId());
                        singer.setUpdateTime(new Date());
                        singerMapper.update(singer, updateWrapper);
                        updateWrapper.clear();
                    } else {
                        singer.setCreateTime(new Date());
                        singer.setUpdateTime(new Date());
                        singerMapper.insert(singer);
                    }
                }
            }
        }
        return syncMusicSingerDetail();
    }

    public Boolean syncMusicSingerDetail() {
        List<Singer> allSingerList = singerMapper.findAll();
        String url = "";
        KugouSingerDetail kugouSingerDetail;
        UpdateWrapper<MusicSingerDetail> updateWrapper = new UpdateWrapper<>();
        for (Singer singer : allSingerList) {
            MusicSingerDetail musicSingerDetail = new MusicSingerDetail();
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/singer/info?singerid=" + singer.getSingerId() + "&with_res_tag=1");
            url = KuGouUtil.relieveKuGouUrlLimit(url);
            if (null != musicSingerDetailMapper.getMusicSingerDetailBySingerId(singer.getSingerId())) {
                kugouSingerDetail = JSON.parseObject(url, KugouSingerDetail.class);
                musicSingerDetail.setUpdateTime(new Date());
                musicSingerDetail.setSingerId(singer.getSingerId());
                musicSingerDetail.setAlbumCount(kugouSingerDetail.getData().getAlbumcount());
                musicSingerDetail.setImgUrl(kugouSingerDetail.getData().getImgurl().replaceAll("\\{size}/", "400/"));
                musicSingerDetail.setIntro(kugouSingerDetail.getData().getIntro());
                musicSingerDetail.setMvCount(kugouSingerDetail.getData().getMvcount());
                musicSingerDetail.setProfile(kugouSingerDetail.getData().getProfile());
                musicSingerDetail.setSingerName(kugouSingerDetail.getData().getSingername());
                musicSingerDetail.setSongCount(kugouSingerDetail.getData().getSongcount());
                updateWrapper.eq("singer_id", singer.getSingerId());
                musicSingerDetailMapper.update(musicSingerDetail, updateWrapper);
                updateWrapper.clear();
            } else {
                kugouSingerDetail = JSON.parseObject(url, KugouSingerDetail.class);
                musicSingerDetail.setSingerId(singer.getSingerId());
                musicSingerDetail.setAlbumCount(kugouSingerDetail.getData().getAlbumcount());
                musicSingerDetail.setImgUrl(kugouSingerDetail.getData().getImgurl().replaceAll("\\{size}/", "400/"));
                musicSingerDetail.setIntro(kugouSingerDetail.getData().getIntro());
                musicSingerDetail.setMvCount(kugouSingerDetail.getData().getMvcount());
                musicSingerDetail.setProfile(kugouSingerDetail.getData().getProfile());
                musicSingerDetail.setSingerName(kugouSingerDetail.getData().getSingername());
                musicSingerDetail.setSongCount(kugouSingerDetail.getData().getSongcount());
                musicSingerDetail.setCreateTime(new Date());
                musicSingerDetail.setUpdateTime(new Date());
                musicSingerDetailMapper.insert(musicSingerDetail);
            }
        }
        return true;
    }

    /**
     * 同步该项必须先同步期数Volid
     * 获取每个榜单前50首歌曲存入数据库
     *
     * @return
     */
    @Override
    public Boolean syncListMusic() {
        List<Volid> volidList = volidMapper.findAll();
        String url = "";
        for (Volid volid : volidList) {
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/song?pagesize=50&page=1&volid=" + volid.getVolid() + "&rankid=" + volid.getRankId() + "&with_res_tag=1");
            url = KuGouUtil.relieveKuGouUrlLimit(url);
            KugouMusicListForRandId kugouMusicListForRandId = JSON.parseObject(url, KugouMusicListForRandId.class);
            UpdateWrapper<RankingListMusic> updateWrapper = new UpdateWrapper<>();
            for (KugouMusicListForRandId.DataBean.InfoBean infoBean : kugouMusicListForRandId.getData().getInfo()) {
                RankingListMusic rankingListMusic = new RankingListMusic();
                rankingListMusic.setAddTime(infoBean.getAddtime());
                rankingListMusic.setAlbumId(infoBean.getAlbum_id());
                rankingListMusic.setAlbumSizableCover(infoBean.getAlbum_sizable_cover().replaceAll("\\{size}/", ""));
                rankingListMusic.setFileName(infoBean.getFilename());
                rankingListMusic.setHash(infoBean.getHash());
                rankingListMusic.setRankCid(infoBean.getRank_cid());
                rankingListMusic.setFileSize(infoBean.getFilesize());
                rankingListMusic.setRemark(infoBean.getRemark());
                rankingListMusic.setRankId(volid.getRankId());
                if (CollUtil.isNotEmpty(rankingListMusicMapper
                        .findMusicByRankIdAndRankCidAndHashAndAlbumId(
                                rankingListMusic.getRankId(),
                                rankingListMusic.getRankCid(),
                                rankingListMusic.getHash(),
                                rankingListMusic.getAlbumId()))) {
                    updateWrapper.eq("rank_id", rankingListMusic.getRankId())
                            .eq("rank_cid", rankingListMusic.getRankCid())
                            .eq("hash", rankingListMusic.getHash())
                            .eq("album_id", rankingListMusic.getAlbumId());
                    rankingListMusic.setUpdateTime(new Date());
                    rankingListMusicMapper.update(rankingListMusic, updateWrapper);
                    updateWrapper.clear();
                } else {
                    rankingListMusic.setUpdateTime(new Date());
                    rankingListMusic.setCreateTime(new Date());
                    rankingListMusicMapper.insert(rankingListMusic);
                }
                commonMusicService.saveMusicByHashAndAlbumId(rankingListMusic.getHash(), rankingListMusic.getAlbumId());
            }
        }
        return true;
    }

    /**
     * 获取华语、欧美、日韩新歌
     *
     * @return
     */
    @Override
    public Boolean syncNewMusic() {
        Integer[] types = new Integer[]{1, 2, 3};
        String url = "";
        KugouNewMusic kugouNewMusic = null;
        UpdateWrapper<NewMusic> updateWrapper = new UpdateWrapper<>();
        for (Integer type : types) {
            url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/rank/newsong?version=9108&plat=0&with_cover=1&pagesize=100&type=" + type + "&area_code=1&page=1&with_res_tag=1");
            url = KuGouUtil.relieveKuGouUrlLimit(url);
            kugouNewMusic = JSON.parseObject(url, KugouNewMusic.class);
            for (KugouNewMusic.DataBean.InfoBean infoBean : kugouNewMusic.getData().getInfo()) {
                NewMusic newMusic = new NewMusic();
                newMusic.setAddTime(infoBean.getAddtime());
                newMusic.setAlbumCover(infoBean.getAlbum_cover());
                newMusic.setAlbumId(infoBean.getAlbum_id());
                newMusic.setAudioId(infoBean.getAudio_id());
                newMusic.setFileName(infoBean.getFilename());
                newMusic.setFileSize(infoBean.getFilesize());
                newMusic.setHash(infoBean.getHash());
                newMusic.setRemark(infoBean.getRemark());
                newMusic.setType(type);
                if (null != newMusicService.getNewMusicByHashAndAlbumId(newMusic.getHash(), newMusic.getAlbumId())) {
                    newMusic.setUpdateTime(new Date());
                    updateWrapper.eq("hash", newMusic.getHash()).eq("album_id", newMusic.getAlbumId());
                    newMusicMapper.update(newMusic, updateWrapper);
                    updateWrapper.clear();
                } else {
                    newMusic.setCreateTime(new Date());
                    newMusic.setUpdateTime(new Date());
                    newMusicMapper.insert(newMusic);
                }
                commonMusicService.saveMusicByHashAndAlbumId(newMusic.getHash(), newMusic.getAlbumId());
            }
        }
        return true;
    }

    /**
     * 同步单个歌单歌曲
     */
    @Override
    public void syncOneSheetMusic(Integer specialId) {
        String url = HttpUtil.get("http://m.kugou.com/plist/list/" + specialId + "?json=true");
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        KugouSheetMusic kugouSheetMusic = JSON.parseObject(url, KugouSheetMusic.class);
        UpdateWrapper<SheetMusic> updateWrapper = new UpdateWrapper<>();
        for (KugouSheetMusic.ListBeanX.ListBean.InfoBean infoBean : kugouSheetMusic.getList().getList().getInfo()) {
            SheetMusic sheetMusic = new SheetMusic();
            sheetMusic.setAlbumId(infoBean.getAlbum_id());
            sheetMusic.setAudioId(infoBean.getAudio_id());
            sheetMusic.setFileName(infoBean.getFilename());
            sheetMusic.setFileSize(infoBean.getFilesize());
            sheetMusic.setHash(infoBean.getHash());
            sheetMusic.setRemark(infoBean.getRemark());
            sheetMusic.setSpecialId(specialId);
            if (null != sheetMusicMapper.getSheetMusicByHashAndAlbumIdAndSpecialId(
                    sheetMusic.getHash(),
                    sheetMusic.getAlbumId(),
                    sheetMusic.getSpecialId())) {
                updateWrapper.eq("hash", sheetMusic.getHash())
                        .eq("album_id", sheetMusic.getAlbumId())
                        .eq("special_id", sheetMusic.getSpecialId());
                sheetMusic.setUpdateTime(new Date());
                sheetMusicMapper.update(sheetMusic, updateWrapper);
                updateWrapper.clear();
            } else {
                sheetMusic.setCreateTime(new Date());
                sheetMusic.setUpdateTime(new Date());
                sheetMusicMapper.insert(sheetMusic);
            }
        }
    }

    /**
     * 同步歌单歌曲
     *
     * @return
     */
    @Override
    @Async
    public void syncSheetMusic() {
        List<MusicSheet> musicSheetAllList = musicSheetMapper.findMusicSheetAll();
        for (MusicSheet musicSheet : musicSheetAllList) {
            syncOneSheetMusic(musicSheet.getSpecialId());
        }
    }

    /**
     * 同步歌手专辑
     */
    @Override
    public void syncSingerAlbum() {
        List<Singer> allSingerList = singerMapper.findAll();
        String url;
        int page = 1;
        KugouSingerAlbum kugouSingerAlbum = null;
        for (Singer singer : allSingerList) {
            while (true) {
                url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/singer/album?version=9108&plat=0&pagesize=20&singerid=" + singer.getSingerId() + "&category=1&area_code=1&page=" + page + "&show_album_tag=0");
                System.out.println("===============" + url + "page=" + page);
                kugouSingerAlbum = JSON.parseObject(url, KugouSingerAlbum.class);
                if (kugouSingerAlbum.getStatus() == 0) {
                    //调用太快休息两秒
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                if (CollUtil.isNotEmpty(kugouSingerAlbum.getData().getInfo())) {
                    for (KugouSingerAlbum.DataBean.InfoBean infoBean : kugouSingerAlbum.getData().getInfo()) {
                        if (null == singerAlbumMapper.getSingerAlbumByAlbumId(infoBean.getAlbumid())) {
                            SingerAlbum singerAlbum = new SingerAlbum();
                            singerAlbum.setAlbumId(infoBean.getAlbumid())
                                    .setAlbumName(infoBean.getAlbumname())
                                    .setImgUrl(infoBean.getImgurl().replaceAll("\\{size}/", "400/"))
                                    .setIntro(infoBean.getIntro())
                                    .setPublishTime(infoBean.getPublishtime())
                                    .setSingerId(infoBean.getSingerid())
                                    .setSingerName(infoBean.getSingername())
                                    .setCreateTime(new Date())
                                    .setUpdateTime(new Date());
                            singerAlbumMapper.insert(singerAlbum);
                        }
                    }
                    page++;
                } else {
                    page = 1;
                    break;
                }
            }
        }
    }

    /**
     * 同步专辑中的歌曲
     */
    @Override
    public void syncSingerAlbumMusic() {
        List<SingerAlbum> singerAlbumList = singerAlbumMapper.findAll();
        KugouSingerAlbumMusic kugouSingerAlbumMusic = null;
        for (SingerAlbum singerAlbum : singerAlbumList) {
            String url = HttpUtil.get("http://mobilecdn.kugou.com/api/v3/album/song?version=9108&albumid=" + singerAlbum.getAlbumId() + "&plat=0&pagesize=100&area_code=1&page=1&with_res_tag=1");
            url=KuGouUtil.relieveKuGouUrlLimit(url);
            kugouSingerAlbumMusic = JSON.parseObject(url, KugouSingerAlbumMusic.class);
            for (KugouSingerAlbumMusic.DataBean.InfoBean infoBean : kugouSingerAlbumMusic.getData().getInfo()) {
                SingerAlbumMusic singerAlbumMusic = new SingerAlbumMusic();
                singerAlbumMusic.setAlbumId(infoBean.getAlbum_id())
                        .setAudioId(infoBean.getAudio_id())
                        .setFileName(infoBean.getFilename())
                        .setFileSize(infoBean.getFilesize())
                        .setHash(infoBean.getHash())
                        .setRemark(infoBean.getRemark());
                if (Objects.isNull(singerAlbumMusicMapper.getSingerAlbumMusicByHashAndAlbumId(singerAlbumMusic.getHash(), singerAlbumMusic.getAlbumId()))){
                    singerAlbumMusic.setCreateTime(new Date());
                    singerAlbumMusic.setUpdateTime(new Date());
                    singerAlbumMusicMapper.insert(singerAlbumMusic);
                }
                commonMusicService.saveMusicByHashAndAlbumId(singerAlbumMusic.getHash(), singerAlbumMusic.getAlbumId());
            }
        }
    }

    /**
     * 调用酷狗API更新音乐数据
     *
     * @param hash
     * @param albumId
     * @return
     */
    @Override
    public HashMap<String, Object> updateMusic(String hash, String albumId) {
        HashMap<String, Object> map = new HashMap<>();
        CommonMusic commonMusic = commonMusicService.updateCommonMusic(hash, albumId);
        if (Objects.nonNull(commonMusic)){
            map.put("statusCode", StatusEnum.MUSIC_SYNC_SUCCESS.getStatusCode());
        }else {
            map.put("statusCode", StatusEnum.MUSIC_SYNC_ERROR.getStatusCode());
        }
        return map;
    }

    /**
     * 在线搜索音乐
     *
     * @param musicSearchKey
     * @return
     */
    @Override
    public HashMap<String, Object> onlineMusicSearch(String musicSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(musicSearchKey);
        String searchKey=(String) jsonObject.get("musicSearchKey");
        String url = HttpUtil.get("http://msearchcdn.kugou.com/api/v3/search/song?showtype=14&highlight=em&pagesize=30&tag_aggr=1&plat=0&sver=5&keyword="+searchKey+"&correct=1&api_ver=1&version=9108&page=1&area_code=1&tag=1&with_res_tag=1");
        HashMap<String, Object> map = new HashMap<>();
        KugouMusicSearch kugouMusicSearch = null;
        try {
            kugouMusicSearch = JSON.parseObject(KuGouUtil.relieveKuGouUrlLimit(url), KugouMusicSearch.class);
            map.put("info",kugouMusicSearch.getData().getInfo());
            map.put("statusCode", StatusEnum.MUSIC_ONLINE_SUCCESS.getStatusCode());
        } catch (Exception e) {
            map.put("statusCode", StatusEnum.MUSIC_ONLINE_ERROR.getStatusCode());
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 在线搜索歌单
     *
     * @param sheetSearchKey
     * @return
     */
    @Override
    public HashMap<String, Object> onlineSheetSearch(String sheetSearchKey) {
        JSONObject jsonObject = JSONUtil.parseObj(sheetSearchKey);
        String searchKey=(String) jsonObject.get("sheetSearchKey");
        String url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/search/special?version=9108&highlight=em&keyword=" + searchKey + "&pagesize=20&filter=0&page=1&sver=2&with_res_tag=1");
        HashMap<String, Object> map = new HashMap<>();
        KugouSheetSearch kugouSheetSearch = null;
        try {
            kugouSheetSearch = JSON.parseObject(KuGouUtil.relieveKuGouUrlLimit(url), KugouSheetSearch.class);
            map.put("info",kugouSheetSearch.getData().getInfo());
            map.put("statusCode", StatusEnum.MUSIC_ONLINE_SUCCESS.getStatusCode());
        } catch (Exception e) {
            map.put("statusCode", StatusEnum.MUSIC_ONLINE_ERROR.getStatusCode());
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 更新单个歌手的细节
     *
     * @param singerId
     * @return
     */
    @Override
    public HashMap<String, Object> updateOneSingerDetail(Integer singerId) {
        String url = HttpUtil.get("http://mobilecdnbj.kugou.com/api/v3/singer/info?singerid=" + singerId + "&with_res_tag=1");
        url = KuGouUtil.relieveKuGouUrlLimit(url);
        MusicSingerDetail musicSingerDetail = new MusicSingerDetail();
        KugouSingerDetail kugouSingerDetail = JSON.parseObject(url, KugouSingerDetail.class);
        musicSingerDetail.setUpdateTime(new Date());
        musicSingerDetail.setSingerId(singerId);
        musicSingerDetail.setAlbumCount(kugouSingerDetail.getData().getAlbumcount());
        musicSingerDetail.setImgUrl(kugouSingerDetail.getData().getImgurl().replaceAll("\\{size}/", "400/"));
        musicSingerDetail.setIntro(kugouSingerDetail.getData().getIntro());
        musicSingerDetail.setMvCount(kugouSingerDetail.getData().getMvcount());
        musicSingerDetail.setProfile(kugouSingerDetail.getData().getProfile());
        musicSingerDetail.setSingerName(kugouSingerDetail.getData().getSingername());
        musicSingerDetail.setSongCount(kugouSingerDetail.getData().getSongcount());
        UpdateWrapper<MusicSingerDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("singer_id", singerId);
        int update = musicSingerDetailMapper.update(musicSingerDetail, updateWrapper);
        updateWrapper.clear();
        HashMap<String, Object> map = new HashMap<>();
        if (update!=0){
            map.put("statusCode", StatusEnum.OTHER_SUCCESS.getStatusCode());
        }else {
            map.put("statusCode", StatusEnum.OTHER_ERROR.getStatusCode());
        }
        return map;
    }
}
