package com.music.admin.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public interface AdminUserService {
    HashMap<String, Object> login(String loginJson);

    void imgCode(HttpServletRequest request, HttpServletResponse response);
}
