package com.music.admin.service;

import java.util.HashMap;

public interface KugouInfoSynchronizationService {

    Boolean syncVolidByRankId();

    Boolean syncAllMusicList();

    Boolean syncMusicSheet();

    Boolean syncMusicSinger();

    Boolean syncListMusic();

    Boolean syncNewMusic();

    void syncSheetMusic();

    void syncOneSheetMusic(Integer specialId);

    void syncSingerAlbum();

    void syncSingerAlbumMusic();

    HashMap<String, Object> updateMusic(String hash, String albumId);

    HashMap<String, Object> onlineMusicSearch(String musicSearchKey);

    HashMap<String, Object> onlineSheetSearch(String musicSearchKey);

    HashMap<String, Object> updateOneSingerDetail(Integer singerId);
}
