package com.music;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.alibaba.fastjson.JSON;
import com.aliyun.oss.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.music.mapper.*;
import com.music.pojo.dto.SameUserLikeInfo;
import com.music.pojo.dto.UserDTO;
import com.music.pojo.entity.*;
import com.music.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MusicApplicationTests {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private AllMusicListMapper allMusicListMapper;

    @Autowired
    private UserLikeMusicMapper userLikeMusicMapper;

    @Autowired
    private UserLikeSingerMapper userLikeSingerMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRelationshipMapper userRelationshipMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    void contextLoads() {
//        redisTemplate.opsForValue().set("123", "123");
//        redisTemplate.expire("123", 10, TimeUnit.SECONDS);
//        redisTemplate.opsForHash().put("userMap", "user1", "123");
//        String s = (String)redisTemplate.opsForHash().get("userMap", "user2");
//        System.out.println(s);

        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzUzMDU3MjQsImV4cCI6MTYzNTMwNzUyNCwidXNlck5hbWUiOiIxMjMxMjMiLCJzZXgiOiLnlLciLCJhZ2UiOjE4LCJtdXNpY0hvYmJ5Ijoi55S16Z-zIn0.cznn-Bn-uxxX6S_Y7323FY0vUX2Q2Kr7YYnTIQFG8mI";
        JWT jwt = JWTUtil.parseToken(token);
        JSONObject payloads = jwt.getPayloads();
        System.out.println(payloads.toString());
    }

    @Test
    void sql() {
        AllList musicByRankId = allMusicListMapper.getMusicByRankId(8888);
        System.out.println("=============" + DateUtil.date(musicByRankId.getUpdateTime()));
    }
    @Test
    void password(){
        String liuyi6161 = bCryptPasswordEncoder.encode("LIUYI6161");
        System.out.println(liuyi6161);
    }

    @Test
    void test1() {
        String sex = "";
        Integer minAge = 0;
        Integer maxAge = 100;
        Integer userId = 21;
        //根据用户选择挑选用户范围
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq(!"".equals(sex), "sex", sex)
                .between("age", minAge, maxAge)
                .ne("id", userId);
        List<User> userList = userMapper.selectList(userQueryWrapper);
        //获取当前用户的喜欢歌曲
        List<UserLikeMusic> userLikeMusicList = userLikeMusicMapper.getLikeMusicByUserId(userId);
        //获取当前用户的喜欢歌手
        List<UserLikeSinger> userLikeSingerList = userLikeSingerMapper.getLikeSingerByUserId(userId);
        //获取当前用户的标签
        User nowUser = userMapper.selectById(userId);
        List<String> musicHobbyList = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(nowUser.getMusicHobby())) {
            musicHobbyList = JSON.parseArray(nowUser.getMusicHobby(), String.class);
        }
        List<UserLikeMusic> sameLikeMusic = null;
        List<UserLikeSinger> sameLikeSinger = null;
        List<String> sameHobby = new ArrayList<>();
        SameUserLikeInfo sameUserLikeInfo = new SameUserLikeInfo();
        HashMap<Integer, Object> userLikeSameMusicMap = new HashMap<>();
        HashMap<Integer, Object> userLikeSameSingerMap = new HashMap<>();
        HashMap<Integer, Object> userSameHobbyTabsMap = new HashMap<>();
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : userList) {
            //用户喜欢相同歌曲
            for (UserLikeMusic userLikeMusic : userLikeMusicMapper.getLikeMusicByUserId(user.getId())) {
                sameLikeMusic = userLikeMusicList.stream()
                        .filter(music -> music.getHash().equals(userLikeMusic.getHash()))
                        .collect(Collectors.toList());
                if (CollUtil.isNotEmpty(sameLikeMusic)) {
                    userLikeSameMusicMap.put(user.getId(), sameLikeMusic);
                    userDTOS.add(Convert.convert(UserDTO.class, user));
                }
            }
            //用户喜欢相同歌手
            for (UserLikeSinger userLikeSinger : userLikeSingerMapper.getLikeSingerByUserId(user.getId())) {
                sameLikeSinger = userLikeSingerList.stream()
                        .filter(music -> music.getSingerId().equals(userLikeSinger.getSingerId()))
                        .collect(Collectors.toList());
                if (CollUtil.isNotEmpty(sameLikeSinger)) {
                    userLikeSameSingerMap.put(user.getId(), sameLikeSinger);
                    userDTOS.add(Convert.convert(UserDTO.class, user));
                }
            }
            //用户具有相同的标签
            for (String s : musicHobbyList) {
                if (user.getMusicHobby().contains(s)){
                    sameHobby.add(s);
                }
            }
            userSameHobbyTabsMap.put(user.getId(), sameHobby);
        }
        //去重userDTOS
        userDTOS = userDTOS.stream().distinct().collect(Collectors.toList());
        sameUserLikeInfo.setUserDTO(userDTOS);
        sameUserLikeInfo.setUserLikeSameMusicMap(userLikeSameMusicMap);
        sameUserLikeInfo.setUserLikeSameSingerMap(userLikeSameSingerMap);
        sameUserLikeInfo.setUserSameHobbyTabsMap(userSameHobbyTabsMap);
        System.out.println(JSONUtil.toJsonStr(sameUserLikeInfo));
    }

    @Test
    void test11(){
        Integer userId=21;
        QueryWrapper<UserRelationship> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId).eq("status", 1);
        List<UserRelationship> userRelationshipList = userRelationshipMapper.selectList(queryWrapper);
        System.out.println("userRelationshipList="+userRelationshipList);
        queryWrapper.clear();
        queryWrapper.eq("fan_id", userId).eq("status", 1);
        List<UserRelationship> fanRelationshipList = userRelationshipMapper.selectList(queryWrapper);
        System.out.println("fanRelationshipList="+fanRelationshipList);
        List<UserDTO> userDTOS = userRelationshipList.stream().map(userRelationship -> {
            for (UserRelationship relationship : fanRelationshipList) {
                if (userRelationship.getUserId().equals(relationship.getFanId()) && userRelationship.getFanId().equals(relationship.getUserId())) {
                    return userService.getUserInfoById(userRelationship.getFanId());
                }
            }
            return null;
        }).collect(Collectors.toList());
        System.out.println(userDTOS);
    }

    @Test
    void json(){
        String url="{\"musicList\":[{\"srctype\":1,\"bitrate\":128,\"source\":\"\",\"rp_type\":\"audio\",\"songname_original\":\"算什么男人\",\"audio_id\":5964261,\"othername\":\"\",\"price\":200,\"mvhash\":\"804094edd417e5da49b550652b07d2f3\",\"feetype\":0,\"extname\":\"mp3\",\"pay_type_sq\":3,\"group\":[],\"rp_publish\":1,\"fold_type\":0,\"othername_original\":\"\",\"songname\":\"算什么男人\",\"pkg_price_320\":1,\"sqprivilege\":10,\"sqfilesize\":32034822,\"accompany\":1,\"filename\":\"周杰伦 - 算什么男人\",\"m4afilesize\":0,\"topic\":\"歌词 : <em>你算什么男人</em> <em>算什么男人</em> 眼睁睁看她走却不闻不问 是有多天真  就别再硬撑 期待你挽回你却拱手让人 <em>你算什么男人</em> <em>算什么男人</em> 还爱着她却不敢叫她再等\",\"pkg_price\":1,\"album_id\":\"962593\",\"pkg_price_sq\":1,\"hash\":\"89ab193ec33e2ae6af04bd408f8f1083\",\"singername\":\"周杰伦\",\"fail_process_320\":4,\"fail_process_sq\":4,\"fail_process\":4,\"sqhash\":\"c0ba9abef4fc8ab12bbb2445f7484968\",\"filesize\":4616776,\"privilege\":10,\"isnew\":0,\"_$320privilege\":10,\"price_sq\":200,\"duration\":288,\"ownercount\":2415471,\"pay_type_320\":3,\"trans_param\":{\"classmap\":{\"attr0\":100667400},\"pay_block_tpl\":1,\"display\":32,\"cpy_grade\":5,\"musicpack_advance\":1,\"appid_block\":\"3124\",\"hash_multitrack\":\"C386DA5AAC2F9268420736E5762B397E\",\"cpy_attr0\":0,\"cpy_level\":1,\"display_rate\":1,\"cid\":2556573},\"album_name\":\"哎呦，不错哦\",\"old_cpy\":0,\"album_audio_id\":32062710,\"pay_type\":3,\"sourceid\":0,\"_$320filesize\":11569458,\"_$320hash\":\"db75f88fc82c704747e178418f63b78a\",\"isoriginal\":1,\"topic_url\":\"\",\"price_320\":200}]}";
        System.out.println(JSONUtil.parseObj(url).get("musicList"));
    }
}
